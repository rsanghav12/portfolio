package org.cmpt370c2.model;

import java.util.Iterator;
import java.util.Set;

import org.cmpt370c2.utilities.HexagonalContainer;
import org.cmpt370c2.utilities.HexagonalPoint;

/**
 * Represents and maintains the game's board state, the game board is made up of individual game
 * tiles.
 * 
 * @author C2
 *
 */
public class GameBoard {
  private HexagonalContainer<GameTile> board;

  /**
   * Creates a new game board with the specified amount of layers
   * 
   * @param layers The amount of layers to be in game board
   */
  public GameBoard(int layers) {
    board = new HexagonalContainer<GameTile>(layers);

    // Center tile.
    HexagonalPoint point = new HexagonalPoint(0, 0);
    GameTile tile = new GameTile();
    board.put(tile, point);

    for (int layer = 1; layer < layers; layer++) {
      for (int direction = 0; direction < 6 * layer; direction++) {
        point = new HexagonalPoint(layer, direction);
        tile = new GameTile();
        board.put(tile, point);
      }
    }
  }

  /**
   * Moves robot to specified hexagonal point location
   * 
   * @param robot The robot to be moved
   * @param location The location where the robot should be moved to
   */
  public void moveRobot(Robot robot, HexagonalPoint location) {
    // Remove it from its current location if it has one.
    if (robot.getPoint() != null) {
      removeRobot(robot);
    }
    GameTile tile = board.get(location);
    tile.add(robot);
    robot.setPoint(location);
  }

  /**
   * Remove robot from the game tile.
   * @param robot Robot to be removed from the tile
   */
  public void removeRobot(Robot robot) {
    if (robot.getPoint() == null) {
      throw new IllegalArgumentException("Attempted to remove robot that hasn't been added.");
    }
    GameTile tile = board.get(robot.getPoint());
    tile.remove(robot);
  }

  public GameTile getTile(HexagonalPoint point) {
    return board.get(point);
  }

  public Iterator<HexagonalPoint> iterator() {
    return board.iterator();
  }

  public Set<HexagonalPoint> getVisibleTiles(HexagonalPoint location, int range) {
    return board.getPointsRange(location, range);
  }

  public HexagonalPoint getShootPoint(HexagonalPoint location, int distance, int rotation,
      int directionOffset) {
    return board.getNeighbour(location, distance, rotation, directionOffset);
  }

  public HexagonalPoint getMovePoint(HexagonalPoint location, int direction) {
    return board.getNeighbour(location, 1, direction, 0);
  }

}
