package org.cmpt370c2.robotlibrarianclient;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;

import org.cmpt370c2.utilities.FileLoader;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Helper class used to load a JSON file from the local resources directory into a JSON object.
 * 
 * @author rutviksanghavi
 *
 */
public class JSONFileLoader extends FileLoader {

  /**
   * The default extension for JSON files.
   */
  private static final String JSON_EXTENSION = ".jsn";

  /**
   * Loads a JSON object from the local resources directory with the specified file name. The file
   * name should not include the extension as .jsn is automatically appended.
   * 
   * @param fileName The name of the JSON file to be loaded.
   * @return
   * @throws IOException Thrown if the file can not be found.
   * @throws URISyntaxException Thrown if the file name is invalid.
   * @throws ParseException Thrown if the loaded file contains invalid JSON.
   */
  public JSONObject load(String fileName) throws IOException, URISyntaxException, ParseException {
    InputStream inputStream = super.load(fileName, JSON_EXTENSION);
    JSONParser jsonParser = new JSONParser();
    return (JSONObject) jsonParser.parse(new InputStreamReader(inputStream, "UTF-8"));
  }
}
