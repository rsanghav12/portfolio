package org.cmpt370c2.view;

/**
 * This class contains font sizes used throughout the view. They are located in a single file to
 * make tweaking easy.
 * 
 * @author allankerr
 *
 */
final class FontSize {

  static final int TITLE_SIZE = 80;

  static final int SUBTITLE_SIZE = 50;

  static final int MENU_BUTTON_SIZE = 25;

  static final int TABLE_BUTTON_SIZE = 15;

  static final int TABLE_TEXT_SIZE = 20;
}
