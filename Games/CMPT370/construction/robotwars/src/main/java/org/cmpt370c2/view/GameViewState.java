package org.cmpt370c2.view;

/**
 * An enum for keeping track of what type of play is in progress in the game view to know whether to
 * accept or reject key and mouse input.
 * 
 * @author allankerr
 *
 */
enum GameViewState {
  STARTING_PLAYER_PLAY, 
  PLAYER_PLAY, 
  COMPUTER_PLAY, 
  NONE;
}
