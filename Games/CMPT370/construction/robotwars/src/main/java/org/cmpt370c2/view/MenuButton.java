package org.cmpt370c2.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JButton;

import org.cmpt370c2.utilities.FontLoader;
import org.cmpt370c2.utilities.FontType;

/**
 * A custom button class to be used instead of a default JButton for aesthetic reasons.
 * 
 * @author C2
 *
 */
@SuppressWarnings("serial")
class MenuButton extends JButton {

  private static final int MARGIN = 5;

  /**
   * The default border color used to draw menu buttons.
   */
  private Color borderColor = Colors.MENU_BUTTON_BORDER_COLOR;

  /**
   * The default color that the button changes when clicked/
   */
  private Color pressedColor = Colors.DARK_COMPONENT_COLOR;

  public Color getBorderColor() {
    return borderColor;
  }

  public void setBorderColor(Color borderColor) {
    this.borderColor = borderColor;
  }

  public Color getPressedColor() {
    return pressedColor;
  }

  public void setPressedColor(Color pressedColor) {
    this.pressedColor = pressedColor;
  }

  /**
   * Constructs a new menu button
   * 
   * @param string The text that should be on the button
   */
  public MenuButton(String string) {
    super(string);

    try {
      this.setFont(FontLoader.getInstance().load(FontType.HELVETICA_NEUE_LT, Font.PLAIN,
          FontSize.MENU_BUTTON_SIZE));
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    this.setFocusable(false);
    this.setBorderPainted(false);
    this.setContentAreaFilled(false);
    this.setFocusPainted(false);
    this.setBackground(Colors.BACKGROUND_COLOR);
  }

  @Override
  public void paintComponent(Graphics g) {
    Graphics2D graphics = (Graphics2D) g;
    graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

    g.setColor(getBorderColor());
    g.drawRoundRect(MARGIN, MARGIN, this.getWidth() - (2 * MARGIN), this.getHeight() - (2 * MARGIN),
        Dimensions.MENU_BUTTON_CORNER_RADIUS, Dimensions.MENU_BUTTON_CORNER_RADIUS);
    if (getModel().isPressed()) {
      g.setColor(getPressedColor());
    } else {
      g.setColor(getBackground());
    }
    g.fillRoundRect(MARGIN, MARGIN, this.getWidth() - (2 * MARGIN), this.getHeight() - (2 * MARGIN),
        Dimensions.MENU_BUTTON_CORNER_RADIUS, Dimensions.MENU_BUTTON_CORNER_RADIUS);

    super.paintComponent(g);
  }
}
