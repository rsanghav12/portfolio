package org.cmpt370c2.view;

import javax.swing.JPanel;

/**
 * A subclass of JPanel designed to act as the super class for all other views.
 * 
 * @author C2
 *
 * @param <L> The listener to be used by the view to send events back to its controller.
 */
@SuppressWarnings("serial")
public abstract class AbstractView<L> extends JPanel {

  /**
   * The listener that all events are sent back to.
   */
  private L listener;

  protected L getListener() {
    return listener;
  }

  /**
   * Constructs a new view to be displayed in a window.
   * 
   * @param listener The listener to which view events are sent back to.
   */
  public AbstractView(L listener) {
    this.listener = listener;
  }
}
