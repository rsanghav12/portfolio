package org.cmpt370c2.view;

import java.awt.Component;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.TableCellRenderer;

import org.cmpt370c2.model.RobotType;
import org.cmpt370c2.model.TeamColor;

/**
 * Table cell to show a robot sprite for a particular team in a table.
 * @author RutvikSanghavi
 */
@SuppressWarnings("serial")
class TableRobotSpriteCell extends JPanel implements TableCellRenderer {

  private Border border[];

  public TableRobotSpriteCell() {
    this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
    this.setOpaque(false);

    border = new Border[3];

    // bottom right
    border[0] = BorderFactory.createCompoundBorder();
    border[0] = BorderFactory.createCompoundBorder(border[0],
        BorderFactory.createMatteBorder(0, 0, 1, 1, Colors.DARK_COMPONENT_COLOR));

    // bottom
    border[1] = BorderFactory.createCompoundBorder();
    border[1] = BorderFactory.createCompoundBorder(border[1],
        BorderFactory.createMatteBorder(0, 0, 1, 0, Colors.DARK_COMPONENT_COLOR));

    // right
    border[2] = BorderFactory.createCompoundBorder();
    border[2] = BorderFactory.createCompoundBorder(border[2],
        BorderFactory.createMatteBorder(0, 0, 0, 1, Colors.DARK_COMPONENT_COLOR));
  }

  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
      boolean hasFocus, int row, int column) {
    if (value instanceof RobotSprite) {
      RobotSprite robotSprite = (RobotSprite) value;
      removeAll();
      add(robotSprite);
      revalidate();

      if ((table.getRowCount() - 1) != row) {
        this.setBorder(border[1]);
      } else {
        this.setBorder(null);
      }
    } else if (value instanceof ArrayList) {
      ArrayList<?> val = (ArrayList<?>) value;
      TeamColor team = (TeamColor) val.get(0);
      RobotType type = (RobotType) val.get(1);

      RobotSprite robotSprite = new RobotSprite(0, 0, 270, team, type);
      removeAll();
      add(robotSprite);
      revalidate();

      Boolean lastColumn = (table.getColumnCount() - 1) == column;
      Boolean lastRow = (table.getRowCount() - 1) == row;

      if (lastColumn) {
        if (!lastRow) {
          this.setBorder(border[1]);
        } else {
          this.setBorder(null);
        }
      } else {
        if (!lastRow) {
          this.setBorder(border[0]);
        } else {
          this.setBorder(border[2]);
        }
      }
    }
    return this;
  }
}
