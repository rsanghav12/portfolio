package org.cmpt370c2.view;

import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

/**
 * Super class for all game cards
 * @author C2
 *
 */
@SuppressWarnings("serial")
abstract class GamePanelCard extends JPanel {
  
  /**
   * Constructs a new game panel instance
   * @param listener The listener the panels needs to communicate
   */
  public GamePanelCard(ActionListener listener) {
    this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    this.setFocusable(false);
    this.setOpaque(false);
  }
}
