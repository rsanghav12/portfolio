package org.cmpt370c2.view;

import org.cmpt370c2.model.RobotType;
import org.cmpt370c2.model.TeamColor;

/**
 * Interface that facilitates communication between the session configuration view
 * and session configuration controller
 * @author C2
 *
 */
public interface SessionConfigurationViewListener {
  
  /**
   * Starts the just configured match
   */
  public void start();
  
  /**
   * Cancels the configuration and goes back to main menu
   */
  public void cancel();
  
  /**
   * Sets the number of players playing; can be 2, 3 or 6
   * @param num The desired number of players
   */
  public void setNumberOfPlayers(int num);
  
  /**
   * Sets how long a player has to make a play
   * @param seconds The desired amount of time in seconds
   */
  public void setPlayDuration(int seconds);
  
  /**
   * Toggles whether a given team will be script controlled or human controlled
   * @param team The team (color) that will be script controlled
   */
  public void toggleComputerTeam(TeamColor team);
  
  /**
   * Opens team configuration for the user to select a script for the computer controlled robot
   * @param team The team that the script is being selected for
   * @param type The type of robot that the script is being selected for
   */
  public void loadScripts(TeamColor team, RobotType type);
}
