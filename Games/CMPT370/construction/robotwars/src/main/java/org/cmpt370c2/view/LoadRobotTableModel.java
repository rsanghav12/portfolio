package org.cmpt370c2.view;

import java.util.Collection;

import javax.swing.table.DefaultTableModel;

import org.cmpt370c2.robotlibrarianclient.RobotData;

/**
 * A custom table model for displaying a table of robot scripts available for download. This class
 * stores the data necessary for drawing the rows and columns in the table.
 * 
 * @author rutviksanghavi
 *
 */
@SuppressWarnings("serial")
class LoadRobotTableModel extends DefaultTableModel {

  private static final String NO_DATA_TEXT = "Data not loaded";

  private static final String TEAM_HEADER = "Team";
  private static final String NAME_HEADER = "Name";
  private static final String MATCHES_HEADER = "Matches";
  private static final String WINS_HEADER = "Wins";
  private static final String LOSSES_HEADER = "Losses";

  private Object[][] data;

  private Collection<RobotData> robots;

  /**
   * Creates a new load robot table model instance
   */
  public LoadRobotTableModel() {
    initData();
  }

  /**
   * Prepares robot data to be displayed for the user to select from.
   * 
   * @param robots
   */
  public void setRobots(Collection<RobotData> robots) {
    this.robots = robots;
    initData();
    fireTableStructureChanged();
  }

  /**
   * Initializes 2D array of robot data to be displayed
   */
  private void initData() {
    if (this.robots == null) {
      this.data = new Object[1][getColumnCount()];
      for (int i = 0; i < getColumnCount(); i++) {
        this.data[0][i] = NO_DATA_TEXT;
      }
    } else {
      Object[][] dupData = new Object[this.robots.size()][getColumnCount()];
      int i = 0;
      for (RobotData robot : this.robots) {
        dupData[i][0] = robot.getTeamName();
        dupData[i][1] = robot.getScriptName();
        dupData[i][2] = robot.getMatches();
        dupData[i][3] = robot.getWins();
        dupData[i][4] = robot.getLosses();
        dupData[i][5] = robot;

        i++;
      }
      this.data = dupData;
    }
  }

  @Override
  public int getRowCount() {
    return (this.robots != null ? this.robots.size() : 1);
  }

  @Override
  public int getColumnCount() {
    return 6;
  }

  @Override
  public String getColumnName(int col) {
    switch (col) {
      case 0:
        return TEAM_HEADER;
      case 1:
        return NAME_HEADER;
      case 2:
        return MATCHES_HEADER;
      case 3:
        return WINS_HEADER;
      case 4:
        return LOSSES_HEADER;
      default:
        return "";
    }
  }

  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    return data[rowIndex][columnIndex];
  }

  @Override
  public Class<?> getColumnClass(int c) {
    return getValueAt(0, c).getClass();
  }

  @Override
  public void setValueAt(Object value, int rowIndex, int columnIndex) {
    data[rowIndex][columnIndex] = value;
    fireTableCellUpdated(rowIndex, columnIndex);
  }

  @Override
  public boolean isCellEditable(int row, int col) {
    return col == 5;
  }
}
