package org.cmpt370c2.interpreter.words;

import org.cmpt370c2.interpreter.controller.ScriptDataSource;
import org.cmpt370c2.interpreter.controller.ScriptListener;
import org.cmpt370c2.interpreter.model.IntValue;
import org.cmpt370c2.interpreter.model.ScriptData;

/**
 * Asks the script listener for the number of robots within visible range of the robot the script is
 * being executed for and pushes it to the stack.
 * 
 * @author allankerr
 *
 */
class ScanWord extends PredefinedWord {

  @Override
  public String getName() {
    return "scan!";
  }

  /**
   * Asks the script listener for the number of robots within visible range of the robot the script
   * is being executed for and pushes it to the stack.
   *
   */
  @Override
  public void execute(ScriptData data, ScriptListener listener, ScriptDataSource dataSource) {
    IntValue result = new IntValue(listener.scan());
    data.push(result);
  }
}
