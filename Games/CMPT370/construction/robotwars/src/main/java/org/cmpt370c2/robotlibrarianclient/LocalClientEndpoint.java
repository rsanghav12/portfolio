package org.cmpt370c2.robotlibrarianclient;

import java.io.IOException;
import java.net.URISyntaxException;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

/**
 * Handles local robot files and is utilized by the robot librarian
 * client to update, download, and request a list of these files.
 * 
 * @author rutvik and allan
 */
public class LocalClientEndpoint implements ClientEndpoint {
  
  /**
   * Used to store the list of robots in List.jsn
   */
  private static JSONObject robotList = null;
  
  /**
   * Keys found in JSONObject requests
   */
  private static final String LIST_REQUEST = "list-request";
  private static final String UPDATE_REQUEST = "update";
  private static final String NAME_KEY = "name";
  
  /**
   * File name containing list of robots
   */
  private static final String LIST_FILE_NAME = "List";

  @Override
  public void sendRequest(JSONObject obj, ClientEndpointListener listener) {
    if (obj.containsKey(LIST_REQUEST)) {
      JSONObject listJSONObject = (JSONObject) obj.get(LIST_REQUEST);
      if (listJSONObject.containsKey(NAME_KEY)) {
        String robotName = (String) listJSONObject.get(NAME_KEY);
        loadRobot(robotName, listener);
      } else {
        loadRobotList(listener);
      }
    } else if (obj.containsKey(UPDATE_REQUEST)) {
      
    }
  }
  
  /**
   * Load a robot with name
   * @param name The name of the robot to load
   * @param listener The listener who requested this robot
   */
  private void loadRobot(String name, ClientEndpointListener listener) {
    try {
      JSONFileLoader loader = new JSONFileLoader();
      listener.receivedResponse(loader.load(name));
    } catch (IOException | URISyntaxException | ParseException e) {
      e.printStackTrace();
      listener.receivedError("Could not find a robot names " + name);
    }
  }

  /**
   * Load the robot list
   * @param listener The listener who requested this list
   */
  private void loadRobotList(ClientEndpointListener listener) {
    if (robotList == null) {
      try {
        JSONFileLoader loader = new JSONFileLoader();
        robotList = loader.load(LIST_FILE_NAME);
      } catch (IOException | URISyntaxException | ParseException e) {
        e.printStackTrace();
        listener.receivedError("Failed to load the list of robots from file.");
      }
    } 
    listener.receivedResponse(robotList);
  }
}
