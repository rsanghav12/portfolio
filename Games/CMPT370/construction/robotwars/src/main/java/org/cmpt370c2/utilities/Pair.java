package org.cmpt370c2.utilities;

/**
 * A generic class that represents a pair of any two objects
 * @author C2
 *
 * @param <S> The class of the first object in the pair
 * @param <T> The class of the second object in the pair
 */
public final class Pair <S, T> {
  
  private S first;
  private T second;
  
  public S getFirst() {
    return first;
  }

  public T getSecond() {
    return second;
  }

  @Override
  public String toString() {
    return "{ " + first + ", " + second + "}"; 
  }
  
  public Pair(S first, T second) {
    this.first = first;
    this.second = second;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((first == null) ? 0 : first.hashCode());
    result = prime * result + ((second == null) ? 0 : second.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Pair<?, ?> other = (Pair<?, ?>) obj;
    if (first == null) {
      if (other.first != null)
        return false;
    } else if (!first.equals(other.first))
      return false;
    if (second == null) {
      if (other.second != null)
        return false;
    } else if (!second.equals(other.second))
      return false;
    return true;
  }

}
