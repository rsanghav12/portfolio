package org.cmpt370c2.utilities;

/**
 * Hexagonal points represent the tiles of the game board. These will be stored within
 * the hexagonal container.
 * @author C2
 *
 */
public class HexagonalPoint {
  
  private int layer;
  private int direction;
  
  /**
   * Check if obj is equal to the current hexagonal point being 
   * compared to.
   */
  @Override
  public boolean equals(Object obj) {
    if(this == obj) {
      return true;
    }
    
    if((obj == null) || (obj.getClass() != this.getClass())) {
      return false;
    }
    
    HexagonalPoint p = (HexagonalPoint)obj;
    return (layer == p.layer) && (direction == p.direction);
  }
  
  /**
   * Unique code to distinguish each hexagonal point, this
   * makes it easier to check if points are equal.
   */
  @Override
  public int hashCode() {
    return (layer * 31) + direction;
  }
  
  @Override
  public String toString() {
    return "{ layer = " + layer + ", direction = " + direction + "}";
  }
  
  public int getLayer() {
    return layer;
  }

  public int getDirection() {
    return direction;
  }

  /**
   * Creates a new hexagonal point
   * @param layer The layer the point will be in
   * @param direction The direction of this point from the point (0,0) 
   */
  public HexagonalPoint(int layer, int direction) {
    if (layer < 0) {
      throw new IllegalArgumentException("A hexagonal point must have a layer greater than or equal to 0.");
    }
    if (layer == 0 && direction != 0) {
      throw new IllegalArgumentException("A hexagonal point with a layer of 0 must have direction 0.");
    }
    if (layer > 0 && (direction < 0 || direction >= 6*layer)) {
      throw new IllegalArgumentException("A hexagonal point with a layer of " + layer + " must have direction between 0 and " + ((layer * 6) - 1) + ".");
    }
    this.layer = layer;
    this.direction = direction;
  }
}
