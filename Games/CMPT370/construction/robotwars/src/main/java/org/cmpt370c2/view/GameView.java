package org.cmpt370c2.view;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.cmpt370c2.model.RobotType;
import org.cmpt370c2.model.TeamColor;
import org.cmpt370c2.utilities.HexagonalPoint;
import org.cmpt370c2.utilities.Pair;

/**
 * Displays the game board and game state when RobotWars is being played.
 * 
 * @author allankerr
 *
 */
@SuppressWarnings("serial")
public class GameView extends AbstractView<GameViewListener>
    implements ActionListener, KeyListener, MouseListener, MouseMotionListener {

  private static final String END_GAME_TEXT = "Quit Game";

  /**
   * The command for events that are sent to the game view listener.
   */
  public static final String END_GAME_COMMAND = "End Game";
  
  /**
   * Speed variables to decide animation speeds of move and
   * rotate.
   */
  private double ROTATION_SPEED = 180;
  private double MOVE_SPEED = 110;
  private static final double ROTATION_SPEED_ORIGINAL = 180;
  private static final double MOVE_SPEED_ORIGINAL = 110;

  /**
   * Local variables
   */
  private Board board;

  private GamePanel gamePanel;

  private HealthBarSprite healthBar;

  /**
   * Maintain the state of the game view to prevent player input when it isn't a human player's
   * turn.
   */
  private GameViewState state = GameViewState.NONE;

  /**
   * Creates a new game view instance
   * 
   * @param layers The number of layers the game board should have
   * @param listener The listener the view needs to communicate with it's controller
   */
  public GameView(int layers, GameViewListener listener) {
    super(listener);
    
    // Assert to make sure layers is between five and seven
    assert layers >=5 && layers <= 7;
    
    setLayout(new BoxLayout(this, BoxLayout.X_AXIS));

    this.gamePanel = new GamePanel(this);
    this.add(gamePanel);
    
    this.add(Box.createHorizontalGlue());

    // Add the panel that the game board is displayed in.
    JPanel boardPanel = new JPanel();
    this.board = new Board(layers, Dimensions.TILE_RADIUS, Dimensions.TILE_BORDER);
    boardPanel.setLayout(new BoxLayout(boardPanel, BoxLayout.Y_AXIS));
    boardPanel.setPreferredSize(this.board.getPreferredSize());
    boardPanel.setMinimumSize(this.board.getMinimumSize());
    boardPanel.add(Box.createVerticalGlue());

    // Add the scroll pane for scrolling the board if it is too large to fit in the maximized
    // screen.
    JScrollPane scrollPane = new JScrollPane(board);
    scrollPane.setBorder(null);
    boardPanel.add(scrollPane);

    this.add(boardPanel);

    this.add(Box.createHorizontalGlue());

    boardPanel.add(Box.createVerticalGlue());

    // Adds the quit game button
    MenuButton endGame = MenuComponentBuilder.buildMenuButton(END_GAME_TEXT, END_GAME_COMMAND);
    endGame.addActionListener(this);
    boardPanel.add(endGame);

    boardPanel.add(Box.createVerticalGlue());

    this.addKeyListener(this);
    this.board.addMouseListener(this);
    this.board.addMouseMotionListener(this);
  }

  /**
   * Receives action and performs appropriate task
   * 
   * @param e The action received
   */
  public void actionPerformed(ActionEvent e) {
    if (e.getActionCommand() == END_GAME_COMMAND) {
      this.getListener().endGame();
    } else if (e.getActionCommand() == StartPlayPanelCard.START_PLAY_COMMAND) {
      this.getListener().startPlay();
    } else if (e.getActionCommand() == PlayPanelCard.END_PLAY_COMMAND) {
      endPlay();
    } else if (e.getActionCommand() == PlayPanelCard.SPEED_PLAY_COMMAND) {
      MenuButton speedButton = (MenuButton) e.getSource();
      
      // update button label before getting it
      PlayPanelCard card = (PlayPanelCard) gamePanel.getCard(GamePanelCardType.PLAY_PANEL);
      card.updateSpeedButton(); 
            
      // update speed according to what the user selected
      switch(speedButton.getText()) {
        case "x1":
          ROTATION_SPEED = ROTATION_SPEED_ORIGINAL;
          MOVE_SPEED = MOVE_SPEED_ORIGINAL;
          break;
        case "x2":
          ROTATION_SPEED = ROTATION_SPEED_ORIGINAL * 2;
          MOVE_SPEED = MOVE_SPEED_ORIGINAL * 2;
          break;
        case "x3":
          ROTATION_SPEED = ROTATION_SPEED_ORIGINAL * 3;
          MOVE_SPEED = MOVE_SPEED_ORIGINAL * 3;
          break;
        case "x6":
          ROTATION_SPEED = ROTATION_SPEED_ORIGINAL * 6;
          MOVE_SPEED = MOVE_SPEED_ORIGINAL * 6;
          break;
        default:
          ROTATION_SPEED = ROTATION_SPEED_ORIGINAL;
          MOVE_SPEED = MOVE_SPEED_ORIGINAL;
          break;
      }
    }
  }

  /**
   * Adds a robot to the current game
   * 
   * @param team The team the robot being added is on
   * @param type The type of the robot being added
   * @param position The location of the robot being added on the board
   * @param direction The direction the robot being added is facing
   */
  public void robotWasAdded(TeamColor team, RobotType type, HexagonalPoint position,
      int direction) {
    double angle = directionToAngle(direction);
    RobotSprite robot = new RobotSprite(0, 0, angle, team, type);
    robot.setCenter(board.converToScreen(position));
    board.addRobot(robot, team, type);
  }

  /**
   * Removes a robot from the board if it is destroyed.
   * 
   * @param team The team the destroyed robot was on
   * @param type The type of the destroyed robot
   */
  public void robotWasDestroyed(TeamColor team, RobotType type) {
    board.removeRobot(team, type);
  }

  /**
   * Displays a robots movement on the board
   * 
   * @param team The team of the moving robot
   * @param type The type of the moving robot
   * @param destination Point that the robot is moving to
   * @param listener The listener the view needs to communicate
   */
  public void robotDidMove(TeamColor team, RobotType type, HexagonalPoint destination,
      ActionListener listener) {
    RobotSprite robot = board.getRobot(team, type);
    MoveAction move = new MoveAction(board.converToScreen(destination), MOVE_SPEED, listener);
    robot.run(move);
  }

  /**
   * Registers and displays a shot from a robot on the board
   * 
   * @param team The team of the robot that shot
   * @param type The type of the robot that shot
   * @param target The point on the board that the robot shot at
   * @param listener The listener the view needs to cummunicate
   */
  public void robotDidShoot(TeamColor team, RobotType type, HexagonalPoint target,
      final ActionListener listener) {
    final RobotSprite robot = board.getRobot(team, type);
    final Point targetLocation = board.converToScreen(target);

    RotateAction rotate = new RotateAction(targetLocation, ROTATION_SPEED, new ActionListener() {
      public void actionPerformed(ActionEvent e) {

        Point location = robot.getCenter();
        double deltaX = targetLocation.getX() - location.getX();
        double deltaY = targetLocation.getY() - location.getY();
        double angle = 90 + Math.toDegrees(Math.atan2(deltaY, deltaX));

        final BulletSprite bullet = new BulletSprite(location.x, location.y, angle);
        board.add(bullet);
        board.setComponentZOrder(bullet, board.getComponentZOrder(robot) + 1);
        bullet.run(new MoveAction(targetLocation, 425, new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            board.remove(bullet);
            board.repaint();

            if (listener != null) {
              listener.actionPerformed(new ActionEvent(this, 0, null));
            }
          }
        }));
      }
    });
    robot.run(rotate);
    robot.run(new DelayAction(0.25, null));

    RotateAction rotateBack = new RotateAction(rotate, null);
    robot.run(rotateBack);
  }

  /**
   * Updates the amount of shots in the side panel
   * 
   * @param shotsRemaining The current robots remaining amount of shots
   */
  public void robotShotsDidUpdate(int shotsRemaining) {
    PlayPanelCard card = (PlayPanelCard) gamePanel.getCard(GamePanelCardType.PLAY_PANEL);
    card.updateShotsRemaining(shotsRemaining);
  }

  /**
   * Updates the amount of moves in the side panel
   * 
   * @param movesRemaining The current robots remaining amount of moves
   */
  public void robotMovesDidUpdate(int movesRemaining) {
    PlayPanelCard card = (PlayPanelCard) gamePanel.getCard(GamePanelCardType.PLAY_PANEL);
    card.updateMovesRemaining(movesRemaining);
  }

  /**
   * Updates the time remaining timer in the side panel
   * 
   * @param timeRemaining The amount of time in seconds remaining for current turn
   */
  public void timerDidUpdate(int timeRemaining) {
    PlayPanelCard card = (PlayPanelCard) gamePanel.getCard(GamePanelCardType.PLAY_PANEL);
    card.updateTimeRemaining(timeRemaining);
  }

  /**
   * Updates a robot sprite if it has rotated
   * 
   * @param team The team of the robot that rotated
   * @param type The type of the robot that rotated
   * @param direction The new direction the robot is facing
   * @param listener The listener the view needs to communicate
   */
  public void robotDidRotate(TeamColor team, RobotType type, int direction,
      ActionListener listener) {
    RobotSprite robot = board.getRobot(team, type);
    double angle = directionToAngle(direction);
    RotateAction rotate = new RotateAction(angle, ROTATION_SPEED, listener);
    robot.run(rotate);
  }

  public void tileDidUpdate(HexagonalPoint p, TileType type) {
    this.board.updateTile(p, type);
  }

  public void playerPlayWillStart(TeamColor color) {
    state = GameViewState.STARTING_PLAYER_PLAY;
    gamePanel.showStartPlayCard(color);
  }

  public void playerPlayDidStart(TeamColor color, RobotType type) {
    boolean isComputerPlay = state != GameViewState.STARTING_PLAYER_PLAY;
    state = isComputerPlay ? GameViewState.COMPUTER_PLAY : GameViewState.PLAYER_PLAY;
    gamePanel.showPlayCard(color, type, isComputerPlay);
  }

  /**
   * Displays the health bar of the specified robot
   * 
   * @param team The team of the robot
   * @param type The type of the robot
   * @param healthRemaining The amount of health said robot has remaining.
   */
  public void healthBarWillShow(TeamColor team, RobotType type, int healthRemaining) {
    RobotSprite robot = board.getRobot(team, type);
    if (!robot.isVisible()) {
      return;
    }
    if (healthBar == null || (healthBar.getHealthRemaining() != healthRemaining)
        || !healthBar.getTeam().equals(team) || !healthBar.getType().equals(type)) {
      if (healthBar != null) {
        board.remove(healthBar);
      }
      Point center = robot.getCenter();
      healthBar = new HealthBarSprite(team, type, healthRemaining);
      healthBar.setCenter(center.x, center.y - (robot.getHeight() / 2));
      board.add(healthBar);
      board.setComponentZOrder(healthBar, 0);
      board.repaint();
    }
  }

  /**
   * Removes the health bar from view.
   */
  public void healthBarWillHide() {
    if (healthBar != null) {
      board.remove(healthBar);
      board.repaint();
      healthBar = null;
    }
  }

  /**
   * Sets specified robot visibility according to visible boolean
   * 
   * @param team The team of the robot whose visibility changed
   * @param type The type of the robot whose visibility changed
   * @param visible True if robot should be visible, false if it should be hidden
   */
  public void robotVisibilityDidChange(TeamColor team, RobotType type, boolean visible) {
    board.updateRobotVisibility(team, type, visible);
  }

  public void keyPressed(KeyEvent e) {
    switch (e.getKeyCode()) {
      case KeyEvent.VK_LEFT:
        if (state == GameViewState.PLAYER_PLAY) {
          this.getListener().turn(-1);
        }
        break;
      case KeyEvent.VK_RIGHT:
        if (state == GameViewState.PLAYER_PLAY) {
          this.getListener().turn(+1);
        }
        break;
      case KeyEvent.VK_UP:
        if (state == GameViewState.PLAYER_PLAY) {
          this.getListener().move();
        }
        break;
      case KeyEvent.VK_SPACE:
        if (state == GameViewState.STARTING_PLAYER_PLAY) {
          this.getListener().startPlay();
        } else if (state == GameViewState.PLAYER_PLAY) {
          endPlay();
        }
        break;
      default:
        // Do nothing, key code not handled
    }
  }
  
  /**
   * Converts a hexagon direction from 0 to 5 to degrees.
   * @param direction The direction to be converted to degrees.
   * @return The angle associated with the direction.
   */
  private int directionToAngle(int direction) {
    return 90 + (direction * 60);
  }
  
  private void endPlay() {
    state = GameViewState.NONE;
    this.getListener().endPlay();
  }

  public void mouseClicked(MouseEvent e) {
    if (state == GameViewState.PLAYER_PLAY) {
      HexagonalPoint point = board.convertToHexagonal(e.getPoint());
      if (point != null) {
        this.getListener().shoot(point);
      }
    }
  }

  public void mouseMoved(MouseEvent e) {
    GameViewListener listener = this.getListener();
    Pair<TeamColor, RobotType> pair = board.getRobotAt(e.getPoint());
    if (pair != null) {
      listener.highlightRobot(pair.getFirst(), pair.getSecond());
    } else {
      listener.unhighlightRobot();
    }
    listener.highlightTile(board.convertToHexagonal(e.getPoint()));
  }

  // Unneeded
  public void keyReleased(KeyEvent e) {}

  public void keyTyped(KeyEvent e) {}

  // Unneeded
  public void mousePressed(MouseEvent e) {}

  public void mouseReleased(MouseEvent e) {}

  public void mouseEntered(MouseEvent e) {}

  public void mouseExited(MouseEvent e) {}

  public void mouseDragged(MouseEvent e) {}
}
