package org.cmpt370c2.view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.cmpt370c2.model.RobotType;
import org.cmpt370c2.model.TeamColor;
import org.cmpt370c2.utilities.FontLoader;
import org.cmpt370c2.utilities.FontType;

/**
 * The card that is displayed while a user plays their turn. Displays relevant robot information
 * such as remaining time, shots, and moves.
 * 
 * @author allankerr and rutviksanghavi
 *
 */
@SuppressWarnings("serial")
public class PlayPanelCard extends GamePanelCard {

  public static final String END_PLAY_COMMAND = "END_PLAY_COMMAND";
  public static final String SPEED_PLAY_COMMAND = "SPEED_PLAY_COMMAND";
  
  private static final String TITLE_TEXT = "Current Robot";
  private static final String END_PLAY_TEXT = "End Play";

  private static final String SHOTS_TEXT = "Shots Remaining: ";
  private static final String MOVES_TEXT = "Moves Remaining: ";
  private static final String TIMER_TEXT = "Time Remaining: ";

  private static final String SPEED_OPTIONS[] = {"x1", "x2", "x3", "x6"};
  
  /**
   * The panel the image the currently controlled robot is displayed in to show the user which robot
   * is they have control of.
   */
  private JPanel robotPanel;

  /**
   * The labels for displaying the number of shots, moves, and time remaining.
   */
  private JLabel shotRemaining;
  private JLabel movesRemaining;
  private JLabel timeRemaining;

  private MenuButton endPlay;
  private MenuButton speedPlay;

  /**
   * Constructs a new play panel card is used for displaying the currently controlled robot for the
   * current player.
   * 
   * @param listener The listener that is notified when the player ends their play.
   */
  public PlayPanelCard(ActionListener listener) {
    super(listener);
    this.add(Box.createVerticalGlue());

    // Add the current robot title
    JLabel robotTitle = new JLabel(TITLE_TEXT);
    try {
      robotTitle.setFont(FontLoader.getInstance().load(FontType.HELVETICA_NEUE_LT, Font.PLAIN,
          FontSize.MENU_BUTTON_SIZE));
    } catch (Exception e) {
      e.printStackTrace();
    }
    robotTitle.setAlignmentX(Component.CENTER_ALIGNMENT);
    this.add(robotTitle);

    // Add the panel containing the sprite for the current robot
    this.robotPanel = new JPanel();
    this.robotPanel.setLayout(new BoxLayout(robotPanel, BoxLayout.X_AXIS));
    this.robotPanel.setOpaque(false);
    this.add(robotPanel);

    Dimension preferredSize = new Dimension(Dimensions.PANEL_WIDTH - (2 * Dimensions.PANEL_MARGINS),
        Dimensions.MENU_BUTTON_HEIGHT);

    // Add the shots remaining text.
    shotRemaining = MenuComponentBuilder.buildDetailLabel(SHOTS_TEXT);
    shotRemaining.setMaximumSize(preferredSize);
    this.add(shotRemaining);

    this.add(Box.createVerticalStrut(Dimensions.MENU_BUTTON_PADDING));

    // Add the moves remaining text.
    movesRemaining = MenuComponentBuilder.buildDetailLabel(MOVES_TEXT);
    movesRemaining.setMaximumSize(preferredSize);
    this.add(movesRemaining);

    this.add(Box.createVerticalStrut(Dimensions.MENU_BUTTON_PADDING));

    // Add the time remaining text.
    timeRemaining = MenuComponentBuilder.buildDetailLabel(TIMER_TEXT);
    timeRemaining.setMaximumSize(preferredSize);
    this.add(timeRemaining);

    Font font = shotRemaining.getFont();
    try {
      font = FontLoader.getInstance().load(FontType.HELVETICA_NEUE_LT, Font.PLAIN,
          FontSize.TABLE_TEXT_SIZE);
    } catch (Exception e) {
      e.printStackTrace();
    }
    shotRemaining.setFont(font);
    movesRemaining.setFont(font);
    timeRemaining.setFont(font);

    this.add(Box.createVerticalGlue());

    // Add the end play button.
    endPlay = MenuComponentBuilder.buildDetailButton(END_PLAY_TEXT, END_PLAY_COMMAND, true);
    endPlay.addActionListener(listener);
    this.add(endPlay);

    this.add(Box.createVerticalGlue());
    
    // Add the speed play button.
    speedPlay = MenuComponentBuilder.buildDetailButton(SPEED_OPTIONS[0], SPEED_PLAY_COMMAND, true);
    speedPlay.addActionListener(listener);
    speedPlay.putClientProperty("index", 0);
    this.add(speedPlay);
    
    this.add(Box.createVerticalGlue());
  }

  /**
   * Updates the speed play button label.
   */
  public void updateSpeedButton() {
    int index = (int) speedPlay.getClientProperty("index");

    // update the client index for string
    index = (index >= 3 ? 0 : ++index);
    speedPlay.putClientProperty("index", index);
    speedPlay.setText(SPEED_OPTIONS[index]);
  }
  
  /**
   * Updates the play panel to display the information for the robot that is currently being
   * controlled.
   * 
   * @param team The team of the robot being controlled during current play.
   * @param type The type of the robot being controlled during current play.
   * @param isComputer If the player is controlled by a computer to determine whether or not the end
   *        play button should be displayed.
   */
  public void updatePlayer(TeamColor team, RobotType type, boolean isComputer) {

    endPlay.setVisible(!isComputer);

    robotPanel.removeAll();

    RobotSprite robot = new RobotSprite(0, 0, 0, team, type);
    robotPanel.add(robot);

    robotPanel.setMaximumSize(robot.getSize());

    robotPanel.revalidate();
    robotPanel.repaint();
  }

  /**
   * Updates the amount of time displayed in the time remaining label.
   * 
   * @param timeRemaining The amount of time remaining.
   */
  public void updateTimeRemaining(int timeRemaining) {
    this.timeRemaining.setText(TIMER_TEXT + timeRemaining);
  }

  /**
   * Updates the number of moves displayed in the moves remaining label.
   * 
   * @param timeRemaining The number of moves remaining.
   */
  public void updateMovesRemaining(int movesRemaining) {
    this.movesRemaining.setText(MOVES_TEXT + movesRemaining);
  }

  /**
   * Updates the number of shots displayed in the shots remaining label.
   * 
   * @param timeRemaining The number of shots remaining.
   */
  public void updateShotsRemaining(int shotsRemaining) {
    this.shotRemaining.setText(SHOTS_TEXT + shotsRemaining);
  }
}
