package org.cmpt370c2.view;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;

/**
 * The main menu view of the view component used to display the initial menu presented to the user
 * when the system launches. It displays the options allowing the user to enter into the system or
 * exit.
 * 
 * @author allankerr and rutviksanghavi
 *
 */
@SuppressWarnings("serial")
public class MainMenuView extends AbstractView<MainMenuViewListener> implements ActionListener {

  private static final String TITLE_TEXT = "RobotWars";
  private static final String START_TEXT = "Start Game";
  private static final String EXIT_TEXT = "Exit";

  public static final String START_GAME_COMMAND = "start game";
  public static final String EXIT_COMMAND = "exit";

  /**
   * Constructs a new main menu view for displaying the initial view that is shown when the system
   * starts up.
   * 
   * @param listener The listener that is notified when the start game or exit buttons are pressed.
   */
  public MainMenuView(MainMenuViewListener listener) {
    super(listener);
    this.setBackground(Color.WHITE);
    this.setOpaque(true);

    this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

    this.add(Box.createVerticalGlue());
    
    // Add the title
    this.add(MenuComponentBuilder.buildTitleLabel(TITLE_TEXT));
    
    this.add(Box.createVerticalGlue());
    
    // Add the start button
    MenuButton start = MenuComponentBuilder.buildMenuButton(START_TEXT, START_GAME_COMMAND);
    start.addActionListener(this);
    this.add(start);
    
    this.add(Box.createVerticalStrut(Dimensions.MENU_BUTTON_PADDING));
    
    // Add the exit button
    MenuButton exit = MenuComponentBuilder.buildMenuButton(EXIT_TEXT, EXIT_COMMAND);
    exit.addActionListener(this);
    this.add(exit);

    this.add(Box.createVerticalGlue());
  }

  public void actionPerformed(ActionEvent e) {
    if (e.getActionCommand() == START_GAME_COMMAND) {
      this.getListener().startGame();
    } else if (e.getActionCommand() == EXIT_COMMAND) {
      this.getListener().exit();
    }
  }
}
