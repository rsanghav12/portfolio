package org.cmpt370c2.view;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.Queue;

import org.cmpt370c2.utilities.ImageLoader;
import org.cmpt370c2.utilities.ImageType;

/**
 * This class is used draw images within the game board such as robots and bullets. It is designed
 * to allow animation through the use of actions. Although it doesn't have to be sub-classed, it
 * usually is to create a subclass for each group of images.
 * 
 * @author allankerr
 *
 */
@SuppressWarnings("serial")
class ImageSprite extends Sprite {

  /**
   * The rotation of the image in degrees.
   */
  private double rotation;

  /**
   * The image that is currently being drawn.
   */
  private BufferedImage image;

  /**
   * The queue of actions that are waiting to be performed on the sprite. Actions are performed
   * sequentially. When one finishes, the next is dequeued.
   */
  private Queue<Action> actionQueue = new LinkedList<Action>();

  public double getRotation() {
    return rotation;
  }

  /**
   * Runs an action of any type on the sprite to animate it.
   * 
   * @param action The action to be performed on the sprite. If the sprite is currently running an
   *        action, this action will be queued until all other actions that came before it have been
   *        completed.
   */
  public void run(final Action action) {
    actionQueue.add(action);
    if (actionQueue.size() == 1) {
      action.start(this);
    }
  }

  public void setRotation(double rotation) {
    this.rotation = rotation % 360;
    this.repaint();
  }

  /**
   * Construct a new image sprite by loading the image from file.
   * 
   * @param fileName The name of the file in the resources directory.
   * @param type The type of image whether it be PNG or JPEG.
   * @param x The starting x position of the sprite.
   * @param y The starting y position of the sprite.
   */
  public ImageSprite(String fileName, ImageType type, int x, int y) {
    super(x, y, 0, 0);

    try {
      image = ImageLoader.getInstance().load(fileName, type);
      setSize(new Dimension(image.getWidth(this), image.getHeight(this)));
      setCenter(x, y);
    } catch (IOException | URISyntaxException e1) {
      throw new Error("Failed to load image with filename " + fileName + "." + type);
    }
  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);

    Graphics2D graphics = (Graphics2D) g;

    double rotationRequired = Math.toRadians(rotation);
    double locationX = image.getWidth(this) / 2;
    double locationY = image.getHeight(this) / 2;
    AffineTransform transform =
        AffineTransform.getRotateInstance(rotationRequired, locationX, locationY);
    AffineTransformOp op = new AffineTransformOp(transform, AffineTransformOp.TYPE_BILINEAR);

    int x = (this.getWidth() - image.getWidth(this)) / 2;
    int y = (this.getHeight() - image.getHeight(this)) / 2;
    graphics.drawImage(op.filter(image, null), x, y, null);
  }

  /**
   * Performed whenever an action being run on the sprite finishes allowing the next to be dequeued
   * and run.
   */
  protected void actionFinished() {
    if (!actionQueue.isEmpty()) {
      actionQueue.remove();
      if (!actionQueue.isEmpty()) {
        actionQueue.peek().start(this);
      }
    }
  }
}
