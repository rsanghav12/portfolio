package org.cmpt370c2.controller;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import org.cmpt370c2.view.AbstractView;

/**
 * Abstract class designed to act as a super class for all other controllers. Provides operations
 * necessary to communicate with the window manager as we as handle sending and receiving of
 * messages.
 * 
 * @author C2
 *
 * @param <V> A type of abstract view to depict the controller
 */
public abstract class AbstractController<V extends AbstractView<?>> implements WindowListener {

  private V view;

  /**
   * Window manager that the controller utilizes to present
   * views.
   */
  private WindowManager windowManager;

  protected abstract V createView();

  protected WindowManager getWindowManager() {
    return windowManager;
  }

  /**
   * Creates a new abstract controller
   * 
   * @param windowManager The window manager the controller needs to communicate with.
   */
  public AbstractController(WindowManager windowManager) {
    this.windowManager = windowManager;
  }

  /**
   * Get the view utilized by the controller
   * 
   * @return view used by the controller
   */
  public V getView() {
    if (view == null) {
      view = this.createView();
    }
    return view;
  }

  @Override
  public void windowOpened(WindowEvent e) {}

  @Override
  public void windowClosing(WindowEvent e) {}

  @Override
  public void windowClosed(WindowEvent e) {}

  @Override
  public void windowIconified(WindowEvent e) {}

  @Override
  public void windowDeiconified(WindowEvent e) {}

  @Override
  public void windowActivated(WindowEvent e) {}

  @Override
  public void windowDeactivated(WindowEvent e) {}
}
