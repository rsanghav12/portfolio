package org.cmpt370c2.view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * Implements a default table and loads all the default settings along with rounded background. 
 * Separate class was created to make it easier to tweak tables on a later date if required.
 * @author RutvikSanghavi
 */
@SuppressWarnings("serial")
class MenuTableView extends JScrollPane {

  private static JTable table;

  public MenuTableView(DefaultTableModel model) {
    super(table = new JTable(model));

    this.setAlignmentX(Component.CENTER_ALIGNMENT);
    this.setOpaque(false);
    this.setBackground(Colors.BACKGROUND_COLOR);
    this.getViewport().setOpaque(false);
    this.setBorder(BorderFactory.createEmptyBorder());
    this.setMaximumSize(new Dimension(Dimensions.TABLE_WIDTH, Dimensions.TABLE_HEIGHT));

    table.setAlignmentX(Component.CENTER_ALIGNMENT);
    table.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
    table.setRowHeight(120);
    table.setFocusable(false);
    table.setRowSelectionAllowed(false);
    table.setColumnSelectionAllowed(false);
    table.setShowGrid(false);
    table.setOpaque(false);
    table.setBorder(null);
    table.setFillsViewportHeight(true);
    table.getTableHeader().setReorderingAllowed(false);
    table.getTableHeader().setResizingAllowed(false);
  }

  public JTable getTable() {
    return table;
  }

  public void removeHeaderColumn() {
    table.setTableHeader(null);
  }

  @Override
  protected void paintComponent(Graphics g) {
    Graphics2D graphics = (Graphics2D) g;
    graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

    graphics.setPaint(getBackground());
    graphics.fillRoundRect(0, 0, this.getWidth(), this.getHeight(), Dimensions.TABLE_CORNER_RADIUS,
        Dimensions.TABLE_CORNER_RADIUS);

    super.paintComponent(g);
  }
}
