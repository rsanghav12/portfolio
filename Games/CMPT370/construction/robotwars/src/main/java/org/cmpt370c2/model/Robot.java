package org.cmpt370c2.model;

import org.cmpt370c2.interpreter.controller.ScriptDataSource;
import org.cmpt370c2.interpreter.model.ScriptData;
import org.cmpt370c2.interpreter.model.Value;
import org.cmpt370c2.utilities.HexagonalPoint;

/**
 * Maintains all information about robot. Contains the script to be used if computer controlled, all
 * statistics regarding robot as it's current status.
 * 
 * @author C2
 *
 */
public class Robot implements ScriptDataSource {

  private int shotsLeft;
  private int movesLeft;
  private int direction;
  private HexagonalPoint point;
  private ScriptData script;
  private RobotStats stats;

  /**
   * Returns whether or not the robot has a script; if it true, the robot is computer controlled
   * 
   * @return True if robot is computer controlled, false otherwise
   */
  public boolean hasScript() {
    return script != null;
  }

  public ScriptData getScript() {
    if (!this.hasScript()) {
      throw new IllegalStateException("Attempted to get script for non-computer robot.");
    }
    return script;
  }

  public RobotStats getStats() {
    return stats;
  }

  public int shotsLeft() {
    return shotsLeft;
  }

  public int getDirection() {
    return direction;
  }

  public void setDirection(int direction) {
    this.direction = (direction + 6) % 6;
  }

  public HexagonalPoint getPoint() {
    return point;
  }

  void setPoint(HexagonalPoint point) {
    this.point = point;
  }

  @Override
  public int hashCode() {
    return stats.hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Robot other = (Robot) obj;
    return stats.equals(other.stats);
  }

  /**
   * Constructs a new human controlled robot
   * 
   * @param team The team which the robot belongs to
   * @param type The type of the new robot to be created
   */
  public Robot(TeamColor team, RobotType type) {
    stats = new RobotStats(type, team);
  }

  /**
   * Constructs a new computer controlled robot
   * 
   * @param team The team which the robot belongs to
   * @param type The type of the new robot to be created
   * @param script The script to be used to control new robot
   */
  public Robot(TeamColor team, RobotType type, ScriptData script) {
    this(team, type);
    this.script = script.clone();
  }

  public void startPlay() {
    movesLeft = stats.getType().getMoves();
    shotsLeft = 1;
  }

  public void decrementShotsLeft() {
    shotsLeft--;
  }

  public void decrementMovesLeft() {
    stats.setDistanceTraveled(stats.getDistanceTraveled() + 1);
    movesLeft--;
  }

  public void tookDamage(int damage) {
    stats.setHealthLeft(stats.getHealthLeft() - damage);
  }
  
  public void killedRobots(int numKilled) {
    if (numKilled < 0) {
      throw new IllegalArgumentException("Cannot kill a negative number of robots.");
    }
    stats.setRobotsKilled(stats.getRobotsKilled() + numKilled);
  }

  public boolean receiveMessage(RobotType sender, Value<?> value) {
    if (value == null) {
      throw new IllegalArgumentException(
          "Attempted to send a null message to " + this.type() + " on " + this.team());
    }
    if (!hasScript()) {
      throw new IllegalStateException("Attempted to send a message to a player-controlled robot");
    }
    // Only alive robots can receive messages
    if (!isDead()) {
      return this.script.saveMessage(sender, value);
    } else {
      return false;
    }
  }

  public boolean isDead() {
    return stats.getHealthLeft() <= 0;
  }

  public int health() {
    return stats.getType().getHealth();
  }

  public int healthLeft() {
    return stats.getHealthLeft();
  }

  public int moves() {
    return stats.getType().getMoves();
  }

  public int movesLeft() {
    return movesLeft;
  }

  public int attack() {
    return stats.getType().getAttack();
  }

  public int range() {
    return stats.getType().getRange();
  }

  public TeamColor team() {
    return stats.getTeam();
  }

  public RobotType type() {
    return stats.getType();
  }

}
