package org.cmpt370c2.utilities;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.HashMap;

import javax.imageio.ImageIO;

/**
 * A class to facilitate the loading of images to be used
 * @author C2
 *
 */
public class ImageLoader extends FileLoader {
 
  private static ImageLoader instance = null;

  private HashMap<String, BufferedImage> imageCache = new HashMap<String, BufferedImage>();

  private ImageLoader() {}
  
  /**
   * Creates new instance of image loader if one does not already exist
   * @return An instance of image loader
   */
  public static ImageLoader getInstance() {
    if (instance == null) {
      instance = new ImageLoader();
    }
    return instance;
  }

  /**
   * Loads the specified image
   * @param fileName The name of the image to be loaded
   * @param type The type of image to be loaded
   * @return A buffered image ready to be displayed
   * @throws IOException If image loading is unsuccessful, throw IO exception
   * @throws URISyntaxException Throws if the image could not be found
   */
  public BufferedImage load(String fileName, ImageType type) throws IOException, URISyntaxException {
    BufferedImage image;
    String pathComponent = fileName + "." + type.toString();
    if (imageCache.containsKey(pathComponent)) {
      image = imageCache.get(pathComponent);
    } else {
      InputStream input = super.load(pathComponent, null);
      
      image = ImageIO.read(input);
      imageCache.put(pathComponent, image);
      input.close();
    }
    return image;
  }

  public void clearCache() {
    imageCache.clear();
  }
}
