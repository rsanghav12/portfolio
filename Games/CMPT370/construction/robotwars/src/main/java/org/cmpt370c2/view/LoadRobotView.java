package org.cmpt370c2.view;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JOptionPane;

import org.cmpt370c2.robotlibrarianclient.RobotData;

/**
 * This class is part of the view component used to display the robot scripts the user can choose
 * from. These robot scripts are to be obtained from the robot librarian.
 * 
 * @author rutviksanghavi
 */
@SuppressWarnings("serial")
public class LoadRobotView extends AbstractView<LoadRobotViewListener> implements ActionListener {

  /**
   * Title for the view.
   */
  private static final String LOAD_ROBOTS_TEXT = "Load Robots";
  
  /**
   * Title for the cancel button.
   */
  private static final String CANCEL_TEXT = "Cancel";

  /**
   * Cancel command utilized by the cancel buttons actionlistener.
   */
  private static final String CANCEL_COMMAND = "cancel";
  
  /**
   * Set robot command utilized by a table cell to set a robot.
   */
  static final String SET_ROBOT_COMMAND = "set robot";

  /**
   * Table model to hold table data and manipulate it.
   */
  private LoadRobotTableModel robotTableModel;
  
  /**
   * Table to display robots recieved from the cliendendpoint.
   */
  private MenuTableView robotTable;

  /**
   * Creates a new instance of load robot view and displays the user interface.
   * 
   * @param listener The listener required for the view to communicate with it's controller.
   */
  public LoadRobotView(LoadRobotViewListener listener) {
    super(listener);

    this.setBackground(Color.WHITE);
    this.setOpaque(true);
    this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

    this.add(Box.createVerticalGlue());
    this.add(MenuComponentBuilder.buildSubtitleLabel(LOAD_ROBOTS_TEXT));
    this.add(Box.createVerticalGlue());

    // create instance of LoadRobotTableModel for the table
    robotTableModel = new LoadRobotTableModel();
    
    // init the table and set its default renderers
    robotTable = new MenuTableView(robotTableModel);
    robotTable.getTable().setDefaultRenderer(String.class, new TableTextCell());
    robotTable.getTable().setDefaultRenderer(Long.class, new TableTextCell());
    robotTable.getTable().setDefaultRenderer(RobotData.class,
        new LoadRobotButtonCell("Set", Colors.DARK_COMPONENT_COLOR, null));
    robotTable.getTable().setDefaultEditor(RobotData.class,
        new LoadRobotButtonCell("Set", Colors.DARK_COMPONENT_COLOR, this));
    robotTable.getTable().getTableHeader().setDefaultRenderer(new MenuTableHeaderRenderer());

    this.add(robotTable);

    this.add(Box.createVerticalGlue());

    // Add the cancel button
    MenuButton cancel = MenuComponentBuilder.buildMenuButton(CANCEL_TEXT, CANCEL_COMMAND);
    cancel.addActionListener(this);
    this.add(cancel);

    this.add(Box.createVerticalGlue());
  }

  /**
   * When robots are grabbed from the clientendpoint this function is called by the controller
   * to update the table model to show the users.
   * 
   * @param robots Collection of robots for the table model to utilize
   */
  public void listRobots(Collection<RobotData> robots) {
    // assert when robots is equal to null
    assert robots != null;
    
    // update the table model
    robotTableModel.setRobots(robots);
  }

  /**
   * Called when robots fail to load, it shows the error message to the user
   * as a dialog box and dismissable with a button.
   * 
   * @param errorMessage Error message to show the in the dialog box
   */
  public void robotLoadDidFail(String errorMessage) {
    JOptionPane.showMessageDialog(this, errorMessage, "Load Failed", JOptionPane.ERROR_MESSAGE);
  }
  
  /**
   * Receives action and performs appropriate task
   * 
   * @param e The action received
   */
  @Override
  public void actionPerformed(ActionEvent e) {
    if (e.getActionCommand() == CANCEL_COMMAND) {
      this.getListener().cancel();
    } else if (e.getActionCommand() == SET_ROBOT_COMMAND) {
      this.getListener().robotSelected(Integer
          .parseInt((String) ((MenuButton) e.getSource()).getClientProperty("Index").toString()));
    }
  }
}
