package org.cmpt370c2.view;

import java.awt.Dimension;
import java.awt.Point;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import javax.swing.JPanel;

import org.cmpt370c2.model.RobotType;
import org.cmpt370c2.model.TeamColor;
import org.cmpt370c2.utilities.HexagonalContainer;
import org.cmpt370c2.utilities.HexagonalPoint;
import org.cmpt370c2.utilities.Pair;

/**
 * This class is used to draw the main game board which all game state is displayed within. It is
 * responsible for organizing the robot sprites and board tiles to be drawn.
 * 
 * @author allankerr
 *
 */
@SuppressWarnings("serial")
class Board extends JPanel {

  /**
   * The container of tiles drawn on the board.
   */
  private HexagonalContainer<TileSprite> tiles;

  /**
   * Used to map team and robot type to a specific sprite on the board.
   */
  private HashMap<TeamColor, HashMap<RobotType, RobotSprite>> teams;

  /**
   * Computes the width of the rectangle the board should be drawn in based on the dimensions of
   * tiles and the number of layers.
   * 
   * @param layers The number of tile-layers in the board.
   * @param hexagonRadius The radius used to draw individual tiles within the board.
   * @param strokeWidth The thickness of the lines that draw the board.
   * @return The width of the rectangle the board should be drawn in.
   */
  private static int computeWidth(int layers, int hexagonRadius, int strokeWidth) {
    double halfHeight =
        Math.sqrt(hexagonRadius * hexagonRadius - (hexagonRadius / 2) * (hexagonRadius / 2));
    return (int) Math.round(2 * (2 * halfHeight * (layers - 1) + halfHeight)) + (2 * strokeWidth);
  }

  /**
   * Constructs a new board with the number of layers th
   * 
   * @param layers The number of tile-layers in the board.
   * @param hexagonRadius The radius used to draw individual tiles within the board.
   * @param strokeWidth The thickness of the lines that draw the board.
   */
  public Board(int layers, int hexagonRadius, int strokeWidth) {
    this(computeWidth(layers, hexagonRadius, strokeWidth), layers, hexagonRadius, strokeWidth);
  }

  /**
   * Constructs a new board with a specified width, height, hexagon radius, and stroke width.
   * 
   * @param width The width of the rectangle the board is drawn in.
   * @param layers The number of tile-layers in the board.
   * @param hexagonRadius The radius used to draw individual tiles within the board.
   * @param strokeWidth The width of the stroke used to draw tile borders.
   */
  private Board(int width, int layers, int hexagonRadius, int strokeWidth) {
    super();
    Dimension size = new Dimension(width, width);
    this.setPreferredSize(size);
    this.setMaximumSize(size);
    this.setMinimumSize(size);
    this.setLayout(null);

    tiles = new HexagonalContainer<TileSprite>(layers);
    teams = new HashMap<TeamColor, HashMap<RobotType, RobotSprite>>();
    for (TeamColor team : TeamColor.values()) {
      teams.put(team, new HashMap<RobotType, RobotSprite>());
    }
    double center = width / 2;
    double halfHeight =
        Math.sqrt(hexagonRadius * hexagonRadius - (hexagonRadius / 2) * (hexagonRadius / 2));

    // Center tile.
    HexagonalPoint point = new HexagonalPoint(0, 0);
    TileSprite tile = new TileSprite((int) center, (int) center, hexagonRadius, strokeWidth);
    tiles.put(tile, point);
    this.add(tile);

    for (int layer = 1; layer < layers; layer++) {

      // The radius of the circle that corner tiles are located along.
      double radius = 2 * halfHeight * layer;

      // The direction the offset tiles should be located in.
      double offsetAngle = 0;

      // The x and y points to offset from.
      double x = 0;
      double y = 0;

      double angle = 0;
      double angleDelta = Math.PI / 3;
      for (int direction = 0; direction < 6 * layer; direction++) {

        point = new HexagonalPoint(layer, direction);

        // The number of tiles offset from a corner tile.
        int offset = direction % layer;
        if (offset == 0) {
          // Draw the corner tile
          x = Math.round(Math.cos(angle) * radius + center);
          y = Math.round(Math.sin(angle) * radius + center);
          tile = new TileSprite((int) x, (int) y, (int) hexagonRadius, strokeWidth);
          tiles.put(tile, point);
          this.add(tile);

          offsetAngle = angle + Math.toRadians(120);
          angle += angleDelta;

        } else {
          double xOffset = Math.round(x + Math.cos(offsetAngle) * halfHeight * 2 * offset);
          double yOffset = Math.round(y + Math.sin(offsetAngle) * halfHeight * 2 * offset);
          tile = new TileSprite((int) xOffset, (int) yOffset, (int) hexagonRadius, strokeWidth);
          tiles.put(tile, point);
          this.add(tile);
        }
      }
    }
  }

  /**
   * Converts a hexagonal point to Cartesian screen coordinates.
   * 
   * @param point The point to be converted to screen coordinates.
   * @return The screen x and y coordinates associated with the hexagonal point.
   */
  public Point converToScreen(HexagonalPoint point) {
    TileSprite tile = tiles.get(point);
    return tile.getCenter();
  }

  /**
   * Converts a screen coordinates to coordinates within the hexagonal coordinate system using layer
   * and direction.
   * 
   * @param point The Cartesian coordinate to be converted.
   * @return The hexagonal point associated with the Cartesian coordinate or null if there isn't
   *         one.
   */
  public HexagonalPoint convertToHexagonal(Point point) {
    Iterator<HexagonalPoint> iter = tiles.iterator();
    while (iter.hasNext()) {
      HexagonalPoint cur = iter.next();
      TileSprite tile = tiles.get(cur);
      if (tile.contains(point)) {
        return cur;
      }
    }
    return null;
  }

  /**
   * Updates the display type of a tile at the specified point. This is used to update whether a
   * tile is displayed visible or not.
   * 
   * @param point The point of the tile to be updated.
   * @param type The display type the tile should be updated to.
   */
  public void updateTile(HexagonalPoint point, TileType type) {
    TileSprite tile = tiles.get(point);
    tile.setType(type);
  }

  /**
   * Adds specified robot sprite to the displayed board.
   * 
   * @param robot The robot sprite to be added
   * @param team The team that the robot to be added belongs to
   * @param type The type of the robot being added
   */
  public void addRobot(RobotSprite robot, TeamColor team, RobotType type) {
    HashMap<RobotType, RobotSprite> robotTeam = teams.get(team);
    if (robotTeam.containsKey(type)) {
      throw new IllegalArgumentException("Attempted to add " + type.toString() + " to team "
          + team.toString() + " but it already exists.");
    }
    robotTeam.put(type, robot);
    add(robot);
    setComponentZOrder(robot, 0);
  }

  /**
   * Removes a robot sprite from the displayed board
   * 
   * @param team The team of the robot sprite that is to be removed
   * @param type The type of robot sprite that is to be removed
   */
  public void removeRobot(TeamColor team, RobotType type) {
    RobotSprite robot = getRobot(team, type);
    HashMap<RobotType, RobotSprite> robotTeam = teams.get(team);
    robotTeam.remove(type);
    remove(robot);
  }

  /**
   * Gets the robot sprite associated with the specified team and robot type.
   * 
   * @param team The team the robot sprite is on.
   * @param type The type of the robot sprite.
   * @return The robot sprite with the corresponding team and type.
   * 
   * @throws IllegalArgumentException Thrown if there is no robot sprite with the associated team
   *         and type.
   */
  public RobotSprite getRobot(TeamColor team, RobotType type) {
    HashMap<RobotType, RobotSprite> robotTeam = teams.get(team);
    if (!robotTeam.containsKey(type)) {
      throw new IllegalArgumentException("Attempted to get " + type.toString() + " from team "
          + team.toString() + " but it doesn't exist.");
    }
    return robotTeam.get(type);
  }

  /**
   * Gets the team color and robot type of the first robot at the specified point. This is used for
   * mouse over over robots so only the upper-most robot should be returned.
   * 
   * @param point The hexagonal point to get the upper-most robot from.
   * @return The upper-most robot at the specified point or null if there is no robot.
   */
  public Pair<TeamColor, RobotType> getRobotAt(Point point) {
    for (Entry<TeamColor, HashMap<RobotType, RobotSprite>> teamEntry : teams.entrySet()) {
      for (Entry<RobotType, RobotSprite> robotEntry : teamEntry.getValue().entrySet()) {
        RobotSprite robot = robotEntry.getValue();
        if (robot.contains(point)) {
          return new Pair<TeamColor, RobotType>(teamEntry.getKey(), robotEntry.getKey());
        }
      }
    }
    return null;
  }

  /**
   * Updates whether a robot is hidden or visible on the game board.
   * 
   * @param team The team of the robot that should have its visibility changed.
   * @param type The type of the robot that should have its type changed.
   * @param visible True if the robot should be made visible or false if it should be hidden.
   */
  public void updateRobotVisibility(TeamColor team, RobotType type, boolean visible) {
    RobotSprite robot = getRobot(team, type);
    robot.setVisible(visible);
  }
}
