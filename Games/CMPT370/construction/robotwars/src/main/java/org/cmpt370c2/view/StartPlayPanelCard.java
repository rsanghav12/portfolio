package org.cmpt370c2.view;

import java.awt.Dimension;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

import org.cmpt370c2.model.RobotType;
import org.cmpt370c2.model.TeamColor;

/**
 * The card that is displayed when it is a human player's play. It displays the next team's color
 * and waits for the next player to click start play to start their play.
 * 
 * @author allankerr and rutviksanghavi
 *
 */
@SuppressWarnings("serial")
class StartPlayPanelCard extends GamePanelCard {

  private static final String START_PLAY_TEXT = "Start Play";

  /**
   * The commands for event that are sent to the game view.
   */
  public static final String START_PLAY_COMMAND = "START_PLAY_COMMAND";

  /**
   * The panel used to display the robots for the player who's turn is next. A panel is required to
   * allow for updating of the displayed sprites without breaking the boxlayout.
   */
  private JPanel robotPanel;

  /**
   * Constructs a new start play panel card to be displayed when a human player is allowed to start
   * their turn.
   * 
   * @param listener The listener that is notified when the player clicks the button to start their
   *        turn.
   */
  public StartPlayPanelCard(ActionListener listener) {
    super(listener);
    this.add(Box.createVerticalGlue());

    // Add the panel containing the robot sprites of the correct color.
    this.robotPanel = new JPanel();
    this.robotPanel.setLayout(new BoxLayout(robotPanel, BoxLayout.X_AXIS));
    this.robotPanel.setOpaque(false);
    this.add(robotPanel);

    // Add the start play button
    MenuButton startPlay = MenuComponentBuilder.buildDetailButton(START_PLAY_TEXT, START_PLAY_COMMAND, true);
    startPlay.addActionListener(listener);
    this.add(startPlay);

    this.add(Box.createVerticalGlue());
  }

  /**
   * Updates the team color that is displayed on the card causing the sprites in the robot panel to
   * change color.
   * 
   * @param team The color of the team whose turn is next
   */
  public void updateTeam(TeamColor team) {
    robotPanel.removeAll();

    int width = 0;
    int height = 0;
    for (RobotType type : RobotType.values()) {
      RobotSprite robot = new RobotSprite(0, 0, 0, team, type);
      robotPanel.add(robot);
      width += robot.getWidth();
      height = robot.getHeight();
    }
    Dimension size = new Dimension(width, height);
    robotPanel.setMaximumSize(size);

    robotPanel.revalidate();
    robotPanel.repaint();
  }
}
