package org.cmpt370c2.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;

import org.cmpt370c2.model.RobotType;
import org.cmpt370c2.model.TeamColor;

/**
 * Displays the user interface to allow the user to configure the upcoming game session. Allows
 * for the selection of teams, robot scripts and amount of players.
 * 
 * @author C2
 */
@SuppressWarnings("serial")
public class SessionConfigurationView extends AbstractView<SessionConfigurationViewListener> implements ActionListener {

  /**
   * Set of strings used by title, and buttons.
   */
  private static final String NEW_GAME_TEXT = "New Game";
  private static final String START_TEXT = "Start";
  private static final String CANCEL_TEXT = "Cancel";

  /**
   * Set of commands used by buttons, jcombobox, and table cells.
   */
  public static final String CANCEL_COMMAND = "cancel";
  public static final String START_COMMAND = "start";
  public static final String SET_PLAYERS_COMMAND = "set players";
  public static final String IS_PLAYER_COMPUTER = "is player computer";
  public static final String LOAD_SCRIPT_COMMAND = "load script for team";

  /**
   * Array to hold jcombobox options.
   */
  private final String[] numberOfPlayerOptions = { "2", "3", "6" };

  /**
   * Session table and session model for the table.
   */
  private MenuTableView sessionTable;
  private SessionTableModel sessionTableModel;

  /**
   * Creates a new session configuration view instance with a dropdown menu for
   * selecting number of players and a table to select computers and scripts for
   * each team.
   * 
   * @param listener The listener the view needs to communicate with it's controller
   */
  public SessionConfigurationView(SessionConfigurationViewListener listener) {
    super(listener);

    // default view setting
    this.setOpaque(true);
    this.setBackground(Color.WHITE);
    this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

    this.add(Box.createVerticalStrut(Dimensions.MENU_BUTTON_PADDING)); 
    this.add(MenuComponentBuilder.buildSubtitleLabel(NEW_GAME_TEXT));

    Dimension preferredSize = new Dimension(Dimensions.MENU_BUTTON_WIDTH, Dimensions.MENU_BUTTON_HEIGHT);

    this.add(Box.createVerticalStrut(Dimensions.MENU_BUTTON_PADDING));

    // create instance of configuration for the jcombobox
    SessionComboBoxConfiguration comboBoxConfiguration = new SessionComboBoxConfiguration();

    // add drop down menu for selecting players
    JComboBox<String> players = new JComboBox<String>(numberOfPlayerOptions);
    players.setRenderer(comboBoxConfiguration);
    players.setEditor(comboBoxConfiguration);
    players.setAlignmentX(Component.CENTER_ALIGNMENT);
    players.setPreferredSize(preferredSize);
    players.setMaximumSize(preferredSize);
    players.setEditable(true);
    players.setSelectedIndex(0);
    players.addActionListener(this);
    players.setActionCommand(SET_PLAYERS_COMMAND);
    this.add(players);

    this.add(Box.createVerticalStrut(Dimensions.MENU_BUTTON_PADDING));

    // init session table based on default players
    sessionTableModel = new SessionTableModel(Integer.parseInt(players.getSelectedItem().toString()));

    // init session table with session table model
    sessionTable = new MenuTableView(sessionTableModel);
    sessionTable.getTable().setDefaultRenderer(RobotSprite.class, new TableRobotSpriteCell());
    sessionTable.getTable().setDefaultRenderer(Boolean.class, new TableCheckBoxCell());
    sessionTable.getTable().setDefaultEditor(Boolean.class, new TableCheckBoxCellEditor(this));
    sessionTable.getTable().setDefaultRenderer(TeamColor.class, new TableLoadScriptCell(null));
    sessionTable.getTable().setDefaultEditor(TeamColor.class, new TableLoadScriptCell(this));
    sessionTable.removeHeaderColumn();
    this.add(sessionTable);

    this.add(Box.createVerticalStrut(Dimensions.MENU_BUTTON_PADDING));

    // Add the start game button
    MenuButton startGame = MenuComponentBuilder.buildMenuButton(START_TEXT, START_COMMAND);
    startGame.addActionListener(this);
    this.add(startGame);

    // Add the cancel configuration button
    MenuButton cancel = MenuComponentBuilder.buildMenuButton(CANCEL_TEXT, CANCEL_COMMAND);
    cancel.addActionListener(this);
    this.add(cancel);   

    this.add(Box.createVerticalGlue());
  }

  /**
   * Update number of players by calling the listener.
   * 
   * @param numberOfPlayers Amount of players selected by the user
   */
  private void updateNumberOfPlayers(int numberOfPlayers) {
    // assert when number of players is not 2, 3, or 6
    assert numberOfPlayers == 2 || numberOfPlayers == 3 || numberOfPlayers == 6;

    // send the amount of players
    this.getListener().setNumberOfPlayers(numberOfPlayers);
  }

  /**
   * Receives action and performs appropriate task
   * 
   * @param e The action received
   */
  @Override
  public void actionPerformed(ActionEvent e) {
    if (e.getActionCommand() == CANCEL_COMMAND) {
      this.getListener().cancel();
    } else  if (e.getActionCommand() == START_COMMAND) {
      this.getListener().start();
    } else if (e.getActionCommand() == SET_PLAYERS_COMMAND) {
      @SuppressWarnings("unchecked")
      JComboBox<String> eventSource = (JComboBox<String>) e.getSource();
      int players = Integer.parseInt(eventSource.getSelectedItem().toString()) ;
      updateNumberOfPlayers(players);
      sessionTableModel.setPlayers(players);
    } else if (e.getActionCommand() == IS_PLAYER_COMPUTER) {
      JCheckBox eventSource = (JCheckBox) e.getSource();
      TeamColor team = (TeamColor) eventSource.getClientProperty("RobotTeam");
      this.getListener().toggleComputerTeam(team);
    } else if (e.getActionCommand() == LOAD_SCRIPT_COMMAND) {
      MenuButton eventSource = (MenuButton) e.getSource();
      TeamColor team = (TeamColor) eventSource.getClientProperty("RobotTeam");
      RobotType type = (RobotType) eventSource.getClientProperty("RobotType");
      this.getListener().loadScripts(team, type);
    }
  }
}
