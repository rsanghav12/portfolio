package org.cmpt370c2.model;

/**
 * Interface to be implemented by model classes that facilitate the validating
 * of stored data
 * @author C2
 *
 */
public interface Validatable {
  /**
   * Validates data in class
   * @return True if data is good, false otherwise
   */
  public boolean validate();
}
