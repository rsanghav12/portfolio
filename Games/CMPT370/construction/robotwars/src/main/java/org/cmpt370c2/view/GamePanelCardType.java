package org.cmpt370c2.view;

/**
 * An enum of the different panels that exist in the system, which are the start play
 * panel and the play panel
 * @author C2
 *
 */
enum GamePanelCardType {
  START_PLAY_PANEL("START_PLAY_PANEL"),
  PLAY_PANEL("PLAY_PANEL");
  
  private String name;       

  @Override
  public String toString() {
     return name;
  }
  
  private GamePanelCardType(String name) {
      this.name = name;
  }
}
