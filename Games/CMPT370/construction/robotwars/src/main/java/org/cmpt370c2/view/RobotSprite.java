package org.cmpt370c2.view;

import java.awt.Point;

import org.cmpt370c2.model.RobotType;
import org.cmpt370c2.model.TeamColor;
import org.cmpt370c2.utilities.ImageType;

/**
 * This class is responsible for display the images for robots in the game board and menus in the
 * view. The image displayed is based on the robot type of SCOUT, SNIPER, and TANK and the color of
 * the team specified.
 * 
 * @author allankerr
 *
 */
@SuppressWarnings("serial")
class RobotSprite extends ImageSprite {

  /**
   * Construct a new robot sprite to display a robot's image based on its team color and type.
   * 
   * @param x The x-coordinate of the new robot's starting location
   * @param y The y-coordinate of the new robot's starting location
   * @param rotation The starting rotation of the new robot in degrees
   * @param team The team that the new robot belongs to determine the color of the robot image.
   * @param type The new robot's type to determine the image that should be used to draw it.
   */
  public RobotSprite(int x, int y, double rotation, TeamColor team, RobotType type) {
    super(team.toString() + type.toString(), ImageType.PNG, x, y);
    this.setRotation(rotation);
  }

  @Override
  public boolean contains(Point p) {
    Point center = this.getCenter();
    double deltaX = center.getX() - p.getX();
    double deltaY = center.getY() - p.getY();
    return Math.sqrt((deltaX * deltaX) + (deltaY * deltaY)) <= (this.getWidth() / 2);
  }
}
