package org.cmpt370c2.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.AbstractCellEditor;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import org.cmpt370c2.robotlibrarianclient.RobotData;
import org.cmpt370c2.utilities.FontLoader;
import org.cmpt370c2.utilities.FontType;

/**
 * This class is used to display the load robot button within the load robots view controller. A
 * subclass is necessary for custom drawing to fit the graphical style of the RobotWars system.
 * 
 * @author rutviksanghavi
 *
 */
@SuppressWarnings("serial")
class LoadRobotButtonCell extends AbstractCellEditor implements TableCellRenderer, TableCellEditor {

  private JPanel panel;

  private MenuButton button;

  private Object value;

  private Border border[];

  /**
   * Creates new load robot button cell. When clicked a message is sent to the view
   * which forwards it to the controller with the script to load for the robot type selected.
   * @param title Title of the button
   * @param borderColor Color of the border
   * @param l Listener to send the click event
   */
  public LoadRobotButtonCell(String title, Color borderColor, ActionListener l) {
    panel = new JPanel();
    panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
    panel.setOpaque(false);

    button = new MenuButton(title);
    button.setAlignmentX(Component.CENTER_ALIGNMENT);
    button.addActionListener(l);
    button.setActionCommand(LoadRobotView.SET_ROBOT_COMMAND);

    Font font = button.getFont();
    try {
      font = FontLoader.getInstance().load(FontType.HELVETICA_NEUE_LT, Font.PLAIN,
          FontSize.TABLE_BUTTON_SIZE);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    button.setFont(font);
    panel.add(button);

    border = new Border[3];

    // bottom right
    border[0] = BorderFactory.createCompoundBorder();
    border[0] = BorderFactory.createCompoundBorder(border[0],
        BorderFactory.createMatteBorder(0, 0, 1, 1, borderColor));

    // bottom
    border[1] = BorderFactory.createCompoundBorder();
    border[1] = BorderFactory.createCompoundBorder(border[1],
        BorderFactory.createMatteBorder(0, 0, 1, 0, borderColor));

    // right
    border[2] = BorderFactory.createCompoundBorder();
    border[2] = BorderFactory.createCompoundBorder(border[2],
        BorderFactory.createMatteBorder(0, 0, 0, 1, borderColor));
  }

  @Override
  public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected,
      int row, int column) {
    this.value = value;

    button.putClientProperty("Index", row);
    button.putClientProperty("RobotData", (RobotData) this.value);

    Boolean lastColumn = (table.getColumnCount() - 1) == column;
    Boolean lastRow = (table.getRowCount() - 1) == row;

    if (lastColumn) {
      if (!lastRow) {
        panel.setBorder(border[1]);
      } else {
        panel.setBorder(null);
      }
    } else {
      if (!lastRow) {
        panel.setBorder(border[0]);
      } else {
        panel.setBorder(border[2]);
      }
    }

    return panel;
  }

  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
      boolean hasFocus, int row, int column) {
    this.value = value;

    Boolean lastColumn = (table.getColumnCount() - 1) == column;
    Boolean lastRow = (table.getRowCount() - 1) == row;

    if (lastColumn) {
      if (!lastRow) {
        panel.setBorder(border[1]);
      } else {
        panel.setBorder(null);
      }
    } else {
      if (!lastRow) {
        panel.setBorder(border[0]);
      } else {
        panel.setBorder(border[2]);
      }
    }

    return panel;
  }

  @Override
  public Object getCellEditorValue() {
    return value;
  }
}
