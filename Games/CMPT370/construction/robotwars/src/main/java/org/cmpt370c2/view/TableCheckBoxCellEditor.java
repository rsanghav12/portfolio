package org.cmpt370c2.view;

import java.awt.Component;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.Border;

/**
 * A cell renderer to display a checkbox. The checkbox is used
 * to toggle if a team in the same row of the table is a computer or not.
 * @author RutvikSanghavi
 */
@SuppressWarnings("serial")
class TableCheckBoxCellEditor extends DefaultCellEditor {

  private JPanel panel;

  private static JCheckBox checkbox;

  private Border border;

  public TableCheckBoxCellEditor(ActionListener l) {
    super(new JCheckBox());

    panel = new JPanel();

    panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
    panel.setFocusable(false);
    panel.setOpaque(false);

    checkbox = new JCheckBox("Computer");
    checkbox.addActionListener(l);
    checkbox.setActionCommand(SessionConfigurationView.IS_PLAYER_COMPUTER);
    checkbox.setFocusable(false);
    panel.add(checkbox);

    border = BorderFactory.createCompoundBorder();
    border = BorderFactory.createCompoundBorder(border,
        BorderFactory.createMatteBorder(0, 0, 1, 0, Colors.DARK_COMPONENT_COLOR));
  }

  @Override
  public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected,
      int row, int column) {
    checkbox.putClientProperty("RobotTeam", table.getValueAt(row, column + 1));
    checkbox.setSelected((value != null && ((Boolean) value).booleanValue()));

    if ((table.getRowCount() - 1) != row) {
      panel.setBorder(border);
    } else {
      panel.setBorder(null);
    }
    return panel;
  }

  @Override
  public Object getCellEditorValue() {
    return (Boolean) (checkbox.isSelected());
  }
}
