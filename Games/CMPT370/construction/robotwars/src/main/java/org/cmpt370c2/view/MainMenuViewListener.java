package org.cmpt370c2.view;

/**
 * Interface that facilitates the communication between the main menu view and the 
 * main menu controller
 * @author C2
 *
 */
public interface MainMenuViewListener {
  
  /**
   * Starts a new game and opens session configuration 
   */
  public void startGame();
  
  /**
   * Exits RobotWars
   */
  public void exit();
}
