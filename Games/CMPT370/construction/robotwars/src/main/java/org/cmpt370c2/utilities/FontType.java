package org.cmpt370c2.utilities;

/**
 * An enumeration of all fonts that will be used throughout they system
 * and stored in FontLoader
 * @author C2
 *
 */
public enum FontType {
  HELVETICA_NEUE_BOLD("Helvetica Neu Bold.ttf"),
  HELVETICA_NEUE_BLACK_COND("HelveticaNeue BlackCond.ttf"),
  HELVETICA_NEUE_LIGHT("HelveticaNeue Light.ttf"),
  HELVETICA_NEUE_MEDIUM("HelveticaNeue Medium.ttf"),
  HELVETICA_NEUE_THIN("HelveticaNeue Thin.ttf"),
  HELVETICA_NEUE("HelveticaNeue.ttf"),
  HELVETICA_NEUE_BD("HelveticaNeueBd.ttf"),
  HELVETICA_NEUE_HV("HelveticaNeueHv.ttf"),
  HELVETICA_NEUE_IT("HelveticaNeueIt.ttf"),
  HELVETICA_NEUE_LT("HelveticaNeueLt.ttf"),
  HELVETICA_NEUE_MED("HelveticaNeueMed.ttf");
  
  private String fileName;       
 
  String getFileName() {
    return fileName;
  }
  
  private FontType(String fileName) {
      this.fileName = fileName;
  }
}
