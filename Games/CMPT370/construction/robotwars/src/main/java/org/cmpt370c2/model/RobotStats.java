package org.cmpt370c2.model;

/**
 * Stores a robot's summarized statistics for the game
 * @author C2
 */
public class RobotStats {
  
  private int healthLeft;
  private int robotsKilled;
  private int distanceTraveled;
  private RobotType type;
  private TeamColor team;
  
  public int getHealthLeft() {
    return healthLeft;
  }

  public void setHealthLeft(int healthLeft) {
    this.healthLeft = Math.max(0, healthLeft);
  }

  public int getRobotsKilled() {
    return robotsKilled;
  }

  public void setRobotsKilled(int robotsKilled) {
    this.robotsKilled = robotsKilled;
  }

  public int getDistanceTraveled() {
    return distanceTraveled;
  }

  public void setDistanceTraveled(int distanceTraveled) {
    this.distanceTraveled = distanceTraveled;
  }

  public RobotType getType() {
    return type;
  }

  public TeamColor getTeam() {
    return team;
  }
   
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((team == null) ? 0 : team.hashCode());
    result = prime * result + ((type == null) ? 0 : type.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    RobotStats other = (RobotStats) obj;
    if (team != other.team)
      return false;
    if (type != other.type)
      return false;
    return true;
  }
  
  /**
   * Constructs a new Robot Statistics object
   * @param type The type of robot the statistics regard
   * @param team The team of robot the statistics regard
   */
  public RobotStats(RobotType type, TeamColor team) {
    super();
    if (type == null || team == null) {
      throw new IllegalArgumentException("RobotStats must be initialized with a non-null type and team.");
    }
    this.healthLeft = type.getHealth();
    this.robotsKilled = 0;
    this.distanceTraveled = 0;
    this.type = type;
    this.team = team;
  }
}
