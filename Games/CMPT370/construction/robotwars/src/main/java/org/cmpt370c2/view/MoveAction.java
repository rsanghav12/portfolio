package org.cmpt370c2.view;

import java.awt.Point;
import java.awt.event.ActionListener;

/**
 * The move action used to animate robot movement from one point to another on the game board.
 * 
 * @author allankerr
 *
 */
public class MoveAction extends Action {
  
  /**
   * The target location for the sprite to move to.
   */
  private Point location;
  
  /**
   * The difference between the start location and end location of the sprite specifying the
   * distance it travels.
   */
  private Point locationDelta;

  /**
   * The current x and y position of the sprite. JComponents use integer locations so doubles are
   * required to allow for smooth movement which are then round.
   */
  private double currentX;
  private double currentY;
  
  /**
   * The velocity in points per second that the sprite should use to move to its target position.
   */
  private double speed;
  
  private Point getLocationDelta(Point targetLocation, Point location) {
    // Compute the x and y distance between the current and target location
    int x = (int)Math.round(targetLocation.getX() - location.getX());
    int y = (int)Math.round(targetLocation.getY() - location.getY());    
    return new Point(x, y);
  }
  
  private int getDuration(Point targetLocation, Point location, double velocity) {
    
    // Compute the distance being traveled.
    Point locationDelta = getLocationDelta(targetLocation, location);
    double length = Math.sqrt((locationDelta.getX()*locationDelta.getX()) 
        + (locationDelta.getY()*locationDelta.getY()));
 
    // Compute the time required and convert it to ms
    return (int)Math.round(1000*(length/velocity));
  }
   
  /**
   * Construct a new move action for animating robot movement.
   * 
   * @param location The location the sprite is supposed to move to.
   * @param speed The speed in points per second that the sprite should move at.
   * @param listener The listener that is notified when the move animation finishes.
   */
  public MoveAction(Point location, double speed, ActionListener listener) {
    super(0, listener);
    this.location = location;
    this.speed = speed;
  }
  
  @Override
  public void start(ImageSprite sprite) {
    Point current = sprite.getCenter();
    this.currentX = current.getX();
    this.currentY = current.getY();    
    this.locationDelta = getLocationDelta(location, current);
    this.setDuration(getDuration(location, current, speed));
    super.start(sprite);
  }
  
  @Override
  protected void update(long delta) { 
    double travelPercentage = (((double)delta)/this.getDuration());
    this.currentX = currentX + locationDelta.getX() * travelPercentage;
    this.currentY = currentY + locationDelta.getY() * travelPercentage;
    getSprite().setCenter(new Point((int)Math.round(currentX), (int)Math.round(currentY)));      
  }
  
  @Override
  protected void finish() {
    this.getSprite().setCenter(location);
    super.finish();
  }
}
