package org.cmpt370c2.robotlibrarianclient;

import java.util.EventListener;
import java.util.List;

/**
 * Interface allowing for a listener to receive data from the robot librarian. This interface is
 * used to receive downloaded robots, lists of downloaded robots, and whether or not stats updates
 * were successful. The listener pattern is used due to asynchronous network activity.
 * 
 * @author C2
 *
 */
public interface RobotLibrarianClientListener extends EventListener {

  /**
   * Called as a response to the robot librarian client's download robot method. indicating the data
   * was successfully retreived from the robot librarian.
   * 
   * @param robot The data of the robot that was requested.
   */
  public void robotReceived(RobotData robot);

  /**
   * The list of all robots on the robot librarian called when a successful response is received
   * from the client's list operation.
   * 
   * @param robots The list of robots that was received from the robot librarian.
   */
  public void robotListReceived(List<RobotData> robots);

  /**
   * Called when a statistics was successfully sent to the robot librarian.
   * 
   * @param success True if the update succeeded or false if it didn't.
   */
  public void robotUpdated(boolean success);

  /**
   * This is called when a request fails due to being unable to reach the robot librarian or
   * receiving malformed data.
   * 
   * @param error An error message describing the failure.
   */
  public void requestFailed(String error);
}
