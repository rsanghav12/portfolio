package org.cmpt370c2.interpreter.words;

import org.cmpt370c2.interpreter.controller.ScriptDataSource;
import org.cmpt370c2.interpreter.controller.ScriptListener;
import org.cmpt370c2.interpreter.model.ScriptData;
import org.cmpt370c2.interpreter.model.Value;

/**
 * Pops the top-most value from the stack and prints it to standard output.
 * @author allankerr
 *
 */
class PrintWord extends PredefinedWord {

  @Override
  public String getName() {
    return ".";
  }

  /**
   * Pops the top-most value from the stack and prints it to standard output.
   */
  @Override
  public void execute(ScriptData data, ScriptListener listener, ScriptDataSource dataSource) {
    Value<?> val = data.pop();
    System.out.println(val);
  }
}
