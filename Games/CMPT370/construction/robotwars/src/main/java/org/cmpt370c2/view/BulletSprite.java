package org.cmpt370c2.view;

import org.cmpt370c2.utilities.ImageType;

/**
 * This class is used to display the bullet sprite shown when a robot fires at another robot.
 * 
 * @author allankerr
 *
 */
@SuppressWarnings("serial")
class BulletSprite extends ImageSprite {

  private static final String BULLET_FILENAME = "BULLET";

  /**
   * Constructs a new bullet with the specified location and rotation.
   * 
   * @param x The x-coordinate the bullet should be displayed at
   * @param y The y-coordinate the bullet should be displayed at
   * @param rotation The orientation of the bullet
   */
  public BulletSprite(int x, int y, double rotation) {
    super(BULLET_FILENAME, ImageType.PNG, x, y);
    this.setRotation(rotation);
  }
}
