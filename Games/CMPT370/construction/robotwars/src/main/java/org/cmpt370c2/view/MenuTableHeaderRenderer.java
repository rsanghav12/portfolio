package org.cmpt370c2.view;

import java.awt.Component;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import org.cmpt370c2.utilities.FontLoader;
import org.cmpt370c2.utilities.FontType;

/**
 * Cell renderer for table headers. A separate class was created to make it easier to tweak
 * on a later date if needed. Also this eliminates repetition of code.
 * @author RutvikSanghavi
 */
public class MenuTableHeaderRenderer implements TableCellRenderer {
  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
      boolean hasFocus, int row, int column) {
    JLabel component = new JLabel();
    component.setText((String) value);
    component.setOpaque(true);
    component.setBackground(Colors.DARK_COMPONENT_COLOR);
    Font font = component.getFont();
    try {
      font = FontLoader.getInstance().load(FontType.HELVETICA_NEUE_LT, Font.PLAIN,
          FontSize.TABLE_BUTTON_SIZE);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    component.setFont(font);
    component.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
    return component;
  }
}
