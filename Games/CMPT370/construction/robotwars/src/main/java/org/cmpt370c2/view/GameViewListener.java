package org.cmpt370c2.view;

import org.cmpt370c2.model.RobotType;
import org.cmpt370c2.model.TeamColor;
import org.cmpt370c2.utilities.HexagonalPoint;

/**
 * The interface that facilitates communication between the game view and the game controller
 * through which events are passed.
 * 
 * @author C2
 *
 */
public interface GameViewListener {

  /**
   * Notifies the listener that movement input was received.
   */
  public void move();

  /**
   * Notifies the listener that a tile was shot at.
   * 
   * @param tilePoint The tile being shot at
   */
  public void shoot(HexagonalPoint tilePoint);

  /**
   * Notifies the listener that a tile was moused over.
   * 
   * @param tilePoint Tile to be highlighted
   */
  public void highlightTile(HexagonalPoint tilePoint);

  /**
   * Notifies the listener that a robot was moused over.
   * 
   * @param team The team of the robot selected
   * @param robot The type of the robot selected
   */
  public void highlightRobot(TeamColor team, RobotType robot);


  /**
   * Notifies the listener that the moused over robot is no longer moused over.
   */
  public void unhighlightRobot();

  /**
   * Notifies the listener that turn input was received.
   * 
   * @param direction The direction the robot is now facing
   */
  public void turn(int direction);

  /**
   * Notifies the listener that the player's play should start.
   */
  public void startPlay();

  /**
   * Notifies the listener that the player's play should end.
   */
  public void endPlay();

  /**
   * Notifies the listener that the game should end.
   */
  public void endGame();
}
