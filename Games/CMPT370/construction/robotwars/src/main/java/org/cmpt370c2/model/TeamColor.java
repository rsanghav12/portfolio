package org.cmpt370c2.model;

import org.cmpt370c2.view.TileType;

/**
 * There are 6 possible colors for a team to be, and there can only be one team
 * associated with any particular color.
 * @author C2
 */
public enum TeamColor {
  RED("RED", TileType.RED_START),
  GREEN("GREEN", TileType.GREEN_START),
  YELLOW("YELLOW", TileType.YELLOW_START),
  BLUE("BLUE", TileType.BLUE_START),
  ORANGE("ORANGE", TileType.ORANGE_START),
  PURPLE("PURPLE", TileType.PURPLE_START);
  
  private String name;     
  
  private TileType type;
  
  public TileType getType() {
    return type;
  }

  @Override
  public String toString() {
     return name;
  }
  
  private TeamColor(String name, TileType type) {
      this.name = name;
      this.type = type;
  }
}
