package org.cmpt370c2.view;

import java.awt.Color;
import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.TableCellRenderer;

/**
 * The check box that will be used to toggle whether a team is computer controlled or not
 * @author C2
 *
 */
@SuppressWarnings("serial")
class TableCheckBoxCell extends JPanel implements TableCellRenderer {
  
  private Color DEFAULT_PRESSED_COLOR = new Color(170, 170, 170);
  
  private JCheckBox checkbox;
  
  private Border border;
  
  /**
   * Creates a new table checkbox cell
   */
  public TableCheckBoxCell() {
    this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
    this.setFocusable(false);
    this.setOpaque(false);
    
    checkbox = new JCheckBox("Computer");
    checkbox.setFocusable(false);
    this.add(checkbox);
    
    border = BorderFactory.createCompoundBorder();
    border = BorderFactory.createCompoundBorder(border, BorderFactory.createMatteBorder(0,0,1,0, DEFAULT_PRESSED_COLOR));
  }

  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
      boolean hasFocus, int row, int column) {
    checkbox.setSelected((value != null && ((Boolean) value).booleanValue()));
    
    if((table.getRowCount() - 1) != row) {
      this.setBorder(border);
    } else {
      this.setBorder(null);
    }
    
    return this;
  }
}
