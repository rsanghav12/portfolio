package org.cmpt370c2.robotlibrarianclient;

/**
 * An extension of Exception that is thrown when a response is received from the librarian
 * that was not expected
 * @author C2
 *
 */
@SuppressWarnings("serial")
class MalformedResponseException extends Exception {
  
  public MalformedResponseException(String message) {
    super(message);
  }
}
