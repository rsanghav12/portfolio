package org.cmpt370c2.view;

import java.awt.event.ActionListener;

/**
 * This class is used to put delays in between actions. The delay action is necessary because
 * actions are added to a queue when run on a sprite that are then run sequentially.
 * 
 * @author allankerr
 *
 */
class DelayAction extends Action {

  /**
   * Constructs a new delay action with specified delay time.
   * @param duration The duration of the delay in seconds.
   * @param listener The listener than is notified when the timei s up.
   */
  public DelayAction(double duration, ActionListener listener) {
    super((int)Math.round(duration * 1000), listener);
  }

  @Override
  protected void update(long delta) {
  
  }
}
