package org.cmpt370c2.view;

import java.awt.Color;

/**
 * An enumeration of all possible tile types, including team start tiles, whether a tile is in or
 * out of range, and whether or not a tile is visible. These types allow for the display color of
 * tiles in the game board to change.
 * 
 * @author allankerr
 *
 */
public enum TileType {
  RED_START(Color.RED),
  GREEN_START(Color.GREEN),
  BLUE_START(Color.BLUE),
  ORANGE_START(Color.ORANGE),
  YELLOW_START(Color.YELLOW),
  PURPLE_START(new Color(0xB2,0x80,0xB2)),
  VISIBLE(Color.WHITE ),
  HIDDEN(new Color(181,181,181)),
  IN_RANGE(new Color(185,255,192)),
  OUT_OF_RANGE(new Color(222,107,107));
  
  /**
   * The color used to display the tile type when drawn.
   */
  private Color color;    

  public Color getColor() {
    return color;
  }
  
  private TileType(Color color) {
      this.color = color;
  }
}
