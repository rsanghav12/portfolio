package org.cmpt370c2.view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.RenderingHints;

/**
 * The sprite that represents individual hexagonal tiles on the board.
 * 
 * @author allankerr
 *
 */
@SuppressWarnings("serial")
class TileSprite extends Sprite {

  /**
   * The default color used to draw the tile's border when displayed in the board.
   */
  private Color strokeColor = Color.BLACK;

  /**
   * The radius of the hexagon to determine the size of game tiles.
   */
  private int radius;

  /**
   * The width in points the tile border should be drawn using.
   */
  private int strokeWidth;


  /**
   * The current display state of the tile used to change the color the tile is displayed as during
   * gameplay to hide and show tiles.
   */
  private TileType type = TileType.HIDDEN;

  public TileType getType() {
    return type;
  }

  /**
   * Update the display state of the tile causing it to draw using the color of the new type.
   * 
   * @param type The new type the tile should use for its color.
   */
  public void setType(TileType type) {
    this.type = type;
    this.repaint();
  }

  /**
   * Constructs a new hexagonal tile
   * 
   * @param x The tile's starting x-coordinate
   * @param y The tile's starting y-coordinate
   * @param radius The radius the tile should be drawn at.
   * @param strokeWidth The thickness of tile's border when drawing.
   */
  public TileSprite(int x, int y, int radius, int strokeWidth) {
    super(x, y, 2 * radius + strokeWidth, 2 * radius + strokeWidth);
    this.strokeWidth = strokeWidth;
    this.radius = radius;
  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);

    Graphics2D graphics = (Graphics2D) g;
    graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

    Polygon hexagon = new Polygon();
    double angleDelta = Math.toRadians(60);
    double angle = Math.toRadians(30);
    for (int i = 0; i < 6; i++) {
      double x = Math.cos(angle) * radius + radius + (strokeWidth / 2);
      double y = Math.sin(angle) * radius + radius + (strokeWidth / 2);
      hexagon.addPoint((int) Math.round(x), (int) Math.round(y));
      angle += angleDelta;
    }
    if (strokeWidth > 0) {
      graphics.setStroke(new BasicStroke(strokeWidth));
      graphics.setColor(strokeColor);
      graphics.drawPolygon(hexagon);
    }
    graphics.setColor(type.getColor());
    graphics.fillPolygon(hexagon);
  }

  @Override
  public boolean contains(Point p) {
    Point center = this.getCenter();
    double deltaX = center.getX() - p.getX();
    double deltaY = center.getY() - p.getY();
    // Use the half-height rather than the radius to avoid overlap between tiles.
    double halfHeight = Math.sqrt((radius * radius) - ((radius / 2) * (radius / 2)));
    double distance = Math.sqrt((deltaX * deltaX) + (deltaY * deltaY));
    return distance <= halfHeight;
  }
}
