package org.cmpt370c2.controller;

import java.awt.BorderLayout;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.cmpt370c2.view.AbstractView;

/**
 * This class is responsible for display abstract view subclasses by presenting their associated
 * controller. Transitioning and displaying views within a window are handled by this class. It acts
 * as the base on which the controller component is built.
 * 
 * @author allankerr
 *
 */
public class WindowManager {

  /**
   * The ratio used for the window width and height when displaying popup windows. This is used to
   * prevent popup windows from covering the entire fullscreen window.
   */
  private static final float POPUP_WINDOW_RATIO = 2 / 3.0f;

  /**
   * The frame used to display all views in.
   */
  private JFrame frame;

  /**
   * The controller who's view is currently displayed in the window.
   */
  private AbstractController<?> controller;

  public WindowManager() {
    this(true);
  }

  /**
   * Construct a new window manager for displaying abstract view subclasses in using their
   * associated controller.
   * 
   * @param fullscreen True if the window should be maximized or false if it is a popup window.
   */
  public WindowManager(boolean fullscreen) {

	 
    frame = new JFrame("Robot Wars");
    frame.setLayout(new BorderLayout());
    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

    GraphicsEnvironment environment = GraphicsEnvironment.getLocalGraphicsEnvironment();
    Rectangle bounds = environment.getMaximumWindowBounds();
    if (fullscreen) {
      // Display a fullscreen window
      frame.setSize(bounds.width, bounds.height);
    } else {
      // Display a popup window
      int width = (int) (bounds.width * POPUP_WINDOW_RATIO);
      int height = (int) (bounds.height * POPUP_WINDOW_RATIO);
      frame.setSize(width, height);
      frame.setLocationRelativeTo(null);
      frame.setAlwaysOnTop(true);
    }
  }

  /**
   * Display the given controllers' view to the user in the existing window by replacing the
   * currently displayed view.
   * 
   * @param controller The controller whose view should be displayed.
   */
  public <V extends AbstractView<?>> void presentController(AbstractController<V> controller) {

    // Remove the current controller if there is one
    if (this.controller != null) {
      frame.removeWindowListener(this.controller);
      frame.remove(this.controller.getView());
    }
    // Add the new controller
    frame.addWindowListener(controller);
    this.controller = controller;

    // Display the controller's view
    JPanel panel = controller.getView();
    panel.setPreferredSize(frame.getSize());
    frame.add(panel, BorderLayout.CENTER);
    frame.validate();

    if (!frame.isVisible()) {
      frame.setVisible(true);
    }
    panel.setFocusable(true);
    panel.requestFocusInWindow();
  }

  public void close() {
    frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
  }
}
