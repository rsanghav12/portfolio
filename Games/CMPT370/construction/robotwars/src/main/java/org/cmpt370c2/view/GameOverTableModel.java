package org.cmpt370c2.view;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import javax.swing.table.DefaultTableModel;

import org.cmpt370c2.model.RobotStats;

/**
 * Subclass of the default table model. This class defines a custom table model for displaying the
 * end of game robot statistics
 * 
 * @author rutviksanghavi
 *
 */
@SuppressWarnings("serial")
class GameOverTableModel extends DefaultTableModel{

  private static final String HEALTH_LEFT_HEADER = "Health Left";
  private static final String DISTANCE_TRAVELLED_HEADER = "Distance Traveled";
  private static final String ROBOTS_KILLED_HEADER = "Robots Killed";

  /**
   * Used to store the data that should be displayed in each column and row consisting of various
   * types of robot stats.
   */
  private Object[][] data;
  
  /**
   * The robot stats that are being displayed in the table.
   */
  private Collection<RobotStats> stats;
  
  /**
   * Creates a new game over table model instance
   * @param stats The robot statistics from the match that just completed
   */
  public GameOverTableModel(Collection<RobotStats> stats) {
    this.stats = stats;
    initData();
  }
  
  /**
   * Prepares the data to be displayed.
   */
  private void initData() {
    data = new Object[stats.size()][getColumnCount()];
    
    for(int i = 0; i < stats.size(); i++) {
      RobotStats stat = (RobotStats) stats.toArray()[i];
      data[i][0] = new ArrayList<Object>(Arrays.asList(stat.getTeam(), stat.getType()));
      data[i][1] = stat.getHealthLeft();
      data[i][2] = stat.getDistanceTraveled();
      data[i][3] = stat.getRobotsKilled();        
    }
    
    fireTableStructureChanged();
  }
  
  @Override
  public int getRowCount() {
    return (stats == null ? 0 : stats.size());
  }
  
  @Override
  public int getColumnCount() {
    return 4;
  }
  
  @Override
  public String getColumnName(int column) {
    switch (column) {
      case 1:
        return HEALTH_LEFT_HEADER;
      case 2:
        return DISTANCE_TRAVELLED_HEADER;
      case 3:
        return ROBOTS_KILLED_HEADER;
      default:
        return "";
    }
  }

  @Override
  public Object getValueAt(int row, int column) {
    return data[row][column];
  }
  
  @Override
  public Class<?> getColumnClass(int c) {
    return getValueAt(0, c).getClass();
  }
  
  @Override
  public void setValueAt(Object value, int row, int column) {
    data[row][column] = value;
    fireTableCellUpdated(row, column);
  }
  
  @Override
  public boolean isCellEditable(int row, int column) {
    return false;
  }
}
