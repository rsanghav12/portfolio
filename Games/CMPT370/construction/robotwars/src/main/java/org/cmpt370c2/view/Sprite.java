package org.cmpt370c2.view;

import java.awt.Dimension;
import java.awt.Point;

import javax.swing.JComponent;

/**
 * An abstract class designed to be the super class for all sprites in RobotWars
 * @author C2
 *
 */
@SuppressWarnings("serial")
abstract class Sprite extends JComponent {

  /**
   * Constructs a new sprite for display in the game board with the specified size and location.
   * @param x The center of the sprite's starting x-coordinate
   * @param y The center of the sprite's starting y-coordinate
   * @param width The width of the sprite's bounding box.
   * @param height The height of the sprite's bounding box.
   */
  public Sprite(int x, int y, int width, int height) {
    super();
        
    this.setSize(new Dimension(width, height));
    
    this.setAlignmentX(0.5f);
    this.setAlignmentY(0.5f);
    this.setCenter(x, y); 
  }
  
  /**
   * Constructs a new sprite at the origin with the specified size.
   * @param width The width of the sprite's bounding box.
   * @param height The height of the sprite's bounding box.
   */
  public Sprite(int width, int height) {
    this(0, 0, width, height);
  }
  
  /**
   * Updates the sprite's position based around the center of its bounding box.
   * @param x The x position of the sprite in the game board.
   * @param y The y position of the sprite in the game board.
   */
  public void setCenter(int x, int y) {
    this.setLocation(x - (int)Math.round(((getWidth()*getAlignmentX()))), 
        y - (int)Math.round((getHeight()*getAlignmentY())));
  }
  
  @Override
  public void setSize(Dimension d) {
    this.setPreferredSize(d);
    super.setSize(d);
  }
  
  public void setCenter(Point point) {
   this.setCenter((int)Math.round(point.getX()), (int)Math.round(point.getY())); 
  }
  
  public Point getCenter() {
    int x = (int)Math.round(getX() + getWidth()*getAlignmentX());
    int y = (int)Math.round(getY() + getHeight()*getAlignmentY());
    return new Point(x,y);
  }
}
