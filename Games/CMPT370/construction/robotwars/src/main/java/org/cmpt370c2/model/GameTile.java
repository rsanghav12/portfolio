package org.cmpt370c2.model;

import java.util.HashSet;
import java.util.Iterator;

import org.cmpt370c2.view.TileType;

/**
 * Represents an individual game tile in the game board. Can be occupied by many or no robots.
 * 
 * @author C2
 *
 */
public class GameTile implements Cloneable {
  /**
   * A set of all robots currently on tile
   */
  private HashSet<Robot> robots;

  /**
   * Type of tile used be view to represent different tiles on game board
   */
  private TileType type = TileType.VISIBLE;

  public TileType getType() {
    return type;
  }

  public void setType(TileType type) {
    this.type = type;
  }

  public GameTile() {
    this.robots = new HashSet<Robot>();
  }

  public boolean isOccupied() {
    return robots.size() > 0;
  }

  /**
   * Adds a robot to the game tile
   * 
   * @param robot The robot to be added to the game tile
   */
  public void add(Robot robot) {
    if (robot == null) {
      throw new IllegalArgumentException("Cannot add a null robot to the game tile.");
    }
    if (robots.contains(robot)) {
      throw new IllegalArgumentException("Robot has already been added to the game tile.");
    }
    robots.add(robot);
  }

  /**
   * Removes a robot from the game tile
   * 
   * @param robot The robot to be removed from the game tile
   */
  public void remove(Robot robot) {
    if (robot != null) {
      robots.remove(robot);
    }
  }

  public Iterator<Robot> iterator() {
    return robots.iterator();
  }

  @Override
  public GameTile clone() {
    try {
      GameTile clone = (GameTile) super.clone();
      clone.robots = new HashSet<Robot>(robots);
      return clone;
    } catch (CloneNotSupportedException e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }
}
