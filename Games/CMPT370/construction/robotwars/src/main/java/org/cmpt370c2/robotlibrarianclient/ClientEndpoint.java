package org.cmpt370c2.robotlibrarianclient;

import org.json.simple.JSONObject;

/**
 * An interface for sending requests to the RobotLibrarian
 * @author C2
 *
 */
interface ClientEndpoint {
  public void sendRequest(JSONObject obj, ClientEndpointListener listener) ;
}
