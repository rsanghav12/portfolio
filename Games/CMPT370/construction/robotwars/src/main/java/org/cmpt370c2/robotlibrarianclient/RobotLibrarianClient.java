package org.cmpt370c2.robotlibrarianclient;

import org.json.simple.JSONObject;

import java.util.HashMap;
import java.util.Map;

import org.cmpt370c2.robotlibrarianclient.RobotLibrarianClientListener;

/**
 * This class is designed to allow the other system components to interact with the Robot Librarian.
 * It allows for the data to be downloaded for individual robots, the data for all robots to be
 * listed, and updates to be send to the robot librarian when games finish containing robot stats.
 * 
 * @author C2
 */
public class RobotLibrarianClient {

  private static final String LIST_REQUEST = "list-request";
  private static final String BRIEF_REQUEST = "brief";
  private static final String FULL_REQUEST = "full";

  private static final String DATA_KEY = "data";
  private static final String NAME_KEY = "name";
  private static final String TEAM_KEY = "team";

  /**
   * Client listener to send back server responses or error on request
   */
  private RobotLibrarianClientListener clientListener;

  /**
   * ClientEndpoint to request and recieve robot files
   */
  final private ClientEndpoint clientEndpoint;

  public RobotLibrarianClient(RobotLibrarianClientListener listener) {
    this(listener, new ServerClientEndpoint());
  }

  /**
   * Constructor to create an instance of RobotLibrarianClient while assigning parameters to its
   * local variables and throws IllegalArgumentException when either of the parameters are null
   * 
   * @param clientListener Listener to send back server response or error when requested
   * @param clientEndpoint ClientEndpoint to request and receive robot files
   */
  RobotLibrarianClient(RobotLibrarianClientListener clientListener, ClientEndpoint endpoint) {
    if (clientListener == null) {
      throw new IllegalArgumentException("Robot Lib Client Listener cannot be null");
    }
    this.clientListener = clientListener;
    this.clientEndpoint = endpoint;
  }

  /**
   * Creates a JSON formatted request for a list of robots and calls sendRequest(...) to send it to
   * the robot librarian.
   * 
   * @param brief Determines if the request should ask for a list of full or brief robot data.
   */
  public void listRobots(boolean isBrief) {

    JSONObject message = buildListRequest(isBrief, null, null);
    clientEndpoint.sendRequest(message, new ClientEndpointListener() {
      @Override
      public void receivedResponse(JSONObject response) {
        RobotListCoder listDecoder = new RobotListCoder();
        try {
          clientListener.robotListReceived(listDecoder.decode(response));
        } catch (MalformedResponseException e) {
          clientListener.requestFailed(e.getMessage());
        }
      }

      @Override
      public void receivedError(String error) {
        clientListener.requestFailed(error);
      }
    });
  }


  /**
   * Creates a JSON formatted request for a single full data robot and uses sendRequest(...) to send
   * it to the robot librarian.
   * 
   * @param robotName The name of the robot requested.
   */
  public void downloadRobot(String robotName, String teamName) {
    if (robotName == null) {
      throw new IllegalArgumentException("Attempted to download robot with an invalid name.");
    }
    JSONObject message = buildListRequest(false, robotName, teamName);
    clientEndpoint.sendRequest(message, new ClientEndpointListener() {
      @Override
      public void receivedResponse(JSONObject response) {
        RobotCoder robotDataDecoder = new RobotCoder();
        try {
          clientListener.robotReceived(robotDataDecoder.decode(response));
        } catch (MalformedResponseException e) {
          clientListener.requestFailed(e.getMessage());
        }
      }

      @Override
      public void receivedError(String errorMessage) {
        clientListener.requestFailed(errorMessage);
      }
    });
  }

  /**
   * Build a new list request used building requests for downloading individual robots and listing
   * all robots.
   * 
   * @param isBrief Whether or not the response should contain brief or full robot data. Full robot
   *        data includes the robot script.
   * @param robotName The name of the robot. This may be null if the entire list of robots is
   *        desired.
   * @return The JSONObject representing the request.
   */
  private JSONObject buildListRequest(boolean isBrief, String robotName, String teamName) {
    // Create data map to hold robot name and data type
    Map<Object, Object> dataMap = new HashMap<Object, Object>();
    if (robotName != null) {
      dataMap.put(NAME_KEY, robotName);
    }
    String type;
    if (isBrief) {
      type = BRIEF_REQUEST;
    } else {
      type = FULL_REQUEST;
    }
    dataMap.put(DATA_KEY, type);
    dataMap.put(TEAM_KEY, teamName);
    
    // Create request map to hold data map in json
    Map<Object, Object> requestMap = new HashMap<Object, Object>();
    requestMap.put(LIST_REQUEST, new JSONObject(dataMap));
    return new JSONObject(requestMap);
  }

  /**
   * Converts a RobotData to a JSON and sends it to the robot librarian with the "update" tag. The
   * received response for key "update-is" will be "ok" or "not ok" which determines what will be
   * called back with the listener.
   * 
   * @param robot The RobotData containing the robot stats to be updated.
   * @throws IllegalArgumentException
   * @throws MalformedResponseException
   * @throws RequestTimeoutException
   */
  public void updateRobot(RobotData robotData) {
    if (robotData == null) {
      throw new IllegalArgumentException("Robot sent to update was null");
    }
    RobotCoder robotDataCoder = new RobotCoder();
        
    clientEndpoint.sendRequest(robotDataCoder.encode(robotData), new ClientEndpointListener() {
      @Override
      public void receivedResponse(JSONObject obj) {
        if(obj != null) {
          UpdateCoder coder = new UpdateCoder();
          try {
            clientListener.robotUpdated(coder.decode(obj));
          } catch (MalformedResponseException ex) {
            clientListener.requestFailed(ex.getMessage());
          }
        }
      }

      @Override
      public void receivedError(String errorMessage) {
        clientListener.requestFailed(errorMessage);
      }
    });
  }
}
