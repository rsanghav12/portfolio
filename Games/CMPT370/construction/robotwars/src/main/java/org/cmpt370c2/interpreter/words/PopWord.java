package org.cmpt370c2.interpreter.words;

import org.cmpt370c2.interpreter.controller.ScriptDataSource;
import org.cmpt370c2.interpreter.controller.ScriptListener;
import org.cmpt370c2.interpreter.model.ScriptData;

/**
 * Removes the top-most value from the stack.
 * 
 * @author allankerr
 *
 */
class PopWord extends PredefinedWord {

  @Override
  public String getName() {
    return "pop";
  }

  /**
   * Removes the top-most value from the stack.
   */
  @Override
  public void execute(ScriptData data, ScriptListener listener, ScriptDataSource dataSource) {
    data.pop();
  }
}
