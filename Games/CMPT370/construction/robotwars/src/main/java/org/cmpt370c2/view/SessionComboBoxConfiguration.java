package org.cmpt370c2.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.RenderingHints;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.plaf.basic.BasicComboBoxEditor;

import org.cmpt370c2.utilities.FontLoader;
import org.cmpt370c2.utilities.FontType;

/**
 * This class will be used to configure the session jcombobox graphically
 * such as customizing selected option, or its selection cells
 * @author rutviksanghavi
 */
class SessionComboBoxConfiguration extends BasicComboBoxEditor implements ListCellRenderer<String> {

  /**
   * Default label for the editor when nothing is selected
   */
  private static final String PLAYER_NUM_TEXT = "Number of players: ";
  
  /**
   * Main panel and Main label utilized by combobox editor to display selected option
   */
  private JPanel mainPanel;
  private JLabel mainLabel;
  
  /**
   * Cell panel and cell label utilized by ListCellRenderer to display cells
   */
  private JPanel cellPanel;
  private JLabel cellLabel;
  
  /**
   * Selected item utilized by the editor
   */
  private String selectedItem;
  
  @SuppressWarnings("serial")
  public SessionComboBoxConfiguration() {
    // init mainpanel and mainlabel
    this.mainPanel = new JPanel();
    this.mainLabel = new JLabel() {
      @Override
      public void paintComponent(Graphics g) {
        Graphics2D graphics = (Graphics2D) g;
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setColor(Colors.BACKGROUND_COLOR);
        g.fillRoundRect(0, 0, this.getWidth(), this.getHeight(), Dimensions.MENU_BUTTON_CORNER_RADIUS,
            Dimensions.MENU_BUTTON_CORNER_RADIUS);
        super.paintComponent(g);
      }
    };
    
    // set layout for mainpanel and default configuration
    this.mainPanel.setLayout(new GridBagLayout());
    this.mainPanel.setOpaque(false);
    
    // create constraints for mainlabel within mainpanel
    GridBagConstraints constraints = new GridBagConstraints();
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.weightx = 1.0;
    constraints.insets = new Insets(2, 5, 2, 2);
    
    // default configuration for mainlabel
    this.mainLabel.setOpaque(false);
    this.mainLabel.setHorizontalAlignment(JLabel.CENTER);
    this.mainLabel.setForeground(Color.BLACK);
        
    // add mainlabel to mainpanel with constraints
    this.mainPanel.add(this.mainLabel, constraints);
    
    // init cellpanel and celllabel
    this.cellPanel = new JPanel();
    this.cellLabel = new JLabel();
    
    // default configuration for cellpanel and set layout
    this.cellPanel.setLayout(new GridBagLayout());
    this.cellPanel.setBackground(Colors.BACKGROUND_COLOR);
    
    // default configuration for celllabel
    this.cellLabel.setHorizontalAlignment(JLabel.LEFT);
    this.cellLabel.setOpaque(true);
    
    // change the constraints inset for celllabel
    constraints.insets = new Insets(2, 2, 2, 2);
    
    // add celllabel to cellpanel with constraints
    this.cellPanel.add(this.cellLabel, constraints);
    
    // load the font for mainlabel and celllabel
    Font font = this.mainLabel.getFont();
    try {
      font = FontLoader.getInstance().load(FontType.HELVETICA_NEUE_LT, Font.PLAIN,
          FontSize.TABLE_TEXT_SIZE);
    } catch (Exception e) {
      e.printStackTrace();
    }
    this.mainLabel.setFont(font);
    this.cellLabel.setFont(font);
  }

  // BasicComboBoxEditor functions
  @Override
  public Component getEditorComponent() {
    return mainPanel;
  }
  
  @Override
  public Object getItem() {
    return selectedItem;
  }
  
  @Override
  public void setItem(Object item) {
    selectedItem = (String) item;
    mainLabel.setText(PLAYER_NUM_TEXT + (String) item);
  }
  
  // ListCellRenderer<String> functions
  @Override @SuppressWarnings("rawtypes")
  public Component getListCellRendererComponent(JList list, String value, int index,
      boolean isSelected, boolean cellHasFocus) {
    cellLabel.setText(value);

    if (isSelected || cellHasFocus) {
      cellLabel.setBackground(Colors.DARK_COMPONENT_COLOR);
    } else {
      cellLabel.setBackground(Colors.BACKGROUND_COLOR);
    }
    
    return cellPanel;
  }
}
