package org.cmpt370c2.view;

/**
 * Interface that facilitates communication between the game over view and game over
 * controller
 * @author C2
 *
 */
public interface GameOverViewListener {
  /**
   * Ends the game when selected by user
   */
  public void finishGame();
  /**
   * Starts a new match with the same configuration as the one that just completed
   */
  public void rematch();
}
