package org.cmpt370c2.model;

import java.util.HashMap;
import java.util.Map;


/**
 * Allows user to configure upcoming game, setting number of players, play duration, and
 * whether the robots are human or computer controlled. Implements the validatable
 * interface to ensure correctness of data.
 * @author C2
 *
 */
public class SessionConfiguration implements Validatable {
  
  private static final int MAX_PLAY_DURATION = 120;
  private static final int MIN_PLAY_DURATION = 10;
  
  private Map<TeamColor, TeamConfiguration> teams;
  private int playDuration = (MIN_PLAY_DURATION + MAX_PLAY_DURATION) / 2;
  private int numberOfPlayers = 2;
  
  /**
   * Creates a new session configuration, preparing details for each team to be set.
   */
  public SessionConfiguration() {    
    teams = new HashMap<TeamColor, TeamConfiguration>();
    for (TeamColor team : TeamColor.values()) {
      teams.put(team, new TeamConfiguration());
    }
  }
  
  public TeamConfiguration getTeamConfiguration(TeamColor color){
    return teams.get(color);
  }
  
  public void setNumberOfPlayers(int num){
    if ( (num == 2) || (num == 3) || (num == 6) ){
      numberOfPlayers = num;
    }else{
      throw new IllegalArgumentException("Number of players must be 2, 3, or 6");
    }
  }
  
  public int getNumberOfPlayers(){
    return this.numberOfPlayers;
  }

  public void setPlayDuration(int duration){
    if (( MIN_PLAY_DURATION > 10) || (duration < MAX_PLAY_DURATION) ){
      playDuration = duration;
    }else{
      throw new IllegalArgumentException("Play duration must be between " 
          + MIN_PLAY_DURATION + " and " + MAX_PLAY_DURATION);
    }
  }
  
  public int getPlayDuration(){
    return playDuration;
  }
  
  public int getBoardSize() {
    switch (numberOfPlayers) {
      case 2:
        return 5;
      case 3:
        return 5;
      case 6:
        return 7;
      default:
        throw new IllegalStateException("Cannot determine board size for " + numberOfPlayers + ".");
    }
  }
  
  /**
   * Checks correctness of all team configuration settings
   * @return true if all settings are valid, false otherwise.
   */
  public boolean validate() {
    for (TeamConfiguration team : teams.values()) {
      if (!team.validate()) {
        return false;
      }
    }
    return true;
  }
}
