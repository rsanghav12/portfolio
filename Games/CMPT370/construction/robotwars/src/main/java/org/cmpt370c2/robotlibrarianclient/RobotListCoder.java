package org.cmpt370c2.robotlibrarianclient;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * This class converts data from a list of robots in a JSONObject to a list of RobotData.
 * RobotListCoder inherits from JSONCoder.
 */
class RobotListCoder extends JSONCoder<List<RobotData>> {

  private static final String LIST_KEY = "list";
  
  /**
   * Not used by RobotLibrarianClient and unsupported.
   * 
   * @throws UnsupportedOperationException Thrown when this method is called since it is unsupported
   *         by RobotListCoder.
   * @author C2
   */
  @Override
  protected JSONObject encode(List<RobotData> robotList) throws UnsupportedOperationException {
    throw new UnsupportedOperationException();
  }

  /**
   * Converts a JSONObject containing an array of robot scripts into a list of RobotData.
   * 
   * @param obj The JSONObject to be converted to a RobotData object.
   * @return The list of robot stats and scripts for each robot.
   */
  @Override
  protected List<RobotData> decode(JSONObject obj) throws IllegalArgumentException, MalformedResponseException {
    List<RobotData> robotDataList = new ArrayList<RobotData>();
    RobotCoder decoder = new RobotCoder();
    JSONArray robot = (JSONArray) obj.get(LIST_KEY);

    if(robot != null) {
      // Used generic and cast the returns of next() so there is no type safety warning.
      Iterator<?> iterator = robot.iterator();
      while (iterator.hasNext()) {
        JSONObject jsonObject = (JSONObject) iterator.next();
        robotDataList.add(decoder.decode(jsonObject));
      }
    } else {
      throw new MalformedResponseException("Failed to decode list of returned robots due to the list being null");
    }

    return robotDataList;
  }
}
