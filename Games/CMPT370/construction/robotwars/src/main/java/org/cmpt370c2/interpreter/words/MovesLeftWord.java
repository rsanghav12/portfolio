package org.cmpt370c2.interpreter.words;

import org.cmpt370c2.interpreter.controller.ScriptDataSource;
import org.cmpt370c2.interpreter.controller.ScriptListener;
import org.cmpt370c2.interpreter.model.IntValue;
import org.cmpt370c2.interpreter.model.ScriptData;

/**
 * The predefined word for getting the number of moves the robot the script is being executed for
 * has remaining.
 * 
 * @author allankerr
 *
 */
public class MovesLeftWord extends PredefinedWord {

  @Override
  public String getName() {
    return "movesLeft";
  }

  /**
   * Asks the data source for the number of moves the robot the script is being executed has
   * remaining and pushes this value to the stack.
   *
   */
  @Override
  public void execute(ScriptData data, ScriptListener listener, ScriptDataSource dataSource) {
    data.push(new IntValue(dataSource.movesLeft()));
  }
}
