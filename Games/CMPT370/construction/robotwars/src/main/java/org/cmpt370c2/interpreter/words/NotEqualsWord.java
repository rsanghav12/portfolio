package org.cmpt370c2.interpreter.words;

import org.cmpt370c2.interpreter.controller.ScriptDataSource;
import org.cmpt370c2.interpreter.controller.ScriptListener;
import org.cmpt370c2.interpreter.model.BoolValue;
import org.cmpt370c2.interpreter.model.ScriptData;
import org.cmpt370c2.interpreter.model.Value;

/**
 * Pops the two top-most values from the stack and pushes true to the top of the stack if they
 * aren't equal to each other.
 * 
 * @author allankerr
 *
 */
class NotEqualsWord extends PredefinedWord {

  @Override
  public String getName() {
    return "<>";
  }

  /**
   * Pops the two top-most values from the stack and pushes true to the top of the stack if they
   * aren't equal to each other. If they are equal, false is pushed.
   *
   */
  @Override
  public void execute(ScriptData data, ScriptListener listener, ScriptDataSource dataSource) {
    Value<?> val1 = data.pop();
    Value<?> val2 = data.pop();
    BoolValue result = new BoolValue(!val1.getValue().equals(val2.getValue()));
    data.push(result);
  }
}
