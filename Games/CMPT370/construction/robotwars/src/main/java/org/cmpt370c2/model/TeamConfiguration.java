package org.cmpt370c2.model;

import java.util.HashMap;
import java.util.Map;

import org.cmpt370c2.interpreter.controller.ScriptController;
import org.cmpt370c2.interpreter.model.ScriptData;

/**
 * A team configuration contains the team color and any scripts needed to run the robots if they are
 * computer controlled. Implements the validatable interface to insure correctness of data.
 * 
 * @author allankerr
 *
 */
public class TeamConfiguration implements Validatable {

  /**
   * The file name to be used automatically for computer players if no script is provided. This file
   * must be present in the resources directory.
   */
  private static final String DEFAULT_SCRIPT_FILENAME = "DEFAULT";
  
  /**
   * The container storing a FORTH script for each robot type
   */
  private Map<RobotType, ScriptData> scripts;
  
  
  /**
   * Specifies whether or not this player is controlled by a script or by a human
   */
  private boolean computer;

  /**
   * Constructs new TeamConfiguration, defaults to team being human controlled
   */
  public TeamConfiguration() {
    this.scripts = new HashMap<RobotType, ScriptData>();
    try {
      ScriptController scriptController = new ScriptController();
      for (RobotType type : RobotType.values()) {
        ScriptData data = scriptController.build(DEFAULT_SCRIPT_FILENAME);
        scripts.put(type, data);
      }
    } catch (Exception e) {
      e.printStackTrace();
    } 
    this.computer = false;
  }

  /**
   * Sets the script for the robot to execute when computer controller.
   * Once a script is set it cannot be unset. A set script will be ignored
   * if the team configuration is changed to non-computer.
   * @param type The robot type the script should control.
   * @param data The data required to run the script.
   */
  public void setScript(RobotType type, ScriptData data) {
    if (type == null) {
      throw new IllegalArgumentException("Attempted to set the script for a null robot type.");
    }
    if (data == null) {
      throw new IllegalArgumentException("Attempted to assign a robot a null script.");
    }
    scripts.put(type, data);
  }

  public ScriptData getScript(RobotType type) {
    if (!scripts.containsKey(type)) {
      throw new IllegalStateException("Attempted to get the script for robot that doesn't have one.");
    }
    if (!isComputer()) {
      throw new IllegalStateException("Attempted to get the script for a robot that is human controller.");
    }
    return scripts.get(type);
  }

  public void setComputer(boolean isComputer) {
    this.computer = isComputer;
  }

  public boolean isComputer() {
    return computer;
  }

  public boolean validate() {
    return !isComputer() || ((scripts.get(RobotType.SCOUT) != null)
        && (scripts.get(RobotType.SNIPER) != null) && (scripts.get(RobotType.TANK) != null));
  }
}
