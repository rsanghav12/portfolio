package org.cmpt370c2.view;

import java.awt.Component;
import java.awt.event.ActionListener;

import javax.swing.AbstractCellEditor;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import org.cmpt370c2.model.RobotType;
import org.cmpt370c2.model.TeamColor;

/**
 * Table cell to show 3 buttons, each with an assigned robot type. 
 * When clicked this button will initiate the laod script view for that robot type
 * for that team.
 * @author RutvikSanghavi
 */
@SuppressWarnings("serial")
class TableLoadScriptCell extends AbstractCellEditor implements TableCellRenderer, TableCellEditor {

  private JPanel mainView;
  private JScrollPane mainScrollPane;

  private Border border;

  private MenuButton buttons[];

  private TeamColor team;

  public TableLoadScriptCell(ActionListener l) {
    mainView = new JPanel();
    mainView.setLayout(new BoxLayout(mainView, BoxLayout.Y_AXIS));
    mainView.setOpaque(false);

    border = BorderFactory.createCompoundBorder();
    border = BorderFactory.createCompoundBorder(border,
        BorderFactory.createMatteBorder(0, 0, 1, 0, Colors.DARK_COMPONENT_COLOR));

    mainScrollPane = new JScrollPane(mainView);
    mainScrollPane.setOpaque(false);
    mainScrollPane.getViewport().setOpaque(false);
    mainView.setAutoscrolls(true);

    // dynamic adding of the buttons to make robust if more types were added in the future
    int i = 0;
    RobotType[] types = RobotType.values();
    buttons = new MenuButton[types.length];
    for (final RobotType type : types) {
      buttons[i] = new MenuButton(type.toString());
      buttons[i].addActionListener(l);
      buttons[i].setActionCommand(SessionConfigurationView.LOAD_SCRIPT_COMMAND);
      buttons[i].putClientProperty("RobotType", type);
      mainView.add(buttons[i]);
      i++;
    }
  }

  @Override
  public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected,
      int row, int column) {
    team = (TeamColor) value;

    for (MenuButton b : buttons) {
      b.putClientProperty("RobotTeam", team);
      b.setBackground(team.getType().getColor());
    }

    if ((table.getRowCount() - 1) != row) {
      mainScrollPane.setBorder(border);
    } else {
      mainScrollPane.setBorder(null);
    }

    return mainScrollPane;
  }

  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
      boolean hasFocus, int row, int column) {
    team = (TeamColor) value;

    for (MenuButton b : buttons) {
      b.setBackground(team.getType().getColor());
    }

    if ((table.getRowCount() - 1) != row) {
      mainScrollPane.setBorder(border);
    } else {
      mainScrollPane.setBorder(null);
    }

    return mainScrollPane;
  }

  @Override
  public Object getCellEditorValue() {
    return team;
  }
}
