package org.cmpt370c2.model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * A team of robots of a given color. Each team consists of a Scout, Tank, and Sniper. Teams can be
 * made up of either human or computer controlled robots.
 * 
 * @author kentwalters
 *
 */
public class RobotTeam {

  private Map<RobotType, Robot> robots;

  private int currentMoves;

  /**
   * Creates a new robot team. Teams can be human controlled or computer controlled with scripts.
   * All robots on the team are constructed depending on if the team is computer controller or not.
   * 
   * @param team The team color of the robot team being created
   * @param configuration The team configuration, including any scripts necessary for robots
   */
  public RobotTeam(TeamColor team, TeamConfiguration configuration) {
    this.robots = new HashMap<RobotType, Robot>();
    for (RobotType type : RobotType.values()) {
      Robot robot;
      if (configuration.isComputer()) {
        robot = new Robot(team, type, configuration.getScript(type));
      } else {
        robot = new Robot(team, type);
      }
      this.robots.put(type, robot);
    }
    this.startTurn();
  }
  
  public void startTurn() {
    currentMoves = Integer.MAX_VALUE;
  }

  /**
   * Returns the robot whose turn is next on the team
   * 
   * @return The robot to play next
   */
  public Robot nextRobot() {
    int newMoves = 0;
    Robot newMovesRobot = null;
    for (Robot robot : robots.values()) {
      if (!robot.isDead()) {
        if ((robot.moves() < currentMoves) && (robot.moves() > newMoves)) {
          newMoves = robot.moves();
          newMovesRobot = robot;
        }
      }
    }
    currentMoves = newMoves;
    return newMovesRobot;
  }

  public Robot getRobot(RobotType type) {
    return robots.get(type);
  }

  public boolean hasRemainingRobots() {
    for (Robot bot : robots.values()) {
      if (!bot.isDead()) {
        return true;
      }
    }
    return false;
  }
  
  public int getNumberOfRemainingBots(){
    int botsAlive = 0;
    for (Robot bot : robots.values()) {
      if (!bot.isDead()) {
        botsAlive++;
      }
    }
    return botsAlive;
  }

  public Iterator<Robot> iterator() {
    return robots.values().iterator();
  }
}
