package org.cmpt370c2.view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import org.cmpt370c2.utilities.FontLoader;
import org.cmpt370c2.utilities.FontType;

final class MenuComponentBuilder {

  /**
   * Builds a new center aligned menu button using the standard menu button color scheme and
   * dimensions.
   * 
   * @param text The text displayed in the menu button.
   * @param command The command sent to the action listener when clicked.
   * @return The newly constructed menu button.
   */
  public static MenuButton buildMenuButton(String text, String command) {
    return buildMenuButton(text, command, false);
  }

  /**
   * Builds a new center aligned menu button using the standard menu button color scheme and
   * dimensions.
   * 
   * @param text The text displayed in the menu button.
   * @param command The command sent to the action listener when clicked.
   * @param isDark True if the button should be created using the secondary color scheme for buttons
   *        on dark backgrounds.
   * @return The newly constructed menu button.
   */
  public static MenuButton buildMenuButton(String text, String command, boolean isDark) {
    Dimension preferredSize =
        new Dimension(Dimensions.MENU_BUTTON_WIDTH, Dimensions.MENU_BUTTON_HEIGHT);
    MenuButton button = buildDetailButton(text, command, isDark);
    button.setPreferredSize(preferredSize);
    button.setMaximumSize(preferredSize);
    return button;
  }

  /**
   * Builds a new center aligned detail menu button using the standard menu button color scheme with
   * the minimize size necessary to fit the button's text.
   * 
   * @param text The text displayed in the menu button.
   * @param command The command sent to the action listener when clicked.
   * @return The newly constructed menu button.
   */
  public static MenuButton buildDetailButton(String text, String command) {
    return buildDetailButton(text, command, false);
  }

  /**
   * Builds a new center aligned detail menu button using the standard menu button color scheme with
   * the minimize size necessary to fit the button's text.
   * 
   * @param text The text displayed in the menu button.
   * @param command The command sent to the action listener when clicked.
   * @param isDark True if the button should be created using the secondary color scheme for buttons
   * @return The newly constructed menu button.
   */
  public static MenuButton buildDetailButton(String text, String command, boolean isDark) {
    MenuButton button = new MenuButton(text);
    button.setAlignmentX(Component.CENTER_ALIGNMENT);
    button.setActionCommand(command);
    button.setFocusable(false);
    if (isDark) {
      button.setBackground(Colors.DARK_COMPONENT_COLOR);
      button.setPressedColor(Colors.BACKGROUND_COLOR);
    }
    return button;
  }

  /**
   * Builds a new center aligned title label with the standard title font.
   * 
   * @param text The text the label should contain.
   * @return The newly constructed title label.
   */
  public static JLabel buildTitleLabel(String text) {
    return buildLabel(text, FontType.HELVETICA_NEUE_THIN, Font.PLAIN, FontSize.TITLE_SIZE);
  }

  /**
   * Builds a new center aligned title label with the standard subtitle font.
   * 
   * @param text The text the label should contain.
   * @return The newly constructed subtitle label.
   */
  public static JLabel buildSubtitleLabel(String text) {
    return buildLabel(text, FontType.HELVETICA_NEUE_THIN, Font.PLAIN, FontSize.SUBTITLE_SIZE);
  }

  /**
   * Builds a new center aligned detail label with the standard detail font used for displaying
   * information in menu views.
   * 
   * @param text The text the label should contain.
   * @return The newly constructed detail label.
   */
  public static JLabel buildDetailLabel(String text) {
    return buildLabel(text, FontType.HELVETICA_NEUE_LT, Font.PLAIN, FontSize.TABLE_TEXT_SIZE);
  }

  /**
   * Builds a new center aligned label with the standard font used for displayed texts or detailed information
   * in menu views.
   * 
   * @param text The text the label should contain.
   * @param type The type of font for the label.
   * @param style The style of fonts for the label.
   * @param fontSize The size of font for the label.
   * @return The newly constructed label.
   */
  private static JLabel buildLabel(String text, FontType type, int style, int fontSize) {
    JLabel label = new JLabel(text, SwingConstants.CENTER);
    try {
      label.setFont(FontLoader.getInstance().load(type, style, fontSize));
    } catch (Exception e) {
      e.printStackTrace();
    }
    label.setAlignmentX(Component.CENTER_ALIGNMENT);
    return label;
  }
}
