package org.cmpt370c2.robotlibrarianclient;

import org.json.simple.JSONObject;

/**
 * An abstract class containing two abstract methods with two subclasses:
 * RobotListCoder and RobotCoder.
 * 
 * @param <E>
 * @author C2
 */
abstract class JSONCoder<E> {

  /**
   * Abstract method for encoding JSONObjects.
   * 
   * @param entity The data structure to convert to JSONObject.
   * @return converted data
   */
  protected abstract JSONObject encode(E entity);
  
  /**
   * Abstract method for decoding JSONObjects
   * 
   * @param obj The JSONObject to be decoded.
   * @return decoded data
   */
  protected abstract E decode(JSONObject obj) throws MalformedResponseException;
}
