package org.cmpt370c2.view;

/**
 * This class contains dimensions used throughout the view. They are located in a single file to
 * make tweaking easy.
 * 
 * @author allankerr
 *
 */
final class Dimensions {
  
  // Menu button dimensions
  
  static final int MENU_BUTTON_WIDTH = 300;
  
  static final int MENU_BUTTON_HEIGHT = 44;

  static final int MENU_BUTTON_PADDING = 15;
  
  static final int MENU_BUTTON_CORNER_RADIUS = 15;
  
  // Side panel Dimensions
  
  static final int PANEL_MARGINS = 10;
  
  static final int PANEL_WIDTH = 250;
  
  // Table dimensions
  
  static final int TABLE_WIDTH = 525;

  static final int TABLE_HEIGHT = 300;

  static final int TABLE_CORNER_RADIUS = 25;
  
  // Board tile dimensions
  
  static final int TILE_RADIUS = 40;
  
  static final int TILE_BORDER = 4;
}
