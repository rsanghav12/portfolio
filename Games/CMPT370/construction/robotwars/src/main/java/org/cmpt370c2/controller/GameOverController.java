package org.cmpt370c2.controller;

import java.util.Collection;
import java.util.List;

import org.cmpt370c2.interpreter.model.ScriptData;
import org.cmpt370c2.model.RobotStats;
import org.cmpt370c2.model.SessionConfiguration;
import org.cmpt370c2.model.TeamConfiguration;
import org.cmpt370c2.robotlibrarianclient.RobotData;
import org.cmpt370c2.robotlibrarianclient.RobotData.RobotDataBuilder;
import org.cmpt370c2.robotlibrarianclient.RobotLibrarianClient;
import org.cmpt370c2.robotlibrarianclient.RobotLibrarianClientListener;
import org.cmpt370c2.view.GameOverView;
import org.cmpt370c2.view.GameOverViewListener;

/**
 * Controls the display of game statistics when the game ends.
 * 
 * @author C2
 */
public class GameOverController extends AbstractController<GameOverView>
    implements RobotLibrarianClientListener, GameOverViewListener {

  /**
   * Client for the robot librarian to update robots
   */
  private RobotLibrarianClient robotLibClient;

  /**
   * Variable holds general configuration of the game
   */
  private SessionConfiguration configuration;

  /**
   * Collection of robot stats used to update robots, and display game stats to the user
   */
  private Collection<RobotStats> robotStats;

  /**
   * Creates a new game over controller instance
   * 
   * @param windowManager The window manager the controller needs to communicate with
   * @param configuration User configuration created during session configuration
   * @param stats A collection of statistics for all robots that played during the match
   */
  public GameOverController(WindowManager windowManager, SessionConfiguration configuration,
      Collection<RobotStats> stats) {
    super(windowManager);
    this.configuration = configuration;
    this.robotLibClient = new RobotLibrarianClient(this);
    this.robotStats = stats;
    sendUpdate();
  }

  /**
   * Update the robots in robot librarian
   */
  private void sendUpdate() {
    for (RobotStats stat : robotStats) {
      TeamConfiguration teamConfig = configuration.getTeamConfiguration(stat.getTeam());
      if (teamConfig.isComputer()) {
        ScriptData script = teamConfig.getScript(stat.getType());
        if (!script.isDefault()) {
          long wins = 0;
          long losses = 0;
          long lived = 0;
          long died = 0;

          if (stat.getHealthLeft() > 0) {
            wins += 1;
            lived += 1;
          } else {
            losses += 1;
            died += 1;
          }

          long absorbed = stat.getType().getHealth() - stat.getHealthLeft();
          long killed = stat.getRobotsKilled();
          long moved = stat.getDistanceTraveled();

          RobotDataBuilder builder =
              new RobotDataBuilder(script.getTeamName(), script.getScriptName());
          RobotData data = builder
              .type(stat.getType())
              .wins(wins)
              .losses(losses)
              .lived(lived)
              .died(died)
              .absorbed(absorbed)
              .killed(killed)
              .moved(moved)
              .build();
          robotLibClient.updateRobot(data);
        }
      }
    }
  }

  @Override
  protected GameOverView createView() {
    return new GameOverView(robotStats, this);
  }

  @Override
  public void robotUpdated(boolean success) {
    if (!success) {
      System.out.println("Robot update failed");
    }
  }

  @Override
  public void finishGame() {
    WindowManager windowManager = this.getWindowManager();
    windowManager.presentController(new MainMenuController(windowManager));
  }

  @Override
  public void rematch() {
    WindowManager windowManager = this.getWindowManager();
    windowManager.presentController(new GameController(windowManager, configuration));
  }

  @Override
  public void robotReceived(RobotData robot) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void robotListReceived(List<RobotData> robots) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void requestFailed(String error) {
    System.out.println(error);
  }
}