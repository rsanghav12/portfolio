package org.cmpt370c2.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;

import org.cmpt370c2.model.RobotStats;
import org.cmpt370c2.utilities.FontLoader;
import org.cmpt370c2.utilities.FontType;

/**
 * The game over view part of the view component. It is responsible for displaying the statistics of
 * the game that just ended.
 * 
 * @author rutviksanghavi
 */
@SuppressWarnings("serial")
public class GameOverView extends AbstractView<GameOverViewListener> implements ActionListener{
  
  /**
   * The commands for events that are sent to the game over view listener.
   */
  private static final String REMATCH_COMMAND = "start game";
  private static final String FINISH_GAME_COMMAND = "finish";

  private static final String REMATCH_TEXT = "Rematch";
  private static final String FINISH_TEXT = "Finish";
  private static final String GAME_OVER_TEXT = "Game Over";

  /**
   * Creates a new game over view instance and displays the statistics.
   * 
   * @param stats The statistics from the completed game to be displayed in the table.
   * @param listener The listener that the view sends events to.
   */
  public GameOverView(Collection<RobotStats> stats, GameOverViewListener listener) {
    super(listener);
    
    assert stats.size() >= 6 || stats.size() <= 18;

    this.setOpaque(true);
    this.setBackground(Colors.BACKGROUND_COLOR);
    this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

    this.add(Box.createVerticalGlue());

    // Add the Game Over title
    JLabel title = new JLabel(GAME_OVER_TEXT);
    title.setAlignmentX(Component.CENTER_ALIGNMENT);
    try {
      title.setFont(FontLoader.getInstance().load(FontType.HELVETICA_NEUE_THIN, Font.PLAIN, FontSize.TITLE_SIZE));
    } catch (Exception e) {
      e.printStackTrace();
    }
    this.add(title);

    this.add(Box.createVerticalGlue());
    
    // Add the game statistics table.
    MenuTableView gameOverTable =  new MenuTableView(new GameOverTableModel(stats));
    gameOverTable.getTable().setRowHeight(50);
    gameOverTable.getTable().setDefaultRenderer(Integer.class, new TableTextCell());
    gameOverTable.getTable().setDefaultRenderer(ArrayList.class, new TableRobotSpriteCell());
    gameOverTable.getTable().getTableHeader().setDefaultRenderer(new MenuTableHeaderRenderer());
    gameOverTable.setBackground(Color.WHITE);
    this.add(gameOverTable);
    
    this.add(Box.createVerticalGlue());

    // Add the rematch button
    MenuButton rematch = MenuComponentBuilder.buildMenuButton(REMATCH_TEXT, REMATCH_COMMAND, true);
    rematch.addActionListener(this);
    this.add(rematch);

    this.add(Box.createVerticalStrut(15));

    // Add the exit button
    MenuButton exit = MenuComponentBuilder.buildMenuButton(FINISH_TEXT, FINISH_GAME_COMMAND, true);
    exit.addActionListener(this);
    this.add(exit);

    this.add(Box.createVerticalGlue());
  }
  
  /**
   * Receives action and performs appropriate task
   * 
   * @param e The action received
   */
  @Override
  public void actionPerformed(ActionEvent e) {
    if (e.getActionCommand() == REMATCH_COMMAND) {
      this.getListener().rematch();
    } else if (e.getActionCommand() == FINISH_GAME_COMMAND) {
      this.getListener().finishGame();
    }
  }
}
