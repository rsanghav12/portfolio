package org.cmpt370c2.interpreter.words;

import org.cmpt370c2.interpreter.controller.ScriptDataSource;
import org.cmpt370c2.interpreter.controller.ScriptListener;
import org.cmpt370c2.interpreter.model.ScriptData;
import org.cmpt370c2.interpreter.model.Value;

/**
 * Duplicates the top-most item in the stack.
 * 
 * @author allankerr
 *
 */
class DupWord extends PredefinedWord {

  @Override
  public String getName() {
    return "dup";
  }

  /**
   * Pops the top-most item from the stack and pushes it to the top of the stack twice to duplicate
   * it.
   */
  @Override
  public void execute(ScriptData data, ScriptListener listener, ScriptDataSource dataSource) {
    Value<?> val = data.pop();
    data.push(val);
    data.push(val);
  }
}
