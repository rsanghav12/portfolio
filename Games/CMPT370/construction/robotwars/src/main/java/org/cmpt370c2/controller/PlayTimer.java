package org.cmpt370c2.controller;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Used to constrain the play timer for each player's play
 * based on a specified duration in the game controller of the model
 * view controller component.
 * 
 * @author allankerr
 *
 */
class PlayTimer {
  
  /**
   * The timer used update the timer once per second.
   */
  private Timer timer = new Timer();
  
  /**
   * The remaining time until the timer finishes.
   */
  private int timeRemaining;
  
  /**
   * The listener that updates to the remaining time are sent to.
   */
  private PlayTimerListener listener;

  public int getTimeRemaining() {
    return timeRemaining;
  }
  
  public PlayTimer(int duration, PlayTimerListener listener) {
    if (duration < 0) {
      throw new IllegalArgumentException("Attempted to create play timer with invalid duration " + duration);
    }
    if (listener == null) {
      throw new IllegalArgumentException("A play timer must have a non-null listener");
    }
    this.timeRemaining = duration;
    this.listener = listener;
  }
  
  /**
   * Starts the timer counting down once a second until
   * there is no time remaining and sending updates to the
   * listener.
   */
  public void start() {
    timer.schedule(new TimerTask(){
      @Override
      public void run() {
        timeRemaining = Math.max(0, timeRemaining - 1);
        if (timeRemaining == 0){
          timer.cancel();
        } 
        listener.timerDidUpdate(timeRemaining);
      }
    }, 0, 1000);
  }
  
  /**
   * Stops the timer early before reaching 0.
   */
  public void stop() {
    timer.cancel();
  }
}
