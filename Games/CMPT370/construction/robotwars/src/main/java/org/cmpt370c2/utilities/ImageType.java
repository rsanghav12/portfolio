package org.cmpt370c2.utilities;

/**
 * Enum used as constants for the types of images that will be loaded and displayed
 * @author C2
 *
 */
public enum ImageType {
  PNG("png"),
  JPG("jpg");
  
  private String name;       

  @Override
  public String toString() {
     return name;
  }
  
  private ImageType(String name) {
      this.name = name;
  }
}
