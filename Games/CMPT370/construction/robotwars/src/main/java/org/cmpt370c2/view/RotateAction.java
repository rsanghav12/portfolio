package org.cmpt370c2.view;

import java.awt.Point;
import java.awt.event.ActionListener;

/**
 * The rotate action used to animate robot rotation from its current rotation to a target rotation.
 * 
 * @author allankerr
 *
 */
class RotateAction extends Action {

  /**
   * The angle the sprite should rotate to.
   */
  private double angle;

  /**
   * The angle the sprite's rotation started at. This is used to allow for rotate actions to be
   * reversed.
   */
  private double startAngle;

  /**
   * The minimum difference between the start angle and the target angle of the robot.
   */
  private double angleDelta;

  /**
   * The target location allowing for rotation animations to specified based the angle between the
   * sprite's current position and the target position.
   */
  private Point targetLocation;

  /**
   * Allows the rotation animation to be initialized with another rotate action which is the
   * performed in reverse.
   */
  private RotateAction reverseAction;

  /**
   * The degrees per second that the sprite animates its rotation at.
   */
  private double degreesPerSecond;

  /**
   * Computes the minimum angle difference between the target angle and start angle.
   * 
   * @param targetAngle The target angle to rotate to.
   * @param angle The current angle.
   * @return The minimum difference between the two angles.
   */
  private double getAngleDelta(double targetAngle, double angle) {
    targetAngle %= 360;
    double clockwise = (targetAngle + 360) - angle;
    double counterClockwise1 = targetAngle - angle;
    double counterClockwise2 = targetAngle - (angle + 360);
    if ((Math.abs(clockwise) <= Math.abs(counterClockwise1))
        && (Math.abs(clockwise) <= Math.abs(counterClockwise2))) {
      return clockwise;
    } else if ((Math.abs(counterClockwise1) <= Math.abs(counterClockwise2))
        && (Math.abs(counterClockwise1) <= Math.abs(clockwise))) {
      return counterClockwise1;
    } else {
      return counterClockwise2;
    }
  }

  /**
   * Construct a new rotate action to animate rotation to the specified angle.
   * 
   * @param angle The angle to rotate to.
   * @param degreesPerSecond The speed the robot should rotate at.
   * @param listener The listener that is notified when the action finishes.
   */
  public RotateAction(double angle, double degreesPerSecond, ActionListener listener) {
    super(0, listener);
    this.angle = angle;
    this.degreesPerSecond = degreesPerSecond;
  }

  /**
   * Construct a new rotate action to animate rotation to the angle between the current location and
   * the target location.
   * 
   * @param targetLocation The target angle to calculate the angle between.
   * @param degreesPerSecond The speed the robot should rotate at.
   * @param listener The listener that is notified when the action finishes.
   */
  public RotateAction(Point targetLocation, double degreesPerSecond, ActionListener listener) {
    super(0, listener);
    this.targetLocation = targetLocation;
    this.degreesPerSecond = degreesPerSecond;
  }

  /**
   * Construct a new rotate action to reverse the rotation animation done in a previous rotate
   * action.
   * 
   * @param reverseAction The rotate action to be performed in reverse.
   * @param listener The listener that is notified when the action finishes.
   */
  public RotateAction(RotateAction reverseAction, ActionListener listener) {
    super(0, listener);
    this.reverseAction = reverseAction;
  }

  @Override
  public void start(ImageSprite sprite) {
    this.startAngle = sprite.getRotation();
    if (this.reverseAction != null) {
      this.angle = this.reverseAction.startAngle;
      this.degreesPerSecond = this.reverseAction.degreesPerSecond;
    } else if (targetLocation != null) {
      Point location = sprite.getCenter();
      double deltaX = targetLocation.getX() - location.getX();
      double deltaY = targetLocation.getY() - location.getY();
      this.angle = 90 + Math.toDegrees(Math.atan2(deltaY, deltaX));
    }
    this.angleDelta = getAngleDelta(angle, sprite.getRotation());
    this.setDuration((int) Math.round(1000 * (Math.abs(angleDelta)) / degreesPerSecond));
    super.start(sprite);
  }

  @Override
  protected void update(long delta) {
    ImageSprite sprite = this.getSprite();
    double numDegreesToMove = angleDelta * (((double) delta) / this.getDuration());
    sprite.setRotation(sprite.getRotation() + numDegreesToMove);
  }

  @Override
  protected void finish() {
    this.getSprite().setRotation(angle);
    super.finish();
  }
}
