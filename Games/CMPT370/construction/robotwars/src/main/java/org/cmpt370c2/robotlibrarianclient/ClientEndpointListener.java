package org.cmpt370c2.robotlibrarianclient;

import org.json.simple.JSONObject;

/**
 * Interface for the listener that is awaiting a response from the robot librarian
 * @author C2
 *
 */
interface ClientEndpointListener {
  public void receivedResponse(JSONObject obj);
  public void receivedError(String errorMessage);
}
