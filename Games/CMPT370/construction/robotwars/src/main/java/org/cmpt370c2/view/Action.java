package org.cmpt370c2.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

/**
 * This class acts as the abstract super class for all sprite animations. It takes a duration as an
 * argument and calls an update(...) method every 1/60th of a second until the time is up. All
 * action subclasses must override update(...).
 * 
 * @author allankerr
 *
 */
abstract class Action implements ActionListener {

  /**
   * The interval in ms that the update method is called at. The default is 60 frames per second.
   */
  private static final int ACTION_DELTA =  (int)Math.round(1000*(1/60f));

  /**
   * The length of time for which the update should be called in ms.
   */
  private int duration;

  /**
   * The system time in ms that the action was started at.
   */
  private long startTime;

  /**
   * The system time in ms of the last update call.
   */
  private long lastUpdate;

  /**
   * The timer used to call the update method.
   */
  private Timer timer;

  /**
   * The sprite used by subclasses to handle animations by modifying properties such as location or
   * rotation.
   */
  private ImageSprite sprite;

  /**
   * The action listener that is notified when the action finishes.
   */
  private ActionListener listener;

  protected int getDuration() {
    return duration;
  }

  protected void setDuration(int duration) {
    this.duration = duration;
  }

  protected void setSprite(ImageSprite sprite) {
    this.sprite = sprite;
  }
  
  protected ImageSprite getSprite() {
    return sprite;
  }

  /**
   * Constructs a new action to run for a specified duration.
   * 
   * @param duration The duration of the action
   * @param listener The listener that is notified when the action finishes.
   */
  public Action(int duration, ActionListener listener) {
    this.duration = duration; 
    this.listener = listener;
  }

  /**
   * Start performing the action for the specified duration.
   * 
   * @param sprite Sprite the action performs operations on.
   */
  public void start(ImageSprite sprite) {     
    if (sprite == null) {
      throw new IllegalArgumentException("An action called be started on a null sprite.");
    }
    this.sprite = sprite;
    this.timer = new Timer(ACTION_DELTA, this);
    this.startTime = System.currentTimeMillis();
    this.lastUpdate = startTime;
    this.timer.start();
  }

  /**
   * Performed every time the timer ticks.
   */
  public void actionPerformed(ActionEvent e) {
    if ((System.currentTimeMillis() - duration) > startTime) {
      finish();
    } else {
      this.update(System.currentTimeMillis() - lastUpdate);
      lastUpdate = System.currentTimeMillis();
    }
  }

  /**
   * Performed when the action has run for its specified duration.
   */
  protected void finish() {
    if (listener != null) {
      listener.actionPerformed(new ActionEvent(this, 0, null));
    }
    timer.stop();
    sprite.actionFinished();
  }

  /**
   * Implemented by all subclasses to perform a timed task over the specified duration.
   * 
   * @param delta The time in ms since the last update.
   */
  protected abstract void update(long delta);
}
