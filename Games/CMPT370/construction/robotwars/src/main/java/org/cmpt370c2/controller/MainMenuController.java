package org.cmpt370c2.controller;

import org.cmpt370c2.view.MainMenuView;
import org.cmpt370c2.view.MainMenuViewListener;

/**
 * Subclass of abstract controller. Controls the main menu view, and serves as the entry point
 * into the RobotWars system.
 * @author C2
 *
 */
public class MainMenuController extends AbstractController<MainMenuView> implements MainMenuViewListener  {
  
  /**
   * Constructs a new MainMenuController
   * @param windowManager The current WindowManager instance.
   */
  public MainMenuController(WindowManager windowManager) {
    super(windowManager);
  }

  @Override
  protected MainMenuView createView() {
      return new MainMenuView(this);
  }

  public void startGame() {
    WindowManager windowManager = this.getWindowManager();
    windowManager.presentController(new SessionConfigurationController(windowManager));
  }

  public void exit() {
    this.getWindowManager().close();
  }
  
  /**
   * Main entry point into the system, creates the window manager that's passed to all controllers.
   * @param args Arguments to be passed on startup, none will be used in this system.
   */
  public static void main(String[] args) {
      WindowManager manager = new WindowManager();
      manager.presentController(new MainMenuController(manager));
  }
}
