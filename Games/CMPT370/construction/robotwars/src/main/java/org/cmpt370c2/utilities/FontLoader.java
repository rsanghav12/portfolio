package org.cmpt370c2.utilities;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.HashMap;
/**
 * Loads fonts and caches for future use in the system
 * @author C2
 *
 */
public class FontLoader extends FileLoader{

  private static FontLoader instance = null;
  
  private HashMap<FontType, Font> fontCache = new HashMap<FontType, Font>();
  
  private FontLoader() {}
  
  /**
   * Creates a new instance if one does not already exists
   * @return Returns an instance of font loader
   */
  public static FontLoader getInstance() {
    if(instance == null) {
       instance = new FontLoader();
    }
    return instance;
 }
  
  /**
   * Loads and returns font
   * @param type The type of font
   * @param style The style of font
   * @param size The size of font
   * @return The desired font
   * @throws IOException If input is null, throws exception
   * @throws FontFormatException  if the fontStream data does not contain the required font tables 
   * for the specified format
   * @throws URISyntaxException 
   */
  public Font load(FontType type, int style, float size) throws IOException, FontFormatException, URISyntaxException {
    Font font;
    if (fontCache.containsKey(type)) {
      font = fontCache.get(type);
    } else {
      InputStream input = super.load(type.getFileName(), null);
      font = Font.createFont(Font.TRUETYPE_FONT, input);
      fontCache.put(type, font);
      input.close();
    }
    return font.deriveFont(style, size);
  }
  
  public void clearCache() {
    fontCache.clear();
  }
}
