package org.cmpt370c2.interpreter.words;

import org.cmpt370c2.interpreter.controller.IdentifyResponse;
import org.cmpt370c2.interpreter.controller.ScriptDataSource;
import org.cmpt370c2.interpreter.controller.ScriptListener;
import org.cmpt370c2.interpreter.model.IntValue;
import org.cmpt370c2.interpreter.model.ScriptData;
import org.cmpt370c2.interpreter.model.StringValue;

/**
 * Pops the topmost value from the stack and uses that value to identify the robot in that
 * direction. The robot's team color, range, direction, and remaining health are pushed to the
 * stack.
 * 
 * @author allankerr
 *
 */
class IdentifyWord extends PredefinedWord {

  @Override
  public String getName() {
    return "identify!";
  }

  /**
   * Pops the topmost value from the stack and uses that value to identify the robot in that
   * direction. The robot's team color, range, direction, and remaining health are pushed to the
   * stack.
   * 
   * @throws IllegalStateException Thrown if the stack's topmost value is not an integer.
   */
  @Override
  public void execute(ScriptData data, ScriptListener listener, ScriptDataSource dataSource) {

    IntValue val;
    try {
      val = (IntValue) data.pop();
    } catch (ClassCastException ex) {
      throw new IllegalStateException(
          "The parameters for the " + getName() + " operation was not an integer");
    }
    IdentifyResponse response = listener.identify(val.getValue());
    StringValue team = new StringValue(response.getTeam().toString());
    IntValue range = new IntValue(response.getRange());
    IntValue direction = new IntValue(response.getDirection());
    IntValue remainingHealth = new IntValue(response.getRemainingHealth());
    data.push(remainingHealth);
    data.push(direction);
    data.push(range);
    data.push(team);
  }
}
