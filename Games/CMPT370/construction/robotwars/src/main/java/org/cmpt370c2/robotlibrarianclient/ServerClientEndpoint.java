package org.cmpt370c2.robotlibrarianclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * This class handles the TCP requests and responses to and from Robot Librarian.
 * 
 * @author rutviksanghavi
 */
class ServerClientEndpoint implements ClientEndpoint {

  /**
   * Keys found in JSONObject requests
   */
  private static final String LIST_KEY = "list";
  private static final String LIST_REQUEST = "list-request";
  private static final String NAME_KEY = "name";
  
  /**
   * Socket settings
   */
  private static final String HOST = "gpu0.usask.ca";
  private static final int PORT = 20001;
  private static final int TIMEOUT_SECONDS = 30;

  /**
   * Sends a JSON string to the Robot Librarian and receives a response.
   * 
   * @param obj Contains the JSON to be sent to the Robot Librarian.
   * @param url The URL destination to send to.
   * @param listener The return pointer to send the response back to.
   */
  @Override
  public void sendRequest(JSONObject obj, ClientEndpointListener listener) {        
    try {
      // Create socket connection with host on port
      Socket socket = new Socket(HOST, PORT);
      socket.setSoTimeout(TIMEOUT_SECONDS * 1000);

      BufferedReader inputStream =
          new BufferedReader(new InputStreamReader(socket.getInputStream()));
      PrintWriter outputStream = new PrintWriter(socket.getOutputStream());

      // write request to client
      outputStream.println(obj.toJSONString());
      outputStream.flush();

      // read response from server
      String serverResponse = inputStream.readLine();
      
      // close socket connection as we are done reading the response
      socket.close();

      // parser the json response
      JSONParser parser = new JSONParser();
      try {
        JSONObject response = null;

        if (obj.containsKey(LIST_REQUEST)) {
          JSONObject listJSONObject = (JSONObject) obj.get(LIST_REQUEST);
          if (listJSONObject.containsKey(NAME_KEY)) {
            JSONArray responseModify = (JSONArray) parser.parse(serverResponse);
            response = (JSONObject) responseModify.get(0);
          } else {
            // response will be a type of array
            JSONArray responseModify = (JSONArray) parser.parse(serverResponse);

            // store it in a jsonobject with key list
            Map<Object, Object> responseMap = new HashMap<Object, Object>();
            responseMap.put(LIST_KEY, responseModify);

            // make response of type JSONObject now
            response = new JSONObject(responseMap);
          }
        }
        listener.receivedResponse(response);
      } catch (ParseException e) {
        listener.receivedError("Received response in an unrecognized format.");
      }
    } catch (IOException ex) {
      listener.receivedError("The robot librarian is currently unreachable.");
    }
  }
}
