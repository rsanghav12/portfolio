package org.cmpt370c2.robotlibrarianclient;

import org.json.simple.JSONObject;

/**
 * This class is used for decoding the response received when a robot stats update has been sent to
 * the robot librarian.
 * 
 * @author allankerr
 *
 */
class UpdateCoder extends JSONCoder<Boolean> {

  private static final String RESPONSE_KEY = "update-is";
  private static final String RESPONSE_SUCCESS = "ok";

  @Override
  protected JSONObject encode(Boolean entity) {
    // This operating is unsupported because this is only ever received as a response.
    throw new UnsupportedOperationException();
  }

  @Override
  protected Boolean decode(JSONObject obj) throws MalformedResponseException {
    try {
      String status = (String) obj.get(RESPONSE_KEY);
      return status.equals(RESPONSE_SUCCESS);
    } catch (RuntimeException ex) {
      // Thrown if status is null or in the case of an illegal cast to string
      throw new MalformedResponseException("Received unexpected response during update.");
    }
  }
}
