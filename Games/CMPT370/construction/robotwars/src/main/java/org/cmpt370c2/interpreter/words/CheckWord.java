package org.cmpt370c2.interpreter.words;

import org.cmpt370c2.interpreter.controller.CheckResponse;
import org.cmpt370c2.interpreter.controller.ScriptDataSource;
import org.cmpt370c2.interpreter.controller.ScriptListener;
import org.cmpt370c2.interpreter.model.IntValue;
import org.cmpt370c2.interpreter.model.ScriptData;
import org.cmpt370c2.interpreter.model.StringValue;

/**
 * Scans for nearby robots and pushes the number of robots to the stack.
 * 
 * @author allankerr
 *
 */
class CheckWord extends PredefinedWord {

  @Override
  public String getName() {
    return "check!";
  }

  /**
   * Asks the listener to scan for robots in visible range and pushes the number of nearby robots to
   * the stack.
   * 
   * @throws IllegalArgumentException Thrown if an invalid check response is received.
   */
  @Override
  public void execute(ScriptData data, ScriptListener listener, ScriptDataSource dataSource) {
    IntValue val;
    try {
      val = (IntValue) data.pop();
    } catch (ClassCastException ex) {
      throw new IllegalStateException(
          "The arguments to '" + this.getName() + "' was not an integer.");
    }
    CheckResponse response = listener.check(val.getValue());
    if (response == null) {
      throw new IllegalArgumentException("Script received a null check response.");
    }
    StringValue result = new StringValue(response.toString());
    data.push(result);
  }
}
