package org.cmpt370c2.utilities;

/**
 * Element wrapper holds generic class of E
 * @author C2
 *
 * @param <E> Type wrapper should hold
 */
class ElementWrapper <E> {
  
  private E element;

  public E getElement() {
    return element;
  }

  public void setElement(E element) {
    this.element = element;
  }
}
