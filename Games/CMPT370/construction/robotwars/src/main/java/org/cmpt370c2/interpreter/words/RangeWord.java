package org.cmpt370c2.interpreter.words;

import org.cmpt370c2.interpreter.controller.ScriptDataSource;
import org.cmpt370c2.interpreter.controller.ScriptListener;
import org.cmpt370c2.interpreter.model.IntValue;
import org.cmpt370c2.interpreter.model.ScriptData;

/**
 * The predefined word for getting the visible range of the robot the script is being executed for.
 * 
 * @author allankerr
 *
 */
public class RangeWord extends PredefinedWord {

  @Override
  public String getName() {
    return "range";
  }

  /**
   * Requests the visible range of the robot from the data source and pushes it to the stack.
   */
  @Override
  public void execute(ScriptData data, ScriptListener listener, ScriptDataSource dataSource) {
    data.push(new IntValue(dataSource.range()));
  }
}
