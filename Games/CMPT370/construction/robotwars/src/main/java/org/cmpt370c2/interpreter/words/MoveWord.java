package org.cmpt370c2.interpreter.words;

import org.cmpt370c2.interpreter.controller.ScriptDataSource;
import org.cmpt370c2.interpreter.controller.ScriptListener;
import org.cmpt370c2.interpreter.model.ScriptData;

/**
 * Notifies the script listener that the robot has moved.
 *
 */
class MoveWord extends PredefinedWord {

  @Override
  public String getName() {
    return "move!";
  }

  /**
   * Notifies the script listener that the robot has moved.
   *
   */
  @Override
  public void execute(ScriptData data, ScriptListener listener, ScriptDataSource dataSource) {
    listener.move();
  }
}
