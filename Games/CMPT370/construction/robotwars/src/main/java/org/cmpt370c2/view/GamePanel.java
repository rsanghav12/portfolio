package org.cmpt370c2.view;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.cmpt370c2.model.RobotType;
import org.cmpt370c2.model.TeamColor;

/**
 * Displays information relevant to current players turn such as how much time, shots, or moves
 * remain when a player's turn is in progress.
 * 
 * @author allankerr and rutviksanghavi
 *
 */
@SuppressWarnings("serial")
class GamePanel extends JPanel {

  /**
   * The card layout used to swap between displaying the start play card and the play card.
   */
  private CardLayout cardLayout;

  private HashMap<GamePanelCardType, GamePanelCard> cards =
      new HashMap<GamePanelCardType, GamePanelCard>();

  public GamePanel(ActionListener listener) {
    super(new CardLayout());
    this.cardLayout = (CardLayout) this.getLayout();

    this.setBackground(Colors.BACKGROUND_COLOR);
    this.setBorder(new EmptyBorder(0, Dimensions.PANEL_MARGINS, 0, Dimensions.PANEL_MARGINS));

    StartPlayPanelCard startPlayPanel = new StartPlayPanelCard(listener);
    PlayPanelCard playPanel = new PlayPanelCard(listener);

    this.setMaximumSize(new Dimension(Dimensions.PANEL_WIDTH, Integer.MAX_VALUE));
    this.addCard(GamePanelCardType.START_PLAY_PANEL, startPlayPanel);
    this.addCard(GamePanelCardType.PLAY_PANEL, playPanel);
    this.setFocusable(false);
  }

  /**
   * Adds one of the cards that the panel swaps between depending on if a player's play is waiting
   * to start or is in progress.
   * 
   * @param type The type of the card which is either starting play or play in progress.
   * @param card The panel that contains the contents that will be displayed when the panel state
   *        changes between starting a play and a play being in progress.
   */
  private void addCard(GamePanelCardType type, GamePanelCard card) {
    this.add(card, type.toString());
    this.cards.put(type, card);
  }

  /**
   * Gets the panel containing the contents displayed for corresponding panel state. The panel state
   * can either be waiting for a player's play to start or play in progress.
   * 
   * @param type The type of panel that should be retrieved.
   * @return The panel that displays the contents for the specified panel state.
   */
  public GamePanelCard getCard(GamePanelCardType type) {
    return cards.get(type);
  }

  /**
   * Changes the panel state to show the information when a player is allowed to start their turn.
   * This panel is displayed before the turn actually
   * 
   * @param color The team color of the team who is allowed to start their turn.
   */
  public void showStartPlayCard(TeamColor color) {
    StartPlayPanelCard card =
        (StartPlayPanelCard) this.cards.get(GamePanelCardType.START_PLAY_PANEL);
    card.updateTeam(color);
    this.cardLayout.show(this, GamePanelCardType.START_PLAY_PANEL.toString());
  }

  /**
   * Changes the panel state to show the information when a player has started their turn. This
   * panel is displayed while the player's turn is in progress.
   * 
   * @param color The team color of the team who's play is in progress.
   * @param type The type of robot being controlled during the play.
   * @param isComputer True if the play in progress is a computer player's play or false if it is a
   *        human player in control.
   */
  public void showPlayCard(TeamColor color, RobotType type, boolean isComputer) {
    PlayPanelCard card = (PlayPanelCard) this.cards.get(GamePanelCardType.PLAY_PANEL);
    card.updatePlayer(color, type, isComputer);
    this.cardLayout.show(this, GamePanelCardType.PLAY_PANEL.toString());
  }
}
