package org.cmpt370c2.controller;

/**
 * Interface for the play timer that counts down alloted time for each play
 * @author C2
 *
 */
interface PlayTimerListener {
  /**
   * Updates play timer
   * @param remainingTime The time remaining on the timer
   */
  public void timerDidUpdate(int remainingTime);
}
