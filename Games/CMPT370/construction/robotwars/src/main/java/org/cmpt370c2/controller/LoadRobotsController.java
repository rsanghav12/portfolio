package org.cmpt370c2.controller;

import java.util.ArrayList;
import java.util.List;

import org.cmpt370c2.interpreter.controller.ScriptController;
import org.cmpt370c2.interpreter.model.ScriptData;
import org.cmpt370c2.model.RobotType;
import org.cmpt370c2.model.TeamConfiguration;
import org.cmpt370c2.robotlibrarianclient.RobotData;
import org.cmpt370c2.robotlibrarianclient.RobotLibrarianClient;
import org.cmpt370c2.robotlibrarianclient.RobotLibrarianClientListener;
import org.cmpt370c2.view.LoadRobotView;
import org.cmpt370c2.view.LoadRobotViewListener;

/**
 * Subclass of Abstract Controller. Controls the browsing and selection of robot scripts tracked by
 * the robot librarian.
 * 
 * @author C2
 *
 */
public class LoadRobotsController extends AbstractController<LoadRobotView>
    implements LoadRobotViewListener, RobotLibrarianClientListener {

  /**
   * RobotType to determine which type to load script for
   */
  private RobotType type;

  /**
   * Variable holds general configuration of the game
   */
  private TeamConfiguration configuration;

  /**
   * Client for the robot librarian to request robots
   */
  private RobotLibrarianClient robotLibClient;

  /**
   * List of robot data used to display selection of robots for the user
   */
  private List<RobotData> robotList;

  /**
   * Constructs a new load robot controller
   * 
   * @param windowManager The window manager the controller communicates with
   * @param type The type of robot being load - can be scout, sniper or tank
   * @param configuration a team configuration where the script should be stored
   */
  public LoadRobotsController(WindowManager windowManager, RobotType type,
      TeamConfiguration configuration) {
    super(windowManager);

    // assert to make sure configuration is never null
    assert configuration != null;

    this.type = type;
    this.configuration = configuration;
    this.robotLibClient = new RobotLibrarianClient(this);
    this.robotLibClient.listRobots(true);
  }

  @Override
  protected LoadRobotView createView() {
    return new LoadRobotView(this);
  }

  @Override
  public void robotSelected(int index) {
    RobotData robot = robotList.get(index);
    robotLibClient.downloadRobot(robot.getScriptName(), robot.getTeamName());
  }

  @Override
  public void cancel() {
    // Exit the window because it is not needed anymore
    this.getWindowManager().close();
  }

  /**
   * Build a script for when a new robot is recieved.
   */
  @Override
  public void robotReceived(RobotData robot) {
    ScriptController scriptController = new ScriptController();
    ScriptData data;
    try {
      // Compile the script
      data = scriptController.build(robot.getTeamName(), robot.getScriptName(), robot.getScript());
      configuration.setScript(type, data);
      this.getWindowManager().close();
    } catch (Exception e) {
      // Generally a bad practice but is used because the only response to an error is
      // to display it to the user
      this.getView().robotLoadDidFail(e.getMessage());
    }
  }

  /**
   * List of robots from the robot librarian. Also filters the list to a specific Robot type such as
   * Scout, Tank, or Sniper.
   */
  @Override
  public void robotListReceived(List<RobotData> robots) {
    List<RobotData> filteredRobots = new ArrayList<RobotData>();
    for (RobotData robot : robots) {
      if (robot.allowsRobotType(type)) {
        filteredRobots.add(robot);
      }
    }
    this.robotList = filteredRobots;

    this.getView().listRobots(this.robotList);
  }

  @Override
  public void robotUpdated(boolean success) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void requestFailed(String error) {
    this.getView().robotLoadDidFail(error);
  }
}
