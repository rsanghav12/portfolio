package org.cmpt370c2.utilities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * This class is used as a container for objects stored in a hexagonal shape. Objects in the
 * container are accessed using the layer in the hexagon and the direction in that layer. For
 * example, layer 0 contains a single item while layer 1 contains 6 items at directions 0 through 5.
 * 
 * @author allankerr and rutviksanghavi
 *
 * @param <E> The type of object being stored in the container.
 */
public class HexagonalContainer<E> implements Iterable<HexagonalPoint> {

  /**
   * The layers used to store the data in the hexagonal container. Each layer contains 6i items
   * where i is the index of the layer excluding layer 0 which contains 1 item.
   */
  private ArrayList<ArrayList<ElementWrapper<E>>> layers;

  public int getLayerCount() {
    return layers.size();
  }

  /**
   * Constructs a new hexagonal container for storing objects in a hexagonal shape.
   * 
   * @param layerCount The number of layers the container should consist of.
   */
  public HexagonalContainer(int layerCount) {
    if (layerCount < 1) {
      throw new IllegalArgumentException("A hexagonal container must have 1 or more layers.");
    }
    layers = new ArrayList<ArrayList<ElementWrapper<E>>>(layerCount);
    layers.add(createLayer(1));

    for (int i = 1; i < layerCount; i++) {
      layers.add(createLayer(6 * i));
    }
  }

  /**
   * Creates a layer with the specified number of items. This helper method is used to populate the
   * layer with ElementWrapper instances. These are used so that elements can be added and removed
   * from the container without causing the indices of other elements to change.
   * 
   * @param capacity The number of items in the layer
   * @return The list representing the layer populated with element wrapper instances.
   */
  private ArrayList<ElementWrapper<E>> createLayer(int capacity) {
    ArrayList<ElementWrapper<E>> layer = new ArrayList<ElementWrapper<E>>(capacity);
    for (int i = 0; i < capacity; i++) {
      layer.add(new ElementWrapper<E>());
    }
    return layer;
  }

  /**
   * Helper method used to get the six points surrounding any point within the hexagonal container.
   * 
   * @param p The point for which the six neighboring point should be found.
   * @return The points of the six elements around point p.
   */
  private ArrayList<HexagonalPoint> getNeighbours(HexagonalPoint p) {
    ArrayList<HexagonalPoint> points = new ArrayList<HexagonalPoint>(7);
    int directions = 6 * p.getLayer();
    if (p.getLayer() == 0) {
      // Handle the special case where p is at the origin.
      points.add(new HexagonalPoint(1, 0));
      points.add(new HexagonalPoint(1, 1));
      points.add(new HexagonalPoint(1, 2));
      points.add(new HexagonalPoint(1, 3));
      points.add(new HexagonalPoint(1, 4));
      points.add(new HexagonalPoint(1, 5));
    } else {

      points
          .add(new HexagonalPoint(p.getLayer(), (p.getDirection() - 1 + directions) % directions));
      HexagonalPoint point = new HexagonalPoint(p.getLayer(), (p.getDirection() + 1) % directions);

      int upperDirections = 6 * (p.getLayer() + 1);
      int lowerDirections = 6 * (p.getLayer() - 1);

      double lowerDirection = p.getDirection() * ((double) (p.getLayer() - 1) / p.getLayer());
      double upperDirection = p.getDirection() * ((double) (p.getLayer() + 1) / p.getLayer());


      if (p.getDirection() % p.getLayer() == 0) {
        // The point is a corner point meaning that there are three points in the outer layer and
        // one in the inner layer.
        int upper = (int) Math.round(upperDirection);
        int lower = (int) Math.round(lowerDirection);
        points.add(
            new HexagonalPoint(p.getLayer() + 1, (upper - 1 + upperDirections) % upperDirections));
        points.add(new HexagonalPoint(p.getLayer() + 1, upper));
        points.add(new HexagonalPoint(p.getLayer() + 1, (upper + 1) % upperDirections));
        points.add(point);
        points.add(new HexagonalPoint(p.getLayer() - 1, lower));
      } else {
        // The point is not a corner point meaning that there are two points in the outer layer and
        // two in the innter layer.
        points.add(new HexagonalPoint(p.getLayer() + 1,
            ((int) Math.floor(upperDirection) + upperDirections) % upperDirections));
        points.add(new HexagonalPoint(p.getLayer() + 1,
            ((int) Math.ceil(upperDirection)) % upperDirections));
        points.add(point);
        points.add(new HexagonalPoint(p.getLayer() - 1,
            ((int) Math.ceil(lowerDirection)) % lowerDirections));
        points.add(new HexagonalPoint(p.getLayer() - 1,
            ((int) Math.floor(lowerDirection) + lowerDirections) % lowerDirections));
      }
    }
    return points;
  }

  /**
   * Helper method used to get the six points surrounding any point within the hexagonal container
   * that also includes the specified point in the returned set of point.
   * 
   * @param p The point for which the six neighboring point should be found.
   * @return The points of the six elements around point p along with p included.
   */
  private ArrayList<HexagonalPoint> getNearby(HexagonalPoint p) {
    ArrayList<HexagonalPoint> nearby = getNeighbours(p);
    nearby.add(p);
    return nearby;
  }

  /**
   * Returns a set of all points within range of the supplied point.
   * 
   * @param point The specified point which serves as the center for the calculations
   * @param distance The number of layers from the center point.
   * @return A set of points that falls within range of specified point
   */
  public Set<HexagonalPoint> getPointsRange(HexagonalPoint point, int distance) {
    if (distance == 0 || distance < 0) {
      throw new IllegalArgumentException("Distance must be equal to or greater than 1.");
    }
    HashSet<HexagonalPoint> points = new HashSet<HexagonalPoint>();
    HashSet<HexagonalPoint> newPoints = new HashSet<HexagonalPoint>();
    newPoints.add(point);

    getRange(newPoints, distance, points);
    return points;
  }

  /**
   * Recursively computes all points within the specified distance of the set of new points.
   * 
   * @param newPoints The set of new points that were found but haven't had their neighbors computed
   *        yet.
   * @param distance The number of layers used for the distance from each of the new points.
   * @param points The set of points that have already been found to avoid adding a point multiple
   *        times.
   */
  private void getRange(HashSet<HexagonalPoint> newPoints, int distance,
      HashSet<HexagonalPoint> points) {
    points.addAll(newPoints);

    if (distance <= 0) {
      return;
    }

    // New points is used to keep track of newly discovered points/
    // If the recursive call is made before all points in the current
    // layer are added then it will fail because some will get visited from
    // the wrong layer.
    HashSet<HexagonalPoint> nextLayer = new HashSet<HexagonalPoint>();
    for (HexagonalPoint centre : newPoints) {
      // Get points that are distance 1 or less away from point
      ArrayList<HexagonalPoint> nearbyPoints = getNearby(centre);
      for (HexagonalPoint p : nearbyPoints) {
        if (p.getLayer() < layers.size() && !points.contains(p)) {
          nextLayer.add(p);
          points.add(p);
        }
      }
    }
    // Perform the recursive calls after all points
    // in the current layer have been added.
    getRange(nextLayer, distance - 1, points);
  }

  /**
   * Takes the relative coordinate system defined with direction as the zero direction and uses the
   * distance and direction offset to compute the point that is that far away and in that direction.
   * 
   * @param p The point to which the distance and direction offset should be applied.
   * @param distance The distance of the neighbor from p.
   * @param direction The relative zero direction used to determine where the point is.
   * @param directionOffset The offset from the relative zero direction to determine where the point
   *        is.
   * @return The point that is of distance and directionOffset away from p as defined based on the
   *         global zero direction.
   */
  public HexagonalPoint getNeighbour(HexagonalPoint p, int distance, int direction,
      int directionOffset) {

    if (distance == 0) {
      return p;
    } else {
      // Compute the direction used to find the point in the next layer.
      int nextDirection = directionOffset;
      for (int d = distance; d > 1; d--) {
        nextDirection = (int) Math.floor(nextDirection * ((double) (d - 1) / d));
      }
      // Compute the index of the point in the six neighbors that
      // has a direction of 0 based on the global coordinate system.
      int zeroDirection;
      if (p.getLayer() == 0) {
        zeroDirection = 0;
      } else {
        // Derived formula based on the relationship between the first point
        // in the neighbors array which is in the same layer with direction - 1.
        // By dividing its direction by the layer the result is in the range 0 to 5
        // This value is used to determine the direction the index that corresponds to 0.
        int directions = 6 * p.getLayer();
        zeroDirection = 1 - ((((p.getDirection() - 1) + directions) % directions) / p.getLayer());
      }
      // Use the zero direction to get the neighbor that is
      // in the direction specified using the distance and direction arguments.
      ArrayList<HexagonalPoint> points = getNeighbours(p);
      int globalDirection = ((zeroDirection + direction + nextDirection) + 6) % 6;

      // Perform the recursive call with a distance of 1 less to find the desired neighbor.
      // Offset is required to recompute the direction offset because there is one less layer.
      int offset = (int) Math.ceil(directionOffset * ((double) (distance - 1) / distance));
      return getNeighbour(points.get(globalDirection), distance - 1, direction, offset);
    }
  }

  /**
   * Helper method for getting the value container in the element wrapper.
   * 
   * @param point The point of the element wrapper within the container.
   * @return The value contained in the element wrapper.
   */
  private ElementWrapper<E> getElementWrapper(HexagonalPoint point) {
    if (point.getLayer() >= layers.size()) {
      throw new IndexOutOfBoundsException("Layer is out of bounds: " + point);
    }
    return layers.get(point.getLayer()).get(point.getDirection());
  }

  /**
   * Gets the element at the specified point.
   * 
   * @param point The point at which to get the element.
   * @return The element at the specified point or null if it doesn't exist.
   */
  public E get(HexagonalPoint point) {
    return getElementWrapper(point).getElement();
  }

  /**
   * Add an element at the specified point. If there is an existing element at the point it is
   * replaced.
   * 
   * @param element The element to be added.
   * @param point The point at which to add the element.
   */
  public void put(E element, HexagonalPoint point) {
    getElementWrapper(point).setElement(element);
  }

  public Iterator<HexagonalPoint> iterator() {
    return new HexagonalIterator<E>(this);
  }
}
