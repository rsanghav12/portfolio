package org.cmpt370c2.utilities;

import java.util.Iterator;

/**
 * An iterator class designed to iterate through the hexagonal points within the hexagonal
 * container
 * @author C2
 *
 * @param <E> Generic object that will make up the tiles to be iterated through
 */
public class HexagonalIterator<E> implements Iterator<HexagonalPoint> {

  private int layer = 0;
  
  private int direction = 0;
  
  private HexagonalContainer<E> container;
  
  /**
   * Constructs a new hexagonal iterator
   * @param container The container to be iterated through
   */
  HexagonalIterator(HexagonalContainer<E> container) {
    this.container = container;
  }
  
  public boolean hasNext() {
    // See if current point is a valid direction within the current
    // layer or there is another layer that has yet to be visited.
    return (direction < Math.max(layer * 6, 1)) && (layer < container.getLayerCount());
  }

  /**
   * Returns the next hexagonal point
   */
  public HexagonalPoint next() {
    HexagonalPoint next = new HexagonalPoint(layer, direction);
    if ((direction + 1) < Math.max(layer * 6, 1)) {
      // Move up one in the current layer
      direction += 1;
    } else {
      // Move to next layer
      direction = 0;
      layer += 1;
    }
    return next;
  }

  public void remove() {
    throw new UnsupportedOperationException();
  }
}
