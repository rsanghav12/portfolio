package org.cmpt370c2.view;

import javax.swing.table.DefaultTableModel;

import org.cmpt370c2.model.RobotType;
import org.cmpt370c2.model.TeamColor;

/**
 * A custom table model for displaying a table of players where they can configure their
 * team of robots and load scripts, or declare them as computers.
 * 
 * @author rutviksanghavi
 *
 */
@SuppressWarnings("serial")
public class SessionTableModel extends DefaultTableModel {
  
  /**
   * Stores the number of players selected in the session configuration view
   */
  private int players = 0;
  
  /**
   * Rotation of sprites in the table
   */
  private static int SPRITE_ROTATION = 270;
    
  /**
   * Array of elements to hold the Team data
   */
  private Object[][] data;
  
  public SessionTableModel(int players) {
    setPlayers(players);
    initData();
  }
  
  /**
   * Initializes data according to the number of selected players
   */
  private void initData() {
    /** Representation of dupData
    private Object[][] data = {
    {new RobotSprite(0, 0, 270, TeamColor.RED, RobotType.SCOUT), new Boolean(false), TeamColor.RED},
    {new RobotSprite(0, 0, 270, TeamColor.BLUE, RobotType.SCOUT), new Boolean(false), TeamColor.BLUE},
    {new RobotSprite(0, 0, 270, TeamColor.ORANGE, RobotType.SCOUT), new Boolean(false), TeamColor.ORANGE},
    {new RobotSprite(0, 0, 270, TeamColor.GREEN, RobotType.SCOUT), new Boolean(false), TeamColor.GREEN},
    {new RobotSprite(0, 0, 270, TeamColor.PURPLE, RobotType.SCOUT), new Boolean(false), TeamColor.PURPLE},
    {new RobotSprite(0, 0, 270, TeamColor.YELLOW, RobotType.SCOUT), new Boolean(false), TeamColor.YELLOW}
    }; 
    **/
    Object[][] dupData = new Object[6][getColumnCount()];
    
    int i = 0;
    for(TeamColor team : TeamColor.values()) {
      dupData[i][0] = new RobotSprite(0, 0, SPRITE_ROTATION, team, RobotType.SCOUT);
      dupData[i][1] = new Boolean(false);
      dupData[i][2] = team;
      i++;
    }
    this.data = dupData;
  }
  
  /**
   * change the number of players shown in the table
   * @param players Number of players
   */
  public void setPlayers(int players) {
    this.players = players;
    fireTableStructureChanged();
  }

  @Override
  public int getRowCount() {
    return players;
  }

  @Override
  public int getColumnCount() {
    return 3;
  }
  
  @Override
  public String getColumnName(int col) {
    return "";
}

  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
      return data[rowIndex][columnIndex];
  }
  
  @Override
  public Class<?> getColumnClass(int c) {
    return getValueAt(0, c).getClass();
  }
  
  @Override
  public void setValueAt(Object value, int rowIndex, int columnIndex) {
    data[rowIndex][columnIndex] = value;
    fireTableCellUpdated(rowIndex, columnIndex);
  }
  
  @Override
  public boolean isCellEditable(int row, int col) {
    return col == 1 || col == 2;
  }
}
