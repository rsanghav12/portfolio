package org.cmpt370c2.controller;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.cmpt370c2.interpreter.controller.CheckResponse;
import org.cmpt370c2.interpreter.controller.IdentifyResponse;
import org.cmpt370c2.interpreter.controller.ScriptController;
import org.cmpt370c2.interpreter.controller.ScriptListener;
import org.cmpt370c2.interpreter.model.Value;
import org.cmpt370c2.model.GameBoard;
import org.cmpt370c2.model.GameTile;
import org.cmpt370c2.model.Robot;
import org.cmpt370c2.model.RobotStats;
import org.cmpt370c2.model.RobotTeam;
import org.cmpt370c2.model.RobotType;
import org.cmpt370c2.model.SessionConfiguration;
import org.cmpt370c2.model.TeamColor;
import org.cmpt370c2.model.TeamConfiguration;
import org.cmpt370c2.utilities.HexagonalPoint;
import org.cmpt370c2.view.GameView;
import org.cmpt370c2.view.GameViewListener;
import org.cmpt370c2.view.TileType;

/**
 * This class is a subclass of AbstractController. It controls player plays and board state while
 * the game is played.
 * 
 * @author kentwalters and allankerr
 *
 */
public class GameController extends AbstractController<GameView>
    implements GameViewListener, ScriptListener {

  private boolean hasEndedGame;
  private boolean hasEndedPlay;
  private int actionsInProgress;

  private ScriptController interpreter;
  private SessionConfiguration configuration;

  /**
   * The robot whose play is underway
   */
  private Robot currentRobot;

  /**
   * Container for the teams involved in the game
   */
  private HashMap<TeamColor, RobotTeam> teams;
  private GameBoard board;
  private TeamColor currentTeam;
  private Set<HexagonalPoint> inRangeTiles;
  private Set<HexagonalPoint> visibleTiles = new HashSet<HexagonalPoint>();
  private PlayTimer timer;
  private List<Robot> nearbyRobots;

  /**
   * Represents the play in the current turn, used to manage which robot plays next
   */
  private int currentPlay;

  /**
   * Constructs a new GameController
   * 
   * @param windowManager The current WindowManager instance
   * @param configuration The session configuration object used for creating initial game state.
   */
  public GameController(WindowManager windowManager, SessionConfiguration configuration) {
    super(windowManager);

    // assert to make sure configuration is never null
    assert configuration != null;

    this.configuration = configuration;
    this.board = new GameBoard(configuration.getBoardSize());
    this.interpreter = new ScriptController(this);
    this.teams = new HashMap<TeamColor, RobotTeam>();

    for (int i = 0; i < configuration.getNumberOfPlayers(); i++) {
      TeamColor team = TeamColor.values()[i];
      TeamConfiguration teamConfig = configuration.getTeamConfiguration(team);
      teams.put(team, new RobotTeam(team, teamConfig));
    }
    layoutBoard(configuration.getBoardSize());
    preparePlay();
  }

  /**
   * Detects whether the next team is a Computer player or human player and prepares next play
   * accordingly
   */
  private void preparePlay() {

    int playsPerTurn = teams.size() * RobotType.values().length;
    if (currentPlay >= playsPerTurn) {
      for (RobotTeam team : teams.values()) {
        team.startTurn();
      }
      this.currentPlay = 0;
    }
    // Reset the board visibility.
    resetVisibility();
    hasEndedPlay = false;
    actionsInProgress = 0;
    currentRobot = null;
    visibleTiles = new HashSet<HexagonalPoint>();
    currentPlay += 1;

    // Prepare the play for the next player.
    TeamColor color = getNextTeam();
    RobotTeam team = teams.get(color);
    currentRobot = team.nextRobot();

    if (currentRobot != null) {
      currentRobot.startPlay();
      if (configuration.getTeamConfiguration(color).isComputer()) {
        startComputerPlay();
      } else {
        getView().playerPlayWillStart(color);
      }
    } else {
      preparePlay();
    }
  }

  /**
   * Returns the team color for the team whose turn is next
   * 
   * @return The team color
   */
  private TeamColor getNextTeam() {
    int teamIndex;
    if (currentTeam == null) {
      teamIndex = 0;
    } else {
      teamIndex = (currentTeam.ordinal() + 1) % configuration.getNumberOfPlayers();
    }
    currentTeam = TeamColor.values()[teamIndex];
    return currentTeam;
  }

  /**
   * Timer that ends play if play time has exceeded the play duration Used to display remaining time
   * to player Cannot be set outside the bounds of MIN and MAX_PLAY_DURATION specified in
   * SessionConfiguration.java
   */
  private void startPlayTimer() {
    timer = new PlayTimer(configuration.getPlayDuration(), new PlayTimerListener() {
      @Override
      public void timerDidUpdate(int remainingTime) {
        getView().timerDidUpdate(remainingTime);
        if (remainingTime == 0) {
          endPlay();
        }
      }
    });
    timer.start();
  }

  /**
   * Layout the initial board state by positioning robots at start points, giving them the proper
   * rotation, and assigning tile types based on start points.
   * 
   * @param layers The number of layers of the current board
   */
  private void layoutBoard(int layers) {
    GameView view = getView();

    // Add the robots at their correct starting positions with the desired starting rotation.
    int startOffset = (((layers - 1) * 6) / configuration.getNumberOfPlayers());
    for (int i = 0; i < configuration.getNumberOfPlayers(); i++) {
      TeamColor teamColor = TeamColor.values()[i];
      int direction = 3 + (i * TeamColor.values().length / configuration.getNumberOfPlayers());
      HexagonalPoint startLocation = new HexagonalPoint(layers - 1, i * startOffset);
      for (RobotType type : RobotType.values()) {

        RobotTeam team = teams.get(teamColor);
        Robot robot = team.getRobot(type);
        robot.setDirection(direction);
        board.moveRobot(robot, startLocation);

        view.robotWasAdded(teamColor, type, startLocation, direction);
      }
      GameTile tile = board.getTile(startLocation);
      tile.setType(teamColor.getType());
    }
  }

  /**
   * Reset the visibility of tiles in the game board based on whether or not they should be visible
   * when preparing a new play.
   */
  private void resetVisibility() {
    // Reset tile visibility based on if they should all be hidden or visible
    Iterator<HexagonalPoint> iter = board.iterator();
    boolean isAllComputerPlayers = isAllComputerPlayers();
    while (iter.hasNext()) {
      HexagonalPoint point = iter.next();
      if (isAllComputerPlayers) {
        GameTile tile = board.getTile(point);
        getView().tileDidUpdate(point, tile.getType());
      } else {
        getView().tileDidUpdate(point, TileType.HIDDEN);
      }
    }
    // Hide the robots if necessary
    for (TeamColor team : this.teams.keySet()) {
      for (RobotType type : RobotType.values()) {
        if (!teams.get(team).getRobot(type).isDead()) {
          getView().robotVisibilityDidChange(team, type, isAllComputerPlayers);
        }
      }
    }
    unhighlightRobot();
  }

  @Override
  protected GameView createView() {
    return new GameView(this.configuration.getBoardSize(), this);
  }

  /**
   * Register a move for the current robot. It also updates the Model for current robot to match the
   * board.
   */
  public void move() {
    // get the destination point
    final HexagonalPoint destination =
        board.getMovePoint(currentRobot.getPoint(), currentRobot.getDirection());

    if (currentRobot.movesLeft() > 0) {
      startAction();
      try {
        // Perform a move action in the view
        this.getView().robotDidMove(currentRobot.team(), currentRobot.type(), destination,
            new ActionListener() {
              public void actionPerformed(ActionEvent e) {
                updateVisibility();
                endAction();
                endPlayIfComplete();
              }
            });
        // Update the model to reflect the move
        currentRobot.decrementMovesLeft();
        board.moveRobot(currentRobot, destination);
        updateInRange();
        getView().robotMovesDidUpdate(currentRobot.movesLeft());
      } catch (IndexOutOfBoundsException ex) {
        // The action must end whether or not it succeeds.
        endAction();
        endPlayIfComplete();
      }
    }
  }

  /**
   * Registers a shot from the current robot, calculates the corresponding hexagonal point and
   * passes to another shot handler
   * 
   * @param direction The direction of the shot fired from the robot
   * @param distance The tile distance from the robot to the target
   */
  public void shoot(int direction, int distance) {
    shoot(board.getShootPoint(currentRobot.getPoint(), distance, currentRobot.getDirection(),
        direction));
  }

  /**
   * Receives a HexagonalPoint of a tile that has been shot at by the current robot and updates any
   * affected robots
   * 
   * @param target The target tile of the current robot's shot
   */
  public void shoot(final HexagonalPoint target) {
    if (inRangeTiles.contains(target) && (currentRobot.shotsLeft() > 0)) {

      // Game tile needs to be copied in case the current robot moves onto the tile it was
      // shooting at while the bullet is traveling.
      final GameTile shotTile = board.getTile(target).clone();
      final int damage = currentRobot.attack();
      final Robot shooter = currentRobot;

      if (currentRobot.hasScript() && !isAllComputerPlayers()) {
        tileWasShot(shooter, shotTile, damage);
        endPlayIfComplete();
      } else {
        startAction();
        try {
          // Perform a shoot action in the view

          this.getView().robotDidShoot(shooter.team(), shooter.type(), target,
              new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                  tileWasShot(shooter, shotTile, damage);

                  endAction();
                  endPlayIfComplete();
                }
              });
        } catch (IndexOutOfBoundsException ex) {
          // The action must end whether or not it succeeds.
          endAction();
          endPlayIfComplete();
        }
      }
      // Update the model
      currentRobot.decrementShotsLeft();
      getView().robotShotsDidUpdate(currentRobot.shotsLeft());
    }
  }

  /**
   * Applies damage from a shot to any and all robots currently on tile
   * 
   * @param tile The tile that has been shot
   * @param damage The amount of damage caused by the shot
   */
  private void tileWasShot(Robot shooter, GameTile tile, int damage) {
    int numKilled = 0;
    Iterator<Robot> iter = tile.iterator();
    while (iter.hasNext()) {
      Robot robot = iter.next();
      robot.tookDamage(damage);
      if (robot.isDead()) {
        getView().robotWasDestroyed(robot.team(), robot.type());
        board.removeRobot(robot);
        numKilled++;
      }
    }
    shooter.killedRobots(numKilled);
    if (isGameOver()) {
      endGame();
    }
  }

  /**
   * Decides if a game is over by checking the number of remaining robots on each team
   * 
   * @return false if game is over otherwise true
   */
  private boolean isGameOver() {
    int numberOfRemainingTeams = 0;
    for (TeamColor team : this.teams.keySet()) {
      if (teams.get(team).hasRemainingRobots()) {
        numberOfRemainingTeams++;
      }
    }
    return numberOfRemainingTeams <= 1;
  }


  /**
   * Check if all the players on board are computers or not.
   * 
   * @return true if all computers otherwise false
   */
  private boolean isAllComputerPlayers() {
    for (TeamColor team : this.teams.keySet()) {
      TeamConfiguration teamConfig = configuration.getTeamConfiguration(team);
      if (!teamConfig.isComputer() && teams.get(team).hasRemainingRobots()) {
        return false;
      }
    }
    return true;
  }

  /**
   * Register a turn for current robot. Also update the model for current robots direction.
   */
  public void turn(int direction) {
    startAction();
    int newDirection = currentRobot.getDirection() + direction;
    currentRobot.setDirection(newDirection);
    try {
      this.getView().robotDidRotate(currentRobot.team(), currentRobot.type(),
          currentRobot.getDirection(), new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              endAction();
              endPlayIfComplete();
            }
          });
    } catch (Exception ex) {
      // The action must end whether or not it succeeds.
      endAction();
      endPlayIfComplete();
    }
  }

  /**
   * Changes the visibility of a single specified tile
   * 
   * @param point The point whose visibilty is being changed
   * @param visible Boolean representing whether or not the tiles is visible
   */
  private void updateVisibility(HexagonalPoint point, boolean visible) {
    GameTile tile = board.getTile(point);
    TileType newType = visible ? tile.getType() : TileType.HIDDEN;
    getView().tileDidUpdate(point, newType);
    Iterator<Robot> iter = tile.iterator();
    while (iter.hasNext()) {
      Robot robot = iter.next();
      if (!robot.isDead()) {
        this.getView().robotVisibilityDidChange(robot.team(), robot.type(), visible);
      }
    }
  }

  /**
   * Update the set of tiles visible to the current team and that are in range of the current robot
   * based on their position.
   */
  private void updateVisibility() {
    // Board visibility is only shown when non-computer players are playing.
    if (currentRobot.hasScript()) {
      return;
    }
    Set<HexagonalPoint> newVisibleTiles = new HashSet<HexagonalPoint>();
    RobotTeam team = teams.get(currentTeam);
    Iterator<Robot> iter = team.iterator();
    while (iter.hasNext()) {
      Robot robot = iter.next();
      if (!robot.isDead()) {
        Set<HexagonalPoint> tiles = board.getVisibleTiles(robot.getPoint(), robot.range());
        newVisibleTiles.addAll(tiles);
      }
    }
    // Iterate through newly visible tiles and reveal them.
    Set<HexagonalPoint> oldVisibleTiles = visibleTiles;
    for (HexagonalPoint p : newVisibleTiles) {
      updateVisibility(p, true);
    }
    // Iterate through tiles that are no longer visible and hide them.
    for (HexagonalPoint p : oldVisibleTiles) {
      if (!newVisibleTiles.contains(p)) {
        updateVisibility(p, false);
      }
    }
    visibleTiles = newVisibleTiles;
  }

  /**
   * Update the set of tiles that are in range of the current robot.
   * 
   * @throws IllegalStateException Thrown if there is no current robot.
   */
  private void updateInRange() {
    if (currentRobot == null) {
      throw new IllegalStateException(
          "Attempted to update the in range tiles with no current robot set.");
    }
    inRangeTiles = board.getVisibleTiles(currentRobot.getPoint(), currentRobot.range());
  }

  /**
   * Starts the play of a any type of player
   */
  public void startPlay() {
    getView().playerPlayDidStart(currentTeam, currentRobot.type());
    getView().robotMovesDidUpdate(currentRobot.movesLeft());
    getView().robotShotsDidUpdate(currentRobot.shotsLeft());
    updateVisibility();
    updateInRange();
    startPlayTimer();
  }

  /**
   * Start play of computer player
   */
  private void startComputerPlay() {
    startPlay();
    interpreter.execute(currentRobot.getScript(), currentRobot);
  }

  private synchronized void startAction() {
    actionsInProgress++;
  }

  private synchronized void endAction() {
    actionsInProgress = Math.max(actionsInProgress - 1, 0);
  }

  /**
   * Checks that the robot ended it's play and that it's actions have completed
   */
  private void endPlayIfComplete() {
    if (hasEndedPlay && actionsInProgress == 0) {
      if (timer != null) {
        timer.stop();
      }
      // This flag is necessary to stop what would otherwise be an infinite recursion of
      // preparePlay, startPlay, and endPlay
      if (!hasEndedGame) {
        preparePlay();
      }
    }
  }

  public void endPlay() {
    hasEndedPlay = true;
    endPlayIfComplete();
  }

  public void endGame() {
    hasEndedPlay = true;
    hasEndedGame = true;
    endPlayIfComplete();

    WindowManager windowManager = this.getWindowManager();
    windowManager
        .presentController(new GameOverController(windowManager, configuration, getStats()));
  }

  private Collection<RobotStats> getStats() {
    LinkedList<RobotStats> collectionOfStats = new LinkedList<RobotStats>();
    for (RobotTeam team : teams.values()) {
      for (RobotType type : RobotType.values()) {
        collectionOfStats.add(team.getRobot(type).getStats());
      }
    }
    return collectionOfStats;
  }

  /**
   * Calculates amount of visible robots and returns how many are present
   * 
   * @return The number of other robots present within robots range
   */
  public int scan() {
    nearbyRobots = new LinkedList<Robot>();
    for (HexagonalPoint point : inRangeTiles) {
      GameTile tile = board.getTile(point);
      Iterator<Robot> iter = tile.iterator();
      while (iter.hasNext()) {
        nearbyRobots.add(iter.next());
      }
    }
    return nearbyRobots.size();
  }

  /**
   * Identify a robot nearby index. Utilized by the interpreter.
   */
  public IdentifyResponse identify(int index) {
    Robot robot = nearbyRobots.get(index);
    // Determine the distance and direction of the robot that is being identified relative to the
    // current robot.
    // This method has the potential to be implemented much more efficiently and will be refactored
    // if time permits.
    if (currentRobot.getPoint().equals(robot.getPoint())) {
      return new IdentifyResponse(robot.team(), 0, 0, robot.healthLeft());
    } else {
      for (int distance = 1; distance <= currentRobot.range(); distance++) {
        for (int direction = 0; direction < 6 * distance; direction++) {
          HexagonalPoint target = board.getShootPoint(currentRobot.getPoint(), distance,
              currentRobot.getDirection(), direction);
          if (robot.getPoint().equals(target)) {
            return new IdentifyResponse(robot.team(), distance, direction, robot.healthLeft());
          }
        }
      }
    }
    // The robot that is attempting to be identified isn't in visible range.
    // This may result from scanning, moving, then identifying, or identifying without scanning.
    throw new IllegalStateException("Attempted to identify robot that was not in range.");
  }

  /**
   * Returns whether the adjacent tile in the given direction is empty, occupied, or out of bounds.
   * 
   * @param direction The direction to be checked
   * @return The current state of the specified adjacent tile
   */
  public CheckResponse check(int direction) {
    HexagonalPoint point = this.board.getShootPoint(currentRobot.getPoint(), 1,
        currentRobot.getDirection(), direction);
    try {
      GameTile tile = board.getTile(point);
      return tile.isOccupied() ? CheckResponse.OCCUPIED : CheckResponse.EMPTY;
    } catch (IndexOutOfBoundsException ex) {
      return CheckResponse.OUT_OF_BOUNDS;
    }
  }

  public boolean sendMessage(RobotType type, Value<?> value) {
    RobotTeam team = teams.get(currentTeam);
    Robot robot = team.getRobot(type);
    return robot.receiveMessage(currentRobot.type(), value);
  }

  public void scriptInterrupted(Exception exception) {
    // In the future it would be nice to gracefully handle a crash, right now a script exception
    // causes the robot's play to end.
    exception.printStackTrace();
  }

  public void scriptFinished() {
    endPlay();
  }

  public void highlightTile(HexagonalPoint tilePoint) {
    // Support for highlighting tiles on mouse over if time permits.
  }

  /**
   * Mouse over was detected on a robot, show health bar.
   */
  public void highlightRobot(TeamColor color, RobotType type) {
    RobotTeam team = teams.get(color);
    Robot robot = team.getRobot(type);
    this.getView().healthBarWillShow(color, type, robot.healthLeft());
  }

  /**
   * Hide the health bar as the robot does not have a mouse over it.
   */
  public void unhighlightRobot() {
    this.getView().healthBarWillHide();
  }

  @Override
  public void windowClosed(WindowEvent e) {
    hasEndedPlay = true;
    hasEndedGame = true;
    endPlayIfComplete();
  }
}
