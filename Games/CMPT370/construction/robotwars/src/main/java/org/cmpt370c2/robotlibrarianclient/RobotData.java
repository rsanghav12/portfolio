package org.cmpt370c2.robotlibrarianclient;

import org.cmpt370c2.model.RobotType;

/**
 * Represents the robot data that is taken from the JSON from the RobotLibrarian. It contains robot
 * stats and the script for a CPU robot player.
 * 
 * @author C2
 */

public class RobotData {

  private String teamName = null;
  private RobotType robotType = null;
  private String scriptName = null;
  private String script = null;
  private long matches;
  private long wins;
  private long losses;
  private long executions;
  private long lived;
  private long died;
  private long absorbed;
  private long killed;
  private long moved;

  @Override
  public String toString() {
    return "{Name: " + scriptName + "; Type: " + robotType.toString() + "; Team: " + teamName
        + ";}";
  }

  private RobotData(RobotDataBuilder builder) {
    this.teamName = builder.teamName;
    this.robotType = builder.robotType;
    this.scriptName = builder.scriptName;
    this.script = builder.script;
    this.matches = builder.matches;
    this.wins = builder.wins;
    this.losses = builder.losses;
    this.executions = builder.executions;
    this.lived = builder.lived;
    this.died = builder.died;
    this.absorbed = builder.absorbed;
    this.killed = builder.killed;
    this.moved = builder.moved;
  }

  public String getTeamName() {
    return teamName;
  }

  /**
   * Used to determine whether or not a script can be run by a certain type of robot.
   * 
   * @param type The type of robot
   * @return True if the specified type can run it or false if it can't.
   */
  public boolean allowsRobotType(RobotType type) {
    return (this.robotType == type) || (this.robotType == null);
  }

  public RobotType getRobotType() {
    return robotType;
  }
  
  public String getScriptName() {
    return scriptName;
  }

  public long getMatches() {
    return matches;
  }

  public long getWins() {
    return wins;
  }

  public long getLosses() {
    return losses;
  }

  public long getExecutions() {
    return executions;
  }

  public long getLived() {
    return lived;
  }

  public long getDied() {
    return died;
  }

  public long getAbsorbed() {
    return absorbed;
  }

  public long getKilled() {
    return killed;
  }

  public long getMoved() {
    return moved;
  }

  public String getScript() {
    return script;
  }

  public static class RobotDataBuilder {
    private String teamName = null;
    private RobotType robotType = null;
    private String scriptName = null;
    private String script = null;
    private long matches;
    private long wins;
    private long losses;
    private long executions;
    private long lived;
    private long died;
    private long absorbed;
    private long killed;
    private long moved;

    public RobotDataBuilder(String teamName, String scriptName) {
      if (teamName == null || scriptName == null) {
        throw new IllegalArgumentException("Null argument was passed to RobotDataBuilder");
      }
      this.teamName = teamName;
      this.scriptName = scriptName;
    }

    RobotDataBuilder script(String script) {
      if (script == null) {
        throw new IllegalArgumentException("Null argument was passed to RobotDataBuilder script");
      }
      this.script = script;
      return this;
    }

    public RobotDataBuilder matches(long matches) {
      if (matches < 0) {
        throw new IllegalArgumentException(
            "Negative argument was passed to RobotDataBuilder matches");
      }
      this.matches = matches;
      return this;
    }
    
    public RobotDataBuilder type(RobotType type) {
      if (type == null) {
        throw new IllegalArgumentException("Null argument was passed to RobotDataBuilder type");
      }
      this.robotType = type;
      return this;
    }

    public RobotDataBuilder wins(long wins) {
      if (wins < 0) {
        throw new IllegalArgumentException("Negative argument was passed to RobotDataBuilder wins");
      }
      this.wins = wins;
      return this;
    }

    public RobotDataBuilder losses(long losses) {
      if (losses < 0) {
        throw new IllegalArgumentException(
            "Negative argument was passed to RobotDataBuilder losses");
      }
      this.losses = losses;
      return this;
    }

    RobotDataBuilder executions(long executions) {
      if (executions < 0) {
        throw new IllegalArgumentException(
            "Negative argument was passed to RobotDataBuilder executions");
      }
      this.executions = executions;
      return this;
    }

    public RobotDataBuilder lived(long lived) {
      if (lived < 0) {
        throw new IllegalArgumentException(
            "Negative argument was passed to RobotDataBuilder lived");
      }
      this.lived = lived;
      return this;
    }

    public RobotDataBuilder died(long died) {
      if (died < 0) {
        throw new IllegalArgumentException("Negative argument was passed to RobotDataBuilder died");
      }
      this.died = died;
      return this;
    }

    public RobotDataBuilder absorbed(long absorbed) {
      if (absorbed < 0) {
        throw new IllegalArgumentException(
            "Negative argument was passed to RobotDataBuilder absorbed");
      }
      this.absorbed = absorbed;
      return this;
    }

    public RobotDataBuilder killed(long killed) {
      if (killed < 0) {
        throw new IllegalArgumentException(
            "Negative argument was passed to RobotDataBuilder killed");
      }
      this.killed = killed;
      return this;
    }

    public RobotDataBuilder moved(long moved) {
      if (moved < 0) {
        throw new IllegalArgumentException(
            "Negative argument was passed to RobotDataBuilder moved");
      }
      this.moved = moved;
      return this;
    }

    public RobotData build() {
      return new RobotData(this);
    }
  }
}

