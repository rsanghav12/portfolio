package org.cmpt370c2.interpreter.words;

import org.cmpt370c2.interpreter.controller.ScriptDataSource;
import org.cmpt370c2.interpreter.controller.ScriptListener;
import org.cmpt370c2.interpreter.model.IntValue;
import org.cmpt370c2.interpreter.model.ScriptData;

/**
 * Pushes the data source's remaining health value to the stack.
 * 
 * @author allankerr
 *
 */
public class HealthLeftWord extends PredefinedWord {

  @Override
  public String getName() {
    return "healthLeft";
  }

  /**
   * Pushes the data source's remaining health value to the stack.
   * 
   */
  @Override
  public void execute(ScriptData data, ScriptListener listener, ScriptDataSource dataSource) {
    data.push(new IntValue(dataSource.healthLeft()));
  }
}
