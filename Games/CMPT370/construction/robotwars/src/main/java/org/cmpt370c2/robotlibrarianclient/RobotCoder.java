package org.cmpt370c2.robotlibrarianclient;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.cmpt370c2.model.RobotType;
import org.cmpt370c2.robotlibrarianclient.RobotData.RobotDataBuilder;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * This class converts data between a single JSONObject and RobotData. RobotCoder inherits from
 * JSONCoder.
 * 
 * @author C2
 */
class RobotCoder extends JSONCoder<RobotData> {

  private static final String UPDATE_REQUEST = "update";

  private static final String SCRIPT_KEY = "script";
  private static final String TEAM_KEY = "team";
  private static final String NAME_KEY = "name";
  private static final String WINS_KEY = "wins";
  private static final String LOSSES_KEY = "losses";
  private static final String LIVED_KEY = "lived";
  private static final String DIED_KEY = "died";
  private static final String ABSORBED_KEY = "absorbed";
  private static final String KILLED_KEY = "killed";
  private static final String DISTANCE_KEY = "distance";
  private static final String CLASS_KEY = "class";
  private static final String MATCHES_KEY = "matches";
  private static final String MOVED_KEY = "moved";
  private static final String CODE_KEY = "code";
  private static final String EXECUTIONS_KEY = "executions";

  /**
   * Converts RobotData to JSONObject, mainly utilized to send a request back to the Robot Librarian
   * in JSON format.
   * 
   * @param robotData the RobotData object containing the values to be put into a JSONObject.
   * @return The JSONObject containing information that was in the RobotData object.
   */
  @Override
  protected JSONObject encode(RobotData robotData) {
    if (robotData == null) {
      throw new IllegalArgumentException("Attempted to encode a null robot.");
    }
    // get needed info in RobotData into a Map for JSONObject
    Map<Object, Object> dataMap = new HashMap<Object, Object>();
//    String type = robotData.getRobotType().name().toLowerCase();
//    String robotType = type.substring(0, 1).toUpperCase() + type.substring(1);
    dataMap.put(CLASS_KEY, robotData.getRobotType().name());
    dataMap.put(TEAM_KEY, robotData.getTeamName());
    dataMap.put(NAME_KEY, robotData.getScriptName());
    dataMap.put(WINS_KEY, robotData.getWins());
    dataMap.put(LOSSES_KEY, robotData.getLosses());
    dataMap.put(LIVED_KEY, robotData.getLived());
    dataMap.put(DIED_KEY, robotData.getDied());
    dataMap.put(ABSORBED_KEY, robotData.getAbsorbed());
    dataMap.put(KILLED_KEY, robotData.getKilled());
    dataMap.put(DISTANCE_KEY, robotData.getMoved());

    Map<Object, Object> request = new HashMap<Object, Object>();
    request.put(UPDATE_REQUEST, new JSONObject(dataMap));
    return new JSONObject(request);
  }

  /**
   * Converts JSONObject values to RobotData values.
   * 
   * @param obj the JSONObject containing values to be added to a new instance of RobotData.
   * @return The RobotData object containing all the robot stats and script.
   * @exception InvalidValueException Thrown when an invalid value is received from the JSONObject.
   */
  @Override
  protected RobotData decode(JSONObject obj) throws MalformedResponseException {    
    obj = (JSONObject) obj.get(SCRIPT_KEY);
    
    RobotData decodedData = null;
    if (!obj.containsKey(CODE_KEY)) {
      decodedData = decodeBrief(obj);
    } else {
      decodedData = decodeScript(obj);
    }
    
    if (decodedData == null) {
      throw new MalformedResponseException("Attempted to decode a script that was neither full nor brief.");
    }
    return decodedData;
  }

  /**
   * Creates a RobotData from a json containing brief information on the robot.
   * 
   * @param briefScript The json containing brief information.
   * @return The RobotData object containing brief information.
   */
  private RobotData decodeBrief(JSONObject briefScript) throws MalformedResponseException {
    RobotData robotData = null;
    try {
      String teamName = (String) briefScript.get(TEAM_KEY);
      String typeName = ((String) briefScript.get(CLASS_KEY));
      String scriptName = (String) briefScript.get(NAME_KEY);
      long matches = (long) briefScript.get(MATCHES_KEY);
      long wins = (long) briefScript.get(WINS_KEY);
      long losses = (long) briefScript.get(LOSSES_KEY);
      
      RobotDataBuilder builder = new RobotDataBuilder(teamName, scriptName);
      if (typeName != null) {
        RobotType type = RobotType.valueOf(typeName.toUpperCase());
        builder.type(type);
      }
      robotData = builder
          .matches(matches)
          .wins(wins)
          .losses(losses)
          .build();      
      
    } catch (RuntimeException e) {
      throw new MalformedResponseException(
          "Failed to decode brief script due to it containing malformed data");
    }

    return robotData;
  }

  /**
   * Creates a RobotData from a json containing full information on the robot.
   * 
   * @param fullScript The json containing full information.
   * @return The RobotData object containing full information.
   */
  private RobotData decodeScript(JSONObject fullScript) throws MalformedResponseException {
    RobotData robotData;
    try {
      String teamName = (String) fullScript.get(TEAM_KEY);
      String typeName = ((String) fullScript.get(CLASS_KEY));
      String scriptName = (String) fullScript.get(NAME_KEY);
      long matches = (long) fullScript.get(MATCHES_KEY);
      long wins = (long) fullScript.get(WINS_KEY);
      long losses = (long) fullScript.get(LOSSES_KEY);
      long executions = (long) fullScript.get(EXECUTIONS_KEY);
      long lived = (long) fullScript.get(LIVED_KEY);
      long died = (long) fullScript.get(DIED_KEY);
      long absorbed = (long) fullScript.get(ABSORBED_KEY);
      long killed = (long) fullScript.get(KILLED_KEY);
      long moved = (long) fullScript.get(MOVED_KEY);
      
      StringBuilder scriptBuilder = new StringBuilder();
      JSONArray code = (JSONArray) fullScript.get(CODE_KEY);
      Iterator<?> iterator = code.iterator();
      while (iterator.hasNext()) {
        String codeLine = (String) iterator.next();
        scriptBuilder.append(codeLine + "\n");
      }
      
      RobotDataBuilder builder = new RobotDataBuilder(teamName, scriptName);
      if (typeName != null) {
        RobotType type = RobotType.valueOf(typeName.toUpperCase());
        builder.type(type);
      } 
      robotData = builder.script(scriptBuilder.toString())
          .matches(matches)
          .wins(wins)
          .losses(losses)
          .executions(executions)
          .lived(lived)
          .died(died)
          .absorbed(absorbed)
          .killed(killed)
          .moved(moved)
          .script(scriptBuilder.toString())
          .build();
      
    } catch (RuntimeException e) {
      throw new MalformedResponseException(
          "Failed to decode full script due to it containing malformed data.");
    }
    return robotData;
  }
}
