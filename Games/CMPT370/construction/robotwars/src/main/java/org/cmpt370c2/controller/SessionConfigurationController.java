package org.cmpt370c2.controller;

import org.cmpt370c2.model.RobotType;
import org.cmpt370c2.model.SessionConfiguration;
import org.cmpt370c2.model.TeamColor;
import org.cmpt370c2.model.TeamConfiguration;
import org.cmpt370c2.view.SessionConfigurationView;
import org.cmpt370c2.view.SessionConfigurationViewListener;

/**
 * Subclass of abstract controller. Controls the configuration of the game before it begins.
 * 
 * @author C2
 *
 */
public class SessionConfigurationController extends AbstractController<SessionConfigurationView>
    implements SessionConfigurationViewListener, LoadRobotListener {

  /**
   * The configuration object that is used to store the session configuration as the game master
   * configures it.
   */
  private SessionConfiguration configuration;

  /**
   * Constructs a new SessionConfiguration instance object
   * 
   * @param windowManager The current WindowManager instance.
   */
  public SessionConfigurationController(WindowManager windowManager) {
    super(windowManager);
    configuration = new SessionConfiguration();
  }

  @Override
  protected SessionConfigurationView createView() {
    return new SessionConfigurationView(this);
  }

  /**
   * Cancels the current session configuration and display the Main Menu
   */
  public void cancel() {
    WindowManager windowManager = this.getWindowManager();
    windowManager.presentController(new MainMenuController(windowManager));
  }

  /**
   * Completes the session configuration and starts the game controller
   */
  public void start() {
    WindowManager windowManager = this.getWindowManager();
    windowManager.presentController(new GameController(windowManager, configuration));
  }

  public void setNumberOfPlayers(int num) {
    configuration.setNumberOfPlayers(num);
  }

  public void setPlayDuration(int seconds) {
    configuration.setPlayDuration(seconds);
  }

  public void toggleComputerTeam(TeamColor team) {
    boolean isCurrentlySetAsComputer = configuration.getTeamConfiguration(team).isComputer();
    configuration.getTeamConfiguration(team).setComputer(!isCurrentlySetAsComputer);
  }

  /**
   * Load script in load robots controller.
   */
  public void loadScripts(TeamColor team, RobotType type) {
    TeamConfiguration teamConfig = configuration.getTeamConfiguration(team);
    WindowManager windowManager = new WindowManager(false);
    windowManager.presentController(new LoadRobotsController(windowManager, type, teamConfig));
  }

  public void loadRobotsDidFinish() {

  }

  public void loadRobotsDidCancel() {

  }
}
