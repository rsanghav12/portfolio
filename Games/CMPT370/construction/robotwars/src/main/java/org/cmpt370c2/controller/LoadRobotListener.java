package org.cmpt370c2.controller;

/**
 * Interface for loading robot scripts, allowing for the view to 
 * communicate with the controller.
 * @author C2
 *
 */
public interface LoadRobotListener {
  /**
   * User selected and downloaded script
   */
  public void loadRobotsDidFinish();
  
  /**
   * User cancelled out of script selection
   */
  public void loadRobotsDidCancel();
}
