package org.cmpt370c2.view;

/**
 * Interface that facilitates communication between the load robot view and load robot controller
 * through which events are passed.
 * 
 * @author C2
 *
 */
public interface LoadRobotViewListener {
  /**
   * Notifies the listener that the row at specified index has been selected.
   * 
   * @param index The index of the robot that the user has selected
   */
  public void robotSelected(int index);

  /**
   * Notifies the listener that a script was not selected.
   */
  public void cancel();
}
