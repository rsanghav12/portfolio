package org.cmpt370c2.interpreter.words;

import org.cmpt370c2.interpreter.controller.ScriptDataSource;
import org.cmpt370c2.interpreter.controller.ScriptListener;
import org.cmpt370c2.interpreter.model.ScriptData;

/**
 * Leaves the body of the currently executing loop.
 * 
 * @author allankerr
 *
 */
class LeaveWord extends PredefinedWord {

  @Override
  public String getName() {
    return "leave";
  }

  /**
   * Throws a LeaveLoopException to exit the body of the loop which is caught by the loop-word
   * executing the body.
   */
  @Override
  public void execute(ScriptData data, ScriptListener listener, ScriptDataSource dataSource) {
    throw new LeaveLoopException();
  }
}
