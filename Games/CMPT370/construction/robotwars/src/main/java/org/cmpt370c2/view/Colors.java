package org.cmpt370c2.view;

import java.awt.Color;

/**
 * Class is used to declare default colours used in buttons,
 * backgrounds, and tables.
 * @author c2
 */
final class Colors {

  static final Color BACKGROUND_COLOR = new Color(0xD9, 0xD9, 0xD9);
  
  static final Color DARK_COMPONENT_COLOR = new Color(170, 170, 170);

  static final Color MENU_BUTTON_BORDER_COLOR = new Color(130, 130, 130);  
}
