package org.cmpt370c2.view;

import org.cmpt370c2.model.RobotType;
import org.cmpt370c2.model.TeamColor;
import org.cmpt370c2.utilities.ImageType;

/**
 * This class is used to display the health bar above robots in the game view when they are moused
 * over.
 * 
 * @author allankerr
 *
 */
@SuppressWarnings("serial")
class HealthBarSprite extends ImageSprite {

  /**
   * The amount of remaining health to show in the health bar.
   */
  private int healthRemaining;

  /**
   * The team the robot is on that the health bar is for.
   */
  private TeamColor team;

  /**
   * The type of the robot the health bar is for. This may be SCOUT, SNIPER, or TANK.
   */
  private RobotType type;

  public int getHealthRemaining() {
    return healthRemaining;
  }

  public TeamColor getTeam() {
    return team;
  }

  public RobotType getType() {
    return type;
  }

  /**
   * Constructs a new health bar sprite for the specified robot.
   * 
   * @param team The team of the robot the health bar pertains to
   * @param type The type of the robot the health bar pertains to
   * @param healthRemaining The amount of health the given robot has remaining.
   */
  public HealthBarSprite(TeamColor team, RobotType type, int healthRemaining) {
    super("HealthBar" + type.getHealth() + healthRemaining, ImageType.PNG, 0, 0);
    this.healthRemaining = healthRemaining;
    this.team = team;
    this.type = type;
  }
}
