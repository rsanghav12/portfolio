package org.cmpt370c2.utilities;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;

/**
 * Load a file from local resources and return the its input stream.
 * @author rutviksanghavi, allankerr
 */
public class FileLoader {
    
  /**
   * Loads the file and returns inputstream
   * @param fileName The name of the file to retrieve
   * @param fileExtension The extension of the file to retrieve
   * @return The input stream of the desired file
   * @throws URISyntaxException Thrown if the file name results in an invalid URI
   * @throws IOException Thrown if the file cannot be found
   */
  public InputStream load(String fileName, String fileExtension) throws URISyntaxException, IOException {
    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    InputStream input = classLoader.getResourceAsStream(fileName + 
        (fileExtension != null ? fileExtension : ""));
    if (input == null) {
      throw new IOException("Unable to find " + fileName);
    }
    return input;
  }
}
