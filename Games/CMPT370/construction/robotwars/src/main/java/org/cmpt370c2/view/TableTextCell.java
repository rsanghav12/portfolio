package org.cmpt370c2.view;

import java.awt.Component;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableCellRenderer;

import org.cmpt370c2.utilities.FontLoader;
import org.cmpt370c2.utilities.FontType;

/**
 * Table cell to display a text. This wraps the text if longer than width, but does
 * not scroll if longer than height.
 * @author RutvikSanghavi
 */
@SuppressWarnings("serial")
class TableTextCell extends JPanel implements TableCellRenderer {

  private Border border[];

  private JTextArea textArea;

  public TableTextCell() {
    this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
    this.setOpaque(false);

    textArea = new JTextArea();
    textArea.setBorder(new EmptyBorder(Dimensions.PANEL_MARGINS, Dimensions.PANEL_MARGINS,
        Dimensions.PANEL_MARGINS, Dimensions.PANEL_MARGINS));
    textArea.setOpaque(false);
    Font font = textArea.getFont();
    try {
      font = FontLoader.getInstance().load(FontType.HELVETICA_NEUE_LT, Font.PLAIN,
          FontSize.TABLE_BUTTON_SIZE);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    textArea.setFont(font);
    textArea.setEditable(false);
    textArea.setWrapStyleWord(true);
    textArea.setLineWrap(true);
    this.add(textArea);

    border = new Border[3];

    // bottom right
    border[0] = BorderFactory.createCompoundBorder();
    border[0] = BorderFactory.createCompoundBorder(border[0],
        BorderFactory.createMatteBorder(0, 0, 1, 1, Colors.DARK_COMPONENT_COLOR));

    // bottom
    border[1] = BorderFactory.createCompoundBorder();
    border[1] = BorderFactory.createCompoundBorder(border[1],
        BorderFactory.createMatteBorder(0, 0, 1, 0, Colors.DARK_COMPONENT_COLOR));

    // right
    border[2] = BorderFactory.createCompoundBorder();
    border[2] = BorderFactory.createCompoundBorder(border[2],
        BorderFactory.createMatteBorder(0, 0, 0, 1, Colors.DARK_COMPONENT_COLOR));
  }

  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
      boolean hasFocus, int row, int column) {
    textArea.setText(value.toString());

    Boolean lastColumn = (table.getColumnCount() - 1) == column;
    Boolean lastRow = (table.getRowCount() - 1) == row;

    if (lastColumn) {
      if (!lastRow) {
        this.setBorder(border[1]);
      } else {
        this.setBorder(null);
      }
    } else {
      if (!lastRow) {
        this.setBorder(border[0]);
      } else {
        this.setBorder(border[2]);
      }
    }

    return this;
  }
}
