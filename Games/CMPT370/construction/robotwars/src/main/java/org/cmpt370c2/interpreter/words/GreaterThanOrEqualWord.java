package org.cmpt370c2.interpreter.words;

import org.cmpt370c2.interpreter.controller.ScriptDataSource;
import org.cmpt370c2.interpreter.controller.ScriptListener;
import org.cmpt370c2.interpreter.model.BoolValue;
import org.cmpt370c2.interpreter.model.IntValue;
import org.cmpt370c2.interpreter.model.ScriptData;

/**
 * Pops the two top-most integers from the stack and pushes true if the second from the top integer
 * is greater than or equal to the top integer or false if it isn't.
 * 
 * @author allankerr
 *
 */
class GreaterThanOrEqualWord extends PredefinedWord {

  @Override
  public String getName() {
    return "=>";
  }

  /**
   * Pops the two top-most integers from the stack and pushes true if the second from the top
   * integer is greater than or equal to the top integer or false if it isn't.
   * 
   * @throws IllegalStateException Thrown if either of the topmost values aren't integers.
   *
   */
  @Override
  public void execute(ScriptData data, ScriptListener listener, ScriptDataSource dataSource) {
    IntValue val1;
    IntValue val2;
    try {
      val1 = (IntValue) data.pop();
      val2 = (IntValue) data.pop();
    } catch (ClassCastException ex) {
      throw new IllegalStateException(
          "One of the arguments to '" + this.getName() + "' was not an integer.");
    }
    BoolValue result = new BoolValue(val2.getValue() >= val1.getValue());
    data.push(result);
  }
}
