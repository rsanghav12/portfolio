package org.cmpt370c2.robotlibrarianclient;

/**
 * The types of responses for the MockClientEndPoint to receive in the testing of
 * RobotLibrarianClient.
 *
 * @author C2
 */
public enum ResponseType {
  Malformed("Malformed"),
  NoResponse("NoResponse"),
  Expected("Expected");

  private String name;

  @Override
  public String toString() {
    return name;
  }

  private ResponseType(String name) {
    this.name = name;
  }
}
