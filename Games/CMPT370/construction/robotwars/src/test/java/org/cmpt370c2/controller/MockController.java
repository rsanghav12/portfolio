package org.cmpt370c2.controller;

import org.cmpt370c2.view.MockView;
import org.cmpt370c2.view.MockViewListener;

/**
 * Mocks a subclass of AbstractController, also used
 * to test the functionality of displaying interfaces
 * and communicating with views. Also used to test presentController
 * found in WindowManager.
 * 
 * @author RutvikSanghavi
 */
public class MockController extends MockAbstractController implements MockViewListener {

  private String titleString = null;
  
  public MockController(WindowManager windowManager) {
    super(windowManager);
  }

  @Override
  public MockView createView() {
    return new MockView(this);
  }
  
  public void setTitle(String title) {
    titleString = title;
  }
  
  public String getTitle() {
    return titleString;
  }
}
