package org.cmpt370c2.utilities;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.IOException;
import java.net.URISyntaxException;

public class FontLoaderTest {

  /**
   * Test that the FontLoader class can successfully load all fonts in the resources directory.
   */
  public void testExistingFonts() throws IOException, FontFormatException {
    for (FontType type : FontType.values()) {
      try {
        FontLoader.getInstance().load(type, Font.PLAIN, 25);
      } catch (URISyntaxException e) {
        e.printStackTrace();
      }
    }
  }
}
