package org.cmpt370c2.interpreter.words;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import org.cmpt370c2.interpreter.controller.CheckResponse;
import org.cmpt370c2.interpreter.controller.IdentifyResponse;
import org.cmpt370c2.interpreter.controller.ScriptDataSource;
import org.cmpt370c2.interpreter.controller.ScriptListener;
import org.cmpt370c2.interpreter.model.Value;
import org.cmpt370c2.model.RobotType;
import org.cmpt370c2.model.TeamColor;

/**
 * This class acts as a mock controller for testing the script controller. It implements the bare
 * minimum functionality to act as a ScriptListener and ScriptDataSource. The output from the script
 * is tracked to allow for comparing to expected values.
 * 
 * @author allankerr
 *
 */
public class MockController implements ScriptListener, ScriptDataSource {

  private int direction = 0;
  
  private int moveCount = 0;
  
  private int shotCount = 0;
  
  private List<Integer> checks = new LinkedList<Integer>();

  private int scanCount = 0;
  
  private int identifyCount = 0;
  
  private List<Value<?>> messages = new LinkedList<Value<?>>();
  
  private boolean interrupted = false;
  
  private boolean finished = false;
  
  private RobotType type;
  
  private CountDownLatch latch;
  
  public int getDirection() {
    return direction;
  }

  public int getMoveCount() {
    return moveCount;
  }

  public int getShotCount() {
    return shotCount;
  }

  public List<Integer> getChecks() {
    return checks;
  }

  public int getScanCount() {
    return scanCount;
  }

  public int getIdentifyCount() {
    return identifyCount;
  }

  public List<Value<?>> getMessages() {
    return messages;
  }

  public boolean isInterrupted() {
    return interrupted;
  }

  public boolean isFinished() {
    return finished;
  }

  public MockController(RobotType mockType, CountDownLatch latch) {
    this.type = mockType;
    this.latch = latch;
  }
  
  @Override
  public void turn(int direction) {
    this.direction = ((this.direction + direction) + 6) % 6;
  }

  @Override
  public void move() {
    moveCount++;    
  }

  @Override
  public void shoot(int direction, int distance) {
    shotCount++;    
  }

  @Override
  public int scan() {
    scanCount++;
    return 0;
  }

  @Override
  public IdentifyResponse identify(int index) {
    identifyCount++;
    return null;
  }

  @Override
  public CheckResponse check(int direction) {
    checks.add(direction);
    return CheckResponse.EMPTY;
  }

  @Override
  public boolean sendMessage(RobotType type, Value<?> value) {
    messages.add(value);
    return true;
  }

  @Override
  public void scriptInterrupted(Exception exception) {
    interrupted = true;    
    latch.countDown();
  }

  @Override
  public void scriptFinished() {
    finished = true;
    latch.countDown();
  }

  @Override
  public int health() {
    return type.getHealth();
  }

  @Override
  public int healthLeft() {
    return type.getHealth();
  }

  @Override
  public int moves() {
    return type.getMoves();
  }

  @Override
  public int movesLeft() {
    return Math.max(type.getMoves() - this.moveCount, 0);
  }

  @Override
  public int attack() {
    return type.getAttack();
  }

  @Override
  public int range() {
    return type.getRange();
  }

  @Override
  public TeamColor team() {
    return TeamColor.RED;
  }

  @Override
  public RobotType type() {
    return type;
  }
}
