package org.cmpt370c2.controller;

import org.cmpt370c2.view.AbstractView;
import org.cmpt370c2.view.MockView;

public abstract class MockAbstractController extends AbstractController<AbstractView<?>> {
  public MockAbstractController(WindowManager windowManager) {
    super(windowManager);
  }

  @Override
  protected AbstractView<?> createView() {
    return this.getView();
  }
  
  public static void main(String[] args) {
    try {
      // first test when a window is displayed with label saying 'Test Label'
      WindowManager testManager = new WindowManager();
      MockController testController = new MockController(testManager);
      testManager.presentController(testController);

      // sleep for a second to make sure
      // window popped up with correct title
      Thread.sleep(1000);

      // second test message passing between controller and view
      // confirm if getView() is non-null
      if(testController.getView() != null) {
        // change the title to 'Title should be changed'
        ((MockView)testController.getView()).setTitleText("Title should be changed");

        // test if message was passed back to the controller as well
        if(testController.getTitle() == null || 
            !testController.getTitle().equals("Title should be changed")) {
          System.out.println("AbstractController <getTitle()>:"
              + "Title is not same as the one changed, so message"
              + "was not passed back to the controller, test failed.");
        }
      } else {
        System.out.println("AbstractController <getView()>:"
            + "Message cannot be passed to View as it is null, test failed.");
      }

      // sleep for a second to make sure
      // window can pass message to view and back
      Thread.sleep(1000);
      
      // third test to verify if window manager's close function works
      testManager.close();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
