package org.cmpt370c2.view;

public interface MockViewListener {
  public void setTitle(String title);
}
