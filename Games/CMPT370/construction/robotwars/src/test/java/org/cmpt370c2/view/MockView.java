package org.cmpt370c2.view;

import java.awt.Component;

import javax.swing.JLabel;

/**
 * Mocks a subclass of the AbstractView
 * used for testing communication between controller and view
 * and also used to verify display of the interface.
 * 
 * @author RutvikSanghavi
 */
@SuppressWarnings("serial")
public class MockView extends AbstractView<MockViewListener> {
  
  /**
   * A test jlabel to make sure the view
   * can handle displaying components.
   */
  private JLabel testLabel;

  public MockView(MockViewListener listener) {
    super(listener);
    
    testLabel = new JLabel("Test Label");
    testLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
    this.add(testLabel);
  }

  /**
   * Function to test the communication between the controller
   * and the view through listeners.
   * 
   * @param text Text to change the testLabel title to
   */
  public void setTitleText(String text) {
    testLabel.setText(text);
    this.getListener().setTitle(text);
  }
}
