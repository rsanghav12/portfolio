package org.cmpt370c2.robotlibrarianclient;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.cmpt370c2.model.RobotType;
import org.cmpt370c2.robotlibrarianclient.RobotData.RobotDataBuilder;
import org.junit.Before;
import org.junit.Test;

/**
 * This class tests the RobotLibrarianClient to handle different response types.
 * The appropriate tests will be run with the response type set in MockClientEndPoint.
 * Asserts are used at the appropriate points that a response can reach.
 *
 * @author C2
 */
public class RobotLibrarianClientTest implements RobotLibrarianClientListener {

  /**
   * Robot librarian client utilized for testing
   */
  private RobotLibrarianClient testRobotLibrarianClient;
  
  /**
   * mock clientendpoint, it fakes malformed data, no response
   * and expected data to fully test the robot librarian client
   */
  private MockClientEndpoint mockClientEndpoint;
  
  /**
   * Variables to store amount to errors or
   * success in the testing
   */
  private int robotReceived;
  private int listReceived;
  private int robotUpdated;
  private int requestFailError;
  
  /**
   * Robot data used to test for updating robots in mock
   * clientendpoint
   */
  private RobotData testRobotData;

  /**
   * Run this before any @Test functions, it initializes
   * variables needed for the test
   */
  @Before
  public void init() {
    mockClientEndpoint = new MockClientEndpoint();
    testRobotLibrarianClient = new RobotLibrarianClient(this, mockClientEndpoint);
    testRobotData = new RobotDataBuilder("A1", "RobotName")
        .type(RobotType.SCOUT)
        .matches(10)
        .losses(1)
        .died(1)
        .wins(9)
        .build();
  }

  /**
   * Testing return of malformed data by the clientendpoint
   * this should test if robot librarian client can handle
   * it or not. We expect to get 3 errors in total one for
   * each request made.
   */
  @Test
  public void malformedTests() {
    mockClientEndpoint.setResponseType(ResponseType.Malformed);

    // added in a try catch loop because one of them may fail
    // as we expect a malformed data
    try{
      testRobotLibrarianClient.listRobots(false);
    } catch (Exception e) {
      requestFailError += 1;
    }
    
    try{
      testRobotLibrarianClient.downloadRobot("RobotName", "A1");
    } catch (Exception e) {
      requestFailError += 1;
    }
    
    try{
      testRobotLibrarianClient.updateRobot(testRobotData);
    } catch (Exception e) {
      requestFailError += 1;
    }
    
    // should expect 3 errors for 3 requests
    // if more than we did not successfully stop an error but continued at some point
    // if less than we overlooked some error we shouldn't
    assertTrue(requestFailError == 3);
  }

  /**
   * Testing return of expected data by the clientendpoint
   * this should test if the robot librarian client can function
   * and handle normal data. We expect no errors and an increment
   * of one for each successful request.
   */
  @Test
  public void expectedTest() {
    mockClientEndpoint.setResponseType(ResponseType.Expected);

    testRobotLibrarianClient.listRobots(false);
    testRobotLibrarianClient.downloadRobot("RobotName", "A1");
    testRobotLibrarianClient.updateRobot(testRobotData);

    // only one robot received
    assertTrue(robotReceived == 1);
    
    // only only list of robots received
    assertTrue(listReceived == 1);
    
    // only one robot updated
    assertTrue(robotUpdated == 1);
    
    // no errors
    assertTrue(requestFailError == 0);
  }

  /**
   * Testing return of no data by the clientendpoint
   * this should test if the robot librarian client can function
   * and handle no response. We expect 3 errors in total one for
   * each request made.
   */
  @Test
  public void noResponseTest() {
    mockClientEndpoint.setResponseType(ResponseType.NoResponse);

    testRobotLibrarianClient.listRobots(false);
    testRobotLibrarianClient.downloadRobot("RobotName", "A1");
    testRobotLibrarianClient.updateRobot(testRobotData);

    // should be expecting 3 errors for each request
    assertTrue(requestFailError == 3);
  }

  @Override
  public void robotReceived(RobotData robot) {
    robotReceived += 1;
  }

  @Override
  public void robotListReceived(List<RobotData> robots) {
    listReceived += 1;
  }

  @Override
  public void robotUpdated(boolean success) {
    robotUpdated += 1;
  }

  @Override
  public void requestFailed(String error) {
    requestFailError += 1;
  }
}
