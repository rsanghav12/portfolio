
package org.cmpt370c2.robotlibrarianclient;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * This class is a mock of the ClientEndpoint. It is designed to help test the RobotLibrarianClient.
 * It will send back a JSONObject depending on its set response type as if the end point had read
 * from the socket and parsed the result into a JSONObject. Malformed should contain a JSON with inappropriate
 * values, NoResponse should not return any thing, and Expected should return a valid JSON.
 *
 * @author C2
 */
public class MockClientEndpoint implements ClientEndpoint {

  /**
   * Response type for current client endpoint request
   */
  private ResponseType responseType;

  /**
   * Set the response type for current client endpoint request
   *
   * @param responseType ResponseType of request
   */
  public void setResponseType(ResponseType responseType) {
    this.responseType = responseType;
  }

  @SuppressWarnings("unchecked")
  @Override
  public void sendRequest(JSONObject obj, ClientEndpointListener listener) {
    switch (responseType) {
      
      // create and send malformed data
      case Malformed:
        Map<Object, Object> malformedMap = new HashMap<Object, Object>();
        malformedMap.put("team", "90001144");
        malformedMap.put("class", "Basic");
        malformedMap.put("code", "?whatiscode");
        malformedMap.put("name", "RobotName");
        malformedMap.put("lived", "lol");
        malformedMap.put("died", "nope");
        malformedMap.put("losses", "loops");

        JSONObject malformedJsonObj = new JSONObject(malformedMap);
        listener.receivedResponse(malformedJsonObj);
        break;
        
      // send error for timed out as the server endpoint will
      case NoResponse:
        listener.receivedError("Endpoint Timed out");
        break;
        
      // create and send expected data
      case Expected:
        Map<Object, Object> expectedMap = new HashMap<Object, Object>();
        expectedMap.put("team", "A1");
        expectedMap.put("class", "Scout");
        expectedMap.put("name", "RobotName");
        expectedMap.put("matches", (long)10);
        expectedMap.put("losses", (long)1);
        expectedMap.put("died", (long)1);
        expectedMap.put("wins", (long)9);
        expectedMap.put("executions", (long)9);
        expectedMap.put("moved", (long)9);
        expectedMap.put("lived", (long)9);
        expectedMap.put("died", (long)9);
        expectedMap.put("absorbed", (long)9);
        expectedMap.put("killed", (long)9);

        JSONArray jsonArray = new JSONArray();
        jsonArray.add("Test");
        jsonArray.add("My");
        jsonArray.add("Word");          
        expectedMap.put("code", jsonArray);

        Map<Object, Object> expectedJSONMap = new HashMap<Object, Object>();
        expectedJSONMap.put("script", new JSONObject(expectedMap));
        expectedJSONMap.put("update-is", "ok");

        Map<Object, Object> listJSONMap = new HashMap<Object, Object>();
        listJSONMap.put("script", new JSONObject(expectedMap));
        JSONArray jsonListArray = new JSONArray();
        jsonListArray.add(new JSONObject(listJSONMap));
        expectedJSONMap.put("list", jsonListArray);

        JSONObject expectedJsonObj = new JSONObject(expectedJSONMap);
        listener.receivedResponse(expectedJsonObj);
        break;
    }
  }
}
