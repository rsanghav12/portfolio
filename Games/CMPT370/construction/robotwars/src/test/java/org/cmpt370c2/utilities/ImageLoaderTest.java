package org.cmpt370c2.utilities;

import java.awt.FontFormatException;
import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.Assert.fail;

public class ImageLoaderTest {
  
  /**
   * Test that the ImageLoader can load an existing image from the resources directory.
   */
  public void testExistingImage() throws IOException, FontFormatException {
    try {
      ImageLoader.getInstance().load("BULLET", ImageType.PNG);
    } catch (URISyntaxException e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Test that the ImageLoader will throw an IOException if the image isn't found.
   */
  public void testNonexistentImage() {
    try {
      ImageLoader.getInstance().load("BULLET", ImageType.JPG);
    } catch (IOException | URISyntaxException e) {}
    fail();
  }
}
