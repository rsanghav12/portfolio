package org.cmpt370c2.utilities;

import java.util.HashSet;
import java.util.Set;

import org.cmpt370c2.utilities.HexagonalContainer;
import org.cmpt370c2.utilities.HexagonalPoint;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

/**
 * This class is used to test the hexagonal container. These tests focus around testing the
 * getPointsRange method which returns the set of points within a specified distance of a specified
 * point. Tests are done with varying distances and in different quadrants to ensure correctness.
 * 
 * @author jameschapman
 *
 */
public class HexagonalContainerTest {
  
  // hexagonal container to be used for range searches
  HexagonalContainer<HexagonalPoint> testContainer;
  
  @Before 
  public void initialize() {
    testContainer = new HexagonalContainer<HexagonalPoint>(7);
 }

  /**
   * Test that the expected points are in range when in the top right quadrant with a distance of 2.
   * All comparison points have been manually computed using pen and paper.
   */
  @Test
  public void topRightQuadTest() {
    
    // add all points that are within range of the first quadrant search
    HashSet<HexagonalPoint> returnedPoints = new HashSet<HexagonalPoint>();
    returnedPoints = (HashSet<HexagonalPoint>) testContainer.getPointsRange(new HexagonalPoint(1,5), 2);
    
    // correct set of all points within range
    Set<HexagonalPoint> knownPoints = new HashSet<HexagonalPoint>();
    knownPoints.add(new HexagonalPoint(1,5));
    knownPoints.add(new HexagonalPoint(3,13));
    knownPoints.add(new HexagonalPoint(3,14));
    knownPoints.add(new HexagonalPoint(3,15));
    knownPoints.add(new HexagonalPoint(3,16));
    knownPoints.add(new HexagonalPoint(3,17));
    knownPoints.add(new HexagonalPoint(2,0));
    knownPoints.add(new HexagonalPoint(2,1));
    knownPoints.add(new HexagonalPoint(1,1));
    knownPoints.add(new HexagonalPoint(1,2));
    knownPoints.add(new HexagonalPoint(1,3));
    knownPoints.add(new HexagonalPoint(2,7));
    knownPoints.add(new HexagonalPoint(2,8));
    knownPoints.add(new HexagonalPoint(2,9));
    knownPoints.add(new HexagonalPoint(2,10));
    knownPoints.add(new HexagonalPoint(2,11));
    knownPoints.add(new HexagonalPoint(1,0));
    knownPoints.add(new HexagonalPoint(0,0));
    knownPoints.add(new HexagonalPoint(1,4));
    
    // remove all known points from range search results.
    for (HexagonalPoint p : knownPoints) {
      returnedPoints.remove(p);
    }
    
    // if empty, all points were returned correctly.
    assertTrue(returnedPoints.isEmpty());
  }
  
  /**
   * Test that the expected points are in range when in the top left quadrant with a distance of 2.
   * All comparison points have been manually computed using pen and paper.
   */
  @Test
  public void topLeftQuadTest() {
    // add all points that are within range of the second quadrant search
    HashSet<HexagonalPoint> returnedPoints = new HashSet<HexagonalPoint>();
    returnedPoints = (HashSet<HexagonalPoint>) testContainer.getPointsRange(new HexagonalPoint(2,7), 2);
    
    // correct set of all points within range
    Set<HexagonalPoint> knownPoints = new HashSet<HexagonalPoint>();
    knownPoints.add(new HexagonalPoint(2,7));
    knownPoints.add(new HexagonalPoint(3,12));
    knownPoints.add(new HexagonalPoint(3,13));
    knownPoints.add(new HexagonalPoint(2,9));
    knownPoints.add(new HexagonalPoint(1,5));
    knownPoints.add(new HexagonalPoint(0,0));
    knownPoints.add(new HexagonalPoint(1,2));
    knownPoints.add(new HexagonalPoint(2,5));
    knownPoints.add(new HexagonalPoint(3,8));
    knownPoints.add(new HexagonalPoint(3,9));
    knownPoints.add(new HexagonalPoint(3,11));
    knownPoints.add(new HexagonalPoint(2,8));
    knownPoints.add(new HexagonalPoint(1,4));
    knownPoints.add(new HexagonalPoint(1,3));
    knownPoints.add(new HexagonalPoint(2,6));
    knownPoints.add(new HexagonalPoint(3,10));
    knownPoints.add(new HexagonalPoint(4,15));
    knownPoints.add(new HexagonalPoint(4,14));
    knownPoints.add(new HexagonalPoint(4,13));
    
    // remove all known points from range search results.
    for (HexagonalPoint p : knownPoints) {
      returnedPoints.remove(p);
    }
    
    // if empty, all points were returned correctly.
    assertTrue(returnedPoints.isEmpty());
  }
  
  /**
   * Test that the expected points are in range when in the bottom left quadrant with a distance of 2.
   * All comparison points have been manually computed using pen and paper.
   */
  @Test
  public void bottomLeftQuadTest() {
    // add all points that are within range of the second quadrant search
    HashSet<HexagonalPoint> returnedPoints = new HashSet<HexagonalPoint>();
    returnedPoints = (HashSet<HexagonalPoint>) testContainer.getPointsRange(new HexagonalPoint(3,5), 2);
    
    // correct set of all points within range
    Set<HexagonalPoint> knownPoints = new HashSet<HexagonalPoint>();
    knownPoints.add(new HexagonalPoint(3,5));
    knownPoints.add(new HexagonalPoint(3,7));
    knownPoints.add(new HexagonalPoint(2,5));
    knownPoints.add(new HexagonalPoint(1,2));
    knownPoints.add(new HexagonalPoint(1,1));
    knownPoints.add(new HexagonalPoint(2,2));
    knownPoints.add(new HexagonalPoint(3,3));
    knownPoints.add(new HexagonalPoint(3,6));
    knownPoints.add(new HexagonalPoint(2,4));
    knownPoints.add(new HexagonalPoint(2,3));
    knownPoints.add(new HexagonalPoint(3,4));
    knownPoints.add(new HexagonalPoint(4,6));
    knownPoints.add(new HexagonalPoint(4,7));
    knownPoints.add(new HexagonalPoint(5,7));
    knownPoints.add(new HexagonalPoint(4,8));
    knownPoints.add(new HexagonalPoint(4,9));
    knownPoints.add(new HexagonalPoint(5,9));
    knownPoints.add(new HexagonalPoint(5,8));
    knownPoints.add(new HexagonalPoint(4,5));
    
    // remove all known points from range search results.
    for (HexagonalPoint p : knownPoints) {
      returnedPoints.remove(p);
    }
    
    // if empty, all points were returned correctly.
    assertTrue(returnedPoints.isEmpty());
  }
  
  /**
   * Test that the expected points are in range when in the bottom right quadrant with a distance of 2.
   * All comparison points have been manually computed using pen and paper.
   */
  @Test
  public void bottomRightQuadTest() {
    // add all points that are within range of the second quadrant search
    HashSet<HexagonalPoint> returnedPoints = new HashSet<HexagonalPoint>();
    returnedPoints = (HashSet<HexagonalPoint>) testContainer.getPointsRange(new HexagonalPoint(2,0), 2);

    // correct set of all points within range
    Set<HexagonalPoint> knownPoints = new HashSet<HexagonalPoint>();
    knownPoints.add(new HexagonalPoint(2,0));
    knownPoints.add(new HexagonalPoint(3,2));
    knownPoints.add(new HexagonalPoint(2,2));
    knownPoints.add(new HexagonalPoint(1,1));
    knownPoints.add(new HexagonalPoint(0,0));
    knownPoints.add(new HexagonalPoint(1,5));
    knownPoints.add(new HexagonalPoint(2,10));
    knownPoints.add(new HexagonalPoint(3,16));
    knownPoints.add(new HexagonalPoint(2,11));
    knownPoints.add(new HexagonalPoint(3,17));
    knownPoints.add(new HexagonalPoint(3,0));
    knownPoints.add(new HexagonalPoint(3,1));
    knownPoints.add(new HexagonalPoint(2,1));
    knownPoints.add(new HexagonalPoint(4,22));
    knownPoints.add(new HexagonalPoint(4,23));
    knownPoints.add(new HexagonalPoint(4,0));
    knownPoints.add(new HexagonalPoint(4,1));
    knownPoints.add(new HexagonalPoint(4,2));
    knownPoints.add(new HexagonalPoint(1,0));
    
    // remove all known points from range search results.
    for (HexagonalPoint p : knownPoints) {
      returnedPoints.remove(p);
    }
    
    // if empty, all points were returned correctly.
    assertTrue(returnedPoints.isEmpty());
  }
  
  /**
   * Test that the expected points are in range with a distance of 1.
   * All comparison points have been manually computed using pen and paper.
   */
  @Test
  public void distOfOneTest() {
    // add all points that are within range of the second quadrant search
    HashSet<HexagonalPoint> returnedPoints = new HashSet<HexagonalPoint>();
    returnedPoints = (HashSet<HexagonalPoint>) testContainer.getPointsRange(new HexagonalPoint(3,16), 1);

    // correct set of all points within range
    Set<HexagonalPoint> knownPoints = new HashSet<HexagonalPoint>();
    knownPoints.add(new HexagonalPoint(3,16));
    knownPoints.add(new HexagonalPoint(3,17));
    knownPoints.add(new HexagonalPoint(2,11));
    knownPoints.add(new HexagonalPoint(2,10));
    knownPoints.add(new HexagonalPoint(3,15));
    knownPoints.add(new HexagonalPoint(4,21));
    knownPoints.add(new HexagonalPoint(4,22));
    
    // remove all known points from range search results.
    for (HexagonalPoint p : knownPoints) {
      returnedPoints.remove(p);
    }
    
    // if empty, all points were returned correctly.
    assertTrue(returnedPoints.isEmpty());
  }
  
  /**
   * Test that the expected points are in range with a distance of 3.
   * All comparison points have been manually computed using pen and paper.
   */
  @Test
  public void distOfThreeTest() {
    // add all points that are within range of the second quadrant search
    HashSet<HexagonalPoint> returnedPoints = new HashSet<HexagonalPoint>();
    returnedPoints = (HashSet<HexagonalPoint>) testContainer.getPointsRange(new HexagonalPoint(1,2), 3);

    // correct set of all points within range
    Set<HexagonalPoint> knownPoints = new HashSet<HexagonalPoint>();
    knownPoints.add(new HexagonalPoint(1,2));
    knownPoints.add(new HexagonalPoint(3,9));
    knownPoints.add(new HexagonalPoint(3,10));
    knownPoints.add(new HexagonalPoint(3,11));
    knownPoints.add(new HexagonalPoint(2,8));
    knownPoints.add(new HexagonalPoint(2,9));
    knownPoints.add(new HexagonalPoint(2,10));
    knownPoints.add(new HexagonalPoint(2,11));
    knownPoints.add(new HexagonalPoint(2,0));
    knownPoints.add(new HexagonalPoint(3,1));
    knownPoints.add(new HexagonalPoint(3,2));
    knownPoints.add(new HexagonalPoint(3,3));
    knownPoints.add(new HexagonalPoint(0,0));
    knownPoints.add(new HexagonalPoint(1,1));
    knownPoints.add(new HexagonalPoint(2,3));
    knownPoints.add(new HexagonalPoint(2,4));
    knownPoints.add(new HexagonalPoint(2,5));
    knownPoints.add(new HexagonalPoint(1,3));
    knownPoints.add(new HexagonalPoint(1,0));
    knownPoints.add(new HexagonalPoint(2,1));
    knownPoints.add(new HexagonalPoint(3,4));
    knownPoints.add(new HexagonalPoint(3,5));
    knownPoints.add(new HexagonalPoint(3,6));
    knownPoints.add(new HexagonalPoint(3,7));
    knownPoints.add(new HexagonalPoint(3,8));
    knownPoints.add(new HexagonalPoint(2,6));
    knownPoints.add(new HexagonalPoint(2,7));
    knownPoints.add(new HexagonalPoint(1,4));
    knownPoints.add(new HexagonalPoint(1,5));
    knownPoints.add(new HexagonalPoint(4,5));
    knownPoints.add(new HexagonalPoint(4,6));
    knownPoints.add(new HexagonalPoint(4,7));
    knownPoints.add(new HexagonalPoint(4,8));
    knownPoints.add(new HexagonalPoint(4,9));
    knownPoints.add(new HexagonalPoint(4,10));
    knownPoints.add(new HexagonalPoint(4,11));
    knownPoints.add(new HexagonalPoint(2,2));
    
    // remove all known points from range search results.
    for (HexagonalPoint p : knownPoints) {
      returnedPoints.remove(p);
    }
    
    // if empty, all points were returned correctly.
    assertTrue(returnedPoints.isEmpty());
  }
}
