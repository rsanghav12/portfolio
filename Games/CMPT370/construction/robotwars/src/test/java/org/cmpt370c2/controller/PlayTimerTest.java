/**
 * 
 */
package org.cmpt370c2.controller;


import org.junit.After;
import org.junit.Test;

/**
 * A class used to test the PlayTimer class
 * @author kentwalters
 *
 */
public class PlayTimerTest {
  PlayTimer testTimer;
  final static int testTime = 2;
  int increment = 0;

  @After
  public void tearDown() {
    testTimer.stop();
  }

  /**
   * Tests the PlayTimer by asserting that the return value decrements once per second
   * @throws InterruptedException
   */
  @Test
  public void testTimerDecrement() throws InterruptedException {
    testTimer = new PlayTimer(testTime, new PlayTimerListener(){
      @Override
      public void timerDidUpdate(int remainingTime) {
        assert(remainingTime  == ((testTime-1) - increment));
        increment++;
      }
    });
    testTimer.start();
    Thread.sleep(testTime*1000);
  }
}
