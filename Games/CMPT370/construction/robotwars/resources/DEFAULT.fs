: destroy!                     
       scan!                    
       dup 0 > if              
           dup 1 -              
           0 do                
               I identify!      
               team <> if      
                   shoot!       
                   leave        
               else            
               then             
           loop                
       else                     
       then ;                  
: play ( -- )                   
   begin                       
       destroy!                 
       2 random 1 - turn!       
       move!                   
       movesLeft 0 <>            
   until                       
       destroy! ;