Attendance: Rutvik Sanghavi, James Chapman, Allan Kerr, Kent Walters, Wai Chung Fung

To Do: Going over ground rules with the TA, and clarifying details about project.

Secretary - Rutvik
Caller - Allan
Moderator - Collective as a group

Things we went over with the TA:
    Established Ground Rules
    	- Code of Conducts (Criminal, & USASK)
    	- Attendance (if you don't attend you will forfeit your right to vote if dont have a valid reason)
    	- Bare minimum for decision will be made by majority (4) (more weight of the "coder" on the case)
    		- during times of exams or missing people the majority will be considered to be 3
    	- Be on time (if not on time, meeting will begin without you)
    		- extreme cases will have the chance re-evaluate decisions if desired
    	- Conflict resolution
    		- Vote for it (remove the conflicting people)
    		- Don't be a jerk
    	- Be respectfuly
    		- give everyone a chance to talk
    	- Deadlines
    		- check Slack for group updates (meetings, due dates, etc.)
    		- check almost every 24 hours
    	- Contributions
    		- everyone should be contributing roughly equally
    		- equally is determined by democracy
    		- basic knowledge of others work
    		
Clarifications:
    - Off-Campus server is good as long as the game runs.
    - For the entire project the process is more important than the end result.
    - Amount of players has to be greater than or equal to 2.
    - Requirment Doc is not entirely fixed (can be changed later on), just a means to establish things to be done.
    - Rotate individual roles to equalize contribution to group converstations.