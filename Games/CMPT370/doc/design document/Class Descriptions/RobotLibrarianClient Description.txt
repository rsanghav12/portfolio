
Class Descriptions:

RobotLibrarianClient

The RobotLibrarianClient class has two members and three operations. The two members are endpoint and listener. The member endpoint is necessary to listen for the ClientEndpoint and the member listener is required to send information back to the game controller. The operations are listRobots(), downloadRobot(), and updateRobot() and they are all called from the controller.  The listRobots() operation returns a list of robots, downloadRobot() operation returns a single robot script, updateRobot sends the robot stats to the Robot Librarian on the moodle server. The RobotLibrarianClient also implements the ClientEndpointListener interface which contains two operations.  The operation receivedResponse() is called when a response is received and the operation receivedError() is called when there is an error.


Abstract JSONCoder
The abstract class JSONCoder contains the abstract for encode() and decode() and has two subclasses: RobotListCoder<List<RobotData>> and RobotCoder<RobotData>. They are responsible for converting between JSONObject and their respective objects.


RobotListCoder<List<RobotData>>

The RobotListCoder class has no members and two operations inherited from JSONCoder. The operations are encode()and decode() which performs functionality as RobotCoder. The operation encode() involves transcribing <List<RobotData>> to JSONObject and decode() involves transcribing JSONObject to <List<RobotData>>.


RobotCoder<RobotData>

The RobotListCoder class has no members and two operations inherited from JSONCoder. The operations are encode()and decode() which performs similar functionality as RobotListCoder. The operation encode() involves transcribing <RobotData> to JSONObject and decode() involves transcribing JSONObject to <RobotData>.


ClientEndpoint

The class ClientEndpoint contains one member and one operation. The member is listener which allows sending the data back to the RobotLibrarianClient or any other class that calls it once it is received from the robot librarian. The operation is sendRequest() and it is responsible for sending a JSONObject to the given URL and receiving a response.



RobotData

RobotData has fourteen members that contains all of the transcribed data taken from a JSONObject for robot scripts.
The members are:
- scriptName: Name of the script
- script: The actual script containing the forth formatted robot code.
- teamName: Name of the team
- type: Type of the robot (tank, sniper, scout)
- name: Name of the robot
- matches: Total number of matches played.
- wins: Total number of wins.
- losses: Total number of losses
- executions: Number of times the script was used in a match.
- lived: Total number of times survived a match
- died: Total number of deaths
- absorbed: Total damage absorbed
- killed: Total number of kills
- moved: Total distance the robot has moved