
a) Main Menu
	This diagram depicts the main menu view which contains two buttons: play game and exit. The “Play Game” button takes the user to the game configuration view while the “Exit” button closes the window and exits the system.

b) Session Configuration Menu
	This diagram depicts the session configuration view which is trigged by clicking start game from the main menu. This menu allows the user to select the number of players, select which players are controlled by computers, load scripts for each computer player to run, and select the maximum time players are give to make a play. After configuring the game settings start can be clicked to start the game or cancel to go back to the main menu view
	
c) Load Robots
	This diagram depicts the load robot view which is presented in a new window when load is clicked from the session configuration view. This view contains a list of robots available on the Moodle Robot Server. Selecting a robot causes it to be added to the session configuration and the window to be closed. Clicking cancel will also return the user to session configuration. If the Moodle Robot Server is unreachable or an invalid response is received an error message is displayed.

f) Game Over
	This diagram depicts the game over view which is displayed when a game ends. Within the view there is a panel containing the winning team and the statistics for each team’s robots. This view contains two buttons: finish game and rematch. The “Finish Game” button brings the user to the main menu while the “Rematch” button starts a new game with the same configuration as the last one.
	
e) Game - Player View
	This diagram depicts the game view which is displayed when a game is running. The view consists of the hexagonal game board, end game button and side panel. The game board provides visual feedback while controlling robots and the ability to spectate games played between computer players. The “End Game” button allows for the game to be ended prematurely. The information displayed in the side panel is detailed on the next page.

f) Game View Side Panels
	START PLAY PANEL
	The start play panel is displayed in the game view when it is a human player’s play. This panel contains the start play button which gives the player control of their robot when clicked. While this panel is displayed all robots are hidden.
	
	PLAY PANEL
	The play panel is displayed in the game view when a human player has started their play. This panel contains information on the players robot’s as well as information on remains shots, moves, and time. This panel also contains an end play button which allows the player to end their play before time runs out.
	
	SPECTATE PANEL
	The spectate panel is displayed in the game view when a game is being played between only computer players. This panel provides information on the actions being performed by the players throughout the game. When the spectate panel is displayed the robots for all teams are visible.