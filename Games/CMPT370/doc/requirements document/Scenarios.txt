
User Interface
Figure U1-1 Main Menu
Figure U1-2 Game Session Configuration View
Figure U1-3 Load Script View
Figure U1-4 Game Over View

Figure U2-1 Start Play View
Figure U2-2 Human Player Play View
Figure U2-3 Observer Game View





2.2.1. Start Game:

The start game scenario begins when the user clicks the "Start Game" button from the main menu. When the button is clicked the system displays the game session configuration view allowing the user to configure the new game session. First the user selects the number of players to be two, three or six. Next the user selects the number of computer players which can be from zero to the selected number of players. For each computer player the user can load a robot script to control the computer's robots. Lastly, the user chooses the number of seconds the players have to perform their play. Once the user is done with configuration the user starts the game session by clicking the "Start" button.

At any point during game session configuration the user can click the "Cancel" button causing the system to discard the new game session and return to the main menu.


Pre conditions:
1. The user is at the system main menu (see Figure U1-1).

Start Game Primary Scenario:
1. The use case begins when the user clicks the "Start Game" button.
2. The system displays the game session configuration view (see Figure U1-2).
3. The user selects the number of players choosing from two, three or six.
4. The user selects the number of computer players from zero to the number of players.
5. For each computer player the user loads a robot script to control the players robots (see Scenario 2.2.2.).
6. The user selects the maximum play duration time in seconds for the players.
7. The user clicks the "Start" button.
8. The system starts the game session with the user's specified configuration.
9. The use case ends.

Post conditions:
1. The system is running a new game session with the user's specified configuration.

Error conditions:
3-6.a1 The user clicks the "Cancel" button.
3-6.a2 The system displays the main menu.
3-6.a3 The use case cancels.





2.2.2. Load Robot Script:

The load robot script scenario begins when the user clicks a computer player's "Load" button from the session configuration view. The robot librarian is asked for a list of robots and a new window is displayed. The list of robots provided by the robot librarian is displayed by the system in the new window. At this point the user selects a robot for the robot librarian to download. Lastly, the user clicks the "Done" button which adds the downloaded robot to the current game session configuration.

The user can close the new window displayed by the system at any point causing the system to return focus to the game session configuration view.


Pre conditions:
1. The user is at the game session configuration view (see Figure U1-2).

Load Robot Script Primary Scenario:
1. The use case begins when the user clicks the "Load" button for a computer player.
2. The system displays a new window to display robots in.
3. A request is sent to the robot librarian asking for a list of robots (see Scenario 2.2.5.).
4. The system displays the list of robots in the new window.
5. The user selects a robot to download.
6. The robot librarian downloads the robot (see Scenario 2.2.6.).
7. The user clicks the "Done" button.
8. The system closes the window.
9. The system adds the robot script to the current game session configuration.
10. The use case ends.

Post conditions:
1. A robot script has been added to the game session configuration for a selected computer player.

Alternate Scenarios:
6.a1 The robot librarian failed to download the robot.
6.a2 An error message is displayed.
6.a3 The use case returns to the primary scenario at step 5.

Error conditions:
2-6.a1 The user clicks the "X" button.
2-6.a2 The system closes the window.
2-6.a3 The use case cancels.

3.b1 The robot librarian failed to get a list of robots.
3.b2 An error message is displayed.
3.b3 The system closes the window.
3.b4 The use case cancels.





2.2.3. End game:

The end game scenario begins when the system is running a game session and the user clicks the "End Game" button. At this point, the system interrupts the current player's play and displays the game over view.


Pre conditions:
1. The system is running an existing game session.

End game Primary Scenario:
1. The use case begins when the user clicks the "End Game" button.
2. The system interrupts the current player's play.
3. The system displays the game over view (see Figure U1-4).
4. The use case ends.

Post conditions:
1. The system has the game over view (see Figure U1-4) displayed.





2.2.4. Exit:

This scenario begins when the user clicks the closes the window the system is running in. When this occurs the system ends any current game session exits.

Pre conditions:
1. None

Exit Primary Scenario:
1. The use case begins when the user clicks the window's "X" button.
2. The system closes the running window.
3. The system exits.
4. The use case ends.

Post conditions:
1. The running window has been closed.
2. The system is no longer running.





2.2.5 Enumerate Robots: 

This scenario begins when the robot librarian receives a request for a list of all robots. At this point, the robot librarian asks the Moodle server for the list. The list of robots is downloaded from the server. Once the list of robots has been downloaded the robot librarian sends the list of robots to the requester.


Pre conditions:
1. None

Enumerate Robots Primary Scenario:
1. The robot librarian receives a request for a list of all robots.
2. The robot librarian asks the Moodle server for the list of all robots.
3. The robot librarian receives the list of all robots from the server.
4. The robot librarian sends the list back to the requester.
5. The use case ends.

Post conditions:
1. The requester has received a valid list of all robots.

Error conditions:
3.a1 The robot librarian fails to receive a valid response from the server.
3.a2 The robot librarian tells the requester that an exception occurred.
3.a3 The use case fails.





2.2.6 Download Robot: 

This scenario begins when the robot librarian receives a request for a robot. When this occurs, the robot librarian asks the Moodle server for the robot by name causing the file associated with the robot to be downloaded from the server. Once the file is downloaded the robot librarian returns the robot file to requester.

Pre conditions:
1. None

Download Robot Primary Scenario:
1. The robot librarian receives a request for a robot by name.
2. The robot libraries asks the Moodle server for the file associated with the robot.
3. The robot librarian receives the robot file from the server.
4. The robot librarian returns the robot file to the requester.
5. The use case ends.

Post conditions:
1. The robot file has been returned to the requester.

Error conditions:
3.a1 The robot librarian fails to receive a valid robot file from the server.
3.a2 The robot librarian tells the requester an exception occurred.
3.a3 The use case fails.





2.2.7 Update Robot Stats: 

This scenario begins when the robot librarian receives a request to update a robots game statistics. When this request is received, the robot librarian packages the data in the format expected by the Moodle server. Next the packaged data is sent to the server. At this point the robot librarian waits for a response from the server saying the update was successful.


Pre conditions:
1. None

Update Robot Stats Primary Scenario:
1. The robot librarian receives a request to update a robots game statistics.
2. The robot librarian packages the data into the format expected by the Moodle server.
3. The robot librarian sends the data to the Moodle server.
4. The robot librarian waits for a response from the server saying the update was successful.
5. The use case ends.

Post conditions:
1. The robot statistics have been updated on the Moodle server.

Error conditions:
4.a1 The robot doesn't receive a successful response from the server.
4.a2 The robot librarian tells the requester an exception occurred.
4.a3 The use case fails.





2.2.8. Move robot:

The move robot scenario begins when a game session is running on the system, it is the player's turn, and the player gives movement input. If the player has no remaining moves the input is ignored. Otherwise, the robot being controlled by the player is rotated to face the direction specified by the input. Once the rotation is complete the robot moves one tile in the direction it is facing. Lastly, the system removes one from the player's remaining moves.


Pre conditions:
1. A game session is currently running on the system.
2. It must be the play of the player attempting to move the robot.

Move Robot Primary Scenario:
1. The use case begins when the system receives movement input from the player.
2. The system rotates the robot to face the direction corresponding to the movement input.
3. The system moves the robot a distance of one hexagonal tile in the direction it is facing.
4. The system decrements the player's move count by one.
5. The use case ends.

Post-conditions:
1. The robot has moved one tile in the direction specified by the player's movement input.

Error conditions:
1a1. The player has no remaining moves.
1a2. The system ignores the player's movement input.
1a3. The use case cancels.


2.2.8a. Human Move Robot - Extension Point

This scenario begins when the player presses one of the movement keys on the keyboard. Robot movement is controlled using 'A'to move west, 'W' to move north west, 'E' to move north east, 'D' to move east, 'X' to move south east, and 'Z' to move south west. At this point the movement input is sent to the system allowing the scenario to return to the move robot scenario.

1a1. The player presses one of: A'to move west, 'W' to move north west, 'E' to move north east, 'D' to move east, 'X' to move south east, or 'Z' to move south west.
1a2. The use case rejoins the move robot scenario (see Scenario 2.2.8.) at 1.


2.2.8b. Computer Move Robot - Extension Point

This scenario begins when movement input is received from the script the computer player is running. At this point the movement input is sent to the system allowing the scenario to return to the move robot scenario.

1b1. The use case begins when movement input is received from the script the computer player is running.
1b2. The use case rejoins the move robot scenario (see Scenario 2.2.8.) at 1.





2.2.9. Shoot robot:

This scenario begins when an existing game session is running on the system, it is the player's turn, and the player gives shoot input. If the player has no remaining shots or the selected tile is out of range the input is ignored. Otherwise, the player's robot is rotated to face in the direction of the shoot input. Once the rotation is finished a shot is fired at the tile. When this occurs, the system decreases the health of all robots on the targeted tile by the damage value of the currently controlled robot. If the health of any of the robots is less than or equal to zero than that robot is considered to be dead and removed from play. Lastly, the system decreases the player's remaining shot count by one. 

If shooting the tile results in all other teams having no robots left then the system current player is declared winner. When this occurs the system displays the game over view along with the statistics for each tank in the game.

If the shot resulted in the currently controlled robot being removed then the system ends the turn.


Pre conditions:
1. A game session is currently running on the system.
2. It must be the play of the player attempting to move the robot.

Shoot Robot Primary Scenario:
1. The use case begins when the system receives shoot input from the player.
2. The system rotates the robot to face the direction corresponding to the shoot input.
3. The system fires a shot at the targeted tile.
4. The system decreases the health of all robots on the tile by the damage value of the currently controlled robot.
5. The system removes any robots with health less than or equal to zero.
6. The system decrements the player's remaining shots by one.
7. The use case ends.

Post conditions:
1. The health of all robots in that space is subtracted by the attack rating of the shooting robot.
2. If the health of the robots in that space is equal to or less than zero than it is considered to be dead and removed from the play.
3. The player's remaining shots has decreased by one.

Alternate Scenarios:
5.a1 There are no remaining robots on any of the other teams.
5.a2 The system displays the game over view (see Figure U1-4).
5.a3 The use case ends.

5.b1 The currently controlled robot's health dropped to less than one.
5.b2 The system ends the player's turn (see Scenario 2.2.11.).
5.b3 The use case ends.

Error conditions:
1a1. The players robot is not in visible range of the targeted tile.
1a2. The system ignores the player's shoot input.
1a3. The use case cancels.

1b1. The player has no remaining shots.
1b2. The system ignores the player's shoot input.
1b3. The use case cancels.


2.2.9.a. Human Shoot Robot - Extension Point

This scenario begins when the player clicks on a tile within the game board. At this point the shoot input associated with the clicked tile is sent to the system allowing the scenario to return to the shoot robot scenario.

1a1. The user clicks a tile within the game board.
1a2. The use case rejoins the shoot robot scenario (see Scenario 2.2.9.) at 1.


2.2.9.b. Computer Move Robot - Extension Point

This scenario begins when shoot input is received from the script the computer player is running. At this point the shoot input is sent to the system allowing the scenario to return to the move robot scenario.

1b1. The use case begins when shoot input is received from the script the computer player is running.
1b2. The use case rejoins the shoot robot scenario (see Scenario 2.2.9.) at 1.





2.2.10. Start play:

The start play scenario begins when the system has prepared the human players turn and the "Start Play" button is clicked. At this point the system reveals the players robots on the board along with the viewable range around them. Next, the system starts the play timer. Lastly, the system starts listening for shoot input and movement input from the human player.


Pre conditions:
1. The human player's turn is up next in the game session.

Start Play Primary Scenario
1. The human player clicks the "Start Play" button.
2. The system reveals the players robots on the board and the viewable area around them.
3. The system starts the play timer.
4. The system starts listening for shoot and movement input from the human player.
5. The use case ends.

Post conditions:
1. The human player's play is now in progress.
2. The system is listing for shoot and movement input.






2.2.11. End Play:

The end play scenario begins when a game session is running on the system, it is a player's turn, and the system receives a message to end the play. At this point the system ends the current players play and queues the next play to take place.

Pre-conditions:
1. It is currently a players play.

End Play Primary Scenario:
1. The use case begins when the system receives a message to end the current player's play.
2. The system ends the current player's play.
3. The system prepares the next player's play.
4. The use case ends.

Post-conditions:
1. The Players play has ended.




2.2.12. Scan robot:

The scan robot scenario begins when a scan input is received from the script the computer player is running.  Then the system gives details on any tiles that were asked for within visible range of the robot the player is currently controlling.

Pre-conditions:
1. A game session is currently running on the system.
2. It is currently a computer player's play.

Scan Robot Primary Scenario:
1. The computer player asks the system for details on any tiles within visible range of the robot the player is currently controlling.
2. The system gives the details.
3. The use case ends.

Post-conditions:
1. The computer player has details on all tiles within visible range of the robot.



2.2.13. Message robot:

The message robot scenario begins when a message input is received from the script the computer player is running.  The system determines from the input if the robot wants to send a message, wants to check for a message, or wants to receive the next message and performs the appropriate action.

Pre-conditions:
1. A game session is currently running on the system.
2. It is currently a computer player's play.

Message Robot Primary Scenario:
1. The computer player requests the system to handle messages going out or coming in for the current robot the player is controlling.
2. The system handles or checks for a message as requested.
3. The use case ends.

Post-conditions:
1. A message is sent, received or checked if one was received.



2.2.11.a. Human End Play - Extension Point

This scenario begins when the human player clicks the end play button. At this point the end play message is sent to the system allowing for the system to return to the end play scenario.

1a1. The use case begins when the user user clicks the end play button.
1a2. The use case rejoins the end play scenario (see Scenario 2.2.11.) at step 1.


2.2.11.b. Computer End Play - Extension Point

This scenario begins when the end play message is sent from the script the computer player is running. The end play message is sent to the system allowing for the system to return to the end play scenario.

1b1. The use case begins when the end play message is sent from the script the computer player is running.
1b2. The use case rejoins the end play scenario (see Scenario 2.2.11.) at step 1.




Finish Game:

The finish game scenario begins when a user clicks the "Finish Game" button from the Game Over screen. The system returns the user to the Main Menu screen.

Pre conditions:
1. The Game Over screen is being displayed.

Finish Game Scenario:
1. The use case begins when the user clicks the "Finish Game" button.
2. The system returns the user to the Main Menu screen.
3. The use case ends.

Post conditions:
1. The Main Menu screen is displayed. 





Rematch:

The rematch scenario begins when a user clicks the "Rematch" button from the Game Over screen. The system restarts a game with the exact same configurations as the game that resulted in the Game Over screen being displayed. 

Pre conditions:
1. The Game Over screen is being displayed.

Finish Game Scenario:
1. The use case begins when the user clicks the "Rematch" button.
2. The system restarts a game with the exact same configurations as the game that resulted in the Game Over screen being displayed. 
3. The use case ends.

Post conditions:
1. A new game is started.















