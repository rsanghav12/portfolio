The RobotWars system will utilize a number of different technologies throughout it's planning, designing, coding, and testing phases. Broadly, these technologies can be grouped into two categories: hardware and software. Hardware describes the physical machines and their capabilities that RobotWars system will run on. Software is a broader group of technologies consisting of languages, tools, libraries, and applications that RobotWars system will make use of. This section describes both the hardware and the software requirements of RobotWars system.

Throughout the project, files will be managed using a version control system called git. Specifically, the files will be stored on a git repository hosted on the University of Saskatchewan's Computer Science Department gitlab server. This system manages multiple team members editing files, and provides tracking of each member's contributions. 

The RobotWars system itself will be written in Java, a general-purpose object-oriented programming language that all team members are experienced in. The project will also make use of a Forth-based control language, RoboSport370, to program the robots in the game. JavaScript Object Notation (JSON) will be used to pass information about robots into the system. JSON is a lightweight data-interchange format ideal to parsing and generating. A Java library will be used to handle JSON within the system, as Java does not provide this functionality natively.  

Modern software systems utilize integrated development environments (IDEs) to allow programmers to write and edit source code, build the project, and debug the project within one application. RobotWars will rely on the Eclipse IDE, a widely used open-source IDE well suited to Java development. 

An advantage of using both the Java programming language and the Eclipse IDE is that neither are operating system dependent. Therefore, RobotWars will be able to be run on any operating system capable of running a Java Runtime Environment (JRE). Within the Computer Science labs, this includes machines running OSX, Linux Mageia, and Windows. 

The hardware requirements of RobotWars will be minimal relative to the capabilities of a modern consumer grade desktop computer. As discussed, any computer with the hardware capabilities to run a JRE will be capable of running RobotWars. 

