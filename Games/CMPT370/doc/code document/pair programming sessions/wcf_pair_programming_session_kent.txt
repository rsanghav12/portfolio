Date: November 17, 2016
Programmers: Wai Chung Fung and Kent Walters
Goal: Complete controllers parts that use robot librarian client

What you learned/why was it valuable:

This was my first pair programming session and experience.  From the experience, working in pairs greatly increased focus since we had to continually discuss what we were doing and how to do it.  Particularly when being the observer or navigator, more attention could be directed to making sure the code was correct and there were no bugs.  The session was useful due to the combination of our expertise. Kent had worked on the controllers and I worked on the robot librarian client.  Our previous knowledge aided us on working on the controllers that interacted with the robot librarian client, specifically the LoadRobotsController and GameOverController.  It was also good for confidence and team building since we overcame any challenges or uncertainties together.


Difficulties/obstacles:

Some difficulties we encountered were non-existing classes that we just created stubs for and uncertainty of variables and methods in the model package.  We needed to use them but had never looked through those classes thoroughly before.  This slowed some progress down as there was often switching back and forth to check the classes.  Although there was improvement in concentration and additional eyes and brains, I don't feel there was a 100% increase in productivity.

What you actually accomplished:

We were able to implement the GameOverController for sending a request and handling a received confirmation response.  I also gained a greater understanding of how the rest of the code worked since I had not worked on those parts previously.


Other difficulties:
An obstacle was working on a single laptop and we could not connect to a larger screen.




PROOFREAD VERSION:

This was my first pair programming session and experience. From the experience, working in pairs greatly increased focus since we had to continually discuss what we were doing and how to do it. Particularly when acting as the observer, more attention could be directed to making sure the code was correct and that there were no bugs. The session was also useful due to the combination of our expertise. Kent had worked on the controllers and I worked on the robot librarian client. Our previous knowledge aided us on working on the controllers that interacted with the robot librarian client, specifically the LoadRobotsController and GameOverController. It was also good for confidence and team building since we overcame any challenges or uncertainties together. Some difficulties we encountered were non-existing classes that we just created stubs for previously so which needed to flesh out before continuing, as well as uncertainty of variables and methods in the model package. We needed to use them but had never looked through those classes thoroughly before. This slowed some progress down as there was often switching back and forth to check the classes. Although there was improvement in concentration and additional eyes and brains, I don't feel like there was a 100% increase in productivity. We were able to implement the GameOverController for sending a request and handling a received confirmation response. I also gained a greater understanding of how the rest of the code worked since I had not worked on those parts previously.

