Date: Tuesday, November 15
Programmers: Kent Walters & Allan Kerr
Goal: Finish GameController class

What you learned/why was it valuable:

I found this pair programming session quite valuable as Allan is an extraordinarily proficient programmer and one learns a lot working alongside him. In general, I thought things went quickly as tiny mistakes or missed lines of code were often caught by the observer. As well, deciding what to work on next or even where to scroll to in the document was always faster when someone was there prompting you. In fact, even just having someone else there with a computer open to the same code was valuable because they can look things up for you or reference code that you're trying to interact with.

Difficulties/obstacles:

Working on a single, small notebook is slightly challenging; future pair programming sessions would benefit from booking a room with a display. 

Of course there is also the inherent fact that unlike when programming alone, you must devote a certain amount of brain power to processing the social sitation you are in during a pair programming session. However, as you get more comfortable with your partner and the process this social processing decreases and you end up gaining the advantage of having two brains.

What you actually accomplished:

We implememnted many of the methods in GameController.java, including preparePlay(), tileWasShot(), isAllComputerPlayers(), updateVisibility(), and getStats(). 
