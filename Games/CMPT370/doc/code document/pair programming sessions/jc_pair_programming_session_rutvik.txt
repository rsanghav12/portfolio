Date: Monday November 18
Programmers: Rutvik and James
Goal: Create functioning load robot view

What you learned/why was it valuable:
This programming session was valuable to me as I ended up learning quite a bit in a short amount of time about user interfaces. Rutvik, previously to our session, had spent a lot of time on other view components and had picked up some pretty interesting and useful techniques for handling JTables and making individual custom table models. Because of this we were able to avoid pitfalls that he had already discovered and worked around on other components of the system.

Our session was both of our second attempts at pair programming, and I feel that because of this it went very well. We went in with a clear goal and worked towards it successfully and smoothly. It was a good demonstration on the benefits of pair programming, and I would be interested in more sessions like this one again.

Difficulties/obstacles:
The biggest difficulty we encountered was probably the skill gap between the two of us. Rutvik has a lot more experience than I do myself, and as well had spent a lot of time previous to this session working on views like we were working on together. Because of this, I believe it slowed us down when I would need some things he was doing explained to me. On the flip side of this, though, is that I gained a lot of knowledge from our session in a much faster way than having done it myself.

What you actually accomplished:
By the end of the session we had a pretty good flow between the two of us. Rutvik still had to stop and explain things to me every now and then, however we were definitely picking up pace and making headway in what we set out to do. I expect that if we had another session together in which I knew a little more about before we started, we could definitely accomplish a lot in a smaller timeframe than we could on our own.