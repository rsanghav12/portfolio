﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// PlatformGenerator generates a platform using 3 provided
/// prefabs (left, middle, right), and will require approximate width
/// to be set. The approximate width is used to see how much of middle
/// tiles can fit with the left and right tiles being there, this means
/// even if approx. width is 0.1 the left and right tile will be present,
/// just no middle tiles in that case.
/// </summary>
public class PlatformGenerator : MonoBehaviour {
  [Tooltip("Approximate width to create this platform, always includes left and right tile.")]
  public float approximate_width;

  /// <summary>
  /// Number of tiles used to build the platform.
  /// </summary>
  public int UsedTiles { get; private set; }

  /* Storing platform tiles to return later on */
  private List<GameObject> platform_tiles;
  /* To store an instance of the platform pool */
  private PlatformPool pool;

  public void Create() {
    /* get pool instance */
    if (pool == null) {
      pool = GameObject.FindGameObjectWithTag("PlatformPool").GetComponent<PlatformPool>();
    }

    /* initialize platform tiles */
    if (platform_tiles == null) {
      platform_tiles = new List<GameObject>();
    }

    /* check if approx. width is greater then 0 or else don't isntantiate anything */
    if (approximate_width > 0) {
      /* get the width of each tile */
      float left_width = pool.left_tile_size.x;
      float mid_width = pool.mid_tile_size.x;
      float right_width = pool.right_tile_size.x;

      /* approximated total width (must have right and left tiles) */
      float approximated_width = left_width + right_width;

      /* a loop that adds approximatedWidth with the width of middle tile
       		 * to see how many we can fit before we go over approximateWidth given */
      for (; approximated_width < approximate_width; approximated_width += mid_width);

      /* amount of middle tiles to have on this platform */
      int mid_tiles = Mathf.RoundToInt((approximated_width - (left_width + right_width)) / mid_width);
      UsedTiles = mid_tiles + 2;

      if (mid_tiles > 0) {
        /* get how many of the mid tiles should be positioned on the left/right */
        float mid_tile_offset_count = mid_tiles / 2.0f;

        /* start position of the tiles to generate */
        float start_pos = (-left_width) - (mid_tile_offset_count * (mid_width)) + (mid_width / 2.0f);

        /* instantiate the left tile and have it offset to the left where the middle tile should start */
        GameObject left_tile = pool.GetTileWithType(gameObject, PlatformType.LEFT);
        if (left_tile != null) {
          left_tile.transform.localPosition = new Vector2(start_pos, 0);
          platform_tiles.Add(left_tile);
        } else {
          Debug.LogError("No more left tile available in the pool.");
        }

        /* update startPos to account for one left tile being added */
        start_pos += left_width;

        /* start at the end of left tile and instantiate a middle tile and keep adding to the startPos
         * to keep track of current position */
        for (int i = 1; i <= mid_tiles; i++) {
          GameObject mid_tile = pool.GetTileWithType(gameObject, PlatformType.MIDDLE);
          if (mid_tile != null) {
            mid_tile.transform.localPosition = new Vector2(start_pos, 0);
            platform_tiles.Add(mid_tile);
          } else {
            Debug.LogError("No more middle tile available in pool.");
          }

          start_pos += mid_width;
        }

        /* instantiate and position the right tile after the last middle tile */
        GameObject right_tile = pool.GetTileWithType(gameObject, PlatformType.RIGHT);
        if (right_tile != null) {
          right_tile.transform.localPosition = new Vector2(start_pos, 0);
          platform_tiles.Add(right_tile);
        } else {
          Debug.LogError("No more right tile available in pool.");
        }
      } else {
        /* no room for mid tiles */
        GameObject left_tile = pool.GetTileWithType(gameObject, PlatformType.LEFT);
        if (left_tile != null) {
          left_tile.transform.localPosition = new Vector2((-left_width) / 2, 0);
          platform_tiles.Add(left_tile);
        } else {
          Debug.LogError("No more left tile available in the pool.");
        }

        GameObject right_tile = pool.GetTileWithType(gameObject, PlatformType.RIGHT);
        if (right_tile != null) {
          right_tile.transform.localPosition = new Vector2((right_width) / 2, 0);
          platform_tiles.Add(right_tile);
        } else {
          Debug.LogError("No more right tile available in pool.");
        }
      }
    } else {
      /* useful for debugging if we missed it */
      if (approximate_width < 0)
        Debug.LogWarning("Approx. Width set to less then zero.");
    }
  }

  /// <summary>
  /// Gets the bounds of the platform.
  /// </summary>
  /// <returns>Bounds of platform.</returns>
  public Bounds GetBounds() {
    if (platform_tiles.Count > 0) {
      SpriteRenderer ren_0 = platform_tiles[0].GetComponent<SpriteRenderer>();
      Bounds bounds = ren_0.bounds;

      for (int i = 0; i < platform_tiles.Count; i++) {
        SpriteRenderer ren = platform_tiles[i].GetComponent<SpriteRenderer>();
        if (!bounds.Equals(ren.bounds))
          bounds.Encapsulate(ren.bounds);
      }

      return bounds;
    }

    return new Bounds();
  }

  private void OnDestroy() {
    if (pool != null)
      ReturnTiles();
  }

  private void ReturnTiles() {
    /* return all the used tiles */
    for (int i = 0; i < platform_tiles.Count; i++) {
      pool.ReturnTile(platform_tiles[i]);
    }
  }

  /// <summary>
  /// Generates the platform.
  /// </summary>
  /// <returns>The generated platform.</returns>
  /// <param name="name">Name of the generated platform.</param>
  /// <param name="pos">Position of the platform to generate.</param>
  /// <param name="width">Approximated Width.</param>
  public static GameObject GeneratePlatform(string name, Vector2 pos, float width) {
    GameObject platform = new GameObject(name);
    platform.transform.position = pos;

    PlatformGenerator platform_gen = platform.AddComponent<PlatformGenerator>();
    platform_gen.approximate_width = width;
    platform_gen.Create();

    BoxCollider2D collider2D = platform.AddComponent<BoxCollider2D>();
    collider2D.size = platform_gen.GetBounds().size + new Vector3(0.2f, 0f);

    OneWayPlatform one_way_platform = platform.AddComponent<OneWayPlatform>();
    one_way_platform.collider = collider2D;

    ParticleSystem[] particles = platform.GetComponentsInChildren<ParticleSystem>();
    foreach (ParticleSystem cur in particles) {
      if (cur.tag.Equals("Smoke") || cur.tag.Equals("Sparks")) {
        cur.Stop();
      }
    }

    return platform;
  }
}
