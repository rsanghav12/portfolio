﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MicroGenerator : MonoBehaviour {
  private const float collider_offset_y = 0f;
  private const float collider_offset_x = 5f;
  private const float blocks_in_view_modifier = 1f;
  private const float fixed_platform_width_modifier = 1f;
  private const float border_inset = 0f;

  private PlatformPool pool;
  private List<GameObject> generated_platform;
  private bool has_created_level = false;
  private bool has_created_another_level = false;
  private Player2D player;
  private GameObject enemy_prefab;

  public int blocks_in_level { private get; set; }

  public float difficulty { private get; set; }

  private void Awake() {
    /* get the player2d script from player */
    player = GameObject.FindGameObjectWithTag(Preferences.player_tag).GetComponent<Player2D>();
  }

  public Vector2 CreateLevel(float height, float width, Vector2 last_platform, bool base_level = false) {
    if (pool == null) {
      pool = GameObject.FindGameObjectWithTag("PlatformPool").GetComponent<PlatformPool>();
    }

    if (generated_platform == null) {
      generated_platform = new List<GameObject>();
    } else {
      generated_platform.Clear();
    }
      
    if (!has_created_level) {
      if (blocks_in_level > 0) {
        for (int i = 0; i < blocks_in_level; i++) {
          if (base_level && i == 0) {
            last_platform = new Vector2(0, -(height/2f) + (pool.left_tile_size.y / 2.0f) + 0.1f);

            GameObject platform = PlatformGenerator.GeneratePlatform("MicroPlatform(" + i + ")", last_platform, width);
            platform.transform.parent = transform;
            generated_platform.Add(platform);
          } else {
            float random_width = Random.Range(1f, width / 4);

            GameObject platform = PlatformGenerator.GeneratePlatform("MicroPlatform(" + i + ")", Vector2.zero, random_width);
            PlatformGenerator platform_gen = platform.GetComponent<PlatformGenerator>();

            float y_range_mod = -((player.GetBounds().size.y / 2.0f) + (pool.left_tile_size.y / 2.0f));
            float xRangeMod = (pool.mid_tile_size.x * platform_gen.UsedTiles)/3f;

            /* compensate for 5% loss in max/min jump height due to fixed time step */
            float max_y_range = player.GetMaxJumpHeight() - (player.GetMaxJumpHeight() * (5.0f / 100.0f)) + y_range_mod;
            float min_y_range = player.GetMinJumpHeight() - (player.GetMinJumpHeight() * (5.0f / 100.0f));

            /* factor in difficulty for the y range */
            float max_y_range_difficulty = Mathf.Clamp(max_y_range * difficulty, min_y_range, max_y_range);

            /* randomize the y range to create unique levels */
            float y_range = Random.Range(min_y_range, max_y_range_difficulty);

            /* calculate t for the given y range */
            float t = Mathf.Sqrt((2 * y_range) / -player.Gravity());

            /* calculate x range given the time using the max x velocity */
            float x_range = (player.GetMoveSpeed() * t * 2) + xRangeMod;

            /* factor in difficulty for the x range */
            x_range = Mathf.Clamp(x_range * difficulty, 0, x_range);

            /* randomize which side the platform spawns */
            float sign = Mathf.Sign(Random.Range(-1.0f, 1.0f));
            x_range = x_range * sign;

            /* add the platform sprite offset to the x and y range */
            float platform_offset_x = pool.left_tile_size.x * sign;
            float platform_offset_y = pool.left_tile_size.y;
            x_range += platform_offset_x;
            y_range += platform_offset_y;

            /* add to the last platform */
            last_platform += new Vector2(x_range, y_range);
            platform.transform.position = last_platform;

            if (difficulty > 1f) {
              bool falling_platform = Mathf.Sign(Random.Range(-1f * difficulty, 1f * (difficulty/2f))) == -1;
              if (falling_platform) {
                foreach (SpriteRenderer renderer in platform.GetComponentsInChildren<SpriteRenderer>()) {
                  renderer.color = new Color(renderer.color.r + 30f, renderer.color.b, renderer.color.g);
                }
                platform.AddComponent<AudioSource>();
                platform.AddComponent<FallingPlatform>();
              }
            }
            platform.transform.parent = transform;
            generated_platform.Add(platform);
          }
        }

        BoxCollider2D trigger_collider = gameObject.AddComponent<BoxCollider2D>();
        trigger_collider.size = new Vector3(width, GetBounds().size.y);
        trigger_collider.offset = GetBounds().center;
        trigger_collider.isTrigger = true;
        gameObject.layer = LayerMask.NameToLayer("MicroLevelTriggers");
        has_created_level = true;
      }
    }

    return last_platform;
  }

  /// <summary>
  /// Gets the bounds of the Level.
  /// </summary>
  /// <returns>Bounds of Level.</returns>
  public Bounds GetBounds() {
    SpriteRenderer[] rens = GetComponentsInChildren<SpriteRenderer>();
    Bounds bounds = rens[0].bounds;
    foreach (SpriteRenderer ren in GetComponentsInChildren<SpriteRenderer>()) {
      if (!bounds.Equals(ren.bounds))
        bounds.Encapsulate(ren.bounds);
    }

    return bounds;
  }

  private void OnTriggerEnter2D(Collider2D collision) {
    if (!has_created_another_level && collision.gameObject.tag == Preferences.player_tag) {
      MacroGenerator macro_generator = GameObject.FindGameObjectWithTag("MacroGenerator").GetComponent<MacroGenerator>();
      macro_generator.CreateLevel();
      has_created_another_level = true;
    }
  }
}
