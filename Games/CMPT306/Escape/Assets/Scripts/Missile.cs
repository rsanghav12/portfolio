﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : Threat {
	private const float move_speed = 7f;
  private const int damage = 1;
  private bool exploded = false;
  private AudioSource audio_source;
  [SerializeField] AudioClip explode;

  protected override void Start() {
    base.Start();
    apply_gravity = false;
    audio_source = GetComponent<AudioSource>();
  }

  private void Update() {
    if(!exploded) {
      transform.position += transform.up * move_speed * Time.deltaTime;
    }
  }

  private void OnTriggerEnter2D(Collider2D collision) {
    GetComponent<Collider2D>().enabled = false;
    exploded = true;
    GetComponent<SpriteRenderer>().sprite = null;

    audio_source.PlayOneShot(explode, Random.Range(0.5f, 1f));

    ParticleSystem sys = GetComponentInChildren<ParticleSystem>();
    sys.Play();

    Hit(collision.gameObject);
  }

  protected override int Damage() {
    return damage;
  }

  protected override void Destroy() {
    Destroy(this.gameObject, 2f);
  }

  private void OnBecameInvisible() {
    Destroy(this.gameObject, 2f);
  }
}
