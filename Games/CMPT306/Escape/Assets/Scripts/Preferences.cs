﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Preferences : MonoBehaviour {
  public static string player_tag = "Player";
  public static float score;
  public static string control_HUD_path = "Prefabs/HUD/ControlHUD";
  public static string missile_prefab_path = "Prefabs/Threats/Missile";

  private void Awake() {
    DontDestroyOnLoad(gameObject);
  }
}
