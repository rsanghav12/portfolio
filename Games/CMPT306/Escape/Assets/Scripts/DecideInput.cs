﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecideInput : MonoBehaviour {
  private void Awake() {
    Player2D player = GameObject.FindGameObjectWithTag(Preferences.player_tag).GetComponent<Player2D>();
#if UNITY_STANDALONE
    KeyboardInput input = player.gameObject.AddComponent<KeyboardInput>();
    player.SetInput(input);
#elif UNITY_EDITOR
      KeyboardInput input = player.gameObject.AddComponent<KeyboardInput>();
      player.SetInput(input);
#elif UNITY_IOS
      GameObject contorlhud_prefab = Resources.Load(Preferences.control_HUD_path) as GameObject;
      GameObject controlhud = Instantiate(contorlhud_prefab);
      Canvas controlhud_canvas = controlhud.GetComponent<Canvas>();
      controlhud_canvas.renderMode = RenderMode.ScreenSpaceCamera;
      controlhud_canvas.worldCamera = Camera.main;
      controlhud_canvas.sortingLayerName = "HUD";

      JoyStick input = controlhud.GetComponentInChildren<JoyStick>();
      player.SetInput(input);
#elif UNITY_ANDROID
      GameObject contorlhud_prefab = Resources.Load(Preferences.control_HUD_path) as GameObject;
      GameObject controlhud = Instantiate(contorlhud_prefab);
      Canvas controlhud_canvas = controlhud.GetComponent<Canvas>();
      controlhud_canvas.renderMode = RenderMode.ScreenSpaceCamera;
      controlhud_canvas.worldCamera = Camera.main;
      controlhud_canvas.sortingLayerName = "HUD";

      JoyStick input = controlhud.GetComponentInChildren<JoyStick>();
      player.SetInput(input);
#endif
    }
}
