﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ship : MonoBehaviour {
  public bool active { private get; set; }

  private Transform target;
  private bool inside_canvas;
  private Vector3 direction;
  private Vector3[] corners;
  private float move_speed = 4f;
  /* Cool down timer */
  private float time_until_fire;
  /* Cool down time for the launcher (default is 2f) */
  [HideInInspector] public float time_to_fire = 4f;
  private GameObject missile_prefab;
  private Image image;
  private bool fading;

  public AudioClip shoot_sound;
  public AudioClip appear_sound;
  private AudioSource audio_source;

  private void Start() {
    active = false;
    fading = false;
    image = GetComponent<Image>();
    target = GameObject.FindGameObjectWithTag(Preferences.player_tag).transform;
    direction = Vector3.right;
    corners = new Vector3[4];
    missile_prefab = Resources.Load(Preferences.missile_prefab_path) as GameObject;
    audio_source = GetComponent<AudioSource>();
  }

  /// <summary>
  /// Fade in or out.
  /// </summary>
  /// <param name="dir">1 = in, -1 = out</param>
  private IEnumerator Fade(int dir) {
    fading = true;
    float duration = 1f;
    if (dir == 1) {
      image.enabled = true;
      for (float t = 0.0f; t < duration; t += Time.deltaTime) {
        image.color = Color.Lerp(image.color, new Color(1f, 1f, 1f, 1f), t / duration);
        yield return null;
      }
    } else if(dir == -1) {
      for (float t = 0.0f; t < duration; t += Time.deltaTime) {
        image.color = Color.Lerp(image.color, new Color(1f, 1f, 1f, 0f), t / duration);
        yield return null;
      }
      image.enabled = false;
    }
    fading = false;
    yield break;
  }

  private void FixedUpdate() {
    if (active) {
      if (!image.enabled && !fading) {
        StartCoroutine(Fade(1));
        audio_source.PlayOneShot(appear_sound, Random.Range(0.5f, 1f));
      }

      inside_canvas = true;
      RectTransform rt = GetComponent<RectTransform>();
      RectTransform rtP = rt.parent.GetComponent<RectTransform>();

      rt.GetWorldCorners(corners);
      if (corners != null) {
        for (int i = 0; i < corners.Length; i++) {
          Vector3 localSpacePoint = rtP.InverseTransformPoint(corners[i]);
          if (rtP.rect.Contains(localSpacePoint) == false) {
            inside_canvas = false;
          }
        }

        if (!inside_canvas) {
          direction = direction * -1;
        }

        transform.localPosition += direction * move_speed;
      }

      FollowTarget();
    } else {
      if (image.enabled && !fading) {
        StartCoroutine(Fade(-1));
      }
    }
  }

  private void FollowTarget() {
    Vector3 direction_of_target = target.transform.position - transform.position;
    float angle = Mathf.Atan2(direction_of_target.y, direction_of_target.x) * Mathf.Rad2Deg;
    transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.AngleAxis(angle - 90, Vector3.forward), Time.fixedDeltaTime * move_speed);
    if (time_until_fire > time_to_fire) {
      Fire();
      time_until_fire = 0f;
    }
    time_until_fire += Time.deltaTime;
  }

  private void Fire() {
    audio_source.PlayOneShot(shoot_sound, Random.Range(0.5f, 1f));
    Instantiate(missile_prefab, transform.position, transform.rotation);
  }
}
