﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class  Input2D : MonoBehaviour {
  public abstract float Horizontal();
  public abstract bool JumpButtonDown();
  public abstract bool JumpButtonUp();
}
