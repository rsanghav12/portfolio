﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Controller2D is used for object movement and general
/// game physics like movement velocity and gravity.
/// </summary>
[RequireComponent(typeof(Rigidbody2D), typeof(Collider2D))]
public class Controller2D : MonoBehaviour {
  [Tooltip("Moving speed of object along the x-axis.")]
	private float move_speed = 6f;

  [Tooltip("Acceleration time while in air.")]
  public float acceleration_air_time = 0.1f;

  [Tooltip("Acceleration time while on ground.")]
  public float acceleration_ground_time = 0.1f;

  /* Defaults */
  Type ColliderType = typeof(PolygonCollider2D);

  /* Layer name of platform */
	private const string platform_layer_name = "Platform";

  /* Jump default values */
	private const float max_jump_height = 2.5f;
	private const float double_jump_height = 1.5f;
	private const float min_jump_height = 1f;
	private const float jump_apex_time = 0.3f;

  /* Raycast for ground check */
	private const float ray_length = 0.4f;
	private const float distance_between_rays = 0.25f;

  /* Body collider used for raycast bounds */
  new private Collider2D collider;
  /* Platform layer mask used for ground checking */
	private LayerMask platform_layer_mask;
  /* Velocity dampener on horizontal axis */
	private float velocity_x_smoothing;
  /* Direction of sprite */
	private bool facing_right = true;

  /* Rigidbody2D is accessable to the inheriter of this class */
  new protected Rigidbody2D rigidbody;

  /* Apply Gravity (true by default) */
  [HideInInspector] public bool apply_gravity = true;

  protected virtual void Start() {
    /* Get the components and set layer mask for platform */
    rigidbody = GetComponent<Rigidbody2D>();
    if (collider == null) collider = GetComponent(ColliderType) as Collider2D;
    platform_layer_mask = LayerMask.GetMask(platform_layer_name);
  }

  /// <summary>
  /// Applies velocity to the attached RigidBody2D.
  /// </summary>
  /// <param name="move">Apply velocity in direction specified in this parameter.</param>
  protected void Move(MoveMSG move) {
    /* Modify velocity thats already being applied to rigidbody2d */
    Vector2 velocity = rigidbody.velocity;
    /* Apply horizontal velocity smoothly */
    velocity.x = Mathf.SmoothDamp(velocity.x, move.horizontal * move_speed, ref velocity_x_smoothing, (IsGrounded() ? acceleration_ground_time : acceleration_air_time));

    /* Flip object based on direction of horizontal velocity */
    if (move.horizontal > 0 && !facing_right) {
      Flip();
    } else if (move.horizontal < 0 && facing_right) {
      Flip();
    }

    /* Jump switch case if its max jump or min jump */
    switch (move.jump) {
      case Jump.MAX:
        velocity.y = MaxJumpVelocity();
        break;
      case Jump.MIN:
        velocity.y = MinJumpVelocity();
        break;
      case Jump.DOUBLE:
        velocity.y = DoubleJumpVelocity();
        break;
      default:
        break;
    }

    /* Apply velocity changes to rigidbody2d */
    rigidbody.velocity = velocity;
  }

  protected virtual void FixedUpdate() {
    if (rigidbody.bodyType != RigidbodyType2D.Static && rigidbody.bodyType != RigidbodyType2D.Kinematic && apply_gravity) {
      /* Apply constant gravity to rigidbody2d */
      rigidbody.velocity = new Vector2(rigidbody.velocity.x, rigidbody.velocity.y + (Gravity() * Time.fixedDeltaTime));
    }
  }

  /// <summary>
  /// Direction of the GameObject.
  /// </summary>
  /// <returns>True if its facing right, left otherwise.</returns>
  protected bool Direction() {
    return facing_right;
  }

  /// <summary>
  /// Flips the GameObject horizontally.
  /// </summary>
  protected void Flip() {
    facing_right = !facing_right;
    Vector3 local_scale = transform.localScale;
    local_scale.x *= -1;
    transform.localScale = local_scale;
  }

  /// <summary>
  /// Checks if GameObject is colliding with platforms. Utilizes rays from raycasts.
  /// </summary>
  /// <returns>True if grounded, otherwise false.</returns>
  protected bool IsGrounded() {
    Bounds bounds = collider.bounds;
    Vector2 bottom_left = new Vector2(bounds.min.x, bounds.min.y);
    int vertical_ray_count = Mathf.RoundToInt(collider.bounds.size.x / distance_between_rays);

    if (rigidbody.velocity.y <= 0) {
      for (int i = 0; i < vertical_ray_count; i++) {
        Vector2 ray_origin = bottom_left;
        ray_origin += Vector2.right * (distance_between_rays * i);
        RaycastHit2D hit = Physics2D.Raycast(ray_origin, -Vector2.up, ray_length, platform_layer_mask);
        Debug.DrawRay(ray_origin, -Vector2.up * ray_length, Color.red);
        if (hit) {
          return true;
        }
      }
    }

    return false;
  }

  /// <summary>
  /// Calculates gravity from preset jump height and apex time of that jump.
  /// </summary>
  /// <returns>Calculated gravity.</returns>
  public float Gravity() {
    return -(2 * max_jump_height) / Mathf.Pow(jump_apex_time, 2);
  }

  /// <summary>
  /// Calculates max jump velocty from preset jump height and calculated gravity.
  /// </summary>
  /// <returns>Calculated max jump velocity.</returns>
  public float MaxJumpVelocity() {
    return Mathf.Abs(Gravity()) * jump_apex_time;
  }

  /// <summary>
  /// Calculates double jump velocity from preset jump height and calculated gravity.
  /// </summary>
  /// <returns>Calculated double jump velocity.</returns>
  public float DoubleJumpVelocity() {
    return Mathf.Sqrt(2 * Mathf.Abs(Gravity()) * double_jump_height);
  }

  /// <summary>
  /// Calculates min jump velocty from preset jump height and calculated gravity.
  /// </summary>
  /// <returns>Calculated min jump velocity.</returns>
  public float MinJumpVelocity() {
    return Mathf.Sqrt(2 * Mathf.Abs(Gravity()) * min_jump_height);
  }

  /// <summary>
  /// Get the max height the player can jump.
  /// </summary>
  /// <returns>Max jump height.</returns>
  public float GetMaxJumpHeight() {
    return max_jump_height;
  }

  /// <summary>
  /// Get the min height the player can jump.
  /// </summary>
  /// <returns>Min jump height.</returns>
  public float GetMinJumpHeight() {
    return min_jump_height;
  }

  /// <summary>
  /// Get the time to reach the apex of a jump.
  /// </summary>
  /// <returns>Jump apex time.</returns>
  public float GetJumpApexTime() {
    return jump_apex_time;
  }

  /// <summary>
  /// Get the move speed of the player.
  /// </summary>
  /// <returns>Move speed.</returns>
  public float GetMoveSpeed() {
    return move_speed;
  }

  /// <summary>
  /// Gets the bounds of the player.
  /// </summary>
  /// <returns>Bounds of player.</returns>
  public Bounds GetBounds() {
    if (collider == null) {
      collider = GetComponent(ColliderType) as Collider2D;
    }

    Bounds bounds = collider.bounds;
    foreach (Collider2D col in GetComponents<Collider2D>()) {
      if (bounds != col.bounds) bounds.Encapsulate(col.bounds);
    }

    return bounds;
  }

  /// <summary>
  /// Container to store applying velocity in the current frame.
  /// </summary>
  protected struct MoveMSG {
    /// <summary>
    /// Horizontal input.
    /// </summary>
    public float horizontal;
    public Jump jump;
  }

  /// <summary>
  /// Level of jump to utilize.
  /// {MAX, MIN, NONE, DJ}
  /// </summary>
  protected enum Jump { MAX, MIN, DOUBLE, NONE };
}
