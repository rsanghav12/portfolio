﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformPool : MonoBehaviour {
  /* Paths of tile prefabs */
  private const string left_tile_path = "Prefabs/Platforms/Left";
  private const string mid_tile_path = "Prefabs/Platforms/Mid";
  private const string right_tile_path = "Prefabs/Platforms/Right";

  /* Amount of tiles to be created */
  private const int left_tiles_amount = 50;
  private const int mid_tiles_amount = 100;
  private const int right_tiles_amount = 50;

  /* Pool of tiles */
  private List<GameObject> left_tiles;
  private List<GameObject> mid_tiles;
  private List<GameObject> right_tiles;

  /* Mutex lock */
  private object _lock;

  /* Size of tiles */
  public Vector2 left_tile_size { get; private set; }

  public Vector2 mid_tile_size { get; private set; }

  public Vector2 right_tile_size { get; private set; }

  private Color original_color;

  private void Awake() {
    _lock = new object();

    left_tiles = new List<GameObject>(left_tiles_amount);
    mid_tiles = new List<GameObject>(mid_tiles_amount);
    right_tiles = new List<GameObject>(right_tiles_amount);

    GameObject left = Resources.Load(left_tile_path) as GameObject;
    GameObject mid = Resources.Load(mid_tile_path) as GameObject;
    GameObject right = Resources.Load(right_tile_path) as GameObject;

    if (left == null) {
      Debug.LogError("Left tile is null.");
    }

    if (mid == null) {
      Debug.LogError("Middle tile is null.");
    }

    if (right == null) {
      Debug.LogError("Right tile is null.");
    }

    original_color = left.GetComponent<SpriteRenderer>().color;

    left_tile_size = new Vector2(WidthOf(left), HeightOf(left));
    mid_tile_size = new Vector2(WidthOf(mid), HeightOf(mid));
    right_tile_size = new Vector2(WidthOf(right), HeightOf(right));

    /* We create only one for loop to go to the max amount of tiles from the tileAmounts
     * this method will be efficient in sense there will only be one for loop used. */
    int[] tileAmounts = { left_tiles_amount, mid_tiles_amount, right_tiles_amount };
    for (int i = 0; i < Mathf.Max(tileAmounts); i++) {
      /* Instantiate tiles, make it inactive, and make the parent this gameobject */
      if (i < left_tiles_amount) {
        GameObject left_tile = Instantiate(left) as GameObject;
        left_tile.SetActive(false);
        left_tile.transform.parent = transform;
        left_tiles.Add(left_tile);
      }

      if (i < mid_tiles_amount) {
        GameObject mid_tile = Instantiate(mid) as GameObject;
        mid_tile.SetActive(false);
        mid_tile.transform.parent = transform;
        mid_tiles.Add(mid_tile);
      }

      if (i < right_tiles_amount) {
        GameObject right_tile = Instantiate(right) as GameObject;
        right_tile.SetActive(false);
        right_tile.transform.parent = transform;
        right_tiles.Add(right_tile);
      }
    }
  }

  public static float WidthOf(GameObject obj) {
    SpriteRenderer sprite = obj.GetComponent<SpriteRenderer>();
    if (sprite != null) {
      return sprite.bounds.size.x;
    }

    return 0f;
  }

  public static float HeightOf(GameObject obj) {
    SpriteRenderer sprite = obj.GetComponent<SpriteRenderer>();
    if (sprite != null) {
      return sprite.bounds.size.y;
    }

    return 0f;
  }

  /// <summary>
  /// Grabs the pool for the platform type.
  /// </summary>
  /// <param name="type">Type of tile being requested.</param>
  /// <returns>List of pool for the type of tiles.</returns>
  private List<GameObject> GetPoolForType(PlatformType type) {
    List<GameObject> pool = null;

    switch (type) {
      case PlatformType.LEFT:
        pool = left_tiles;
        break;
      case PlatformType.MIDDLE:
        pool = mid_tiles;
        break;
      case PlatformType.RIGHT:
        pool = right_tiles;
        break;
      default:
        break;
    }

    return pool;
  }

  private void ActivateTile(GameObject parent, GameObject tile) {
    tile.transform.localRotation = Quaternion.identity;
    tile.GetComponent<SpriteRenderer>().color = original_color;
    tile.transform.parent = parent.transform;
    tile.SetActive(true);
  }

  /// <summary>
  /// Grabs a tile of type from the tile pool.
  /// </summary>
  /// <param name="parent">Parent of the tile being requested.</param>
  /// <param name="type">Type of tile being requested.</param>
  /// <returns>Tile as GameObject.</returns>
  public GameObject GetTileWithType(GameObject parent, PlatformType type) {
    lock (_lock) {
      List<GameObject> pool = GetPoolForType(type);
      for (int i = 0; i < pool.Count; i++) {
        if (pool[i] == null) {
          switch (type) {
            case PlatformType.LEFT:
              GameObject left = Resources.Load(left_tile_path) as GameObject;

              GameObject left_tile = Instantiate(left) as GameObject;
              left_tile.SetActive(false);
              left_tile.transform.parent = transform;
              pool[i] = left_tile;
              break;
            case PlatformType.MIDDLE:
              GameObject mid = Resources.Load(mid_tile_path) as GameObject;

              GameObject mid_tile = Instantiate(mid) as GameObject;
              mid_tile.SetActive(false);
              mid_tile.transform.parent = transform;
              pool[i] = mid_tile;
              break;
            case PlatformType.RIGHT:
              GameObject right = Resources.Load(right_tile_path) as GameObject;

              GameObject right_tile = Instantiate(right) as GameObject;
              right_tile.SetActive(false);
              right_tile.transform.parent = transform;
              pool[i] = right_tile;
              break;
            default:
              break;
          }
        }

        if (!pool[i].activeInHierarchy) {
          ActivateTile(parent, pool[i]);
          return pool[i];
        }
      }

      return null;
    }
  }

  public void ReturnTile(GameObject tile) {
    lock (_lock) {
      tile.transform.parent = transform;
      tile.SetActive(false);
    }
  }
}

public enum PlatformType { LEFT, MIDDLE, RIGHT};
