﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {
  private Text score_text;
  private float score = 0;
  private Player2D player;
  private Color original_color;

  private void Start() {
    score_text = GetComponent<Text>();
    player = GameObject.FindGameObjectWithTag(Preferences.player_tag).GetComponent<Player2D>();
    original_color = Camera.main.backgroundColor;
  }

  private IEnumerator BackgroundFlash() {
    Color start = original_color;
    Color end = new Color(62f/255f, 17f / 255f, 17f / 255f, 0.2f);
    float duration = 0.5f;

    for (float t = 0.0f; t < duration; t += Time.deltaTime) {
      Color changedColor = Color.Lerp(start, end, t / duration);
      Camera.main.backgroundColor = changedColor;
      yield return null;
    }

    yield return null;

    for (float t = 0.0f; t < duration; t += Time.deltaTime) {
      Color changedColor = Color.Lerp(end, start, t / duration);
      Camera.main.backgroundColor = changedColor;
      yield return null;
    }

    yield break;
  }

  private void LateUpdate() {
    if (score % 100 == 0) {
      StartCoroutine(BackgroundFlash());
    }

    if(score != player.current_score) {
      score = player.current_score;
      score_text.text = "Score: " + score;
      Preferences.score = score;
    }
  }
}
