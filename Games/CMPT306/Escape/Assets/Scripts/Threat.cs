﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class Threat : Controller2D {
  /* Damage to sustain on player */
  protected abstract int Damage();

  /* Destroy threat as it has hit an object */
  protected abstract void Destroy();

  /// <summary>
  /// Get the player game objects present in screen.
  /// </summary>
  /// <returns>The playe game objects.</returns>
  public static GameObject[] PlayerObjects() {
    return GameObject.FindGameObjectsWithTag(Preferences.player_tag);
  }

  /// <summary>
  /// Send a sustain damage message to the player.
  /// Then send a destroy message to the callee.
  /// </summary>
  /// <param name="possiblePlayer">Object the threat collided with.</param>
  protected void Hit(GameObject possible_player) {
    /* If its a player tell him to take a damage */
    if (possible_player.tag == Preferences.player_tag) {
      possible_player.SendMessage("SustainDamage", Damage());
    }

    /* Let the callee handle how it gets destroyed */
    Destroy();
  }
}