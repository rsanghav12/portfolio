﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Load scenes or quit application.
/// </summary>
public class LoadScene : MonoBehaviour {
  /// <summary>
  /// Load scene by index (index given at build).
  /// </summary>
  /// <param name="index">index of the scene</param>
  public void LoadSceneWithIndex(int index) {
    LoadTransition scene_transition = Camera.main.gameObject.GetComponent<LoadTransition>();
    if(scene_transition) {
      StartCoroutine(LoadSceneWithTransition(index, scene_transition));
    }  else {
      SceneManager.LoadScene(index);
    }
  }

  private IEnumerator LoadSceneWithTransition(int index, LoadTransition transition) {
    yield return new WaitForSeconds(transition.BeginFade(1));
    SceneManager.LoadScene(index);
  }

  /// <summary>
  /// Quits application.
  /// </summary>
  public void QuitApplication() {
    Application.Quit();
  }
}
