﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

// http://www.theappguruz.com/blog/beginners-guide-learn-to-make-simple-virtual-joystick-in-unity
public class JoyStick : Input2D, IDragHandler, IPointerUpHandler, IPointerDownHandler {
  private Image jsContainer;
  private Image joystick;
  private Vector3 InputDirection;
  private bool jump_down;
  private bool jump_up;

  private void Start() {
    jsContainer = GetComponent<Image>();
    joystick = transform.GetChild(0).GetComponent<Image>(); //this command is used because there is only one child in hierarchy
    InputDirection = Vector3.zero;
  }

  public void OnDrag(PointerEventData eventData) {
    Vector2 position = Vector2.zero;

    RectTransformUtility.ScreenPointToLocalPointInRectangle
            (jsContainer.rectTransform,
            eventData.position,
            eventData.pressEventCamera,
            out position);

    position.x = (position.x / jsContainer.rectTransform.sizeDelta.x);
    position.y = (position.y / jsContainer.rectTransform.sizeDelta.y);

    float x = (jsContainer.rectTransform.pivot.x == 1f) ? position.x * 2 + 1 : position.x * 2 - 1;
    float y = (jsContainer.rectTransform.pivot.y == 1f) ? position.y * 2 + 1 : position.y * 2 - 1;

    InputDirection = new Vector3(x, y, 0);
    InputDirection = (InputDirection.magnitude > 1) ? InputDirection.normalized : InputDirection;

    joystick.rectTransform.anchoredPosition = new Vector3(InputDirection.x * (jsContainer.rectTransform.sizeDelta.x / 3)
                                                           , InputDirection.y * (jsContainer.rectTransform.sizeDelta.y) / 3);
  }

  public void OnPointerDown(PointerEventData ped) {
    OnDrag(ped);
  }

  public void OnPointerUp(PointerEventData ped) {
    InputDirection = Vector3.zero;
    joystick.rectTransform.anchoredPosition = Vector3.zero;
  }

  public override float Horizontal() {
    if (InputDirection.x > 0) {
      return 1;
    } else if (InputDirection.x < 0) {
      return -1;
    } else {
      return 0;
    }
  }

  public override bool JumpButtonDown() {
    return jump_down;
  }

  public override bool JumpButtonUp() {
    return jump_up;
  }

  public void JumpButtonClick() {
    jump_down = true;
  }

  public void JumpButtonClickUp() {
    jump_up = true;
  }

  private void Update() {
    jump_up = false;
    jump_down = false;
  }
}
