﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardInput : Input2D {
  private float horizontal = 0.0f;
  private bool jump_button_down = false;
  private bool jump_button_up = false;

  public override float Horizontal() {
    return horizontal;
  }

  public override bool JumpButtonDown() {
    return jump_button_down;
  }

  public override bool JumpButtonUp() {
    return jump_button_up;
  }

  private void Update() {
    horizontal = Input.GetAxisRaw("Horizontal");
    jump_button_down = Input.GetButtonDown("Jump");
    jump_button_up = Input.GetButtonUp("Jump");
  }
}
