﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Transition to change between screens.
/// </summary>
public class LoadTransition : MonoBehaviour {
  /* Fading transition taken from 
   * https://www.youtube.com/watch?v=0HwZQt94uHQ */

  /* Fade speed */
  private const float fade_speed = 0.6f;
  /* Fade texture to show while fading */
  public Texture2D fade_texture;

  /* Draw hierarchy (lower the number the higher) */
  private int draw_depth = -1000;
  /* Default alpha value */
  private float alpha = 1f;
  /* Method to keep track of current fade status
   * (1 = fadeout, -1 = fadein) */
  private int fade_dir = -1;

  /* Fade in as soon as this script is applied (this script should be placed
   * in main camera for the best effect */
  private void OnEnable() {
    BeginFade(-1);
  }

  /* Fading */
  public float BeginFade(int direction) {
    fade_dir = direction;
    return fade_speed;
  }

  /* Drawing the texture and fading */
  private void OnGUI() {
    alpha += fade_dir * fade_speed * Time.deltaTime;
    alpha = Mathf.Clamp01(alpha);

    GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, alpha);
    GUI.depth = draw_depth;
    GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), fade_texture);
  }
}
