﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthHUD : MonoBehaviour {
  [SerializeField] public GameObject[] hearts;
  [SerializeField] public Sprite fullheart;
  [SerializeField] public Sprite emptyheart;
  private Player2D player;
  private bool errors;
  private int current_health;


  private void Awake() {
    player = GameObject.FindGameObjectWithTag(Preferences.player_tag).GetComponent<Player2D>();
    errors = false;

    if (hearts.Length != player.GetMaxHealth()) {
      Debug.LogError("Incorrect amount hearts in health bar to represent the player max health.");
      errors = true;
    }

    current_health = player.GetMaxHealth();
  }

  private void LateUpdate() {
    if(!errors && player.GetHealth() != current_health) { // update current health
      if(current_health > player.GetHealth()) { // current health needs to decrease
        Image heart = hearts[--current_health].GetComponent<Image>();
        if (heart) {
          if (emptyheart) {
            heart.sprite = emptyheart;
          }
        }
      } else { // current health needs to increase
        if((current_health + 1) < hearts.Length) { // can increase health
          Image heart = hearts[++current_health].GetComponent<Image>();
          if (heart) {
            if (fullheart) {
              heart.sprite = fullheart;
            }
          }
        }
      }
    }
  }
}
