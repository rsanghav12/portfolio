﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (BoxCollider2D))]
public class OneWayPlatform : MonoBehaviour {
	private const string platform_layer_name = "Platform";
	private const string pass_through_platform_layer_name = "PassThroughPlatform";
  private const float buffer = 0.35f;

  [HideInInspector] new public BoxCollider2D collider;

	private float collider_top;
  private Player2D player;

  private void Start() {
    player = GameObject.FindGameObjectWithTag(Preferences.player_tag).GetComponent<Player2D>();


    Vector3 player_min = player.GetBounds().min;
    collider_top = collider.bounds.max.y;
  }

  private void Update() {
    Vector3 platformPosInCamera = Camera.main.WorldToViewportPoint(gameObject.transform.position);
    bool belowScreen = platformPosInCamera.y < 0;

    if (belowScreen) {
      Destroy(gameObject);
    }

    Vector2 start = new Vector2(-100, player.GetBounds().min.y);
    Vector2 end = new Vector2(100, player.GetBounds().min.y);
    Debug.DrawLine(start, end, Color.black);

    Vector2 start_2 = new Vector2(-100, (collider_top - buffer));
		Vector2 end_2 = new Vector2(100, (collider_top - buffer));
    Debug.DrawLine(start_2, end_2, Color.green);

    if ((collider_top - buffer) > player.GetBounds().min.y) {
      gameObject.layer = LayerMask.NameToLayer(pass_through_platform_layer_name);
    } else {
      gameObject.layer = LayerMask.NameToLayer(platform_layer_name);
    }
  }
}
