﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Collects gargabe every 30 frame.
/// This method is recommended by Unity if smooth framerate
/// is a main concern for games running for long periods.
/// https://docs.unity3d.com/Manual/UnderstandingAutomaticMemoryManagement.html
/// </summary>
public class GarbageCollector : MonoBehaviour {
  private void Update() {
    /* collect garbage every 30 frame count */
    if (Time.frameCount % 30 == 0) {
      System.GC.Collect();
    }
  }
}
