﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingPlatform : MonoBehaviour {
  private float timer = 1f;
  private bool sparks_flying;

  private void OnCollisionStay2D(Collision2D col) {
    if (col.gameObject.tag == Preferences.player_tag) {
      if (!sparks_flying) {
        ParticleSystem[] particles = gameObject.GetComponentsInChildren<ParticleSystem>();
        foreach (ParticleSystem cur in particles) {
          if (cur.tag.Equals("Smoke") || cur.tag.Equals("Sparks")) {
            cur.Play();
          }
        }
        sparks_flying = true;

        AudioClip fallingsound = GameObject.FindGameObjectWithTag("MacroGenerator").GetComponent<MacroGenerator>().fallingplatform_sound;
        AudioSource audio_source = GetComponent<AudioSource>();
        audio_source.PlayOneShot(fallingsound, Random.Range(0.1f, 0.2f));
      }
      Invoke("DropPlatform", timer);
    }
  }

  private void DropPlatform() {
    if (gameObject.GetComponent<Rigidbody2D>() == null) {
      

      ParticleSystem[] thursters = gameObject.GetComponentsInChildren<ParticleSystem>();
      foreach (ParticleSystem cur in thursters) {
        cur.Stop();
      }

      gameObject.AddComponent<Rigidbody2D>();
    }
  }
}
