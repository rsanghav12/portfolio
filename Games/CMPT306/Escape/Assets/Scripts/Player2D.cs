﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player2D : Controller2D {
  /* Max health for the player */
  private const int max_health = 3;
  /* Time to stay immune after getting hit by a threat */
  private const float immunity_time = 2.0f;
  /* How fast it flickers when immune */
  private const float flicker_rate = 0.1f;
  /* How much to rotate while moving */
  private Quaternion target_rotation;
  private SpriteRenderer sprite_renderer;
  /* Current health of player */
  private int health;
  /* If the player is immune or not */
  private bool is_immune;
  /* Flicker coroutine to start and stop anytime */
	private Coroutine flicker_coroutine;
  /* OG color of player to get back to after the flicker */
	private Color original_color;
  /* Input for the player */
  private Input2D input;
  /* Start score of the player (mainly used to start the score at 0 no
   * matter where the player starts as the y value of player
   * is the score. */
  private int score_offset;
  /* Current score that the player has accumulated */
  public int current_score { get; private set; }

  [SerializeField] public AudioClip jump_sound;
  private AudioSource audio_source;

  protected override void Start() {
    base.Start();

    /* default values */
    health = max_health;
    is_immune = false;
    sprite_renderer = GetComponent<SpriteRenderer>();
    original_color = sprite_renderer.color;
    target_rotation = Quaternion.identity;
    score_offset = Mathf.RoundToInt(transform.position.y);
    current_score = 0;
    audio_source = GetComponent<AudioSource>();

    /* start tilting towards target */
    InvokeRepeating("TiltTowards", 0, 0.001f);
    Invoke("RemoveSpawnEffect", 1f);
  }

  private void RemoveSpawnEffect() {
    StartCoroutine(SpawnEffectFade());
  }

  private IEnumerator SpawnEffectFade() {
    float duration = 2f;
    ParticleSystem p_sys = GameObject.FindGameObjectWithTag("SpawnEffect").GetComponent<ParticleSystem>();
    Color start = p_sys.main.startColor.color;
    Color end = new Color(start.r, start.g, start.b, 0f);
    for (float t = 0.0f; t < duration; t += Time.deltaTime) {
      Color changedColor = Color.Lerp(start, end, t / duration);
      var main = p_sys.main;
      main.startColor = new ParticleSystem.MinMaxGradient(changedColor);
      yield return null;
    }
    //Destroy(GameObject.FindGameObjectWithTag("SpawnEffect"), duration + 1f);
    yield break;
  }

  private void Update() {
    /* move depending on the input then handle the score */
    MoveMSG move = new MoveMSG();
    move.jump = Jump.NONE;
    move.horizontal = input.Horizontal();

    if (move.horizontal != 0) {
      int dir = (Direction() ? -1 : 1);
      target_rotation = Quaternion.Euler(0, 0, dir * 20f);
    } else {
      target_rotation = Quaternion.identity;
    }

    if (input.JumpButtonDown() && IsGrounded()) {
      audio_source.PlayOneShot(jump_sound, Random.Range(0.5f, 1f));
      move.jump = Jump.MAX;
    }

    if (input.JumpButtonUp()) {
      float minJumpVelocity = MinJumpVelocity();
      if (rigidbody.velocity.y > minJumpVelocity) {
        move.jump = Jump.MIN;
      }
    }

    Move(move);
    HandleScore();
  }

  /* Flicker coroutine (very smooth flickers) */
  private IEnumerator Flicker(float flickerRate) {
    bool alt = false;
    Color start = original_color;
    Color end = new Color(1f, original_color.g * 0.5f, original_color.b * 0.5f);
    while(true) {
      for (float t = 0.0f; t < 0.2f; t += Time.deltaTime) {
        Color changedColor = (alt ? Color.Lerp(end, start, t / 0.2f) : Color.Lerp(start, end, t / 0.2f));
        sprite_renderer.color = changedColor;
        yield return null;
      }
      alt = !alt;
      yield return new WaitForSeconds(flickerRate);
    }
  }

  /* Sustain damage or if health is 0 then end game or start ignoring collisions with
   * enemies and threats */
  private void SustainDamage(int Damage) {
    if (is_immune == false) {
      health--;

      if (health == 0) {
        quitting = true;
        EndGame();
      }

      is_immune = true;
      flicker_coroutine = StartCoroutine(Flicker(flicker_rate));
      Invoke("RemoveImmunity", immunity_time);
      Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Threat"));
    }
  }

  /* Remove immunity and stop ignoring collisons */
  private void RemoveImmunity() {
    StopCoroutine(flicker_coroutine);
    sprite_renderer.color = original_color;
    is_immune = false;
    Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Threat"), false);
  }

  private void TiltTowards() {
    transform.rotation = Quaternion.Slerp(transform.rotation, target_rotation, Time.deltaTime);
  }

  private void HandleScore() {
    int tempScore = Mathf.RoundToInt(transform.position.y) - score_offset;
    if (tempScore > current_score) {
      current_score = tempScore;
    }
  }

  private void EndGame() {
    LoadScene load_scene = Camera.main.gameObject.GetComponent<LoadScene>();
    if (load_scene) load_scene.LoadSceneWithIndex(2);
  }
		
  private bool quitting;
	private void OnBecameInvisible() {
    if (!quitting) {
      EndGame();
    }
	}

  private void OnApplicationQuit() {
    quitting = true;
  }

  /// <summary>
  /// Get max helath of the player.
  /// </summary>
  /// <returns>Max health</returns>
  public int GetMaxHealth() {
    return max_health;
  }

  /// <summary>
  /// Get current health of the player.
  /// </summary>
  /// <returns>Current health</returns>
  public int GetHealth() {
    return health;
  }

  /// <summary>
  /// Set the input source of player.
  /// </summary>
  /// <param name="_input">Input source</param>
  public void SetInput(Input2D _input) {
    input = _input;
  }
}
