﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Follows the target on y-axis. It will also not go down. This is useful for infinite jumper games.
/// </summary>
public class FollowTarget : MonoBehaviour {
  /* The dampner in camera movement for smooth translation */
  private const float damp_time = 0.15f;

  /* Transform of the target */
  private Transform target;

  /* Velocity of the camera movement */
  private Vector3 velocity;

  private void Start() {
    velocity = Vector3.zero;
    target = GameObject.FindGameObjectWithTag(Preferences.player_tag).transform;
  }

  /* Camera should be updated on LateUpdate to reflect all changes */
  private void FixedUpdate() {
    /* if target is set */
    if (target) {
      /* get position of target */
      Vector3 target_pos = Camera.main.WorldToViewportPoint(target.position);
      /* get where camera should move */
      Vector3 destination = target.position - Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, target_pos.z));
      destination += transform.position;
      /* apply smooth damp to destination */
      Vector3 camera_pos = Vector3.SmoothDamp(transform.position, destination, ref velocity, damp_time);
      /* if camera position is higher, then update camera position, else don't update */
      camera_pos = new Vector3(camera_pos.x, (camera_pos.y < transform.position.y ? transform.position.y : camera_pos.y), camera_pos.z);
      transform.position = camera_pos;
    }
  }
}
