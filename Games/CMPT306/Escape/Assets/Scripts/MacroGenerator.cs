﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MacroGenerator : MonoBehaviour {
  /* Platforms in each created level */
	private const int blocks_in_view = 4;
  /* Increase difficulty every defined rate in seconds */
	private const float difficulty_repeat_rate = 0.5f;
  /* Increase difficulty by a defined value */
	private const float difficulty_rate = 0.06f;
  /* Start value for difficulty */
	private const float difficulty_start = 0.1f;
  /* Duration of some threats on screen (will be changed according
   * to difficulty but this is the max value) */
  private const float threat_duration = 10f;

  /* Size of the screen */
  private Vector2 screen_size;
  /* List of created micro levels */
  private List<GameObject> micro_levels;
  /* Total created levels (used to keep track of
   * micro level positions */
  private int total_created_level;
  /* Mutex lock to prevent race conditions
   * accessing the micro levels list */
  private object _lock;
  /* Complexity of the created level (increases
   * as time passes by and more levels get created) */
  private float difficulty;

	private Vector2 last_platform_pos;

  [SerializeField] public AudioClip fallingplatform_sound;

  protected void Awake() {
    /* get top right position of camera */
    Vector2 edge_vector = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));
    float height = edge_vector.y * 2; /* total height of screen */
    float width = edge_vector.x * 2; /* total width of screen */

    screen_size = new Vector2(width, height);
    micro_levels = new List<GameObject>();
    _lock = new object();
    last_platform_pos = Vector2.zero;
    difficulty = difficulty_start;
    InvokeRepeating("MeasureDifficulty", 0, difficulty_repeat_rate);

    Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Threat"), false);
  }

  protected void Start() {
     CreateLevel(true);
  }

  private void Update() {
    /* lock is used to make sure list count
     * does not change and we can delete from it
     * safely. */
    lock (_lock) {
      if (micro_levels.Count > 0 && micro_levels.Count % 5 == 0) {
        GameObject micro_level = micro_levels[0];
        micro_levels.Remove(micro_level);
        Destroy(micro_level);
      }
    }
  }

  public void CreateLevel(bool base_level = false) {
    /* have micro generator create a level for us and wait to be added
     * to the list and positioned */
    GameObject micro_level = new GameObject("MicroLevel(" + total_created_level++ + ")");
    MicroGenerator micro_generator = micro_level.AddComponent<MicroGenerator>();
    micro_generator.blocks_in_level = blocks_in_view;
    micro_generator.difficulty = difficulty;

    last_platform_pos = micro_generator.CreateLevel(screen_size.y, screen_size.x, last_platform_pos, base_level);

    /* every 9th creation of level show a missile launcher */
    if (total_created_level % 10 == 0) {
      /* duration is defined by the current difficulty */
      float duration = Mathf.Clamp(threat_duration * difficulty, 0f, threat_duration);
      ActivateShip(duration);
    }

    lock (_lock) {
      micro_levels.Add(micro_level);
    }
  }

  private void ActivateShip(float duration) {
    GameObject.FindGameObjectWithTag("Ship").GetComponent<Ship>().active = true;
    Invoke("DeactivateShip", duration);
  }

  private void DeactivateShip() {
    GameObject.FindGameObjectWithTag("Ship").GetComponent<Ship>().active = false;
  }

  /// <summary>
  /// Increase difficulty by the defined
  /// difficulty rate.
  /// </summary>
  private void MeasureDifficulty() {
    difficulty += difficulty_rate;
  }
}
