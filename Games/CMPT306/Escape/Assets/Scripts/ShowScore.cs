﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowScore : MonoBehaviour {
  private Text score_text;

  private void Start() {
    score_text = GetComponent<Text>();
    score_text.text = "Score\n" + Preferences.score;
  }
}
