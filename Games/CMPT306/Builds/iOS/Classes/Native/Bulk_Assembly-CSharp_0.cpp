﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// Controller2D
struct Controller2D_t802485922;
// System.Type
struct Type_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;
// UnityEngine.Object
struct Object_t1021602117;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Collider2D
struct Collider2D_t646061738;
// UnityEngine.Collider2D[]
struct Collider2DU5BU5D_t3535523695;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// DecideInput
struct DecideInput_t109196168;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Player2D
struct Player2D_t165353355;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.Camera
struct Camera_t189460977;
// JoyStick
struct JoyStick_t513152754;
// Input2D
struct Input2D_t2938435410;
// FallingPlatform
struct FallingPlatform_t2590793300;
// UnityEngine.Collision2D
struct Collision2D_t1539500754;
// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t1490986844;
// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// FollowTarget
struct FollowTarget_t2817477884;
// GarbageCollector
struct GarbageCollector_t1348916102;
// HealthHUD
struct HealthHUD_t66113683;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1599784723;
// UnityEngine.UI.Graphic
struct Graphic_t2426225576;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// KeyboardInput
struct KeyboardInput_t1795993661;
// LoadScene
struct LoadScene_t1133380052;
// LoadTransition
struct LoadTransition_t4049825803;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// LoadScene/<LoadSceneWithTransition>c__Iterator0
struct U3CLoadSceneWithTransitionU3Ec__Iterator0_t1207375911;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t3839502067;
// System.NotSupportedException
struct NotSupportedException_t1793819818;
// UnityEngine.Texture
struct Texture_t2243626319;
// MacroGenerator
struct MacroGenerator_t1652377675;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// MicroGenerator
struct MicroGenerator_t2658307363;
// Ship
struct Ship_t1116303770;
// PlatformPool
struct PlatformPool_t3559850805;
// PlatformGenerator
struct PlatformGenerator_t395172572;
// UnityEngine.SpriteRenderer[]
struct SpriteRendererU5BU5D_t1098056643;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// UnityEngine.BoxCollider2D
struct BoxCollider2D_t948534547;
// UnityEngine.Renderer
struct Renderer_t257310565;
// Missile
struct Missile_t813944928;
// Threat
struct Threat_t2614656216;
// UnityEngine.Behaviour
struct Behaviour_t955675639;
// OneWayPlatform
struct OneWayPlatform_t3777955472;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// Player2D/<SpawnEffectFade>c__Iterator0
struct U3CSpawnEffectFadeU3Ec__Iterator0_t2652861317;
// Player2D/<Flicker>c__Iterator1
struct U3CFlickerU3Ec__Iterator1_t3991061274;
// Preferences
struct Preferences_t60026258;
// Score
struct Score_t1518975274;
// UnityEngine.UI.Text
struct Text_t356221433;
// Ship/<Fade>c__Iterator0
struct U3CFadeU3Ec__Iterator0_t3715132008;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// ShowScore
struct ShowScore_t4012426113;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.IntPtr[]
struct IntPtrU5BU5D_t169632028;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t3466835263;
// System.Void
struct Void_t1841601450;
// UnityEngine.ContactPoint2D[]
struct ContactPoint2DU5BU5D_t1215651809;
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_t2336171397;
// UnityEngine.Gradient
struct Gradient_t3600583008;
// System.Type[]
struct TypeU5BU5D_t1664964607;
// System.Reflection.MemberFilter
struct MemberFilter_t3405857066;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t834278767;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t3522132132;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t2020713228;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t261436805;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t385374196;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3177091249;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t1156185964;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3778758259;
// UnityEngine.UI.FontData
struct FontData_t2614388407;
// UnityEngine.TextGenerator
struct TextGenerator_t647235000;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t3048644023;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;

extern const RuntimeType* PolygonCollider2D_t3220183178_0_0_0_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Controller2D__ctor_m1972723229_MetadataUsageId;
extern RuntimeClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern RuntimeClass* Collider2D_t646061738_il2cpp_TypeInfo_var;
extern RuntimeClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral1086556859;
extern const uint32_t Controller2D_Start_m774250573_MetadataUsageId;
extern RuntimeClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t Controller2D_Move_m3918239025_MetadataUsageId;
extern RuntimeClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern RuntimeClass* Physics2D_t2540166467_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const uint32_t Controller2D_IsGrounded_m2912631519_MetadataUsageId;
extern const uint32_t Controller2D_Gravity_m418077541_MetadataUsageId;
extern const uint32_t Controller2D_MaxJumpVelocity_m2382483678_MetadataUsageId;
extern const uint32_t Controller2D_DoubleJumpVelocity_m2506104035_MetadataUsageId;
extern const uint32_t Controller2D_MinJumpVelocity_m4136881028_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponents_TisCollider2D_t646061738_m753859713_RuntimeMethod_var;
extern const uint32_t Controller2D_GetBounds_m1748675743_MetadataUsageId;
extern RuntimeClass* Preferences_t60026258_il2cpp_TypeInfo_var;
extern RuntimeClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_GetComponent_TisPlayer2D_t165353355_m1754251190_RuntimeMethod_var;
extern const RuntimeMethod* Object_Instantiate_TisGameObject_t1756533147_m3664764861_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisCanvas_t209405766_m195193039_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponentInChildren_TisJoyStick_t513152754_m3063844157_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral884247417;
extern const uint32_t DecideInput_Awake_m4169716228_MetadataUsageId;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_GetComponentsInChildren_TisParticleSystem_t3394631041_m2199618894_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral4243273451;
extern Il2CppCodeGenString* _stringLiteral2566379072;
extern Il2CppCodeGenString* _stringLiteral1973320476;
extern const uint32_t FallingPlatform_OnCollisionStay2D_m3937589890_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisRigidbody2D_t502193897_m2081092396_RuntimeMethod_var;
extern const uint32_t FallingPlatform_DropPlatform_m757953137_MetadataUsageId;
extern RuntimeClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t FollowTarget_Start_m1233455719_MetadataUsageId;
extern const uint32_t FollowTarget_FixedUpdate_m2936679024_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3924929881;
extern const uint32_t HealthHUD_Awake_m574236355_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisImage_t2042527209_m4162535761_RuntimeMethod_var;
extern const uint32_t HealthHUD_LateUpdate_m3904430699_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisImage_t2042527209_m2189462422_RuntimeMethod_var;
extern const uint32_t JoyStick_Start_m3563140509_MetadataUsageId;
extern RuntimeClass* RectTransformUtility_t2941082270_il2cpp_TypeInfo_var;
extern const uint32_t JoyStick_OnDrag_m1637494730_MetadataUsageId;
extern const uint32_t JoyStick_OnPointerUp_m4080876746_MetadataUsageId;
extern RuntimeClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral855845486;
extern Il2CppCodeGenString* _stringLiteral842948034;
extern const uint32_t KeyboardInput_Update_m2215788453_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisLoadTransition_t4049825803_m3996224546_RuntimeMethod_var;
extern const uint32_t LoadScene_LoadSceneWithIndex_m216160262_MetadataUsageId;
extern RuntimeClass* U3CLoadSceneWithTransitionU3Ec__Iterator0_t1207375911_il2cpp_TypeInfo_var;
extern const uint32_t LoadScene_LoadSceneWithTransition_m748142228_MetadataUsageId;
extern RuntimeClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const uint32_t U3CLoadSceneWithTransitionU3Ec__Iterator0_MoveNext_m463968662_MetadataUsageId;
extern RuntimeClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CLoadSceneWithTransitionU3Ec__Iterator0_Reset_m773062411_MetadataUsageId;
extern RuntimeClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern const uint32_t LoadTransition_OnGUI_m3203620930_MetadataUsageId;
extern RuntimeClass* List_1_t1125654279_il2cpp_TypeInfo_var;
extern RuntimeClass* RuntimeObject_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m704351054_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral520868631;
extern Il2CppCodeGenString* _stringLiteral1875862075;
extern Il2CppCodeGenString* _stringLiteral3342734734;
extern const uint32_t MacroGenerator_Awake_m1893318087_MetadataUsageId;
extern const RuntimeMethod* List_1_get_Count_m2764296230_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m939767277_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Remove_m2287078133_RuntimeMethod_var;
extern const uint32_t MacroGenerator_Update_m1313472171_MetadataUsageId;
extern RuntimeClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_AddComponent_TisMicroGenerator_t2658307363_m1900695421_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m3441471442_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral2138312532;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern const uint32_t MacroGenerator_CreateLevel_m2116947579_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisShip_t1116303770_m1555807537_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral1844382288;
extern Il2CppCodeGenString* _stringLiteral3290965114;
extern const uint32_t MacroGenerator_ActivateShip_m2260158446_MetadataUsageId;
extern const uint32_t MacroGenerator_DeactivateShip_m3391759862_MetadataUsageId;
extern const uint32_t MicroGenerator_Awake_m2021636335_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisPlatformPool_t3559850805_m831201488_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m4030601119_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisPlatformGenerator_t395172572_m3551609875_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponentsInChildren_TisSpriteRenderer_t1209076198_m2671756693_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisFallingPlatform_t2590793300_m4057477800_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisBoxCollider2D_t948534547_m2942557550_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral4287929323;
extern Il2CppCodeGenString* _stringLiteral1333771709;
extern Il2CppCodeGenString* _stringLiteral1761669571;
extern const uint32_t MicroGenerator_CreateLevel_m1130512212_MetadataUsageId;
extern RuntimeClass* Bounds_t3033363703_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponentsInChildren_TisSpriteRenderer_t1209076198_m1456864797_RuntimeMethod_var;
extern const uint32_t MicroGenerator_GetBounds_m2624662798_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisMacroGenerator_t1652377675_m1524168656_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral2380456193;
extern const uint32_t MicroGenerator_OnTriggerEnter2D_m1406339080_MetadataUsageId;
extern const uint32_t Missile_Update_m430249726_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisCollider2D_t646061738_m2950827934_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponentInChildren_TisParticleSystem_t3394631041_m347610273_RuntimeMethod_var;
extern const uint32_t Missile_OnTriggerEnter2D_m2921101119_MetadataUsageId;
extern const uint32_t Missile_Destroy_m1179550945_MetadataUsageId;
extern const uint32_t Missile_OnBecameInvisible_m3876137554_MetadataUsageId;
extern const uint32_t OneWayPlatform_Start_m1213573435_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2096328193;
extern const uint32_t OneWayPlatform_Update_m3913346926_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2395017070;
extern Il2CppCodeGenString* _stringLiteral2328503377;
extern Il2CppCodeGenString* _stringLiteral2241924114;
extern Il2CppCodeGenString* _stringLiteral1393335811;
extern const uint32_t PlatformGenerator_Create_m875964463_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_RuntimeMethod_var;
extern const uint32_t PlatformGenerator_GetBounds_m3188841413_MetadataUsageId;
extern const uint32_t PlatformGenerator_OnDestroy_m3031313454_MetadataUsageId;
extern const uint32_t PlatformGenerator_ReturnTiles_m2688380672_MetadataUsageId;
extern const RuntimeMethod* GameObject_AddComponent_TisPlatformGenerator_t395172572_m1088084192_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisOneWayPlatform_t3777955472_m2849326318_RuntimeMethod_var;
extern const uint32_t PlatformGenerator_GeneratePlatform_m470059935_MetadataUsageId;
extern RuntimeClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m4211463620_RuntimeMethod_var;
extern RuntimeField* U3CPrivateImplementationDetailsU3E_t1486305142____U24fieldU2D5CF7DBEB679CBFEC1FCB176A380E936AD360D7AA_0_FieldInfo_var;
extern Il2CppCodeGenString* _stringLiteral1964570358;
extern Il2CppCodeGenString* _stringLiteral511365757;
extern Il2CppCodeGenString* _stringLiteral763868829;
extern Il2CppCodeGenString* _stringLiteral3893686066;
extern Il2CppCodeGenString* _stringLiteral1695186766;
extern Il2CppCodeGenString* _stringLiteral2859695323;
extern const uint32_t PlatformPool_Awake_m1199261169_MetadataUsageId;
extern const uint32_t PlatformPool_WidthOf_m3300936485_MetadataUsageId;
extern const uint32_t PlatformPool_HeightOf_m2336189100_MetadataUsageId;
extern RuntimeClass* Quaternion_t4030073918_il2cpp_TypeInfo_var;
extern const uint32_t PlatformPool_ActivateTile_m4049973885_MetadataUsageId;
extern const RuntimeMethod* List_1_set_Item_m1811192197_RuntimeMethod_var;
extern const uint32_t PlatformPool_GetTileWithType_m1945733132_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2810074457;
extern Il2CppCodeGenString* _stringLiteral360295816;
extern const uint32_t Player2D_Start_m995759222_MetadataUsageId;
extern RuntimeClass* U3CSpawnEffectFadeU3Ec__Iterator0_t2652861317_il2cpp_TypeInfo_var;
extern const uint32_t Player2D_SpawnEffectFade_m1418828654_MetadataUsageId;
extern RuntimeClass* MoveMSG_t2690495395_il2cpp_TypeInfo_var;
extern const uint32_t Player2D_Update_m805001203_MetadataUsageId;
extern RuntimeClass* U3CFlickerU3Ec__Iterator1_t3991061274_il2cpp_TypeInfo_var;
extern const uint32_t Player2D_Flicker_m4174058381_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3026533180;
extern const uint32_t Player2D_SustainDamage_m3638841263_MetadataUsageId;
extern const uint32_t Player2D_RemoveImmunity_m3717843014_MetadataUsageId;
extern const uint32_t Player2D_TiltTowards_m26670375_MetadataUsageId;
extern const uint32_t Player2D_HandleScore_m3074900822_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisLoadScene_t1133380052_m1576248279_RuntimeMethod_var;
extern const uint32_t Player2D_EndGame_m4133746719_MetadataUsageId;
extern const uint32_t U3CFlickerU3Ec__Iterator1_MoveNext_m4204913829_MetadataUsageId;
extern const uint32_t U3CFlickerU3Ec__Iterator1_Reset_m1687388178_MetadataUsageId;
extern const RuntimeMethod* GameObject_GetComponent_TisParticleSystem_t3394631041_m1329366749_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral802309048;
extern const uint32_t U3CSpawnEffectFadeU3Ec__Iterator0_MoveNext_m2533121988_MetadataUsageId;
extern const uint32_t U3CSpawnEffectFadeU3Ec__Iterator0_Reset_m2103006869_MetadataUsageId;
extern const uint32_t Preferences_Awake_m2221711938_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3746908446;
extern Il2CppCodeGenString* _stringLiteral400676400;
extern const uint32_t Preferences__cctor_m1876238888_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisText_t356221433_m1342661039_RuntimeMethod_var;
extern const uint32_t Score_Start_m1559619439_MetadataUsageId;
extern RuntimeClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1756683522;
extern const uint32_t Score_LateUpdate_m2915824364_MetadataUsageId;
extern RuntimeClass* Vector3U5BU5D_t1172311765_il2cpp_TypeInfo_var;
extern const uint32_t Ship_Start_m1273291801_MetadataUsageId;
extern RuntimeClass* U3CFadeU3Ec__Iterator0_t3715132008_il2cpp_TypeInfo_var;
extern const uint32_t Ship_Fade_m2975741434_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisRectTransform_t3349966182_m1310250299_RuntimeMethod_var;
extern const uint32_t Ship_FixedUpdate_m2175155974_MetadataUsageId;
extern const uint32_t Ship_FollowTarget_m1654037237_MetadataUsageId;
extern const RuntimeMethod* Object_Instantiate_TisGameObject_t1756533147_m3064851704_RuntimeMethod_var;
extern const uint32_t Ship_Fire_m2857528497_MetadataUsageId;
extern const uint32_t U3CFadeU3Ec__Iterator0_Reset_m4019819668_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2018315346;
extern const uint32_t ShowScore_Start_m298709576_MetadataUsageId;
extern const uint32_t Threat_PlayerObjects_m1428971637_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3935001822;
extern const uint32_t Threat_Hit_m4230409352_MetadataUsageId;
struct ContactPoint2D_t3659330976 ;
struct Gradient_t3600583008_marshaled_com;

struct StringU5BU5D_t1642385972;
struct Collider2DU5BU5D_t3535523695;
struct ParticleSystemU5BU5D_t1490986844;
struct GameObjectU5BU5D_t3057952154;
struct SpriteRendererU5BU5D_t1098056643;
struct Int32U5BU5D_t3030399641;
struct Vector3U5BU5D_t1172311765;


#ifndef U3CMODULEU3E_T3783534233_H
#define U3CMODULEU3E_T3783534233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534233 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534233_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t1328083999* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t1328083999* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t1328083999** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t1328083999* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef EXCEPTION_T1927440687_H
#define EXCEPTION_T1927440687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t1927440687  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t169632028* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t1927440687 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t169632028* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t169632028** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t169632028* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___inner_exception_1)); }
	inline Exception_t1927440687 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t1927440687 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t1927440687 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t1927440687, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T1927440687_H
#ifndef U3CLOADSCENEWITHTRANSITIONU3EC__ITERATOR0_T1207375911_H
#define U3CLOADSCENEWITHTRANSITIONU3EC__ITERATOR0_T1207375911_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadScene/<LoadSceneWithTransition>c__Iterator0
struct  U3CLoadSceneWithTransitionU3Ec__Iterator0_t1207375911  : public RuntimeObject
{
public:
	// LoadTransition LoadScene/<LoadSceneWithTransition>c__Iterator0::transition
	LoadTransition_t4049825803 * ___transition_0;
	// System.Int32 LoadScene/<LoadSceneWithTransition>c__Iterator0::index
	int32_t ___index_1;
	// System.Object LoadScene/<LoadSceneWithTransition>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean LoadScene/<LoadSceneWithTransition>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 LoadScene/<LoadSceneWithTransition>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_transition_0() { return static_cast<int32_t>(offsetof(U3CLoadSceneWithTransitionU3Ec__Iterator0_t1207375911, ___transition_0)); }
	inline LoadTransition_t4049825803 * get_transition_0() const { return ___transition_0; }
	inline LoadTransition_t4049825803 ** get_address_of_transition_0() { return &___transition_0; }
	inline void set_transition_0(LoadTransition_t4049825803 * value)
	{
		___transition_0 = value;
		Il2CppCodeGenWriteBarrier((&___transition_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(U3CLoadSceneWithTransitionU3Ec__Iterator0_t1207375911, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CLoadSceneWithTransitionU3Ec__Iterator0_t1207375911, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CLoadSceneWithTransitionU3Ec__Iterator0_t1207375911, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CLoadSceneWithTransitionU3Ec__Iterator0_t1207375911, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADSCENEWITHTRANSITIONU3EC__ITERATOR0_T1207375911_H
#ifndef LIST_1_T1125654279_H
#define LIST_1_T1125654279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct  List_1_t1125654279  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	GameObjectU5BU5D_t3057952154* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1125654279, ____items_1)); }
	inline GameObjectU5BU5D_t3057952154* get__items_1() const { return ____items_1; }
	inline GameObjectU5BU5D_t3057952154** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(GameObjectU5BU5D_t3057952154* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1125654279, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1125654279, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1125654279_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	GameObjectU5BU5D_t3057952154* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1125654279_StaticFields, ___EmptyArray_4)); }
	inline GameObjectU5BU5D_t3057952154* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(GameObjectU5BU5D_t3057952154* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1125654279_H
#ifndef U3CFADEU3EC__ITERATOR0_T3715132008_H
#define U3CFADEU3EC__ITERATOR0_T3715132008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ship/<Fade>c__Iterator0
struct  U3CFadeU3Ec__Iterator0_t3715132008  : public RuntimeObject
{
public:
	// System.Single Ship/<Fade>c__Iterator0::<duration>__0
	float ___U3CdurationU3E__0_0;
	// System.Int32 Ship/<Fade>c__Iterator0::dir
	int32_t ___dir_1;
	// System.Single Ship/<Fade>c__Iterator0::<t>__1
	float ___U3CtU3E__1_2;
	// System.Single Ship/<Fade>c__Iterator0::<t>__2
	float ___U3CtU3E__2_3;
	// Ship Ship/<Fade>c__Iterator0::$this
	Ship_t1116303770 * ___U24this_4;
	// System.Object Ship/<Fade>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean Ship/<Fade>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 Ship/<Fade>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CdurationU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t3715132008, ___U3CdurationU3E__0_0)); }
	inline float get_U3CdurationU3E__0_0() const { return ___U3CdurationU3E__0_0; }
	inline float* get_address_of_U3CdurationU3E__0_0() { return &___U3CdurationU3E__0_0; }
	inline void set_U3CdurationU3E__0_0(float value)
	{
		___U3CdurationU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_dir_1() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t3715132008, ___dir_1)); }
	inline int32_t get_dir_1() const { return ___dir_1; }
	inline int32_t* get_address_of_dir_1() { return &___dir_1; }
	inline void set_dir_1(int32_t value)
	{
		___dir_1 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E__1_2() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t3715132008, ___U3CtU3E__1_2)); }
	inline float get_U3CtU3E__1_2() const { return ___U3CtU3E__1_2; }
	inline float* get_address_of_U3CtU3E__1_2() { return &___U3CtU3E__1_2; }
	inline void set_U3CtU3E__1_2(float value)
	{
		___U3CtU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E__2_3() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t3715132008, ___U3CtU3E__2_3)); }
	inline float get_U3CtU3E__2_3() const { return ___U3CtU3E__2_3; }
	inline float* get_address_of_U3CtU3E__2_3() { return &___U3CtU3E__2_3; }
	inline void set_U3CtU3E__2_3(float value)
	{
		___U3CtU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t3715132008, ___U24this_4)); }
	inline Ship_t1116303770 * get_U24this_4() const { return ___U24this_4; }
	inline Ship_t1116303770 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(Ship_t1116303770 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t3715132008, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t3715132008, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t3715132008, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFADEU3EC__ITERATOR0_T3715132008_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef YIELDINSTRUCTION_T3462875981_H
#define YIELDINSTRUCTION_T3462875981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t3462875981  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t3462875981_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t3462875981_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T3462875981_H
#ifndef ABSTRACTEVENTDATA_T1333959294_H
#define ABSTRACTEVENTDATA_T1333959294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.AbstractEventData
struct  AbstractEventData_t1333959294  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.EventSystems.AbstractEventData::m_Used
	bool ___m_Used_0;

public:
	inline static int32_t get_offset_of_m_Used_0() { return static_cast<int32_t>(offsetof(AbstractEventData_t1333959294, ___m_Used_0)); }
	inline bool get_m_Used_0() const { return ___m_Used_0; }
	inline bool* get_address_of_m_Used_0() { return &___m_Used_0; }
	inline void set_m_Used_0(bool value)
	{
		___m_Used_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTEVENTDATA_T1333959294_H
#ifndef LAYERMASK_T3188175821_H
#define LAYERMASK_T3188175821_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3188175821 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3188175821, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3188175821_H
#ifndef QUATERNION_T4030073918_H
#define QUATERNION_T4030073918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t4030073918 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t4030073918_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t4030073918  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t4030073918  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t4030073918 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t4030073918  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T4030073918_H
#ifndef SINGLE_T2076509932_H
#define SINGLE_T2076509932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t2076509932 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t2076509932, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T2076509932_H
#ifndef RECT_T3681755626_H
#define RECT_T3681755626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t3681755626 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T3681755626_H
#ifndef WAITFORSECONDS_T3839502067_H
#define WAITFORSECONDS_T3839502067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WaitForSeconds
struct  WaitForSeconds_t3839502067  : public YieldInstruction_t3462875981
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t3839502067, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t3839502067_marshaled_pinvoke : public YieldInstruction_t3462875981_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t3839502067_marshaled_com : public YieldInstruction_t3462875981_marshaled_com
{
	float ___m_Seconds_0;
};
#endif // WAITFORSECONDS_T3839502067_H
#ifndef UINT32_T2149682021_H
#define UINT32_T2149682021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2149682021 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t2149682021, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2149682021_H
#ifndef BOOLEAN_T3825574718_H
#define BOOLEAN_T3825574718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3825574718 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3825574718, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3825574718_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3825574718_H
#ifndef MAINMODULE_T6751348_H
#define MAINMODULE_T6751348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/MainModule
struct  MainModule_t6751348 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/MainModule::m_ParticleSystem
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(MainModule_t6751348, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t3394631041 * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t3394631041 ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t3394631041 * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleSystem_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_t6751348_marshaled_pinvoke
{
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_t6751348_marshaled_com
{
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;
};
#endif // MAINMODULE_T6751348_H
#ifndef SYSTEMEXCEPTION_T3877406272_H
#define SYSTEMEXCEPTION_T3877406272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t3877406272  : public Exception_t1927440687
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T3877406272_H
#ifndef U24ARRAYTYPEU3D12_T1568637718_H
#define U24ARRAYTYPEU3D12_T1568637718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t1568637718 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t1568637718__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T1568637718_H
#ifndef BASEEVENTDATA_T2681005625_H
#define BASEEVENTDATA_T2681005625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseEventData
struct  BaseEventData_t2681005625  : public AbstractEventData_t1333959294
{
public:
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseEventData::m_EventSystem
	EventSystem_t3466835263 * ___m_EventSystem_1;

public:
	inline static int32_t get_offset_of_m_EventSystem_1() { return static_cast<int32_t>(offsetof(BaseEventData_t2681005625, ___m_EventSystem_1)); }
	inline EventSystem_t3466835263 * get_m_EventSystem_1() const { return ___m_EventSystem_1; }
	inline EventSystem_t3466835263 ** get_address_of_m_EventSystem_1() { return &___m_EventSystem_1; }
	inline void set_m_EventSystem_1(EventSystem_t3466835263 * value)
	{
		___m_EventSystem_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEEVENTDATA_T2681005625_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef INT32_T2071877448_H
#define INT32_T2071877448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2071877448 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2071877448, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2071877448_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef RAYCASTHIT2D_T4063908774_H
#define RAYCASTHIT2D_T4063908774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit2D
struct  RaycastHit2D_t4063908774 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_t2243707579  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_t2243707579  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_t2243707579  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// UnityEngine.Collider2D UnityEngine.RaycastHit2D::m_Collider
	Collider2D_t646061738 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Centroid_0)); }
	inline Vector2_t2243707579  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_t2243707579 * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_t2243707579  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Point_1)); }
	inline Vector2_t2243707579  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_t2243707579 * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_t2243707579  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Normal_2)); }
	inline Vector2_t2243707579  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_t2243707579 * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_t2243707579  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t4063908774, ___m_Collider_5)); }
	inline Collider2D_t646061738 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider2D_t646061738 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider2D_t646061738 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t4063908774_marshaled_pinvoke
{
	Vector2_t2243707579  ___m_Centroid_0;
	Vector2_t2243707579  ___m_Point_1;
	Vector2_t2243707579  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t646061738 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t4063908774_marshaled_com
{
	Vector2_t2243707579  ___m_Centroid_0;
	Vector2_t2243707579  ___m_Point_1;
	Vector2_t2243707579  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t646061738 * ___m_Collider_5;
};
#endif // RAYCASTHIT2D_T4063908774_H
#ifndef BINDINGFLAGS_T1082350898_H
#define BINDINGFLAGS_T1082350898_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t1082350898 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t1082350898, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T1082350898_H
#ifndef COROUTINE_T2299508840_H
#define COROUTINE_T2299508840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Coroutine
struct  Coroutine_t2299508840  : public YieldInstruction_t3462875981
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t2299508840, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t2299508840_marshaled_pinvoke : public YieldInstruction_t3462875981_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t2299508840_marshaled_com : public YieldInstruction_t3462875981_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // COROUTINE_T2299508840_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef BOUNDS_T3033363703_H
#define BOUNDS_T3033363703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t3033363703 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t2243707580  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t2243707580  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t3033363703, ___m_Center_0)); }
	inline Vector3_t2243707580  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t2243707580 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t2243707580  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t3033363703, ___m_Extents_1)); }
	inline Vector3_t2243707580  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t2243707580 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t2243707580  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T3033363703_H
#ifndef TYPE_T3352948571_H
#define TYPE_T3352948571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/Type
struct  Type_t3352948571 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t3352948571, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T3352948571_H
#ifndef NOTSUPPORTEDEXCEPTION_T1793819818_H
#define NOTSUPPORTEDEXCEPTION_T1793819818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_t1793819818  : public SystemException_t3877406272
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_T1793819818_H
#ifndef JUMP_T1663674001_H
#define JUMP_T1663674001_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Controller2D/Jump
struct  Jump_t1663674001 
{
public:
	// System.Int32 Controller2D/Jump::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Jump_t1663674001, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JUMP_T1663674001_H
#ifndef RUNTIMEFIELDHANDLE_T2331729674_H
#define RUNTIMEFIELDHANDLE_T2331729674_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeFieldHandle
struct  RuntimeFieldHandle_t2331729674 
{
public:
	// System.IntPtr System.RuntimeFieldHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeFieldHandle_t2331729674, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEFIELDHANDLE_T2331729674_H
#ifndef PLATFORMTYPE_T1536521725_H
#define PLATFORMTYPE_T1536521725_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlatformType
struct  PlatformType_t1536521725 
{
public:
	// System.Int32 PlatformType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PlatformType_t1536521725, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMTYPE_T1536521725_H
#ifndef U3CFLICKERU3EC__ITERATOR1_T3991061274_H
#define U3CFLICKERU3EC__ITERATOR1_T3991061274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Player2D/<Flicker>c__Iterator1
struct  U3CFlickerU3Ec__Iterator1_t3991061274  : public RuntimeObject
{
public:
	// System.Boolean Player2D/<Flicker>c__Iterator1::<alt>__0
	bool ___U3CaltU3E__0_0;
	// UnityEngine.Color Player2D/<Flicker>c__Iterator1::<start>__0
	Color_t2020392075  ___U3CstartU3E__0_1;
	// UnityEngine.Color Player2D/<Flicker>c__Iterator1::<end>__0
	Color_t2020392075  ___U3CendU3E__0_2;
	// System.Single Player2D/<Flicker>c__Iterator1::<t>__1
	float ___U3CtU3E__1_3;
	// UnityEngine.Color Player2D/<Flicker>c__Iterator1::<changedColor>__2
	Color_t2020392075  ___U3CchangedColorU3E__2_4;
	// System.Single Player2D/<Flicker>c__Iterator1::flickerRate
	float ___flickerRate_5;
	// Player2D Player2D/<Flicker>c__Iterator1::$this
	Player2D_t165353355 * ___U24this_6;
	// System.Object Player2D/<Flicker>c__Iterator1::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean Player2D/<Flicker>c__Iterator1::$disposing
	bool ___U24disposing_8;
	// System.Int32 Player2D/<Flicker>c__Iterator1::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CaltU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFlickerU3Ec__Iterator1_t3991061274, ___U3CaltU3E__0_0)); }
	inline bool get_U3CaltU3E__0_0() const { return ___U3CaltU3E__0_0; }
	inline bool* get_address_of_U3CaltU3E__0_0() { return &___U3CaltU3E__0_0; }
	inline void set_U3CaltU3E__0_0(bool value)
	{
		___U3CaltU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CstartU3E__0_1() { return static_cast<int32_t>(offsetof(U3CFlickerU3Ec__Iterator1_t3991061274, ___U3CstartU3E__0_1)); }
	inline Color_t2020392075  get_U3CstartU3E__0_1() const { return ___U3CstartU3E__0_1; }
	inline Color_t2020392075 * get_address_of_U3CstartU3E__0_1() { return &___U3CstartU3E__0_1; }
	inline void set_U3CstartU3E__0_1(Color_t2020392075  value)
	{
		___U3CstartU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CendU3E__0_2() { return static_cast<int32_t>(offsetof(U3CFlickerU3Ec__Iterator1_t3991061274, ___U3CendU3E__0_2)); }
	inline Color_t2020392075  get_U3CendU3E__0_2() const { return ___U3CendU3E__0_2; }
	inline Color_t2020392075 * get_address_of_U3CendU3E__0_2() { return &___U3CendU3E__0_2; }
	inline void set_U3CendU3E__0_2(Color_t2020392075  value)
	{
		___U3CendU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E__1_3() { return static_cast<int32_t>(offsetof(U3CFlickerU3Ec__Iterator1_t3991061274, ___U3CtU3E__1_3)); }
	inline float get_U3CtU3E__1_3() const { return ___U3CtU3E__1_3; }
	inline float* get_address_of_U3CtU3E__1_3() { return &___U3CtU3E__1_3; }
	inline void set_U3CtU3E__1_3(float value)
	{
		___U3CtU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CchangedColorU3E__2_4() { return static_cast<int32_t>(offsetof(U3CFlickerU3Ec__Iterator1_t3991061274, ___U3CchangedColorU3E__2_4)); }
	inline Color_t2020392075  get_U3CchangedColorU3E__2_4() const { return ___U3CchangedColorU3E__2_4; }
	inline Color_t2020392075 * get_address_of_U3CchangedColorU3E__2_4() { return &___U3CchangedColorU3E__2_4; }
	inline void set_U3CchangedColorU3E__2_4(Color_t2020392075  value)
	{
		___U3CchangedColorU3E__2_4 = value;
	}

	inline static int32_t get_offset_of_flickerRate_5() { return static_cast<int32_t>(offsetof(U3CFlickerU3Ec__Iterator1_t3991061274, ___flickerRate_5)); }
	inline float get_flickerRate_5() const { return ___flickerRate_5; }
	inline float* get_address_of_flickerRate_5() { return &___flickerRate_5; }
	inline void set_flickerRate_5(float value)
	{
		___flickerRate_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CFlickerU3Ec__Iterator1_t3991061274, ___U24this_6)); }
	inline Player2D_t165353355 * get_U24this_6() const { return ___U24this_6; }
	inline Player2D_t165353355 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(Player2D_t165353355 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CFlickerU3Ec__Iterator1_t3991061274, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CFlickerU3Ec__Iterator1_t3991061274, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CFlickerU3Ec__Iterator1_t3991061274, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFLICKERU3EC__ITERATOR1_T3991061274_H
#ifndef RUNTIMETYPEHANDLE_T2330101084_H
#define RUNTIMETYPEHANDLE_T2330101084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t2330101084 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t2330101084, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T2330101084_H
#ifndef RIGIDBODYTYPE2D_T1116729369_H
#define RIGIDBODYTYPE2D_T1116729369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RigidbodyType2D
struct  RigidbodyType2D_t1116729369 
{
public:
	// System.Int32 UnityEngine.RigidbodyType2D::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RigidbodyType2D_t1116729369, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODYTYPE2D_T1116729369_H
#ifndef FILLMETHOD_T1640962579_H
#define FILLMETHOD_T1640962579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image/FillMethod
struct  FillMethod_t1640962579 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FillMethod_t1640962579, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLMETHOD_T1640962579_H
#ifndef COLLISION2D_T1539500754_H
#define COLLISION2D_T1539500754_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collision2D
struct  Collision2D_t1539500754  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Collision2D::m_Collider
	int32_t ___m_Collider_0;
	// System.Int32 UnityEngine.Collision2D::m_OtherCollider
	int32_t ___m_OtherCollider_1;
	// System.Int32 UnityEngine.Collision2D::m_Rigidbody
	int32_t ___m_Rigidbody_2;
	// System.Int32 UnityEngine.Collision2D::m_OtherRigidbody
	int32_t ___m_OtherRigidbody_3;
	// UnityEngine.ContactPoint2D[] UnityEngine.Collision2D::m_Contacts
	ContactPoint2DU5BU5D_t1215651809* ___m_Contacts_4;
	// UnityEngine.Vector2 UnityEngine.Collision2D::m_RelativeVelocity
	Vector2_t2243707579  ___m_RelativeVelocity_5;
	// System.Int32 UnityEngine.Collision2D::m_Enabled
	int32_t ___m_Enabled_6;

public:
	inline static int32_t get_offset_of_m_Collider_0() { return static_cast<int32_t>(offsetof(Collision2D_t1539500754, ___m_Collider_0)); }
	inline int32_t get_m_Collider_0() const { return ___m_Collider_0; }
	inline int32_t* get_address_of_m_Collider_0() { return &___m_Collider_0; }
	inline void set_m_Collider_0(int32_t value)
	{
		___m_Collider_0 = value;
	}

	inline static int32_t get_offset_of_m_OtherCollider_1() { return static_cast<int32_t>(offsetof(Collision2D_t1539500754, ___m_OtherCollider_1)); }
	inline int32_t get_m_OtherCollider_1() const { return ___m_OtherCollider_1; }
	inline int32_t* get_address_of_m_OtherCollider_1() { return &___m_OtherCollider_1; }
	inline void set_m_OtherCollider_1(int32_t value)
	{
		___m_OtherCollider_1 = value;
	}

	inline static int32_t get_offset_of_m_Rigidbody_2() { return static_cast<int32_t>(offsetof(Collision2D_t1539500754, ___m_Rigidbody_2)); }
	inline int32_t get_m_Rigidbody_2() const { return ___m_Rigidbody_2; }
	inline int32_t* get_address_of_m_Rigidbody_2() { return &___m_Rigidbody_2; }
	inline void set_m_Rigidbody_2(int32_t value)
	{
		___m_Rigidbody_2 = value;
	}

	inline static int32_t get_offset_of_m_OtherRigidbody_3() { return static_cast<int32_t>(offsetof(Collision2D_t1539500754, ___m_OtherRigidbody_3)); }
	inline int32_t get_m_OtherRigidbody_3() const { return ___m_OtherRigidbody_3; }
	inline int32_t* get_address_of_m_OtherRigidbody_3() { return &___m_OtherRigidbody_3; }
	inline void set_m_OtherRigidbody_3(int32_t value)
	{
		___m_OtherRigidbody_3 = value;
	}

	inline static int32_t get_offset_of_m_Contacts_4() { return static_cast<int32_t>(offsetof(Collision2D_t1539500754, ___m_Contacts_4)); }
	inline ContactPoint2DU5BU5D_t1215651809* get_m_Contacts_4() const { return ___m_Contacts_4; }
	inline ContactPoint2DU5BU5D_t1215651809** get_address_of_m_Contacts_4() { return &___m_Contacts_4; }
	inline void set_m_Contacts_4(ContactPoint2DU5BU5D_t1215651809* value)
	{
		___m_Contacts_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Contacts_4), value);
	}

	inline static int32_t get_offset_of_m_RelativeVelocity_5() { return static_cast<int32_t>(offsetof(Collision2D_t1539500754, ___m_RelativeVelocity_5)); }
	inline Vector2_t2243707579  get_m_RelativeVelocity_5() const { return ___m_RelativeVelocity_5; }
	inline Vector2_t2243707579 * get_address_of_m_RelativeVelocity_5() { return &___m_RelativeVelocity_5; }
	inline void set_m_RelativeVelocity_5(Vector2_t2243707579  value)
	{
		___m_RelativeVelocity_5 = value;
	}

	inline static int32_t get_offset_of_m_Enabled_6() { return static_cast<int32_t>(offsetof(Collision2D_t1539500754, ___m_Enabled_6)); }
	inline int32_t get_m_Enabled_6() const { return ___m_Enabled_6; }
	inline int32_t* get_address_of_m_Enabled_6() { return &___m_Enabled_6; }
	inline void set_m_Enabled_6(int32_t value)
	{
		___m_Enabled_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Collision2D
struct Collision2D_t1539500754_marshaled_pinvoke
{
	int32_t ___m_Collider_0;
	int32_t ___m_OtherCollider_1;
	int32_t ___m_Rigidbody_2;
	int32_t ___m_OtherRigidbody_3;
	ContactPoint2D_t3659330976 * ___m_Contacts_4;
	Vector2_t2243707579  ___m_RelativeVelocity_5;
	int32_t ___m_Enabled_6;
};
// Native definition for COM marshalling of UnityEngine.Collision2D
struct Collision2D_t1539500754_marshaled_com
{
	int32_t ___m_Collider_0;
	int32_t ___m_OtherCollider_1;
	int32_t ___m_Rigidbody_2;
	int32_t ___m_OtherRigidbody_3;
	ContactPoint2D_t3659330976 * ___m_Contacts_4;
	Vector2_t2243707579  ___m_RelativeVelocity_5;
	int32_t ___m_Enabled_6;
};
#endif // COLLISION2D_T1539500754_H
#ifndef U3CSPAWNEFFECTFADEU3EC__ITERATOR0_T2652861317_H
#define U3CSPAWNEFFECTFADEU3EC__ITERATOR0_T2652861317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Player2D/<SpawnEffectFade>c__Iterator0
struct  U3CSpawnEffectFadeU3Ec__Iterator0_t2652861317  : public RuntimeObject
{
public:
	// System.Single Player2D/<SpawnEffectFade>c__Iterator0::<duration>__0
	float ___U3CdurationU3E__0_0;
	// UnityEngine.ParticleSystem Player2D/<SpawnEffectFade>c__Iterator0::<p_sys>__0
	ParticleSystem_t3394631041 * ___U3Cp_sysU3E__0_1;
	// UnityEngine.Color Player2D/<SpawnEffectFade>c__Iterator0::<start>__0
	Color_t2020392075  ___U3CstartU3E__0_2;
	// UnityEngine.Color Player2D/<SpawnEffectFade>c__Iterator0::<end>__0
	Color_t2020392075  ___U3CendU3E__0_3;
	// System.Single Player2D/<SpawnEffectFade>c__Iterator0::<t>__1
	float ___U3CtU3E__1_4;
	// UnityEngine.Color Player2D/<SpawnEffectFade>c__Iterator0::<changedColor>__2
	Color_t2020392075  ___U3CchangedColorU3E__2_5;
	// UnityEngine.ParticleSystem/MainModule Player2D/<SpawnEffectFade>c__Iterator0::<main>__2
	MainModule_t6751348  ___U3CmainU3E__2_6;
	// System.Object Player2D/<SpawnEffectFade>c__Iterator0::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean Player2D/<SpawnEffectFade>c__Iterator0::$disposing
	bool ___U24disposing_8;
	// System.Int32 Player2D/<SpawnEffectFade>c__Iterator0::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CdurationU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSpawnEffectFadeU3Ec__Iterator0_t2652861317, ___U3CdurationU3E__0_0)); }
	inline float get_U3CdurationU3E__0_0() const { return ___U3CdurationU3E__0_0; }
	inline float* get_address_of_U3CdurationU3E__0_0() { return &___U3CdurationU3E__0_0; }
	inline void set_U3CdurationU3E__0_0(float value)
	{
		___U3CdurationU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3Cp_sysU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSpawnEffectFadeU3Ec__Iterator0_t2652861317, ___U3Cp_sysU3E__0_1)); }
	inline ParticleSystem_t3394631041 * get_U3Cp_sysU3E__0_1() const { return ___U3Cp_sysU3E__0_1; }
	inline ParticleSystem_t3394631041 ** get_address_of_U3Cp_sysU3E__0_1() { return &___U3Cp_sysU3E__0_1; }
	inline void set_U3Cp_sysU3E__0_1(ParticleSystem_t3394631041 * value)
	{
		___U3Cp_sysU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cp_sysU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CstartU3E__0_2() { return static_cast<int32_t>(offsetof(U3CSpawnEffectFadeU3Ec__Iterator0_t2652861317, ___U3CstartU3E__0_2)); }
	inline Color_t2020392075  get_U3CstartU3E__0_2() const { return ___U3CstartU3E__0_2; }
	inline Color_t2020392075 * get_address_of_U3CstartU3E__0_2() { return &___U3CstartU3E__0_2; }
	inline void set_U3CstartU3E__0_2(Color_t2020392075  value)
	{
		___U3CstartU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CendU3E__0_3() { return static_cast<int32_t>(offsetof(U3CSpawnEffectFadeU3Ec__Iterator0_t2652861317, ___U3CendU3E__0_3)); }
	inline Color_t2020392075  get_U3CendU3E__0_3() const { return ___U3CendU3E__0_3; }
	inline Color_t2020392075 * get_address_of_U3CendU3E__0_3() { return &___U3CendU3E__0_3; }
	inline void set_U3CendU3E__0_3(Color_t2020392075  value)
	{
		___U3CendU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E__1_4() { return static_cast<int32_t>(offsetof(U3CSpawnEffectFadeU3Ec__Iterator0_t2652861317, ___U3CtU3E__1_4)); }
	inline float get_U3CtU3E__1_4() const { return ___U3CtU3E__1_4; }
	inline float* get_address_of_U3CtU3E__1_4() { return &___U3CtU3E__1_4; }
	inline void set_U3CtU3E__1_4(float value)
	{
		___U3CtU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CchangedColorU3E__2_5() { return static_cast<int32_t>(offsetof(U3CSpawnEffectFadeU3Ec__Iterator0_t2652861317, ___U3CchangedColorU3E__2_5)); }
	inline Color_t2020392075  get_U3CchangedColorU3E__2_5() const { return ___U3CchangedColorU3E__2_5; }
	inline Color_t2020392075 * get_address_of_U3CchangedColorU3E__2_5() { return &___U3CchangedColorU3E__2_5; }
	inline void set_U3CchangedColorU3E__2_5(Color_t2020392075  value)
	{
		___U3CchangedColorU3E__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CmainU3E__2_6() { return static_cast<int32_t>(offsetof(U3CSpawnEffectFadeU3Ec__Iterator0_t2652861317, ___U3CmainU3E__2_6)); }
	inline MainModule_t6751348  get_U3CmainU3E__2_6() const { return ___U3CmainU3E__2_6; }
	inline MainModule_t6751348 * get_address_of_U3CmainU3E__2_6() { return &___U3CmainU3E__2_6; }
	inline void set_U3CmainU3E__2_6(MainModule_t6751348  value)
	{
		___U3CmainU3E__2_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CSpawnEffectFadeU3Ec__Iterator0_t2652861317, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CSpawnEffectFadeU3Ec__Iterator0_t2652861317, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CSpawnEffectFadeU3Ec__Iterator0_t2652861317, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSPAWNEFFECTFADEU3EC__ITERATOR0_T2652861317_H
#ifndef PARTICLESYSTEMGRADIENTMODE_T4205168402_H
#define PARTICLESYSTEMGRADIENTMODE_T4205168402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystemGradientMode
struct  ParticleSystemGradientMode_t4205168402 
{
public:
	// System.Int32 UnityEngine.ParticleSystemGradientMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParticleSystemGradientMode_t4205168402, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMGRADIENTMODE_T4205168402_H
#ifndef GRADIENT_T3600583008_H
#define GRADIENT_T3600583008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Gradient
struct  Gradient_t3600583008  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Gradient::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Gradient_t3600583008, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Gradient
struct Gradient_t3600583008_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Gradient
struct Gradient_t3600583008_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // GRADIENT_T3600583008_H
#ifndef RENDERMODE_T4280533217_H
#define RENDERMODE_T4280533217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderMode
struct  RenderMode_t4280533217 
{
public:
	// System.Int32 UnityEngine.RenderMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderMode_t4280533217, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERMODE_T4280533217_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305142_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305142  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-5CF7DBEB679CBFEC1FCB176A380E936AD360D7AA
	U24ArrayTypeU3D12_t1568637718  ___U24fieldU2D5CF7DBEB679CBFEC1FCB176A380E936AD360D7AA_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D5CF7DBEB679CBFEC1FCB176A380E936AD360D7AA_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields, ___U24fieldU2D5CF7DBEB679CBFEC1FCB176A380E936AD360D7AA_0)); }
	inline U24ArrayTypeU3D12_t1568637718  get_U24fieldU2D5CF7DBEB679CBFEC1FCB176A380E936AD360D7AA_0() const { return ___U24fieldU2D5CF7DBEB679CBFEC1FCB176A380E936AD360D7AA_0; }
	inline U24ArrayTypeU3D12_t1568637718 * get_address_of_U24fieldU2D5CF7DBEB679CBFEC1FCB176A380E936AD360D7AA_0() { return &___U24fieldU2D5CF7DBEB679CBFEC1FCB176A380E936AD360D7AA_0; }
	inline void set_U24fieldU2D5CF7DBEB679CBFEC1FCB176A380E936AD360D7AA_0(U24ArrayTypeU3D12_t1568637718  value)
	{
		___U24fieldU2D5CF7DBEB679CBFEC1FCB176A380E936AD360D7AA_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305142_H
#ifndef INPUTBUTTON_T2981963041_H
#define INPUTBUTTON_T2981963041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData/InputButton
struct  InputButton_t2981963041 
{
public:
	// System.Int32 UnityEngine.EventSystems.PointerEventData/InputButton::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputButton_t2981963041, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTBUTTON_T2981963041_H
#ifndef RAYCASTRESULT_T21186376_H
#define RAYCASTRESULT_T21186376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.RaycastResult
struct  RaycastResult_t21186376 
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.RaycastResult::m_GameObject
	GameObject_t1756533147 * ___m_GameObject_0;
	// UnityEngine.EventSystems.BaseRaycaster UnityEngine.EventSystems.RaycastResult::module
	BaseRaycaster_t2336171397 * ___module_1;
	// System.Single UnityEngine.EventSystems.RaycastResult::distance
	float ___distance_2;
	// System.Single UnityEngine.EventSystems.RaycastResult::index
	float ___index_3;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::depth
	int32_t ___depth_4;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingLayer
	int32_t ___sortingLayer_5;
	// System.Int32 UnityEngine.EventSystems.RaycastResult::sortingOrder
	int32_t ___sortingOrder_6;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldPosition
	Vector3_t2243707580  ___worldPosition_7;
	// UnityEngine.Vector3 UnityEngine.EventSystems.RaycastResult::worldNormal
	Vector3_t2243707580  ___worldNormal_8;
	// UnityEngine.Vector2 UnityEngine.EventSystems.RaycastResult::screenPosition
	Vector2_t2243707579  ___screenPosition_9;

public:
	inline static int32_t get_offset_of_m_GameObject_0() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___m_GameObject_0)); }
	inline GameObject_t1756533147 * get_m_GameObject_0() const { return ___m_GameObject_0; }
	inline GameObject_t1756533147 ** get_address_of_m_GameObject_0() { return &___m_GameObject_0; }
	inline void set_m_GameObject_0(GameObject_t1756533147 * value)
	{
		___m_GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_GameObject_0), value);
	}

	inline static int32_t get_offset_of_module_1() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___module_1)); }
	inline BaseRaycaster_t2336171397 * get_module_1() const { return ___module_1; }
	inline BaseRaycaster_t2336171397 ** get_address_of_module_1() { return &___module_1; }
	inline void set_module_1(BaseRaycaster_t2336171397 * value)
	{
		___module_1 = value;
		Il2CppCodeGenWriteBarrier((&___module_1), value);
	}

	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___index_3)); }
	inline float get_index_3() const { return ___index_3; }
	inline float* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(float value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_depth_4() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___depth_4)); }
	inline int32_t get_depth_4() const { return ___depth_4; }
	inline int32_t* get_address_of_depth_4() { return &___depth_4; }
	inline void set_depth_4(int32_t value)
	{
		___depth_4 = value;
	}

	inline static int32_t get_offset_of_sortingLayer_5() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___sortingLayer_5)); }
	inline int32_t get_sortingLayer_5() const { return ___sortingLayer_5; }
	inline int32_t* get_address_of_sortingLayer_5() { return &___sortingLayer_5; }
	inline void set_sortingLayer_5(int32_t value)
	{
		___sortingLayer_5 = value;
	}

	inline static int32_t get_offset_of_sortingOrder_6() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___sortingOrder_6)); }
	inline int32_t get_sortingOrder_6() const { return ___sortingOrder_6; }
	inline int32_t* get_address_of_sortingOrder_6() { return &___sortingOrder_6; }
	inline void set_sortingOrder_6(int32_t value)
	{
		___sortingOrder_6 = value;
	}

	inline static int32_t get_offset_of_worldPosition_7() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___worldPosition_7)); }
	inline Vector3_t2243707580  get_worldPosition_7() const { return ___worldPosition_7; }
	inline Vector3_t2243707580 * get_address_of_worldPosition_7() { return &___worldPosition_7; }
	inline void set_worldPosition_7(Vector3_t2243707580  value)
	{
		___worldPosition_7 = value;
	}

	inline static int32_t get_offset_of_worldNormal_8() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___worldNormal_8)); }
	inline Vector3_t2243707580  get_worldNormal_8() const { return ___worldNormal_8; }
	inline Vector3_t2243707580 * get_address_of_worldNormal_8() { return &___worldNormal_8; }
	inline void set_worldNormal_8(Vector3_t2243707580  value)
	{
		___worldNormal_8 = value;
	}

	inline static int32_t get_offset_of_screenPosition_9() { return static_cast<int32_t>(offsetof(RaycastResult_t21186376, ___screenPosition_9)); }
	inline Vector2_t2243707579  get_screenPosition_9() const { return ___screenPosition_9; }
	inline Vector2_t2243707579 * get_address_of_screenPosition_9() { return &___screenPosition_9; }
	inline void set_screenPosition_9(Vector2_t2243707579  value)
	{
		___screenPosition_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t21186376_marshaled_pinvoke
{
	GameObject_t1756533147 * ___m_GameObject_0;
	BaseRaycaster_t2336171397 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t2243707580  ___worldPosition_7;
	Vector3_t2243707580  ___worldNormal_8;
	Vector2_t2243707579  ___screenPosition_9;
};
// Native definition for COM marshalling of UnityEngine.EventSystems.RaycastResult
struct RaycastResult_t21186376_marshaled_com
{
	GameObject_t1756533147 * ___m_GameObject_0;
	BaseRaycaster_t2336171397 * ___module_1;
	float ___distance_2;
	float ___index_3;
	int32_t ___depth_4;
	int32_t ___sortingLayer_5;
	int32_t ___sortingOrder_6;
	Vector3_t2243707580  ___worldPosition_7;
	Vector3_t2243707580  ___worldNormal_8;
	Vector2_t2243707579  ___screenPosition_9;
};
#endif // RAYCASTRESULT_T21186376_H
#ifndef MOVEMSG_T2690495395_H
#define MOVEMSG_T2690495395_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Controller2D/MoveMSG
struct  MoveMSG_t2690495395 
{
public:
	// System.Single Controller2D/MoveMSG::horizontal
	float ___horizontal_0;
	// Controller2D/Jump Controller2D/MoveMSG::jump
	int32_t ___jump_1;

public:
	inline static int32_t get_offset_of_horizontal_0() { return static_cast<int32_t>(offsetof(MoveMSG_t2690495395, ___horizontal_0)); }
	inline float get_horizontal_0() const { return ___horizontal_0; }
	inline float* get_address_of_horizontal_0() { return &___horizontal_0; }
	inline void set_horizontal_0(float value)
	{
		___horizontal_0 = value;
	}

	inline static int32_t get_offset_of_jump_1() { return static_cast<int32_t>(offsetof(MoveMSG_t2690495395, ___jump_1)); }
	inline int32_t get_jump_1() const { return ___jump_1; }
	inline int32_t* get_address_of_jump_1() { return &___jump_1; }
	inline void set_jump_1(int32_t value)
	{
		___jump_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVEMSG_T2690495395_H
#ifndef MINMAXGRADIENT_T3708298175_H
#define MINMAXGRADIENT_T3708298175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/MinMaxGradient
struct  MinMaxGradient_t3708298175 
{
public:
	// UnityEngine.ParticleSystemGradientMode UnityEngine.ParticleSystem/MinMaxGradient::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.Gradient UnityEngine.ParticleSystem/MinMaxGradient::m_GradientMin
	Gradient_t3600583008 * ___m_GradientMin_1;
	// UnityEngine.Gradient UnityEngine.ParticleSystem/MinMaxGradient::m_GradientMax
	Gradient_t3600583008 * ___m_GradientMax_2;
	// UnityEngine.Color UnityEngine.ParticleSystem/MinMaxGradient::m_ColorMin
	Color_t2020392075  ___m_ColorMin_3;
	// UnityEngine.Color UnityEngine.ParticleSystem/MinMaxGradient::m_ColorMax
	Color_t2020392075  ___m_ColorMax_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(MinMaxGradient_t3708298175, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_GradientMin_1() { return static_cast<int32_t>(offsetof(MinMaxGradient_t3708298175, ___m_GradientMin_1)); }
	inline Gradient_t3600583008 * get_m_GradientMin_1() const { return ___m_GradientMin_1; }
	inline Gradient_t3600583008 ** get_address_of_m_GradientMin_1() { return &___m_GradientMin_1; }
	inline void set_m_GradientMin_1(Gradient_t3600583008 * value)
	{
		___m_GradientMin_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_GradientMin_1), value);
	}

	inline static int32_t get_offset_of_m_GradientMax_2() { return static_cast<int32_t>(offsetof(MinMaxGradient_t3708298175, ___m_GradientMax_2)); }
	inline Gradient_t3600583008 * get_m_GradientMax_2() const { return ___m_GradientMax_2; }
	inline Gradient_t3600583008 ** get_address_of_m_GradientMax_2() { return &___m_GradientMax_2; }
	inline void set_m_GradientMax_2(Gradient_t3600583008 * value)
	{
		___m_GradientMax_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_GradientMax_2), value);
	}

	inline static int32_t get_offset_of_m_ColorMin_3() { return static_cast<int32_t>(offsetof(MinMaxGradient_t3708298175, ___m_ColorMin_3)); }
	inline Color_t2020392075  get_m_ColorMin_3() const { return ___m_ColorMin_3; }
	inline Color_t2020392075 * get_address_of_m_ColorMin_3() { return &___m_ColorMin_3; }
	inline void set_m_ColorMin_3(Color_t2020392075  value)
	{
		___m_ColorMin_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMax_4() { return static_cast<int32_t>(offsetof(MinMaxGradient_t3708298175, ___m_ColorMax_4)); }
	inline Color_t2020392075  get_m_ColorMax_4() const { return ___m_ColorMax_4; }
	inline Color_t2020392075 * get_address_of_m_ColorMax_4() { return &___m_ColorMax_4; }
	inline void set_m_ColorMax_4(Color_t2020392075  value)
	{
		___m_ColorMax_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/MinMaxGradient
struct MinMaxGradient_t3708298175_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Gradient_t3600583008_marshaled_pinvoke ___m_GradientMin_1;
	Gradient_t3600583008_marshaled_pinvoke ___m_GradientMax_2;
	Color_t2020392075  ___m_ColorMin_3;
	Color_t2020392075  ___m_ColorMax_4;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/MinMaxGradient
struct MinMaxGradient_t3708298175_marshaled_com
{
	int32_t ___m_Mode_0;
	Gradient_t3600583008_marshaled_com* ___m_GradientMin_1;
	Gradient_t3600583008_marshaled_com* ___m_GradientMax_2;
	Color_t2020392075  ___m_ColorMin_3;
	Color_t2020392075  ___m_ColorMax_4;
};
#endif // MINMAXGRADIENT_T3708298175_H
#ifndef TEXTURE_T2243626319_H
#define TEXTURE_T2243626319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t2243626319  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T2243626319_H
#ifndef POINTEREVENTDATA_T1599784723_H
#define POINTEREVENTDATA_T1599784723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.PointerEventData
struct  PointerEventData_t1599784723  : public BaseEventData_t2681005625
{
public:
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerEnter>k__BackingField
	GameObject_t1756533147 * ___U3CpointerEnterU3Ek__BackingField_2;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::m_PointerPress
	GameObject_t1756533147 * ___m_PointerPress_3;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<lastPress>k__BackingField
	GameObject_t1756533147 * ___U3ClastPressU3Ek__BackingField_4;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<rawPointerPress>k__BackingField
	GameObject_t1756533147 * ___U3CrawPointerPressU3Ek__BackingField_5;
	// UnityEngine.GameObject UnityEngine.EventSystems.PointerEventData::<pointerDrag>k__BackingField
	GameObject_t1756533147 * ___U3CpointerDragU3Ek__BackingField_6;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerCurrentRaycast>k__BackingField
	RaycastResult_t21186376  ___U3CpointerCurrentRaycastU3Ek__BackingField_7;
	// UnityEngine.EventSystems.RaycastResult UnityEngine.EventSystems.PointerEventData::<pointerPressRaycast>k__BackingField
	RaycastResult_t21186376  ___U3CpointerPressRaycastU3Ek__BackingField_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityEngine.EventSystems.PointerEventData::hovered
	List_1_t1125654279 * ___hovered_9;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<eligibleForClick>k__BackingField
	bool ___U3CeligibleForClickU3Ek__BackingField_10;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<pointerId>k__BackingField
	int32_t ___U3CpointerIdU3Ek__BackingField_11;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<position>k__BackingField
	Vector2_t2243707579  ___U3CpositionU3Ek__BackingField_12;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<delta>k__BackingField
	Vector2_t2243707579  ___U3CdeltaU3Ek__BackingField_13;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<pressPosition>k__BackingField
	Vector2_t2243707579  ___U3CpressPositionU3Ek__BackingField_14;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldPosition>k__BackingField
	Vector3_t2243707580  ___U3CworldPositionU3Ek__BackingField_15;
	// UnityEngine.Vector3 UnityEngine.EventSystems.PointerEventData::<worldNormal>k__BackingField
	Vector3_t2243707580  ___U3CworldNormalU3Ek__BackingField_16;
	// System.Single UnityEngine.EventSystems.PointerEventData::<clickTime>k__BackingField
	float ___U3CclickTimeU3Ek__BackingField_17;
	// System.Int32 UnityEngine.EventSystems.PointerEventData::<clickCount>k__BackingField
	int32_t ___U3CclickCountU3Ek__BackingField_18;
	// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::<scrollDelta>k__BackingField
	Vector2_t2243707579  ___U3CscrollDeltaU3Ek__BackingField_19;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<useDragThreshold>k__BackingField
	bool ___U3CuseDragThresholdU3Ek__BackingField_20;
	// System.Boolean UnityEngine.EventSystems.PointerEventData::<dragging>k__BackingField
	bool ___U3CdraggingU3Ek__BackingField_21;
	// UnityEngine.EventSystems.PointerEventData/InputButton UnityEngine.EventSystems.PointerEventData::<button>k__BackingField
	int32_t ___U3CbuttonU3Ek__BackingField_22;

public:
	inline static int32_t get_offset_of_U3CpointerEnterU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CpointerEnterU3Ek__BackingField_2)); }
	inline GameObject_t1756533147 * get_U3CpointerEnterU3Ek__BackingField_2() const { return ___U3CpointerEnterU3Ek__BackingField_2; }
	inline GameObject_t1756533147 ** get_address_of_U3CpointerEnterU3Ek__BackingField_2() { return &___U3CpointerEnterU3Ek__BackingField_2; }
	inline void set_U3CpointerEnterU3Ek__BackingField_2(GameObject_t1756533147 * value)
	{
		___U3CpointerEnterU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpointerEnterU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_m_PointerPress_3() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___m_PointerPress_3)); }
	inline GameObject_t1756533147 * get_m_PointerPress_3() const { return ___m_PointerPress_3; }
	inline GameObject_t1756533147 ** get_address_of_m_PointerPress_3() { return &___m_PointerPress_3; }
	inline void set_m_PointerPress_3(GameObject_t1756533147 * value)
	{
		___m_PointerPress_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointerPress_3), value);
	}

	inline static int32_t get_offset_of_U3ClastPressU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3ClastPressU3Ek__BackingField_4)); }
	inline GameObject_t1756533147 * get_U3ClastPressU3Ek__BackingField_4() const { return ___U3ClastPressU3Ek__BackingField_4; }
	inline GameObject_t1756533147 ** get_address_of_U3ClastPressU3Ek__BackingField_4() { return &___U3ClastPressU3Ek__BackingField_4; }
	inline void set_U3ClastPressU3Ek__BackingField_4(GameObject_t1756533147 * value)
	{
		___U3ClastPressU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClastPressU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CrawPointerPressU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CrawPointerPressU3Ek__BackingField_5)); }
	inline GameObject_t1756533147 * get_U3CrawPointerPressU3Ek__BackingField_5() const { return ___U3CrawPointerPressU3Ek__BackingField_5; }
	inline GameObject_t1756533147 ** get_address_of_U3CrawPointerPressU3Ek__BackingField_5() { return &___U3CrawPointerPressU3Ek__BackingField_5; }
	inline void set_U3CrawPointerPressU3Ek__BackingField_5(GameObject_t1756533147 * value)
	{
		___U3CrawPointerPressU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrawPointerPressU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CpointerDragU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CpointerDragU3Ek__BackingField_6)); }
	inline GameObject_t1756533147 * get_U3CpointerDragU3Ek__BackingField_6() const { return ___U3CpointerDragU3Ek__BackingField_6; }
	inline GameObject_t1756533147 ** get_address_of_U3CpointerDragU3Ek__BackingField_6() { return &___U3CpointerDragU3Ek__BackingField_6; }
	inline void set_U3CpointerDragU3Ek__BackingField_6(GameObject_t1756533147 * value)
	{
		___U3CpointerDragU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpointerDragU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CpointerCurrentRaycastU3Ek__BackingField_7)); }
	inline RaycastResult_t21186376  get_U3CpointerCurrentRaycastU3Ek__BackingField_7() const { return ___U3CpointerCurrentRaycastU3Ek__BackingField_7; }
	inline RaycastResult_t21186376 * get_address_of_U3CpointerCurrentRaycastU3Ek__BackingField_7() { return &___U3CpointerCurrentRaycastU3Ek__BackingField_7; }
	inline void set_U3CpointerCurrentRaycastU3Ek__BackingField_7(RaycastResult_t21186376  value)
	{
		___U3CpointerCurrentRaycastU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CpointerPressRaycastU3Ek__BackingField_8)); }
	inline RaycastResult_t21186376  get_U3CpointerPressRaycastU3Ek__BackingField_8() const { return ___U3CpointerPressRaycastU3Ek__BackingField_8; }
	inline RaycastResult_t21186376 * get_address_of_U3CpointerPressRaycastU3Ek__BackingField_8() { return &___U3CpointerPressRaycastU3Ek__BackingField_8; }
	inline void set_U3CpointerPressRaycastU3Ek__BackingField_8(RaycastResult_t21186376  value)
	{
		___U3CpointerPressRaycastU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_hovered_9() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___hovered_9)); }
	inline List_1_t1125654279 * get_hovered_9() const { return ___hovered_9; }
	inline List_1_t1125654279 ** get_address_of_hovered_9() { return &___hovered_9; }
	inline void set_hovered_9(List_1_t1125654279 * value)
	{
		___hovered_9 = value;
		Il2CppCodeGenWriteBarrier((&___hovered_9), value);
	}

	inline static int32_t get_offset_of_U3CeligibleForClickU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CeligibleForClickU3Ek__BackingField_10)); }
	inline bool get_U3CeligibleForClickU3Ek__BackingField_10() const { return ___U3CeligibleForClickU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CeligibleForClickU3Ek__BackingField_10() { return &___U3CeligibleForClickU3Ek__BackingField_10; }
	inline void set_U3CeligibleForClickU3Ek__BackingField_10(bool value)
	{
		___U3CeligibleForClickU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CpointerIdU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CpointerIdU3Ek__BackingField_11)); }
	inline int32_t get_U3CpointerIdU3Ek__BackingField_11() const { return ___U3CpointerIdU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CpointerIdU3Ek__BackingField_11() { return &___U3CpointerIdU3Ek__BackingField_11; }
	inline void set_U3CpointerIdU3Ek__BackingField_11(int32_t value)
	{
		___U3CpointerIdU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CpositionU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CpositionU3Ek__BackingField_12)); }
	inline Vector2_t2243707579  get_U3CpositionU3Ek__BackingField_12() const { return ___U3CpositionU3Ek__BackingField_12; }
	inline Vector2_t2243707579 * get_address_of_U3CpositionU3Ek__BackingField_12() { return &___U3CpositionU3Ek__BackingField_12; }
	inline void set_U3CpositionU3Ek__BackingField_12(Vector2_t2243707579  value)
	{
		___U3CpositionU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CdeltaU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CdeltaU3Ek__BackingField_13)); }
	inline Vector2_t2243707579  get_U3CdeltaU3Ek__BackingField_13() const { return ___U3CdeltaU3Ek__BackingField_13; }
	inline Vector2_t2243707579 * get_address_of_U3CdeltaU3Ek__BackingField_13() { return &___U3CdeltaU3Ek__BackingField_13; }
	inline void set_U3CdeltaU3Ek__BackingField_13(Vector2_t2243707579  value)
	{
		___U3CdeltaU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CpressPositionU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CpressPositionU3Ek__BackingField_14)); }
	inline Vector2_t2243707579  get_U3CpressPositionU3Ek__BackingField_14() const { return ___U3CpressPositionU3Ek__BackingField_14; }
	inline Vector2_t2243707579 * get_address_of_U3CpressPositionU3Ek__BackingField_14() { return &___U3CpressPositionU3Ek__BackingField_14; }
	inline void set_U3CpressPositionU3Ek__BackingField_14(Vector2_t2243707579  value)
	{
		___U3CpressPositionU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CworldPositionU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CworldPositionU3Ek__BackingField_15)); }
	inline Vector3_t2243707580  get_U3CworldPositionU3Ek__BackingField_15() const { return ___U3CworldPositionU3Ek__BackingField_15; }
	inline Vector3_t2243707580 * get_address_of_U3CworldPositionU3Ek__BackingField_15() { return &___U3CworldPositionU3Ek__BackingField_15; }
	inline void set_U3CworldPositionU3Ek__BackingField_15(Vector3_t2243707580  value)
	{
		___U3CworldPositionU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CworldNormalU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CworldNormalU3Ek__BackingField_16)); }
	inline Vector3_t2243707580  get_U3CworldNormalU3Ek__BackingField_16() const { return ___U3CworldNormalU3Ek__BackingField_16; }
	inline Vector3_t2243707580 * get_address_of_U3CworldNormalU3Ek__BackingField_16() { return &___U3CworldNormalU3Ek__BackingField_16; }
	inline void set_U3CworldNormalU3Ek__BackingField_16(Vector3_t2243707580  value)
	{
		___U3CworldNormalU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CclickTimeU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CclickTimeU3Ek__BackingField_17)); }
	inline float get_U3CclickTimeU3Ek__BackingField_17() const { return ___U3CclickTimeU3Ek__BackingField_17; }
	inline float* get_address_of_U3CclickTimeU3Ek__BackingField_17() { return &___U3CclickTimeU3Ek__BackingField_17; }
	inline void set_U3CclickTimeU3Ek__BackingField_17(float value)
	{
		___U3CclickTimeU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CclickCountU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CclickCountU3Ek__BackingField_18)); }
	inline int32_t get_U3CclickCountU3Ek__BackingField_18() const { return ___U3CclickCountU3Ek__BackingField_18; }
	inline int32_t* get_address_of_U3CclickCountU3Ek__BackingField_18() { return &___U3CclickCountU3Ek__BackingField_18; }
	inline void set_U3CclickCountU3Ek__BackingField_18(int32_t value)
	{
		___U3CclickCountU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CscrollDeltaU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CscrollDeltaU3Ek__BackingField_19)); }
	inline Vector2_t2243707579  get_U3CscrollDeltaU3Ek__BackingField_19() const { return ___U3CscrollDeltaU3Ek__BackingField_19; }
	inline Vector2_t2243707579 * get_address_of_U3CscrollDeltaU3Ek__BackingField_19() { return &___U3CscrollDeltaU3Ek__BackingField_19; }
	inline void set_U3CscrollDeltaU3Ek__BackingField_19(Vector2_t2243707579  value)
	{
		___U3CscrollDeltaU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CuseDragThresholdU3Ek__BackingField_20)); }
	inline bool get_U3CuseDragThresholdU3Ek__BackingField_20() const { return ___U3CuseDragThresholdU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseDragThresholdU3Ek__BackingField_20() { return &___U3CuseDragThresholdU3Ek__BackingField_20; }
	inline void set_U3CuseDragThresholdU3Ek__BackingField_20(bool value)
	{
		___U3CuseDragThresholdU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CdraggingU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CdraggingU3Ek__BackingField_21)); }
	inline bool get_U3CdraggingU3Ek__BackingField_21() const { return ___U3CdraggingU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CdraggingU3Ek__BackingField_21() { return &___U3CdraggingU3Ek__BackingField_21; }
	inline void set_U3CdraggingU3Ek__BackingField_21(bool value)
	{
		___U3CdraggingU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CbuttonU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(PointerEventData_t1599784723, ___U3CbuttonU3Ek__BackingField_22)); }
	inline int32_t get_U3CbuttonU3Ek__BackingField_22() const { return ___U3CbuttonU3Ek__BackingField_22; }
	inline int32_t* get_address_of_U3CbuttonU3Ek__BackingField_22() { return &___U3CbuttonU3Ek__BackingField_22; }
	inline void set_U3CbuttonU3Ek__BackingField_22(int32_t value)
	{
		___U3CbuttonU3Ek__BackingField_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTEREVENTDATA_T1599784723_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef GAMEOBJECT_T1756533147_H
#define GAMEOBJECT_T1756533147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1756533147  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1756533147_H
#ifndef SPRITE_T309593783_H
#define SPRITE_T309593783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Sprite
struct  Sprite_t309593783  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITE_T309593783_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t2330101084  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t2330101084  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t2330101084 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t2330101084  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t1664964607* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t3405857066 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t3405857066 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t3405857066 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t1664964607* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t1664964607** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t1664964607* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t3405857066 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t3405857066 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t3405857066 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t3405857066 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t3405857066 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t3405857066 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t3405857066 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t3405857066 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t3405857066 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef RENDERER_T257310565_H
#define RENDERER_T257310565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t257310565  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T257310565_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef PARTICLESYSTEM_T3394631041_H
#define PARTICLESYSTEM_T3394631041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem
struct  ParticleSystem_t3394631041  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEM_T3394631041_H
#ifndef TRANSFORM_T3275118058_H
#define TRANSFORM_T3275118058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3275118058  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3275118058_H
#ifndef RIGIDBODY2D_T502193897_H
#define RIGIDBODY2D_T502193897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rigidbody2D
struct  Rigidbody2D_t502193897  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODY2D_T502193897_H
#ifndef TEXTURE2D_T3542995729_H
#define TEXTURE2D_T3542995729_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture2D
struct  Texture2D_t3542995729  : public Texture_t2243626319
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2D_T3542995729_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef CAMERA_T189460977_H
#define CAMERA_T189460977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t189460977  : public Behaviour_t955675639
{
public:

public:
};

struct Camera_t189460977_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t834278767 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t834278767 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t834278767 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t189460977_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t834278767 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t834278767 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t834278767 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t189460977_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t834278767 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t834278767 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t834278767 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t189460977_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t834278767 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t834278767 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t834278767 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T189460977_H
#ifndef CANVAS_T209405766_H
#define CANVAS_T209405766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Canvas
struct  Canvas_t209405766  : public Behaviour_t955675639
{
public:

public:
};

struct Canvas_t209405766_StaticFields
{
public:
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_t3522132132 * ___willRenderCanvases_2;

public:
	inline static int32_t get_offset_of_willRenderCanvases_2() { return static_cast<int32_t>(offsetof(Canvas_t209405766_StaticFields, ___willRenderCanvases_2)); }
	inline WillRenderCanvases_t3522132132 * get_willRenderCanvases_2() const { return ___willRenderCanvases_2; }
	inline WillRenderCanvases_t3522132132 ** get_address_of_willRenderCanvases_2() { return &___willRenderCanvases_2; }
	inline void set_willRenderCanvases_2(WillRenderCanvases_t3522132132 * value)
	{
		___willRenderCanvases_2 = value;
		Il2CppCodeGenWriteBarrier((&___willRenderCanvases_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVAS_T209405766_H
#ifndef SPRITERENDERER_T1209076198_H
#define SPRITERENDERER_T1209076198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SpriteRenderer
struct  SpriteRenderer_t1209076198  : public Renderer_t257310565
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITERENDERER_T1209076198_H
#ifndef RECTTRANSFORM_T3349966182_H
#define RECTTRANSFORM_T3349966182_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform
struct  RectTransform_t3349966182  : public Transform_t3275118058
{
public:

public:
};

struct RectTransform_t3349966182_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t2020713228 * ___reapplyDrivenProperties_2;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_2() { return static_cast<int32_t>(offsetof(RectTransform_t3349966182_StaticFields, ___reapplyDrivenProperties_2)); }
	inline ReapplyDrivenProperties_t2020713228 * get_reapplyDrivenProperties_2() const { return ___reapplyDrivenProperties_2; }
	inline ReapplyDrivenProperties_t2020713228 ** get_address_of_reapplyDrivenProperties_2() { return &___reapplyDrivenProperties_2; }
	inline void set_reapplyDrivenProperties_2(ReapplyDrivenProperties_t2020713228 * value)
	{
		___reapplyDrivenProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___reapplyDrivenProperties_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORM_T3349966182_H
#ifndef COLLIDER2D_T646061738_H
#define COLLIDER2D_T646061738_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider2D
struct  Collider2D_t646061738  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER2D_T646061738_H
#ifndef SCORE_T1518975274_H
#define SCORE_T1518975274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Score
struct  Score_t1518975274  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text Score::score_text
	Text_t356221433 * ___score_text_2;
	// System.Single Score::score
	float ___score_3;
	// Player2D Score::player
	Player2D_t165353355 * ___player_4;

public:
	inline static int32_t get_offset_of_score_text_2() { return static_cast<int32_t>(offsetof(Score_t1518975274, ___score_text_2)); }
	inline Text_t356221433 * get_score_text_2() const { return ___score_text_2; }
	inline Text_t356221433 ** get_address_of_score_text_2() { return &___score_text_2; }
	inline void set_score_text_2(Text_t356221433 * value)
	{
		___score_text_2 = value;
		Il2CppCodeGenWriteBarrier((&___score_text_2), value);
	}

	inline static int32_t get_offset_of_score_3() { return static_cast<int32_t>(offsetof(Score_t1518975274, ___score_3)); }
	inline float get_score_3() const { return ___score_3; }
	inline float* get_address_of_score_3() { return &___score_3; }
	inline void set_score_3(float value)
	{
		___score_3 = value;
	}

	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(Score_t1518975274, ___player_4)); }
	inline Player2D_t165353355 * get_player_4() const { return ___player_4; }
	inline Player2D_t165353355 ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(Player2D_t165353355 * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((&___player_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCORE_T1518975274_H
#ifndef SHOWSCORE_T4012426113_H
#define SHOWSCORE_T4012426113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShowScore
struct  ShowScore_t4012426113  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text ShowScore::score_text
	Text_t356221433 * ___score_text_2;

public:
	inline static int32_t get_offset_of_score_text_2() { return static_cast<int32_t>(offsetof(ShowScore_t4012426113, ___score_text_2)); }
	inline Text_t356221433 * get_score_text_2() const { return ___score_text_2; }
	inline Text_t356221433 ** get_address_of_score_text_2() { return &___score_text_2; }
	inline void set_score_text_2(Text_t356221433 * value)
	{
		___score_text_2 = value;
		Il2CppCodeGenWriteBarrier((&___score_text_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWSCORE_T4012426113_H
#ifndef UIBEHAVIOUR_T3960014691_H
#define UIBEHAVIOUR_T3960014691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3960014691  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3960014691_H
#ifndef CONTROLLER2D_T802485922_H
#define CONTROLLER2D_T802485922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Controller2D
struct  Controller2D_t802485922  : public MonoBehaviour_t1158329972
{
public:
	// System.Single Controller2D::move_speed
	float ___move_speed_2;
	// System.Single Controller2D::acceleration_air_time
	float ___acceleration_air_time_3;
	// System.Single Controller2D::acceleration_ground_time
	float ___acceleration_ground_time_4;
	// System.Type Controller2D::ColliderType
	Type_t * ___ColliderType_5;
	// UnityEngine.Collider2D Controller2D::collider
	Collider2D_t646061738 * ___collider_13;
	// UnityEngine.LayerMask Controller2D::platform_layer_mask
	LayerMask_t3188175821  ___platform_layer_mask_14;
	// System.Single Controller2D::velocity_x_smoothing
	float ___velocity_x_smoothing_15;
	// System.Boolean Controller2D::facing_right
	bool ___facing_right_16;
	// UnityEngine.Rigidbody2D Controller2D::rigidbody
	Rigidbody2D_t502193897 * ___rigidbody_17;
	// System.Boolean Controller2D::apply_gravity
	bool ___apply_gravity_18;

public:
	inline static int32_t get_offset_of_move_speed_2() { return static_cast<int32_t>(offsetof(Controller2D_t802485922, ___move_speed_2)); }
	inline float get_move_speed_2() const { return ___move_speed_2; }
	inline float* get_address_of_move_speed_2() { return &___move_speed_2; }
	inline void set_move_speed_2(float value)
	{
		___move_speed_2 = value;
	}

	inline static int32_t get_offset_of_acceleration_air_time_3() { return static_cast<int32_t>(offsetof(Controller2D_t802485922, ___acceleration_air_time_3)); }
	inline float get_acceleration_air_time_3() const { return ___acceleration_air_time_3; }
	inline float* get_address_of_acceleration_air_time_3() { return &___acceleration_air_time_3; }
	inline void set_acceleration_air_time_3(float value)
	{
		___acceleration_air_time_3 = value;
	}

	inline static int32_t get_offset_of_acceleration_ground_time_4() { return static_cast<int32_t>(offsetof(Controller2D_t802485922, ___acceleration_ground_time_4)); }
	inline float get_acceleration_ground_time_4() const { return ___acceleration_ground_time_4; }
	inline float* get_address_of_acceleration_ground_time_4() { return &___acceleration_ground_time_4; }
	inline void set_acceleration_ground_time_4(float value)
	{
		___acceleration_ground_time_4 = value;
	}

	inline static int32_t get_offset_of_ColliderType_5() { return static_cast<int32_t>(offsetof(Controller2D_t802485922, ___ColliderType_5)); }
	inline Type_t * get_ColliderType_5() const { return ___ColliderType_5; }
	inline Type_t ** get_address_of_ColliderType_5() { return &___ColliderType_5; }
	inline void set_ColliderType_5(Type_t * value)
	{
		___ColliderType_5 = value;
		Il2CppCodeGenWriteBarrier((&___ColliderType_5), value);
	}

	inline static int32_t get_offset_of_collider_13() { return static_cast<int32_t>(offsetof(Controller2D_t802485922, ___collider_13)); }
	inline Collider2D_t646061738 * get_collider_13() const { return ___collider_13; }
	inline Collider2D_t646061738 ** get_address_of_collider_13() { return &___collider_13; }
	inline void set_collider_13(Collider2D_t646061738 * value)
	{
		___collider_13 = value;
		Il2CppCodeGenWriteBarrier((&___collider_13), value);
	}

	inline static int32_t get_offset_of_platform_layer_mask_14() { return static_cast<int32_t>(offsetof(Controller2D_t802485922, ___platform_layer_mask_14)); }
	inline LayerMask_t3188175821  get_platform_layer_mask_14() const { return ___platform_layer_mask_14; }
	inline LayerMask_t3188175821 * get_address_of_platform_layer_mask_14() { return &___platform_layer_mask_14; }
	inline void set_platform_layer_mask_14(LayerMask_t3188175821  value)
	{
		___platform_layer_mask_14 = value;
	}

	inline static int32_t get_offset_of_velocity_x_smoothing_15() { return static_cast<int32_t>(offsetof(Controller2D_t802485922, ___velocity_x_smoothing_15)); }
	inline float get_velocity_x_smoothing_15() const { return ___velocity_x_smoothing_15; }
	inline float* get_address_of_velocity_x_smoothing_15() { return &___velocity_x_smoothing_15; }
	inline void set_velocity_x_smoothing_15(float value)
	{
		___velocity_x_smoothing_15 = value;
	}

	inline static int32_t get_offset_of_facing_right_16() { return static_cast<int32_t>(offsetof(Controller2D_t802485922, ___facing_right_16)); }
	inline bool get_facing_right_16() const { return ___facing_right_16; }
	inline bool* get_address_of_facing_right_16() { return &___facing_right_16; }
	inline void set_facing_right_16(bool value)
	{
		___facing_right_16 = value;
	}

	inline static int32_t get_offset_of_rigidbody_17() { return static_cast<int32_t>(offsetof(Controller2D_t802485922, ___rigidbody_17)); }
	inline Rigidbody2D_t502193897 * get_rigidbody_17() const { return ___rigidbody_17; }
	inline Rigidbody2D_t502193897 ** get_address_of_rigidbody_17() { return &___rigidbody_17; }
	inline void set_rigidbody_17(Rigidbody2D_t502193897 * value)
	{
		___rigidbody_17 = value;
		Il2CppCodeGenWriteBarrier((&___rigidbody_17), value);
	}

	inline static int32_t get_offset_of_apply_gravity_18() { return static_cast<int32_t>(offsetof(Controller2D_t802485922, ___apply_gravity_18)); }
	inline bool get_apply_gravity_18() const { return ___apply_gravity_18; }
	inline bool* get_address_of_apply_gravity_18() { return &___apply_gravity_18; }
	inline void set_apply_gravity_18(bool value)
	{
		___apply_gravity_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLLER2D_T802485922_H
#ifndef LOADSCENE_T1133380052_H
#define LOADSCENE_T1133380052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadScene
struct  LoadScene_t1133380052  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADSCENE_T1133380052_H
#ifndef LOADTRANSITION_T4049825803_H
#define LOADTRANSITION_T4049825803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadTransition
struct  LoadTransition_t4049825803  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Texture2D LoadTransition::fade_texture
	Texture2D_t3542995729 * ___fade_texture_3;
	// System.Int32 LoadTransition::draw_depth
	int32_t ___draw_depth_4;
	// System.Single LoadTransition::alpha
	float ___alpha_5;
	// System.Int32 LoadTransition::fade_dir
	int32_t ___fade_dir_6;

public:
	inline static int32_t get_offset_of_fade_texture_3() { return static_cast<int32_t>(offsetof(LoadTransition_t4049825803, ___fade_texture_3)); }
	inline Texture2D_t3542995729 * get_fade_texture_3() const { return ___fade_texture_3; }
	inline Texture2D_t3542995729 ** get_address_of_fade_texture_3() { return &___fade_texture_3; }
	inline void set_fade_texture_3(Texture2D_t3542995729 * value)
	{
		___fade_texture_3 = value;
		Il2CppCodeGenWriteBarrier((&___fade_texture_3), value);
	}

	inline static int32_t get_offset_of_draw_depth_4() { return static_cast<int32_t>(offsetof(LoadTransition_t4049825803, ___draw_depth_4)); }
	inline int32_t get_draw_depth_4() const { return ___draw_depth_4; }
	inline int32_t* get_address_of_draw_depth_4() { return &___draw_depth_4; }
	inline void set_draw_depth_4(int32_t value)
	{
		___draw_depth_4 = value;
	}

	inline static int32_t get_offset_of_alpha_5() { return static_cast<int32_t>(offsetof(LoadTransition_t4049825803, ___alpha_5)); }
	inline float get_alpha_5() const { return ___alpha_5; }
	inline float* get_address_of_alpha_5() { return &___alpha_5; }
	inline void set_alpha_5(float value)
	{
		___alpha_5 = value;
	}

	inline static int32_t get_offset_of_fade_dir_6() { return static_cast<int32_t>(offsetof(LoadTransition_t4049825803, ___fade_dir_6)); }
	inline int32_t get_fade_dir_6() const { return ___fade_dir_6; }
	inline int32_t* get_address_of_fade_dir_6() { return &___fade_dir_6; }
	inline void set_fade_dir_6(int32_t value)
	{
		___fade_dir_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADTRANSITION_T4049825803_H
#ifndef HEALTHHUD_T66113683_H
#define HEALTHHUD_T66113683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HealthHUD
struct  HealthHUD_t66113683  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject[] HealthHUD::hearts
	GameObjectU5BU5D_t3057952154* ___hearts_2;
	// UnityEngine.Sprite HealthHUD::fullheart
	Sprite_t309593783 * ___fullheart_3;
	// UnityEngine.Sprite HealthHUD::emptyheart
	Sprite_t309593783 * ___emptyheart_4;
	// Player2D HealthHUD::player
	Player2D_t165353355 * ___player_5;
	// System.Boolean HealthHUD::errors
	bool ___errors_6;
	// System.Int32 HealthHUD::current_health
	int32_t ___current_health_7;

public:
	inline static int32_t get_offset_of_hearts_2() { return static_cast<int32_t>(offsetof(HealthHUD_t66113683, ___hearts_2)); }
	inline GameObjectU5BU5D_t3057952154* get_hearts_2() const { return ___hearts_2; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_hearts_2() { return &___hearts_2; }
	inline void set_hearts_2(GameObjectU5BU5D_t3057952154* value)
	{
		___hearts_2 = value;
		Il2CppCodeGenWriteBarrier((&___hearts_2), value);
	}

	inline static int32_t get_offset_of_fullheart_3() { return static_cast<int32_t>(offsetof(HealthHUD_t66113683, ___fullheart_3)); }
	inline Sprite_t309593783 * get_fullheart_3() const { return ___fullheart_3; }
	inline Sprite_t309593783 ** get_address_of_fullheart_3() { return &___fullheart_3; }
	inline void set_fullheart_3(Sprite_t309593783 * value)
	{
		___fullheart_3 = value;
		Il2CppCodeGenWriteBarrier((&___fullheart_3), value);
	}

	inline static int32_t get_offset_of_emptyheart_4() { return static_cast<int32_t>(offsetof(HealthHUD_t66113683, ___emptyheart_4)); }
	inline Sprite_t309593783 * get_emptyheart_4() const { return ___emptyheart_4; }
	inline Sprite_t309593783 ** get_address_of_emptyheart_4() { return &___emptyheart_4; }
	inline void set_emptyheart_4(Sprite_t309593783 * value)
	{
		___emptyheart_4 = value;
		Il2CppCodeGenWriteBarrier((&___emptyheart_4), value);
	}

	inline static int32_t get_offset_of_player_5() { return static_cast<int32_t>(offsetof(HealthHUD_t66113683, ___player_5)); }
	inline Player2D_t165353355 * get_player_5() const { return ___player_5; }
	inline Player2D_t165353355 ** get_address_of_player_5() { return &___player_5; }
	inline void set_player_5(Player2D_t165353355 * value)
	{
		___player_5 = value;
		Il2CppCodeGenWriteBarrier((&___player_5), value);
	}

	inline static int32_t get_offset_of_errors_6() { return static_cast<int32_t>(offsetof(HealthHUD_t66113683, ___errors_6)); }
	inline bool get_errors_6() const { return ___errors_6; }
	inline bool* get_address_of_errors_6() { return &___errors_6; }
	inline void set_errors_6(bool value)
	{
		___errors_6 = value;
	}

	inline static int32_t get_offset_of_current_health_7() { return static_cast<int32_t>(offsetof(HealthHUD_t66113683, ___current_health_7)); }
	inline int32_t get_current_health_7() const { return ___current_health_7; }
	inline int32_t* get_address_of_current_health_7() { return &___current_health_7; }
	inline void set_current_health_7(int32_t value)
	{
		___current_health_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEALTHHUD_T66113683_H
#ifndef GARBAGECOLLECTOR_T1348916102_H
#define GARBAGECOLLECTOR_T1348916102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageCollector
struct  GarbageCollector_t1348916102  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GARBAGECOLLECTOR_T1348916102_H
#ifndef FOLLOWTARGET_T2817477884_H
#define FOLLOWTARGET_T2817477884_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FollowTarget
struct  FollowTarget_t2817477884  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform FollowTarget::target
	Transform_t3275118058 * ___target_3;
	// UnityEngine.Vector3 FollowTarget::velocity
	Vector3_t2243707580  ___velocity_4;

public:
	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(FollowTarget_t2817477884, ___target_3)); }
	inline Transform_t3275118058 * get_target_3() const { return ___target_3; }
	inline Transform_t3275118058 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(Transform_t3275118058 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_velocity_4() { return static_cast<int32_t>(offsetof(FollowTarget_t2817477884, ___velocity_4)); }
	inline Vector3_t2243707580  get_velocity_4() const { return ___velocity_4; }
	inline Vector3_t2243707580 * get_address_of_velocity_4() { return &___velocity_4; }
	inline void set_velocity_4(Vector3_t2243707580  value)
	{
		___velocity_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOLLOWTARGET_T2817477884_H
#ifndef FALLINGPLATFORM_T2590793300_H
#define FALLINGPLATFORM_T2590793300_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FallingPlatform
struct  FallingPlatform_t2590793300  : public MonoBehaviour_t1158329972
{
public:
	// System.Single FallingPlatform::timer
	float ___timer_2;
	// System.Boolean FallingPlatform::sparks_flying
	bool ___sparks_flying_3;

public:
	inline static int32_t get_offset_of_timer_2() { return static_cast<int32_t>(offsetof(FallingPlatform_t2590793300, ___timer_2)); }
	inline float get_timer_2() const { return ___timer_2; }
	inline float* get_address_of_timer_2() { return &___timer_2; }
	inline void set_timer_2(float value)
	{
		___timer_2 = value;
	}

	inline static int32_t get_offset_of_sparks_flying_3() { return static_cast<int32_t>(offsetof(FallingPlatform_t2590793300, ___sparks_flying_3)); }
	inline bool get_sparks_flying_3() const { return ___sparks_flying_3; }
	inline bool* get_address_of_sparks_flying_3() { return &___sparks_flying_3; }
	inline void set_sparks_flying_3(bool value)
	{
		___sparks_flying_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FALLINGPLATFORM_T2590793300_H
#ifndef PREFERENCES_T60026258_H
#define PREFERENCES_T60026258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Preferences
struct  Preferences_t60026258  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct Preferences_t60026258_StaticFields
{
public:
	// System.String Preferences::player_tag
	String_t* ___player_tag_2;
	// System.Single Preferences::score
	float ___score_3;
	// System.String Preferences::control_HUD_path
	String_t* ___control_HUD_path_4;
	// System.String Preferences::missile_prefab_path
	String_t* ___missile_prefab_path_5;

public:
	inline static int32_t get_offset_of_player_tag_2() { return static_cast<int32_t>(offsetof(Preferences_t60026258_StaticFields, ___player_tag_2)); }
	inline String_t* get_player_tag_2() const { return ___player_tag_2; }
	inline String_t** get_address_of_player_tag_2() { return &___player_tag_2; }
	inline void set_player_tag_2(String_t* value)
	{
		___player_tag_2 = value;
		Il2CppCodeGenWriteBarrier((&___player_tag_2), value);
	}

	inline static int32_t get_offset_of_score_3() { return static_cast<int32_t>(offsetof(Preferences_t60026258_StaticFields, ___score_3)); }
	inline float get_score_3() const { return ___score_3; }
	inline float* get_address_of_score_3() { return &___score_3; }
	inline void set_score_3(float value)
	{
		___score_3 = value;
	}

	inline static int32_t get_offset_of_control_HUD_path_4() { return static_cast<int32_t>(offsetof(Preferences_t60026258_StaticFields, ___control_HUD_path_4)); }
	inline String_t* get_control_HUD_path_4() const { return ___control_HUD_path_4; }
	inline String_t** get_address_of_control_HUD_path_4() { return &___control_HUD_path_4; }
	inline void set_control_HUD_path_4(String_t* value)
	{
		___control_HUD_path_4 = value;
		Il2CppCodeGenWriteBarrier((&___control_HUD_path_4), value);
	}

	inline static int32_t get_offset_of_missile_prefab_path_5() { return static_cast<int32_t>(offsetof(Preferences_t60026258_StaticFields, ___missile_prefab_path_5)); }
	inline String_t* get_missile_prefab_path_5() const { return ___missile_prefab_path_5; }
	inline String_t** get_address_of_missile_prefab_path_5() { return &___missile_prefab_path_5; }
	inline void set_missile_prefab_path_5(String_t* value)
	{
		___missile_prefab_path_5 = value;
		Il2CppCodeGenWriteBarrier((&___missile_prefab_path_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFERENCES_T60026258_H
#ifndef INPUT2D_T2938435410_H
#define INPUT2D_T2938435410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Input2D
struct  Input2D_t2938435410  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUT2D_T2938435410_H
#ifndef DECIDEINPUT_T109196168_H
#define DECIDEINPUT_T109196168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DecideInput
struct  DecideInput_t109196168  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIDEINPUT_T109196168_H
#ifndef MACROGENERATOR_T1652377675_H
#define MACROGENERATOR_T1652377675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MacroGenerator
struct  MacroGenerator_t1652377675  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector2 MacroGenerator::screen_size
	Vector2_t2243707579  ___screen_size_7;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> MacroGenerator::micro_levels
	List_1_t1125654279 * ___micro_levels_8;
	// System.Int32 MacroGenerator::total_created_level
	int32_t ___total_created_level_9;
	// System.Object MacroGenerator::_lock
	RuntimeObject * ____lock_10;
	// System.Single MacroGenerator::difficulty
	float ___difficulty_11;
	// UnityEngine.Vector2 MacroGenerator::last_platform_pos
	Vector2_t2243707579  ___last_platform_pos_12;

public:
	inline static int32_t get_offset_of_screen_size_7() { return static_cast<int32_t>(offsetof(MacroGenerator_t1652377675, ___screen_size_7)); }
	inline Vector2_t2243707579  get_screen_size_7() const { return ___screen_size_7; }
	inline Vector2_t2243707579 * get_address_of_screen_size_7() { return &___screen_size_7; }
	inline void set_screen_size_7(Vector2_t2243707579  value)
	{
		___screen_size_7 = value;
	}

	inline static int32_t get_offset_of_micro_levels_8() { return static_cast<int32_t>(offsetof(MacroGenerator_t1652377675, ___micro_levels_8)); }
	inline List_1_t1125654279 * get_micro_levels_8() const { return ___micro_levels_8; }
	inline List_1_t1125654279 ** get_address_of_micro_levels_8() { return &___micro_levels_8; }
	inline void set_micro_levels_8(List_1_t1125654279 * value)
	{
		___micro_levels_8 = value;
		Il2CppCodeGenWriteBarrier((&___micro_levels_8), value);
	}

	inline static int32_t get_offset_of_total_created_level_9() { return static_cast<int32_t>(offsetof(MacroGenerator_t1652377675, ___total_created_level_9)); }
	inline int32_t get_total_created_level_9() const { return ___total_created_level_9; }
	inline int32_t* get_address_of_total_created_level_9() { return &___total_created_level_9; }
	inline void set_total_created_level_9(int32_t value)
	{
		___total_created_level_9 = value;
	}

	inline static int32_t get_offset_of__lock_10() { return static_cast<int32_t>(offsetof(MacroGenerator_t1652377675, ____lock_10)); }
	inline RuntimeObject * get__lock_10() const { return ____lock_10; }
	inline RuntimeObject ** get_address_of__lock_10() { return &____lock_10; }
	inline void set__lock_10(RuntimeObject * value)
	{
		____lock_10 = value;
		Il2CppCodeGenWriteBarrier((&____lock_10), value);
	}

	inline static int32_t get_offset_of_difficulty_11() { return static_cast<int32_t>(offsetof(MacroGenerator_t1652377675, ___difficulty_11)); }
	inline float get_difficulty_11() const { return ___difficulty_11; }
	inline float* get_address_of_difficulty_11() { return &___difficulty_11; }
	inline void set_difficulty_11(float value)
	{
		___difficulty_11 = value;
	}

	inline static int32_t get_offset_of_last_platform_pos_12() { return static_cast<int32_t>(offsetof(MacroGenerator_t1652377675, ___last_platform_pos_12)); }
	inline Vector2_t2243707579  get_last_platform_pos_12() const { return ___last_platform_pos_12; }
	inline Vector2_t2243707579 * get_address_of_last_platform_pos_12() { return &___last_platform_pos_12; }
	inline void set_last_platform_pos_12(Vector2_t2243707579  value)
	{
		___last_platform_pos_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MACROGENERATOR_T1652377675_H
#ifndef ONEWAYPLATFORM_T3777955472_H
#define ONEWAYPLATFORM_T3777955472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OneWayPlatform
struct  OneWayPlatform_t3777955472  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.BoxCollider2D OneWayPlatform::collider
	BoxCollider2D_t948534547 * ___collider_5;
	// System.Single OneWayPlatform::collider_top
	float ___collider_top_6;
	// Player2D OneWayPlatform::player
	Player2D_t165353355 * ___player_7;

public:
	inline static int32_t get_offset_of_collider_5() { return static_cast<int32_t>(offsetof(OneWayPlatform_t3777955472, ___collider_5)); }
	inline BoxCollider2D_t948534547 * get_collider_5() const { return ___collider_5; }
	inline BoxCollider2D_t948534547 ** get_address_of_collider_5() { return &___collider_5; }
	inline void set_collider_5(BoxCollider2D_t948534547 * value)
	{
		___collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___collider_5), value);
	}

	inline static int32_t get_offset_of_collider_top_6() { return static_cast<int32_t>(offsetof(OneWayPlatform_t3777955472, ___collider_top_6)); }
	inline float get_collider_top_6() const { return ___collider_top_6; }
	inline float* get_address_of_collider_top_6() { return &___collider_top_6; }
	inline void set_collider_top_6(float value)
	{
		___collider_top_6 = value;
	}

	inline static int32_t get_offset_of_player_7() { return static_cast<int32_t>(offsetof(OneWayPlatform_t3777955472, ___player_7)); }
	inline Player2D_t165353355 * get_player_7() const { return ___player_7; }
	inline Player2D_t165353355 ** get_address_of_player_7() { return &___player_7; }
	inline void set_player_7(Player2D_t165353355 * value)
	{
		___player_7 = value;
		Il2CppCodeGenWriteBarrier((&___player_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONEWAYPLATFORM_T3777955472_H
#ifndef PLATFORMPOOL_T3559850805_H
#define PLATFORMPOOL_T3559850805_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlatformPool
struct  PlatformPool_t3559850805  : public MonoBehaviour_t1158329972
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> PlatformPool::left_tiles
	List_1_t1125654279 * ___left_tiles_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> PlatformPool::mid_tiles
	List_1_t1125654279 * ___mid_tiles_9;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> PlatformPool::right_tiles
	List_1_t1125654279 * ___right_tiles_10;
	// System.Object PlatformPool::_lock
	RuntimeObject * ____lock_11;
	// UnityEngine.Vector2 PlatformPool::<left_tile_size>k__BackingField
	Vector2_t2243707579  ___U3Cleft_tile_sizeU3Ek__BackingField_12;
	// UnityEngine.Vector2 PlatformPool::<mid_tile_size>k__BackingField
	Vector2_t2243707579  ___U3Cmid_tile_sizeU3Ek__BackingField_13;
	// UnityEngine.Vector2 PlatformPool::<right_tile_size>k__BackingField
	Vector2_t2243707579  ___U3Cright_tile_sizeU3Ek__BackingField_14;
	// UnityEngine.Color PlatformPool::original_color
	Color_t2020392075  ___original_color_15;

public:
	inline static int32_t get_offset_of_left_tiles_8() { return static_cast<int32_t>(offsetof(PlatformPool_t3559850805, ___left_tiles_8)); }
	inline List_1_t1125654279 * get_left_tiles_8() const { return ___left_tiles_8; }
	inline List_1_t1125654279 ** get_address_of_left_tiles_8() { return &___left_tiles_8; }
	inline void set_left_tiles_8(List_1_t1125654279 * value)
	{
		___left_tiles_8 = value;
		Il2CppCodeGenWriteBarrier((&___left_tiles_8), value);
	}

	inline static int32_t get_offset_of_mid_tiles_9() { return static_cast<int32_t>(offsetof(PlatformPool_t3559850805, ___mid_tiles_9)); }
	inline List_1_t1125654279 * get_mid_tiles_9() const { return ___mid_tiles_9; }
	inline List_1_t1125654279 ** get_address_of_mid_tiles_9() { return &___mid_tiles_9; }
	inline void set_mid_tiles_9(List_1_t1125654279 * value)
	{
		___mid_tiles_9 = value;
		Il2CppCodeGenWriteBarrier((&___mid_tiles_9), value);
	}

	inline static int32_t get_offset_of_right_tiles_10() { return static_cast<int32_t>(offsetof(PlatformPool_t3559850805, ___right_tiles_10)); }
	inline List_1_t1125654279 * get_right_tiles_10() const { return ___right_tiles_10; }
	inline List_1_t1125654279 ** get_address_of_right_tiles_10() { return &___right_tiles_10; }
	inline void set_right_tiles_10(List_1_t1125654279 * value)
	{
		___right_tiles_10 = value;
		Il2CppCodeGenWriteBarrier((&___right_tiles_10), value);
	}

	inline static int32_t get_offset_of__lock_11() { return static_cast<int32_t>(offsetof(PlatformPool_t3559850805, ____lock_11)); }
	inline RuntimeObject * get__lock_11() const { return ____lock_11; }
	inline RuntimeObject ** get_address_of__lock_11() { return &____lock_11; }
	inline void set__lock_11(RuntimeObject * value)
	{
		____lock_11 = value;
		Il2CppCodeGenWriteBarrier((&____lock_11), value);
	}

	inline static int32_t get_offset_of_U3Cleft_tile_sizeU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PlatformPool_t3559850805, ___U3Cleft_tile_sizeU3Ek__BackingField_12)); }
	inline Vector2_t2243707579  get_U3Cleft_tile_sizeU3Ek__BackingField_12() const { return ___U3Cleft_tile_sizeU3Ek__BackingField_12; }
	inline Vector2_t2243707579 * get_address_of_U3Cleft_tile_sizeU3Ek__BackingField_12() { return &___U3Cleft_tile_sizeU3Ek__BackingField_12; }
	inline void set_U3Cleft_tile_sizeU3Ek__BackingField_12(Vector2_t2243707579  value)
	{
		___U3Cleft_tile_sizeU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3Cmid_tile_sizeU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PlatformPool_t3559850805, ___U3Cmid_tile_sizeU3Ek__BackingField_13)); }
	inline Vector2_t2243707579  get_U3Cmid_tile_sizeU3Ek__BackingField_13() const { return ___U3Cmid_tile_sizeU3Ek__BackingField_13; }
	inline Vector2_t2243707579 * get_address_of_U3Cmid_tile_sizeU3Ek__BackingField_13() { return &___U3Cmid_tile_sizeU3Ek__BackingField_13; }
	inline void set_U3Cmid_tile_sizeU3Ek__BackingField_13(Vector2_t2243707579  value)
	{
		___U3Cmid_tile_sizeU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3Cright_tile_sizeU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(PlatformPool_t3559850805, ___U3Cright_tile_sizeU3Ek__BackingField_14)); }
	inline Vector2_t2243707579  get_U3Cright_tile_sizeU3Ek__BackingField_14() const { return ___U3Cright_tile_sizeU3Ek__BackingField_14; }
	inline Vector2_t2243707579 * get_address_of_U3Cright_tile_sizeU3Ek__BackingField_14() { return &___U3Cright_tile_sizeU3Ek__BackingField_14; }
	inline void set_U3Cright_tile_sizeU3Ek__BackingField_14(Vector2_t2243707579  value)
	{
		___U3Cright_tile_sizeU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_original_color_15() { return static_cast<int32_t>(offsetof(PlatformPool_t3559850805, ___original_color_15)); }
	inline Color_t2020392075  get_original_color_15() const { return ___original_color_15; }
	inline Color_t2020392075 * get_address_of_original_color_15() { return &___original_color_15; }
	inline void set_original_color_15(Color_t2020392075  value)
	{
		___original_color_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMPOOL_T3559850805_H
#ifndef BOXCOLLIDER2D_T948534547_H
#define BOXCOLLIDER2D_T948534547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.BoxCollider2D
struct  BoxCollider2D_t948534547  : public Collider2D_t646061738
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOXCOLLIDER2D_T948534547_H
#ifndef PLATFORMGENERATOR_T395172572_H
#define PLATFORMGENERATOR_T395172572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlatformGenerator
struct  PlatformGenerator_t395172572  : public MonoBehaviour_t1158329972
{
public:
	// System.Single PlatformGenerator::approximate_width
	float ___approximate_width_2;
	// System.Int32 PlatformGenerator::<UsedTiles>k__BackingField
	int32_t ___U3CUsedTilesU3Ek__BackingField_3;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> PlatformGenerator::platform_tiles
	List_1_t1125654279 * ___platform_tiles_4;
	// PlatformPool PlatformGenerator::pool
	PlatformPool_t3559850805 * ___pool_5;

public:
	inline static int32_t get_offset_of_approximate_width_2() { return static_cast<int32_t>(offsetof(PlatformGenerator_t395172572, ___approximate_width_2)); }
	inline float get_approximate_width_2() const { return ___approximate_width_2; }
	inline float* get_address_of_approximate_width_2() { return &___approximate_width_2; }
	inline void set_approximate_width_2(float value)
	{
		___approximate_width_2 = value;
	}

	inline static int32_t get_offset_of_U3CUsedTilesU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PlatformGenerator_t395172572, ___U3CUsedTilesU3Ek__BackingField_3)); }
	inline int32_t get_U3CUsedTilesU3Ek__BackingField_3() const { return ___U3CUsedTilesU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CUsedTilesU3Ek__BackingField_3() { return &___U3CUsedTilesU3Ek__BackingField_3; }
	inline void set_U3CUsedTilesU3Ek__BackingField_3(int32_t value)
	{
		___U3CUsedTilesU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_platform_tiles_4() { return static_cast<int32_t>(offsetof(PlatformGenerator_t395172572, ___platform_tiles_4)); }
	inline List_1_t1125654279 * get_platform_tiles_4() const { return ___platform_tiles_4; }
	inline List_1_t1125654279 ** get_address_of_platform_tiles_4() { return &___platform_tiles_4; }
	inline void set_platform_tiles_4(List_1_t1125654279 * value)
	{
		___platform_tiles_4 = value;
		Il2CppCodeGenWriteBarrier((&___platform_tiles_4), value);
	}

	inline static int32_t get_offset_of_pool_5() { return static_cast<int32_t>(offsetof(PlatformGenerator_t395172572, ___pool_5)); }
	inline PlatformPool_t3559850805 * get_pool_5() const { return ___pool_5; }
	inline PlatformPool_t3559850805 ** get_address_of_pool_5() { return &___pool_5; }
	inline void set_pool_5(PlatformPool_t3559850805 * value)
	{
		___pool_5 = value;
		Il2CppCodeGenWriteBarrier((&___pool_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMGENERATOR_T395172572_H
#ifndef MICROGENERATOR_T2658307363_H
#define MICROGENERATOR_T2658307363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MicroGenerator
struct  MicroGenerator_t2658307363  : public MonoBehaviour_t1158329972
{
public:
	// PlatformPool MicroGenerator::pool
	PlatformPool_t3559850805 * ___pool_7;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> MicroGenerator::generated_platform
	List_1_t1125654279 * ___generated_platform_8;
	// System.Boolean MicroGenerator::has_created_level
	bool ___has_created_level_9;
	// System.Boolean MicroGenerator::has_created_another_level
	bool ___has_created_another_level_10;
	// Player2D MicroGenerator::player
	Player2D_t165353355 * ___player_11;
	// UnityEngine.GameObject MicroGenerator::enemy_prefab
	GameObject_t1756533147 * ___enemy_prefab_12;
	// System.Int32 MicroGenerator::<blocks_in_level>k__BackingField
	int32_t ___U3Cblocks_in_levelU3Ek__BackingField_13;
	// System.Single MicroGenerator::<difficulty>k__BackingField
	float ___U3CdifficultyU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_pool_7() { return static_cast<int32_t>(offsetof(MicroGenerator_t2658307363, ___pool_7)); }
	inline PlatformPool_t3559850805 * get_pool_7() const { return ___pool_7; }
	inline PlatformPool_t3559850805 ** get_address_of_pool_7() { return &___pool_7; }
	inline void set_pool_7(PlatformPool_t3559850805 * value)
	{
		___pool_7 = value;
		Il2CppCodeGenWriteBarrier((&___pool_7), value);
	}

	inline static int32_t get_offset_of_generated_platform_8() { return static_cast<int32_t>(offsetof(MicroGenerator_t2658307363, ___generated_platform_8)); }
	inline List_1_t1125654279 * get_generated_platform_8() const { return ___generated_platform_8; }
	inline List_1_t1125654279 ** get_address_of_generated_platform_8() { return &___generated_platform_8; }
	inline void set_generated_platform_8(List_1_t1125654279 * value)
	{
		___generated_platform_8 = value;
		Il2CppCodeGenWriteBarrier((&___generated_platform_8), value);
	}

	inline static int32_t get_offset_of_has_created_level_9() { return static_cast<int32_t>(offsetof(MicroGenerator_t2658307363, ___has_created_level_9)); }
	inline bool get_has_created_level_9() const { return ___has_created_level_9; }
	inline bool* get_address_of_has_created_level_9() { return &___has_created_level_9; }
	inline void set_has_created_level_9(bool value)
	{
		___has_created_level_9 = value;
	}

	inline static int32_t get_offset_of_has_created_another_level_10() { return static_cast<int32_t>(offsetof(MicroGenerator_t2658307363, ___has_created_another_level_10)); }
	inline bool get_has_created_another_level_10() const { return ___has_created_another_level_10; }
	inline bool* get_address_of_has_created_another_level_10() { return &___has_created_another_level_10; }
	inline void set_has_created_another_level_10(bool value)
	{
		___has_created_another_level_10 = value;
	}

	inline static int32_t get_offset_of_player_11() { return static_cast<int32_t>(offsetof(MicroGenerator_t2658307363, ___player_11)); }
	inline Player2D_t165353355 * get_player_11() const { return ___player_11; }
	inline Player2D_t165353355 ** get_address_of_player_11() { return &___player_11; }
	inline void set_player_11(Player2D_t165353355 * value)
	{
		___player_11 = value;
		Il2CppCodeGenWriteBarrier((&___player_11), value);
	}

	inline static int32_t get_offset_of_enemy_prefab_12() { return static_cast<int32_t>(offsetof(MicroGenerator_t2658307363, ___enemy_prefab_12)); }
	inline GameObject_t1756533147 * get_enemy_prefab_12() const { return ___enemy_prefab_12; }
	inline GameObject_t1756533147 ** get_address_of_enemy_prefab_12() { return &___enemy_prefab_12; }
	inline void set_enemy_prefab_12(GameObject_t1756533147 * value)
	{
		___enemy_prefab_12 = value;
		Il2CppCodeGenWriteBarrier((&___enemy_prefab_12), value);
	}

	inline static int32_t get_offset_of_U3Cblocks_in_levelU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(MicroGenerator_t2658307363, ___U3Cblocks_in_levelU3Ek__BackingField_13)); }
	inline int32_t get_U3Cblocks_in_levelU3Ek__BackingField_13() const { return ___U3Cblocks_in_levelU3Ek__BackingField_13; }
	inline int32_t* get_address_of_U3Cblocks_in_levelU3Ek__BackingField_13() { return &___U3Cblocks_in_levelU3Ek__BackingField_13; }
	inline void set_U3Cblocks_in_levelU3Ek__BackingField_13(int32_t value)
	{
		___U3Cblocks_in_levelU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CdifficultyU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(MicroGenerator_t2658307363, ___U3CdifficultyU3Ek__BackingField_14)); }
	inline float get_U3CdifficultyU3Ek__BackingField_14() const { return ___U3CdifficultyU3Ek__BackingField_14; }
	inline float* get_address_of_U3CdifficultyU3Ek__BackingField_14() { return &___U3CdifficultyU3Ek__BackingField_14; }
	inline void set_U3CdifficultyU3Ek__BackingField_14(float value)
	{
		___U3CdifficultyU3Ek__BackingField_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MICROGENERATOR_T2658307363_H
#ifndef SHIP_T1116303770_H
#define SHIP_T1116303770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ship
struct  Ship_t1116303770  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean Ship::<active>k__BackingField
	bool ___U3CactiveU3Ek__BackingField_2;
	// UnityEngine.Transform Ship::target
	Transform_t3275118058 * ___target_3;
	// System.Boolean Ship::inside_canvas
	bool ___inside_canvas_4;
	// UnityEngine.Vector3 Ship::direction
	Vector3_t2243707580  ___direction_5;
	// UnityEngine.Vector3[] Ship::corners
	Vector3U5BU5D_t1172311765* ___corners_6;
	// System.Single Ship::move_speed
	float ___move_speed_7;
	// System.Single Ship::time_until_fire
	float ___time_until_fire_8;
	// System.Single Ship::time_to_fire
	float ___time_to_fire_9;
	// UnityEngine.GameObject Ship::missile_prefab
	GameObject_t1756533147 * ___missile_prefab_10;
	// UnityEngine.UI.Image Ship::image
	Image_t2042527209 * ___image_11;
	// System.Boolean Ship::fading
	bool ___fading_12;

public:
	inline static int32_t get_offset_of_U3CactiveU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Ship_t1116303770, ___U3CactiveU3Ek__BackingField_2)); }
	inline bool get_U3CactiveU3Ek__BackingField_2() const { return ___U3CactiveU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CactiveU3Ek__BackingField_2() { return &___U3CactiveU3Ek__BackingField_2; }
	inline void set_U3CactiveU3Ek__BackingField_2(bool value)
	{
		___U3CactiveU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(Ship_t1116303770, ___target_3)); }
	inline Transform_t3275118058 * get_target_3() const { return ___target_3; }
	inline Transform_t3275118058 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(Transform_t3275118058 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_inside_canvas_4() { return static_cast<int32_t>(offsetof(Ship_t1116303770, ___inside_canvas_4)); }
	inline bool get_inside_canvas_4() const { return ___inside_canvas_4; }
	inline bool* get_address_of_inside_canvas_4() { return &___inside_canvas_4; }
	inline void set_inside_canvas_4(bool value)
	{
		___inside_canvas_4 = value;
	}

	inline static int32_t get_offset_of_direction_5() { return static_cast<int32_t>(offsetof(Ship_t1116303770, ___direction_5)); }
	inline Vector3_t2243707580  get_direction_5() const { return ___direction_5; }
	inline Vector3_t2243707580 * get_address_of_direction_5() { return &___direction_5; }
	inline void set_direction_5(Vector3_t2243707580  value)
	{
		___direction_5 = value;
	}

	inline static int32_t get_offset_of_corners_6() { return static_cast<int32_t>(offsetof(Ship_t1116303770, ___corners_6)); }
	inline Vector3U5BU5D_t1172311765* get_corners_6() const { return ___corners_6; }
	inline Vector3U5BU5D_t1172311765** get_address_of_corners_6() { return &___corners_6; }
	inline void set_corners_6(Vector3U5BU5D_t1172311765* value)
	{
		___corners_6 = value;
		Il2CppCodeGenWriteBarrier((&___corners_6), value);
	}

	inline static int32_t get_offset_of_move_speed_7() { return static_cast<int32_t>(offsetof(Ship_t1116303770, ___move_speed_7)); }
	inline float get_move_speed_7() const { return ___move_speed_7; }
	inline float* get_address_of_move_speed_7() { return &___move_speed_7; }
	inline void set_move_speed_7(float value)
	{
		___move_speed_7 = value;
	}

	inline static int32_t get_offset_of_time_until_fire_8() { return static_cast<int32_t>(offsetof(Ship_t1116303770, ___time_until_fire_8)); }
	inline float get_time_until_fire_8() const { return ___time_until_fire_8; }
	inline float* get_address_of_time_until_fire_8() { return &___time_until_fire_8; }
	inline void set_time_until_fire_8(float value)
	{
		___time_until_fire_8 = value;
	}

	inline static int32_t get_offset_of_time_to_fire_9() { return static_cast<int32_t>(offsetof(Ship_t1116303770, ___time_to_fire_9)); }
	inline float get_time_to_fire_9() const { return ___time_to_fire_9; }
	inline float* get_address_of_time_to_fire_9() { return &___time_to_fire_9; }
	inline void set_time_to_fire_9(float value)
	{
		___time_to_fire_9 = value;
	}

	inline static int32_t get_offset_of_missile_prefab_10() { return static_cast<int32_t>(offsetof(Ship_t1116303770, ___missile_prefab_10)); }
	inline GameObject_t1756533147 * get_missile_prefab_10() const { return ___missile_prefab_10; }
	inline GameObject_t1756533147 ** get_address_of_missile_prefab_10() { return &___missile_prefab_10; }
	inline void set_missile_prefab_10(GameObject_t1756533147 * value)
	{
		___missile_prefab_10 = value;
		Il2CppCodeGenWriteBarrier((&___missile_prefab_10), value);
	}

	inline static int32_t get_offset_of_image_11() { return static_cast<int32_t>(offsetof(Ship_t1116303770, ___image_11)); }
	inline Image_t2042527209 * get_image_11() const { return ___image_11; }
	inline Image_t2042527209 ** get_address_of_image_11() { return &___image_11; }
	inline void set_image_11(Image_t2042527209 * value)
	{
		___image_11 = value;
		Il2CppCodeGenWriteBarrier((&___image_11), value);
	}

	inline static int32_t get_offset_of_fading_12() { return static_cast<int32_t>(offsetof(Ship_t1116303770, ___fading_12)); }
	inline bool get_fading_12() const { return ___fading_12; }
	inline bool* get_address_of_fading_12() { return &___fading_12; }
	inline void set_fading_12(bool value)
	{
		___fading_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHIP_T1116303770_H
#ifndef KEYBOARDINPUT_T1795993661_H
#define KEYBOARDINPUT_T1795993661_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// KeyboardInput
struct  KeyboardInput_t1795993661  : public Input2D_t2938435410
{
public:
	// System.Single KeyboardInput::horizontal
	float ___horizontal_2;
	// System.Boolean KeyboardInput::jump_button_down
	bool ___jump_button_down_3;
	// System.Boolean KeyboardInput::jump_button_up
	bool ___jump_button_up_4;

public:
	inline static int32_t get_offset_of_horizontal_2() { return static_cast<int32_t>(offsetof(KeyboardInput_t1795993661, ___horizontal_2)); }
	inline float get_horizontal_2() const { return ___horizontal_2; }
	inline float* get_address_of_horizontal_2() { return &___horizontal_2; }
	inline void set_horizontal_2(float value)
	{
		___horizontal_2 = value;
	}

	inline static int32_t get_offset_of_jump_button_down_3() { return static_cast<int32_t>(offsetof(KeyboardInput_t1795993661, ___jump_button_down_3)); }
	inline bool get_jump_button_down_3() const { return ___jump_button_down_3; }
	inline bool* get_address_of_jump_button_down_3() { return &___jump_button_down_3; }
	inline void set_jump_button_down_3(bool value)
	{
		___jump_button_down_3 = value;
	}

	inline static int32_t get_offset_of_jump_button_up_4() { return static_cast<int32_t>(offsetof(KeyboardInput_t1795993661, ___jump_button_up_4)); }
	inline bool get_jump_button_up_4() const { return ___jump_button_up_4; }
	inline bool* get_address_of_jump_button_up_4() { return &___jump_button_up_4; }
	inline void set_jump_button_up_4(bool value)
	{
		___jump_button_up_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBOARDINPUT_T1795993661_H
#ifndef GRAPHIC_T2426225576_H
#define GRAPHIC_T2426225576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t2426225576  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t193706927 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2020392075  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3349966182 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRender
	CanvasRenderer_t261436805 * ___m_CanvasRender_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t209405766 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t4025899511 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t4025899511 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t4025899511 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3177091249 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Material_4)); }
	inline Material_t193706927 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t193706927 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t193706927 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Color_5)); }
	inline Color_t2020392075  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2020392075 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2020392075  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_RectTransform_7)); }
	inline RectTransform_t3349966182 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3349966182 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3349966182 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRender_8() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_CanvasRender_8)); }
	inline CanvasRenderer_t261436805 * get_m_CanvasRender_8() const { return ___m_CanvasRender_8; }
	inline CanvasRenderer_t261436805 ** get_address_of_m_CanvasRender_8() { return &___m_CanvasRender_8; }
	inline void set_m_CanvasRender_8(CanvasRenderer_t261436805 * value)
	{
		___m_CanvasRender_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRender_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Canvas_9)); }
	inline Canvas_t209405766 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t209405766 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t209405766 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3177091249 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3177091249 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3177091249 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t2426225576_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t193706927 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3542995729 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t1356156583 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t385374196 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t193706927 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t193706927 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t193706927 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3542995729 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3542995729 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3542995729 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t1356156583 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t1356156583 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t1356156583 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t385374196 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t385374196 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t385374196 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T2426225576_H
#ifndef THREAT_T2614656216_H
#define THREAT_T2614656216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Threat
struct  Threat_t2614656216  : public Controller2D_t802485922
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREAT_T2614656216_H
#ifndef PLAYER2D_T165353355_H
#define PLAYER2D_T165353355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Player2D
struct  Player2D_t165353355  : public Controller2D_t802485922
{
public:
	// UnityEngine.Quaternion Player2D::target_rotation
	Quaternion_t4030073918  ___target_rotation_22;
	// UnityEngine.SpriteRenderer Player2D::sprite_renderer
	SpriteRenderer_t1209076198 * ___sprite_renderer_23;
	// System.Int32 Player2D::health
	int32_t ___health_24;
	// System.Boolean Player2D::is_immune
	bool ___is_immune_25;
	// UnityEngine.Coroutine Player2D::flicker_coroutine
	Coroutine_t2299508840 * ___flicker_coroutine_26;
	// UnityEngine.Color Player2D::original_color
	Color_t2020392075  ___original_color_27;
	// Input2D Player2D::input
	Input2D_t2938435410 * ___input_28;
	// System.Int32 Player2D::score_offset
	int32_t ___score_offset_29;
	// System.Int32 Player2D::<current_score>k__BackingField
	int32_t ___U3Ccurrent_scoreU3Ek__BackingField_30;
	// System.Boolean Player2D::quitting
	bool ___quitting_31;

public:
	inline static int32_t get_offset_of_target_rotation_22() { return static_cast<int32_t>(offsetof(Player2D_t165353355, ___target_rotation_22)); }
	inline Quaternion_t4030073918  get_target_rotation_22() const { return ___target_rotation_22; }
	inline Quaternion_t4030073918 * get_address_of_target_rotation_22() { return &___target_rotation_22; }
	inline void set_target_rotation_22(Quaternion_t4030073918  value)
	{
		___target_rotation_22 = value;
	}

	inline static int32_t get_offset_of_sprite_renderer_23() { return static_cast<int32_t>(offsetof(Player2D_t165353355, ___sprite_renderer_23)); }
	inline SpriteRenderer_t1209076198 * get_sprite_renderer_23() const { return ___sprite_renderer_23; }
	inline SpriteRenderer_t1209076198 ** get_address_of_sprite_renderer_23() { return &___sprite_renderer_23; }
	inline void set_sprite_renderer_23(SpriteRenderer_t1209076198 * value)
	{
		___sprite_renderer_23 = value;
		Il2CppCodeGenWriteBarrier((&___sprite_renderer_23), value);
	}

	inline static int32_t get_offset_of_health_24() { return static_cast<int32_t>(offsetof(Player2D_t165353355, ___health_24)); }
	inline int32_t get_health_24() const { return ___health_24; }
	inline int32_t* get_address_of_health_24() { return &___health_24; }
	inline void set_health_24(int32_t value)
	{
		___health_24 = value;
	}

	inline static int32_t get_offset_of_is_immune_25() { return static_cast<int32_t>(offsetof(Player2D_t165353355, ___is_immune_25)); }
	inline bool get_is_immune_25() const { return ___is_immune_25; }
	inline bool* get_address_of_is_immune_25() { return &___is_immune_25; }
	inline void set_is_immune_25(bool value)
	{
		___is_immune_25 = value;
	}

	inline static int32_t get_offset_of_flicker_coroutine_26() { return static_cast<int32_t>(offsetof(Player2D_t165353355, ___flicker_coroutine_26)); }
	inline Coroutine_t2299508840 * get_flicker_coroutine_26() const { return ___flicker_coroutine_26; }
	inline Coroutine_t2299508840 ** get_address_of_flicker_coroutine_26() { return &___flicker_coroutine_26; }
	inline void set_flicker_coroutine_26(Coroutine_t2299508840 * value)
	{
		___flicker_coroutine_26 = value;
		Il2CppCodeGenWriteBarrier((&___flicker_coroutine_26), value);
	}

	inline static int32_t get_offset_of_original_color_27() { return static_cast<int32_t>(offsetof(Player2D_t165353355, ___original_color_27)); }
	inline Color_t2020392075  get_original_color_27() const { return ___original_color_27; }
	inline Color_t2020392075 * get_address_of_original_color_27() { return &___original_color_27; }
	inline void set_original_color_27(Color_t2020392075  value)
	{
		___original_color_27 = value;
	}

	inline static int32_t get_offset_of_input_28() { return static_cast<int32_t>(offsetof(Player2D_t165353355, ___input_28)); }
	inline Input2D_t2938435410 * get_input_28() const { return ___input_28; }
	inline Input2D_t2938435410 ** get_address_of_input_28() { return &___input_28; }
	inline void set_input_28(Input2D_t2938435410 * value)
	{
		___input_28 = value;
		Il2CppCodeGenWriteBarrier((&___input_28), value);
	}

	inline static int32_t get_offset_of_score_offset_29() { return static_cast<int32_t>(offsetof(Player2D_t165353355, ___score_offset_29)); }
	inline int32_t get_score_offset_29() const { return ___score_offset_29; }
	inline int32_t* get_address_of_score_offset_29() { return &___score_offset_29; }
	inline void set_score_offset_29(int32_t value)
	{
		___score_offset_29 = value;
	}

	inline static int32_t get_offset_of_U3Ccurrent_scoreU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(Player2D_t165353355, ___U3Ccurrent_scoreU3Ek__BackingField_30)); }
	inline int32_t get_U3Ccurrent_scoreU3Ek__BackingField_30() const { return ___U3Ccurrent_scoreU3Ek__BackingField_30; }
	inline int32_t* get_address_of_U3Ccurrent_scoreU3Ek__BackingField_30() { return &___U3Ccurrent_scoreU3Ek__BackingField_30; }
	inline void set_U3Ccurrent_scoreU3Ek__BackingField_30(int32_t value)
	{
		___U3Ccurrent_scoreU3Ek__BackingField_30 = value;
	}

	inline static int32_t get_offset_of_quitting_31() { return static_cast<int32_t>(offsetof(Player2D_t165353355, ___quitting_31)); }
	inline bool get_quitting_31() const { return ___quitting_31; }
	inline bool* get_address_of_quitting_31() { return &___quitting_31; }
	inline void set_quitting_31(bool value)
	{
		___quitting_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYER2D_T165353355_H
#ifndef JOYSTICK_T513152754_H
#define JOYSTICK_T513152754_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JoyStick
struct  JoyStick_t513152754  : public Input2D_t2938435410
{
public:
	// UnityEngine.UI.Image JoyStick::jsContainer
	Image_t2042527209 * ___jsContainer_2;
	// UnityEngine.UI.Image JoyStick::joystick
	Image_t2042527209 * ___joystick_3;
	// UnityEngine.Vector3 JoyStick::InputDirection
	Vector3_t2243707580  ___InputDirection_4;
	// System.Boolean JoyStick::jump_down
	bool ___jump_down_5;
	// System.Boolean JoyStick::jump_up
	bool ___jump_up_6;

public:
	inline static int32_t get_offset_of_jsContainer_2() { return static_cast<int32_t>(offsetof(JoyStick_t513152754, ___jsContainer_2)); }
	inline Image_t2042527209 * get_jsContainer_2() const { return ___jsContainer_2; }
	inline Image_t2042527209 ** get_address_of_jsContainer_2() { return &___jsContainer_2; }
	inline void set_jsContainer_2(Image_t2042527209 * value)
	{
		___jsContainer_2 = value;
		Il2CppCodeGenWriteBarrier((&___jsContainer_2), value);
	}

	inline static int32_t get_offset_of_joystick_3() { return static_cast<int32_t>(offsetof(JoyStick_t513152754, ___joystick_3)); }
	inline Image_t2042527209 * get_joystick_3() const { return ___joystick_3; }
	inline Image_t2042527209 ** get_address_of_joystick_3() { return &___joystick_3; }
	inline void set_joystick_3(Image_t2042527209 * value)
	{
		___joystick_3 = value;
		Il2CppCodeGenWriteBarrier((&___joystick_3), value);
	}

	inline static int32_t get_offset_of_InputDirection_4() { return static_cast<int32_t>(offsetof(JoyStick_t513152754, ___InputDirection_4)); }
	inline Vector3_t2243707580  get_InputDirection_4() const { return ___InputDirection_4; }
	inline Vector3_t2243707580 * get_address_of_InputDirection_4() { return &___InputDirection_4; }
	inline void set_InputDirection_4(Vector3_t2243707580  value)
	{
		___InputDirection_4 = value;
	}

	inline static int32_t get_offset_of_jump_down_5() { return static_cast<int32_t>(offsetof(JoyStick_t513152754, ___jump_down_5)); }
	inline bool get_jump_down_5() const { return ___jump_down_5; }
	inline bool* get_address_of_jump_down_5() { return &___jump_down_5; }
	inline void set_jump_down_5(bool value)
	{
		___jump_down_5 = value;
	}

	inline static int32_t get_offset_of_jump_up_6() { return static_cast<int32_t>(offsetof(JoyStick_t513152754, ___jump_up_6)); }
	inline bool get_jump_up_6() const { return ___jump_up_6; }
	inline bool* get_address_of_jump_up_6() { return &___jump_up_6; }
	inline void set_jump_up_6(bool value)
	{
		___jump_up_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICK_T513152754_H
#ifndef MISSILE_T813944928_H
#define MISSILE_T813944928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Missile
struct  Missile_t813944928  : public Threat_t2614656216
{
public:
	// System.Boolean Missile::exploded
	bool ___exploded_21;

public:
	inline static int32_t get_offset_of_exploded_21() { return static_cast<int32_t>(offsetof(Missile_t813944928, ___exploded_21)); }
	inline bool get_exploded_21() const { return ___exploded_21; }
	inline bool* get_address_of_exploded_21() { return &___exploded_21; }
	inline void set_exploded_21(bool value)
	{
		___exploded_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISSILE_T813944928_H
#ifndef MASKABLEGRAPHIC_T540192618_H
#define MASKABLEGRAPHIC_T540192618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t540192618  : public Graphic_t2426225576
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t193706927 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t1156185964 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3778758259 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1172311765* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_MaskMaterial_20)); }
	inline Material_t193706927 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t193706927 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t193706927 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ParentMask_21)); }
	inline RectMask2D_t1156185964 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t1156185964 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t1156185964 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3778758259 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3778758259 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3778758259 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1172311765* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1172311765* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T540192618_H
#ifndef TEXT_T356221433_H
#define TEXT_T356221433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_t356221433  : public MaskableGraphic_t540192618
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t2614388407 * ___m_FontData_28;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_29;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t647235000 * ___m_TextCache_30;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t647235000 * ___m_TextCacheForLayout_31;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_33;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_t3048644023* ___m_TempVerts_34;

public:
	inline static int32_t get_offset_of_m_FontData_28() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_FontData_28)); }
	inline FontData_t2614388407 * get_m_FontData_28() const { return ___m_FontData_28; }
	inline FontData_t2614388407 ** get_address_of_m_FontData_28() { return &___m_FontData_28; }
	inline void set_m_FontData_28(FontData_t2614388407 * value)
	{
		___m_FontData_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_28), value);
	}

	inline static int32_t get_offset_of_m_Text_29() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_Text_29)); }
	inline String_t* get_m_Text_29() const { return ___m_Text_29; }
	inline String_t** get_address_of_m_Text_29() { return &___m_Text_29; }
	inline void set_m_Text_29(String_t* value)
	{
		___m_Text_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_29), value);
	}

	inline static int32_t get_offset_of_m_TextCache_30() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_TextCache_30)); }
	inline TextGenerator_t647235000 * get_m_TextCache_30() const { return ___m_TextCache_30; }
	inline TextGenerator_t647235000 ** get_address_of_m_TextCache_30() { return &___m_TextCache_30; }
	inline void set_m_TextCache_30(TextGenerator_t647235000 * value)
	{
		___m_TextCache_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_30), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_31() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_TextCacheForLayout_31)); }
	inline TextGenerator_t647235000 * get_m_TextCacheForLayout_31() const { return ___m_TextCacheForLayout_31; }
	inline TextGenerator_t647235000 ** get_address_of_m_TextCacheForLayout_31() { return &___m_TextCacheForLayout_31; }
	inline void set_m_TextCacheForLayout_31(TextGenerator_t647235000 * value)
	{
		___m_TextCacheForLayout_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_31), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_33() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_DisableFontTextureRebuiltCallback_33)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_33() const { return ___m_DisableFontTextureRebuiltCallback_33; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_33() { return &___m_DisableFontTextureRebuiltCallback_33; }
	inline void set_m_DisableFontTextureRebuiltCallback_33(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_33 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_34() { return static_cast<int32_t>(offsetof(Text_t356221433, ___m_TempVerts_34)); }
	inline UIVertexU5BU5D_t3048644023* get_m_TempVerts_34() const { return ___m_TempVerts_34; }
	inline UIVertexU5BU5D_t3048644023** get_address_of_m_TempVerts_34() { return &___m_TempVerts_34; }
	inline void set_m_TempVerts_34(UIVertexU5BU5D_t3048644023* value)
	{
		___m_TempVerts_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_34), value);
	}
};

struct Text_t356221433_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t193706927 * ___s_DefaultText_32;

public:
	inline static int32_t get_offset_of_s_DefaultText_32() { return static_cast<int32_t>(offsetof(Text_t356221433_StaticFields, ___s_DefaultText_32)); }
	inline Material_t193706927 * get_s_DefaultText_32() const { return ___s_DefaultText_32; }
	inline Material_t193706927 ** get_address_of_s_DefaultText_32() { return &___s_DefaultText_32; }
	inline void set_s_DefaultText_32(Material_t193706927 * value)
	{
		___s_DefaultText_32 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_T356221433_H
#ifndef IMAGE_T2042527209_H
#define IMAGE_T2042527209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Image
struct  Image_t2042527209  : public MaskableGraphic_t540192618
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t309593783 * ___m_Sprite_29;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t309593783 * ___m_OverrideSprite_30;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_31;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_32;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_33;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_34;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_35;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_36;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_37;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_38;

public:
	inline static int32_t get_offset_of_m_Sprite_29() { return static_cast<int32_t>(offsetof(Image_t2042527209, ___m_Sprite_29)); }
	inline Sprite_t309593783 * get_m_Sprite_29() const { return ___m_Sprite_29; }
	inline Sprite_t309593783 ** get_address_of_m_Sprite_29() { return &___m_Sprite_29; }
	inline void set_m_Sprite_29(Sprite_t309593783 * value)
	{
		___m_Sprite_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Sprite_29), value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_30() { return static_cast<int32_t>(offsetof(Image_t2042527209, ___m_OverrideSprite_30)); }
	inline Sprite_t309593783 * get_m_OverrideSprite_30() const { return ___m_OverrideSprite_30; }
	inline Sprite_t309593783 ** get_address_of_m_OverrideSprite_30() { return &___m_OverrideSprite_30; }
	inline void set_m_OverrideSprite_30(Sprite_t309593783 * value)
	{
		___m_OverrideSprite_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_OverrideSprite_30), value);
	}

	inline static int32_t get_offset_of_m_Type_31() { return static_cast<int32_t>(offsetof(Image_t2042527209, ___m_Type_31)); }
	inline int32_t get_m_Type_31() const { return ___m_Type_31; }
	inline int32_t* get_address_of_m_Type_31() { return &___m_Type_31; }
	inline void set_m_Type_31(int32_t value)
	{
		___m_Type_31 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_32() { return static_cast<int32_t>(offsetof(Image_t2042527209, ___m_PreserveAspect_32)); }
	inline bool get_m_PreserveAspect_32() const { return ___m_PreserveAspect_32; }
	inline bool* get_address_of_m_PreserveAspect_32() { return &___m_PreserveAspect_32; }
	inline void set_m_PreserveAspect_32(bool value)
	{
		___m_PreserveAspect_32 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_33() { return static_cast<int32_t>(offsetof(Image_t2042527209, ___m_FillCenter_33)); }
	inline bool get_m_FillCenter_33() const { return ___m_FillCenter_33; }
	inline bool* get_address_of_m_FillCenter_33() { return &___m_FillCenter_33; }
	inline void set_m_FillCenter_33(bool value)
	{
		___m_FillCenter_33 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_34() { return static_cast<int32_t>(offsetof(Image_t2042527209, ___m_FillMethod_34)); }
	inline int32_t get_m_FillMethod_34() const { return ___m_FillMethod_34; }
	inline int32_t* get_address_of_m_FillMethod_34() { return &___m_FillMethod_34; }
	inline void set_m_FillMethod_34(int32_t value)
	{
		___m_FillMethod_34 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_35() { return static_cast<int32_t>(offsetof(Image_t2042527209, ___m_FillAmount_35)); }
	inline float get_m_FillAmount_35() const { return ___m_FillAmount_35; }
	inline float* get_address_of_m_FillAmount_35() { return &___m_FillAmount_35; }
	inline void set_m_FillAmount_35(float value)
	{
		___m_FillAmount_35 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_36() { return static_cast<int32_t>(offsetof(Image_t2042527209, ___m_FillClockwise_36)); }
	inline bool get_m_FillClockwise_36() const { return ___m_FillClockwise_36; }
	inline bool* get_address_of_m_FillClockwise_36() { return &___m_FillClockwise_36; }
	inline void set_m_FillClockwise_36(bool value)
	{
		___m_FillClockwise_36 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_37() { return static_cast<int32_t>(offsetof(Image_t2042527209, ___m_FillOrigin_37)); }
	inline int32_t get_m_FillOrigin_37() const { return ___m_FillOrigin_37; }
	inline int32_t* get_address_of_m_FillOrigin_37() { return &___m_FillOrigin_37; }
	inline void set_m_FillOrigin_37(int32_t value)
	{
		___m_FillOrigin_37 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_38() { return static_cast<int32_t>(offsetof(Image_t2042527209, ___m_AlphaHitTestMinimumThreshold_38)); }
	inline float get_m_AlphaHitTestMinimumThreshold_38() const { return ___m_AlphaHitTestMinimumThreshold_38; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_38() { return &___m_AlphaHitTestMinimumThreshold_38; }
	inline void set_m_AlphaHitTestMinimumThreshold_38(float value)
	{
		___m_AlphaHitTestMinimumThreshold_38 = value;
	}
};

struct Image_t2042527209_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t193706927 * ___s_ETC1DefaultUI_28;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_t686124026* ___s_VertScratch_39;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_t686124026* ___s_UVScratch_40;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t1172311765* ___s_Xy_41;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t1172311765* ___s_Uv_42;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_28() { return static_cast<int32_t>(offsetof(Image_t2042527209_StaticFields, ___s_ETC1DefaultUI_28)); }
	inline Material_t193706927 * get_s_ETC1DefaultUI_28() const { return ___s_ETC1DefaultUI_28; }
	inline Material_t193706927 ** get_address_of_s_ETC1DefaultUI_28() { return &___s_ETC1DefaultUI_28; }
	inline void set_s_ETC1DefaultUI_28(Material_t193706927 * value)
	{
		___s_ETC1DefaultUI_28 = value;
		Il2CppCodeGenWriteBarrier((&___s_ETC1DefaultUI_28), value);
	}

	inline static int32_t get_offset_of_s_VertScratch_39() { return static_cast<int32_t>(offsetof(Image_t2042527209_StaticFields, ___s_VertScratch_39)); }
	inline Vector2U5BU5D_t686124026* get_s_VertScratch_39() const { return ___s_VertScratch_39; }
	inline Vector2U5BU5D_t686124026** get_address_of_s_VertScratch_39() { return &___s_VertScratch_39; }
	inline void set_s_VertScratch_39(Vector2U5BU5D_t686124026* value)
	{
		___s_VertScratch_39 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertScratch_39), value);
	}

	inline static int32_t get_offset_of_s_UVScratch_40() { return static_cast<int32_t>(offsetof(Image_t2042527209_StaticFields, ___s_UVScratch_40)); }
	inline Vector2U5BU5D_t686124026* get_s_UVScratch_40() const { return ___s_UVScratch_40; }
	inline Vector2U5BU5D_t686124026** get_address_of_s_UVScratch_40() { return &___s_UVScratch_40; }
	inline void set_s_UVScratch_40(Vector2U5BU5D_t686124026* value)
	{
		___s_UVScratch_40 = value;
		Il2CppCodeGenWriteBarrier((&___s_UVScratch_40), value);
	}

	inline static int32_t get_offset_of_s_Xy_41() { return static_cast<int32_t>(offsetof(Image_t2042527209_StaticFields, ___s_Xy_41)); }
	inline Vector3U5BU5D_t1172311765* get_s_Xy_41() const { return ___s_Xy_41; }
	inline Vector3U5BU5D_t1172311765** get_address_of_s_Xy_41() { return &___s_Xy_41; }
	inline void set_s_Xy_41(Vector3U5BU5D_t1172311765* value)
	{
		___s_Xy_41 = value;
		Il2CppCodeGenWriteBarrier((&___s_Xy_41), value);
	}

	inline static int32_t get_offset_of_s_Uv_42() { return static_cast<int32_t>(offsetof(Image_t2042527209_StaticFields, ___s_Uv_42)); }
	inline Vector3U5BU5D_t1172311765* get_s_Uv_42() const { return ___s_Uv_42; }
	inline Vector3U5BU5D_t1172311765** get_address_of_s_Uv_42() { return &___s_Uv_42; }
	inline void set_s_Uv_42(Vector3U5BU5D_t1172311765* value)
	{
		___s_Uv_42 = value;
		Il2CppCodeGenWriteBarrier((&___s_Uv_42), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGE_T2042527209_H
// System.String[]
struct StringU5BU5D_t1642385972  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Collider2D[]
struct Collider2DU5BU5D_t3535523695  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Collider2D_t646061738 * m_Items[1];

public:
	inline Collider2D_t646061738 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Collider2D_t646061738 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Collider2D_t646061738 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Collider2D_t646061738 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Collider2D_t646061738 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Collider2D_t646061738 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t1490986844  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ParticleSystem_t3394631041 * m_Items[1];

public:
	inline ParticleSystem_t3394631041 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ParticleSystem_t3394631041 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ParticleSystem_t3394631041 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ParticleSystem_t3394631041 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ParticleSystem_t3394631041 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ParticleSystem_t3394631041 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_t1756533147 * m_Items[1];

public:
	inline GameObject_t1756533147 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_t1756533147 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_t1756533147 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GameObject_t1756533147 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_t1756533147 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_t1756533147 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.SpriteRenderer[]
struct SpriteRendererU5BU5D_t1098056643  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) SpriteRenderer_t1209076198 * m_Items[1];

public:
	inline SpriteRenderer_t1209076198 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SpriteRenderer_t1209076198 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SpriteRenderer_t1209076198 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline SpriteRenderer_t1209076198 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SpriteRenderer_t1209076198 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SpriteRenderer_t1209076198 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Int32[]
struct Int32U5BU5D_t3030399641  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t2243707580  m_Items[1];

public:
	inline Vector3_t2243707580  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t2243707580 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t2243707580  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t2243707580  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t2243707580 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t2243707580  value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m2724124387_gshared (Component_t3819376471 * __this, const RuntimeMethod* method);
// !!0[] UnityEngine.Component::GetComponents<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* Component_GetComponents_TisRuntimeObject_m3998315035_gshared (Component_t3819376471 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared (GameObject_t1756533147 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C"  RuntimeObject * Object_Instantiate_TisRuntimeObject_m447919519_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponentInChildren<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponentInChildren_TisRuntimeObject_m327292296_gshared (GameObject_t1756533147 * __this, const RuntimeMethod* method);
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* GameObject_GetComponentsInChildren_TisRuntimeObject_m1467294482_gshared (GameObject_t1756533147 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_AddComponent_TisRuntimeObject_m3813873105_gshared (GameObject_t1756533147 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m310736118_gshared (List_1_t2058570427 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2375293942_gshared (List_1_t2058570427 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  RuntimeObject * List_1_get_Item_m2062981835_gshared (List_1_t2058570427 * __this, int32_t p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
extern "C"  bool List_1_Remove_m3164383811_gshared (List_1_t2058570427 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m4157722533_gshared (List_1_t2058570427 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C"  void List_1_Clear_m4254626809_gshared (List_1_t2058570427 * __this, const RuntimeMethod* method);
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* Component_GetComponentsInChildren_TisRuntimeObject_m3978412804_gshared (Component_t3819376471 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  RuntimeObject * Component_GetComponentInChildren_TisRuntimeObject_m2461586036_gshared (Component_t3819376471 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m136460305_gshared (List_1_t2058570427 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::set_Item(System.Int32,!0)
extern "C"  void List_1_set_Item_m4246197648_gshared (List_1_t2058570427 * __this, int32_t p0, RuntimeObject * p1, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  RuntimeObject * Object_Instantiate_TisRuntimeObject_m3829784634_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, Vector3_t2243707580  p1, Quaternion_t4030073918  p2, const RuntimeMethod* method);

// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m432505302 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t2330101084  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m1825328214 (MonoBehaviour_t1158329972 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody2D>()
#define Component_GetComponent_TisRigidbody2D_t502193897_m3702757851(__this, method) ((  Rigidbody2D_t502193897 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2724124387_gshared)(__this, method)
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m2516226135 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.Component::GetComponent(System.Type)
extern "C"  Component_t3819376471 * Component_GetComponent_m1409092944 (Component_t3819376471 * __this, Type_t * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.LayerMask::GetMask(System.String[])
extern "C"  int32_t LayerMask_GetMask_m4167990913 (RuntimeObject * __this /* static, unused */, StringU5BU5D_t1642385972* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.LayerMask UnityEngine.LayerMask::op_Implicit(System.Int32)
extern "C"  LayerMask_t3188175821  LayerMask_op_Implicit_m1796475436 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_velocity()
extern "C"  Vector2_t2243707579  Rigidbody2D_get_velocity_m1631921214 (Rigidbody2D_t502193897 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Controller2D::IsGrounded()
extern "C"  bool Controller2D_IsGrounded_m2912631519 (Controller2D_t802485922 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::SmoothDamp(System.Single,System.Single,System.Single&,System.Single)
extern "C"  float Mathf_SmoothDamp_m3504509584 (RuntimeObject * __this /* static, unused */, float p0, float p1, float* p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Controller2D::Flip()
extern "C"  void Controller2D_Flip_m689078882 (Controller2D_t802485922 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Controller2D::MaxJumpVelocity()
extern "C"  float Controller2D_MaxJumpVelocity_m2382483678 (Controller2D_t802485922 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Controller2D::MinJumpVelocity()
extern "C"  float Controller2D_MinJumpVelocity_m4136881028 (Controller2D_t802485922 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Controller2D::DoubleJumpVelocity()
extern "C"  float Controller2D_DoubleJumpVelocity_m2506104035 (Controller2D_t802485922 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::set_velocity(UnityEngine.Vector2)
extern "C"  void Rigidbody2D_set_velocity_m2161463521 (Rigidbody2D_t502193897 * __this, Vector2_t2243707579  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RigidbodyType2D UnityEngine.Rigidbody2D::get_bodyType()
extern "C"  int32_t Rigidbody2D_get_bodyType_m512807397 (Rigidbody2D_t502193897 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Controller2D::Gravity()
extern "C"  float Controller2D_Gravity_m418077541 (Controller2D_t802485922 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_fixedDeltaTime()
extern "C"  float Time_get_fixedDeltaTime_m2740561775 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m847450945 (Vector2_t2243707579 * __this, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3275118058 * Component_get_transform_m3374354972 (Component_t3819376471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern "C"  Vector3_t2243707580  Transform_get_localScale_m46214814 (Transform_t3275118058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C"  void Transform_set_localScale_m1442831667 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds UnityEngine.Collider2D::get_bounds()
extern "C"  Bounds_t3033363703  Collider2D_get_bounds_m242690441 (Collider2D_t646061738 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_min()
extern "C"  Vector3_t2243707580  Bounds_get_min_m3351470040 (Bounds_t3033363703 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_size()
extern "C"  Vector3_t2243707580  Bounds_get_size_m3951110137 (Bounds_t3033363703 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mathf::RoundToInt(System.Single)
extern "C"  int32_t Mathf_RoundToInt_m1833913145 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_right()
extern "C"  Vector2_t2243707579  Vector2_get_right_m4262310679 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t2243707579  Vector2_op_Multiply_m3676538483 (RuntimeObject * __this /* static, unused */, Vector2_t2243707579  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2243707579  Vector2_op_Addition_m4229571528 (RuntimeObject * __this /* static, unused */, Vector2_t2243707579  p0, Vector2_t2243707579  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_up()
extern "C"  Vector2_t2243707579  Vector2_get_up_m1721661564 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_UnaryNegation(UnityEngine.Vector2)
extern "C"  Vector2_t2243707579  Vector2_op_UnaryNegation_m1410369452 (RuntimeObject * __this /* static, unused */, Vector2_t2243707579  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.LayerMask::op_Implicit(UnityEngine.LayerMask)
extern "C"  int32_t LayerMask_op_Implicit_m1604355880 (RuntimeObject * __this /* static, unused */, LayerMask_t3188175821  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern "C"  RaycastHit2D_t4063908774  Physics2D_Raycast_m2434840196 (RuntimeObject * __this /* static, unused */, Vector2_t2243707579  p0, Vector2_t2243707579  p1, float p2, int32_t p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C"  Vector3_t2243707580  Vector2_op_Implicit_m129629632 (RuntimeObject * __this /* static, unused */, Vector2_t2243707579  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_red()
extern "C"  Color_t2020392075  Color_get_red_m3374418718 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::DrawRay(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color)
extern "C"  void Debug_DrawRay_m999504727 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, Color_t2020392075  p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.RaycastHit2D::op_Implicit(UnityEngine.RaycastHit2D)
extern "C"  bool RaycastHit2D_op_Implicit_m3937142044 (RuntimeObject * __this /* static, unused */, RaycastHit2D_t4063908774  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0[] UnityEngine.Component::GetComponents<UnityEngine.Collider2D>()
#define Component_GetComponents_TisCollider2D_t646061738_m753859713(__this, method) ((  Collider2DU5BU5D_t3535523695* (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponents_TisRuntimeObject_m3998315035_gshared)(__this, method)
// System.Boolean UnityEngine.Bounds::op_Inequality(UnityEngine.Bounds,UnityEngine.Bounds)
extern "C"  bool Bounds_op_Inequality_m1440807554 (RuntimeObject * __this /* static, unused */, Bounds_t3033363703  p0, Bounds_t3033363703  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bounds::Encapsulate(UnityEngine.Bounds)
extern "C"  void Bounds_Encapsulate_m2657927334 (Bounds_t3033363703 * __this, Bounds_t3033363703  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::FindGameObjectWithTag(System.String)
extern "C"  GameObject_t1756533147 * GameObject_FindGameObjectWithTag_m1433464258 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<Player2D>()
#define GameObject_GetComponent_TisPlayer2D_t165353355_m1754251190(__this, method) ((  Player2D_t165353355 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// UnityEngine.Object UnityEngine.Resources::Load(System.String)
extern "C"  Object_t1021602117 * Resources_Load_m3480536692 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t1756533147_m3664764861(__this /* static, unused */, p0, method) ((  GameObject_t1756533147 * (*) (RuntimeObject * /* static, unused */, GameObject_t1756533147 *, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m447919519_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Canvas>()
#define GameObject_GetComponent_TisCanvas_t209405766_m195193039(__this, method) ((  Canvas_t209405766 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// System.Void UnityEngine.Canvas::set_renderMode(UnityEngine.RenderMode)
extern "C"  void Canvas_set_renderMode_m1632154567 (Canvas_t209405766 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t189460977 * Camera_get_main_m881971336 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Canvas::set_worldCamera(UnityEngine.Camera)
extern "C"  void Canvas_set_worldCamera_m2637367989 (Canvas_t209405766 * __this, Camera_t189460977 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Canvas::set_sortingLayerName(System.String)
extern "C"  void Canvas_set_sortingLayerName_m3179341450 (Canvas_t209405766 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponentInChildren<JoyStick>()
#define GameObject_GetComponentInChildren_TisJoyStick_t513152754_m3063844157(__this, method) ((  JoyStick_t513152754 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponentInChildren_TisRuntimeObject_m327292296_gshared)(__this, method)
// System.Void Player2D::SetInput(Input2D)
extern "C"  void Player2D_SetInput_m1210245776 (Player2D_t165353355 * __this, Input2D_t2938435410 * ____input0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Collision2D::get_gameObject()
extern "C"  GameObject_t1756533147 * Collision2D_get_gameObject_m1296200797 (Collision2D_t1539500754 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GameObject::get_tag()
extern "C"  String_t* GameObject_get_tag_m3359901967 (GameObject_t1756533147 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m1790663636 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1756533147 * Component_get_gameObject_m2159020946 (Component_t3819376471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.ParticleSystem>()
#define GameObject_GetComponentsInChildren_TisParticleSystem_t3394631041_m2199618894(__this, method) ((  ParticleSystemU5BU5D_t1490986844* (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponentsInChildren_TisRuntimeObject_m1467294482_gshared)(__this, method)
// System.String UnityEngine.Component::get_tag()
extern "C"  String_t* Component_get_tag_m124558427 (Component_t3819376471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::Equals(System.String)
extern "C"  bool String_Equals_m2633592423 (String_t* __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Play()
extern "C"  void ParticleSystem_Play_m2510524101 (ParticleSystem_t3394631041 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)
extern "C"  void MonoBehaviour_Invoke_m2201735803 (MonoBehaviour_t1158329972 * __this, String_t* p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Rigidbody2D>()
#define GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143(__this, method) ((  Rigidbody2D_t502193897 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// System.Void UnityEngine.ParticleSystem::Stop()
extern "C"  void ParticleSystem_Stop_m3868680149 (ParticleSystem_t3394631041 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Rigidbody2D>()
#define GameObject_AddComponent_TisRigidbody2D_t502193897_m2081092396(__this, method) ((  Rigidbody2D_t502193897 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3813873105_gshared)(__this, method)
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t2243707580  Vector3_get_zero_m3202043185 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3275118058 * GameObject_get_transform_m3490276752 (GameObject_t1756533147 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m1757773010 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t2243707580  Transform_get_position_m2304215762 (Transform_t3275118058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Camera::WorldToViewportPoint(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Camera_WorldToViewportPoint_m311010509 (Camera_t189460977 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m1555724485 (Vector3_t2243707580 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Camera::ViewportToWorldPoint(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Camera_ViewportToWorldPoint_m283095757 (Camera_t189460977 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_op_Subtraction_m4046047256 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_op_Addition_m394909128 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::SmoothDamp(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&,System.Single)
extern "C"  Vector3_t2243707580  Vector3_SmoothDamp_m3493216268 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, Vector3_t2243707580 * p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m2942701431 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Time::get_frameCount()
extern "C"  int32_t Time_get_frameCount_m3403277510 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.GC::Collect()
extern "C"  void GC_Collect_m2249328497 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Player2D::GetMaxHealth()
extern "C"  int32_t Player2D_GetMaxHealth_m1138988990 (Player2D_t165353355 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m3299155069 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Player2D::GetHealth()
extern "C"  int32_t Player2D_GetHealth_m2808744642 (Player2D_t165353355 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Image>()
#define GameObject_GetComponent_TisImage_t2042527209_m4162535761(__this, method) ((  Image_t2042527209 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// System.Void UnityEngine.UI.Image::set_sprite(UnityEngine.Sprite)
extern "C"  void Image_set_sprite_m1800056820 (Image_t2042527209 * __this, Sprite_t309593783 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Input2D::.ctor()
extern "C"  void Input2D__ctor_m431867499 (Input2D_t2938435410 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
#define Component_GetComponent_TisImage_t2042527209_m2189462422(__this, method) ((  Image_t2042527209 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2724124387_gshared)(__this, method)
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C"  Transform_t3275118058 * Transform_GetChild_m3136145191 (Transform_t3275118058 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C"  Vector2_t2243707579  Vector2_get_zero_m1210615473 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform UnityEngine.UI.Graphic::get_rectTransform()
extern "C"  RectTransform_t3349966182 * Graphic_get_rectTransform_m2697395074 (Graphic_t2426225576 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.EventSystems.PointerEventData::get_position()
extern "C"  Vector2_t2243707579  PointerEventData_get_position_m2131765015 (PointerEventData_t1599784723 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.EventSystems.PointerEventData::get_pressEventCamera()
extern "C"  Camera_t189460977 * PointerEventData_get_pressEventCamera_m724559964 (PointerEventData_t1599784723 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToLocalPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector2&)
extern "C"  bool RectTransformUtility_ScreenPointToLocalPointInRectangle_m1119989730 (RuntimeObject * __this /* static, unused */, RectTransform_t3349966182 * p0, Vector2_t2243707579  p1, Camera_t189460977 * p2, Vector2_t2243707579 * p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
extern "C"  Vector2_t2243707579  RectTransform_get_sizeDelta_m2505406265 (RectTransform_t3349966182 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_pivot()
extern "C"  Vector2_t2243707579  RectTransform_get_pivot_m3474500578 (RectTransform_t3349966182 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::get_magnitude()
extern "C"  float Vector3_get_magnitude_m4021906415 (Vector3_t2243707580 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern "C"  Vector3_t2243707580  Vector3_get_normalized_m1057036856 (Vector3_t2243707580 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single)
extern "C"  void Vector3__ctor_m2761639014 (Vector3_t2243707580 * __this, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector2_t2243707579  Vector2_op_Implicit_m385881926 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchoredPosition_m4223859262 (RectTransform_t3349966182 * __this, Vector2_t2243707579  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void JoyStick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void JoyStick_OnDrag_m1637494730 (JoyStick_t513152754 * __this, PointerEventData_t1599784723 * ___eventData0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Input::GetAxisRaw(System.String)
extern "C"  float Input_GetAxisRaw_m1913129537 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetButtonDown(System.String)
extern "C"  bool Input_GetButtonDown_m717298472 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetButtonUp(System.String)
extern "C"  bool Input_GetButtonUp_m3002013723 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<LoadTransition>()
#define GameObject_GetComponent_TisLoadTransition_t4049825803_m3996224546(__this, method) ((  LoadTransition_t4049825803 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// System.Collections.IEnumerator LoadScene::LoadSceneWithTransition(System.Int32,LoadTransition)
extern "C"  RuntimeObject* LoadScene_LoadSceneWithTransition_m748142228 (LoadScene_t1133380052 * __this, int32_t ___index0, LoadTransition_t4049825803 * ___transition1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t2299508840 * MonoBehaviour_StartCoroutine_m2678710497 (MonoBehaviour_t1158329972 * __this, RuntimeObject* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32)
extern "C"  void SceneManager_LoadScene_m1620702025 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void LoadScene/<LoadSceneWithTransition>c__Iterator0::.ctor()
extern "C"  void U3CLoadSceneWithTransitionU3Ec__Iterator0__ctor_m2803670358 (U3CLoadSceneWithTransitionU3Ec__Iterator0_t1207375911 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application::Quit()
extern "C"  void Application_Quit_m3096163801 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single LoadTransition::BeginFade(System.Int32)
extern "C"  float LoadTransition_BeginFade_m3778697406 (LoadTransition_t4049825803 * __this, int32_t ___direction0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C"  void WaitForSeconds__ctor_m632080402 (WaitForSeconds_t3839502067 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m3232764727 (NotSupportedException_t1793819818 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m3925508629 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
extern "C"  float Mathf_Clamp01_m1777088257 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.GUI::get_color()
extern "C"  Color_t2020392075  GUI_get_color_m3388074834 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m2736804035 (Color_t2020392075 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_color(UnityEngine.Color)
extern "C"  void GUI_set_color_m1289920875 (RuntimeObject * __this /* static, unused */, Color_t2020392075  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_depth(System.Int32)
extern "C"  void GUI_set_depth_m314110018 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m3526633787 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_height()
extern "C"  int32_t Screen_get_height_m3831644396 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m2109726426 (Rect_t3681755626 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::DrawTexture(UnityEngine.Rect,UnityEngine.Texture)
extern "C"  void GUI_DrawTexture_m178176081 (RuntimeObject * __this /* static, unused */, Rect_t3681755626  p0, Texture_t2243626319 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::.ctor()
#define List_1__ctor_m704351054(__this, method) ((  void (*) (List_1_t1125654279 *, const RuntimeMethod*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)
extern "C"  void MonoBehaviour_InvokeRepeating_m505769937 (MonoBehaviour_t1158329972 * __this, String_t* p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.LayerMask::NameToLayer(System.String)
extern "C"  int32_t LayerMask_NameToLayer_m3213714080 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::IgnoreLayerCollision(System.Int32,System.Int32,System.Boolean)
extern "C"  void Physics2D_IgnoreLayerCollision_m1690433947 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, bool p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void MacroGenerator::CreateLevel(System.Boolean)
extern "C"  void MacroGenerator_CreateLevel_m2116947579 (MacroGenerator_t1652377675 * __this, bool ___base_level0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Monitor::Enter(System.Object)
extern "C"  void Monitor_Enter_m2136705809 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Count()
#define List_1_get_Count_m2764296230(__this, method) ((  int32_t (*) (List_1_t1125654279 *, const RuntimeMethod*))List_1_get_Count_m2375293942_gshared)(__this, method)
// !0 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Item(System.Int32)
#define List_1_get_Item_m939767277(__this, p0, method) ((  GameObject_t1756533147 * (*) (List_1_t1125654279 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2062981835_gshared)(__this, p0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GameObject>::Remove(!0)
#define List_1_Remove_m2287078133(__this, p0, method) ((  bool (*) (List_1_t1125654279 *, GameObject_t1756533147 *, const RuntimeMethod*))List_1_Remove_m3164383811_gshared)(__this, p0, method)
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m3959286051 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Monitor::Exit(System.Object)
extern "C"  void Monitor_Exit_m2677760297 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object,System.Object)
extern "C"  String_t* String_Concat_m2000667605 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::.ctor(System.String)
extern "C"  void GameObject__ctor_m4283735819 (GameObject_t1756533147 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<MicroGenerator>()
#define GameObject_AddComponent_TisMicroGenerator_t2658307363_m1900695421(__this, method) ((  MicroGenerator_t2658307363 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3813873105_gshared)(__this, method)
// System.Void MicroGenerator::set_blocks_in_level(System.Int32)
extern "C"  void MicroGenerator_set_blocks_in_level_m2818797397 (MicroGenerator_t2658307363 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void MicroGenerator::set_difficulty(System.Single)
extern "C"  void MicroGenerator_set_difficulty_m4066586773 (MicroGenerator_t2658307363 * __this, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 MicroGenerator::CreateLevel(System.Single,System.Single,UnityEngine.Vector2,System.Boolean)
extern "C"  Vector2_t2243707579  MicroGenerator_CreateLevel_m1130512212 (MicroGenerator_t2658307363 * __this, float ___height0, float ___width1, Vector2_t2243707579  ___last_platform2, bool ___base_level3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Clamp_m1779415170 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void MacroGenerator::ActivateShip(System.Single)
extern "C"  void MacroGenerator_ActivateShip_m2260158446 (MacroGenerator_t1652377675 * __this, float ___duration0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::Add(!0)
#define List_1_Add_m3441471442(__this, p0, method) ((  void (*) (List_1_t1125654279 *, GameObject_t1756533147 *, const RuntimeMethod*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<Ship>()
#define GameObject_GetComponent_TisShip_t1116303770_m1555807537(__this, method) ((  Ship_t1116303770 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// System.Void Ship::set_active(System.Boolean)
extern "C"  void Ship_set_active_m3844682543 (Ship_t1116303770 * __this, bool ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<PlatformPool>()
#define GameObject_GetComponent_TisPlatformPool_t3559850805_m831201488(__this, method) ((  PlatformPool_t3559850805 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::Clear()
#define List_1_Clear_m4030601119(__this, method) ((  void (*) (List_1_t1125654279 *, const RuntimeMethod*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.Int32 MicroGenerator::get_blocks_in_level()
extern "C"  int32_t MicroGenerator_get_blocks_in_level_m3243240612 (MicroGenerator_t2658307363 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 PlatformPool::get_left_tile_size()
extern "C"  Vector2_t2243707579  PlatformPool_get_left_tile_size_m2896263910 (PlatformPool_t3559850805 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject PlatformGenerator::GeneratePlatform(System.String,UnityEngine.Vector2,System.Single)
extern "C"  GameObject_t1756533147 * PlatformGenerator_GeneratePlatform_m470059935 (RuntimeObject * __this /* static, unused */, String_t* ___name0, Vector2_t2243707579  ___pos1, float ___width2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
extern "C"  void Transform_set_parent_m3178143156 (Transform_t3275118058 * __this, Transform_t3275118058 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
extern "C"  float Random_Range_m4291245934 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<PlatformGenerator>()
#define GameObject_GetComponent_TisPlatformGenerator_t395172572_m3551609875(__this, method) ((  PlatformGenerator_t395172572 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// UnityEngine.Bounds Controller2D::GetBounds()
extern "C"  Bounds_t3033363703  Controller2D_GetBounds_m1748675743 (Controller2D_t802485922 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 PlatformPool::get_mid_tile_size()
extern "C"  Vector2_t2243707579  PlatformPool_get_mid_tile_size_m1657243535 (PlatformPool_t3559850805 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlatformGenerator::get_UsedTiles()
extern "C"  int32_t PlatformGenerator_get_UsedTiles_m3574650352 (PlatformGenerator_t395172572 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Controller2D::GetMaxJumpHeight()
extern "C"  float Controller2D_GetMaxJumpHeight_m1470428714 (Controller2D_t802485922 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Controller2D::GetMinJumpHeight()
extern "C"  float Controller2D_GetMinJumpHeight_m1114922044 (Controller2D_t802485922 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single MicroGenerator::get_difficulty()
extern "C"  float MicroGenerator_get_difficulty_m1319765580 (MicroGenerator_t2658307363 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Controller2D::GetMoveSpeed()
extern "C"  float Controller2D_GetMoveSpeed_m1351954791 (Controller2D_t802485922 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Sign(System.Single)
extern "C"  float Mathf_Sign_m3240469006 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.SpriteRenderer>()
#define GameObject_GetComponentsInChildren_TisSpriteRenderer_t1209076198_m2671756693(__this, method) ((  SpriteRendererU5BU5D_t1098056643* (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponentsInChildren_TisRuntimeObject_m1467294482_gshared)(__this, method)
// UnityEngine.Color UnityEngine.SpriteRenderer::get_color()
extern "C"  Color_t2020392075  SpriteRenderer_get_color_m4269640491 (SpriteRenderer_t1209076198 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m3831565866 (Color_t2020392075 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpriteRenderer::set_color(UnityEngine.Color)
extern "C"  void SpriteRenderer_set_color_m675324012 (SpriteRenderer_t1209076198 * __this, Color_t2020392075  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<FallingPlatform>()
#define GameObject_AddComponent_TisFallingPlatform_t2590793300_m4057477800(__this, method) ((  FallingPlatform_t2590793300 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.BoxCollider2D>()
#define GameObject_AddComponent_TisBoxCollider2D_t948534547_m2942557550(__this, method) ((  BoxCollider2D_t948534547 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3813873105_gshared)(__this, method)
// UnityEngine.Bounds MicroGenerator::GetBounds()
extern "C"  Bounds_t3033363703  MicroGenerator_GetBounds_m2624662798 (MicroGenerator_t2658307363 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.BoxCollider2D::set_size(UnityEngine.Vector2)
extern "C"  void BoxCollider2D_set_size_m30428549 (BoxCollider2D_t948534547 * __this, Vector2_t2243707579  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_center()
extern "C"  Vector3_t2243707580  Bounds_get_center_m2113902127 (Bounds_t3033363703 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Collider2D::set_offset(UnityEngine.Vector2)
extern "C"  void Collider2D_set_offset_m3644752562 (Collider2D_t646061738 * __this, Vector2_t2243707579  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Collider2D::set_isTrigger(System.Boolean)
extern "C"  void Collider2D_set_isTrigger_m3424244684 (Collider2D_t646061738 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::set_layer(System.Int32)
extern "C"  void GameObject_set_layer_m3642810622 (GameObject_t1756533147 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.SpriteRenderer>()
#define Component_GetComponentsInChildren_TisSpriteRenderer_t1209076198_m1456864797(__this, method) ((  SpriteRendererU5BU5D_t1098056643* (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponentsInChildren_TisRuntimeObject_m3978412804_gshared)(__this, method)
// UnityEngine.Bounds UnityEngine.Renderer::get_bounds()
extern "C"  Bounds_t3033363703  Renderer_get_bounds_m2683206870 (Renderer_t257310565 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Bounds::Equals(System.Object)
extern "C"  bool Bounds_Equals_m497757122 (Bounds_t3033363703 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<MacroGenerator>()
#define GameObject_GetComponent_TisMacroGenerator_t1652377675_m1524168656(__this, method) ((  MacroGenerator_t1652377675 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// System.Void Threat::.ctor()
extern "C"  void Threat__ctor_m1337334961 (Threat_t2614656216 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Controller2D::Start()
extern "C"  void Controller2D_Start_m774250573 (Controller2D_t802485922 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_up()
extern "C"  Vector3_t2243707580  Transform_get_up_m1990654328 (Transform_t3275118058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  Vector3_op_Multiply_m2498445460 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Collider2D>()
#define Component_GetComponent_TisCollider2D_t646061738_m2950827934(__this, method) ((  Collider2D_t646061738 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2724124387_gshared)(__this, method)
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C"  void Behaviour_set_enabled_m602406666 (Behaviour_t955675639 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
#define Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(__this, method) ((  SpriteRenderer_t1209076198 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2724124387_gshared)(__this, method)
// System.Void UnityEngine.SpriteRenderer::set_sprite(UnityEngine.Sprite)
extern "C"  void SpriteRenderer_set_sprite_m3307887080 (SpriteRenderer_t1209076198 * __this, Sprite_t309593783 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.ParticleSystem>()
#define Component_GetComponentInChildren_TisParticleSystem_t3394631041_m347610273(__this, method) ((  ParticleSystem_t3394631041 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponentInChildren_TisRuntimeObject_m2461586036_gshared)(__this, method)
// System.Void Threat::Hit(UnityEngine.GameObject)
extern "C"  void Threat_Hit_m4230409352 (Threat_t2614656216 * __this, GameObject_t1756533147 * ___possible_player0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern "C"  void Object_Destroy_m682534386 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_max()
extern "C"  Vector3_t2243707580  Bounds_get_max_m1939845278 (Bounds_t3033363703 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_black()
extern "C"  Color_t2020392075  Color_get_black_m455352838 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color)
extern "C"  void Debug_DrawLine_m1261213347 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, Color_t2020392075  p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_green()
extern "C"  Color_t2020392075  Color_get_green_m3094897666 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 PlatformPool::get_right_tile_size()
extern "C"  Vector2_t2243707579  PlatformPool_get_right_tile_size_m2644186623 (PlatformPool_t3559850805 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformGenerator::set_UsedTiles(System.Int32)
extern "C"  void PlatformGenerator_set_UsedTiles_m4242393899 (PlatformGenerator_t395172572 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject PlatformPool::GetTileWithType(UnityEngine.GameObject,PlatformType)
extern "C"  GameObject_t1756533147 * PlatformPool_GetTileWithType_m1945733132 (PlatformPool_t3559850805 * __this, GameObject_t1756533147 * ___parent0, int32_t ___type1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m3768854296 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C"  void Transform_set_localPosition_m1073050816 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C"  void Debug_LogWarning_m1382493163 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.SpriteRenderer>()
#define GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(__this, method) ((  SpriteRenderer_t1209076198 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// System.Void PlatformGenerator::ReturnTiles()
extern "C"  void PlatformGenerator_ReturnTiles_m2688380672 (PlatformGenerator_t395172572 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformPool::ReturnTile(UnityEngine.GameObject)
extern "C"  void PlatformPool_ReturnTile_m1498587706 (PlatformPool_t3559850805 * __this, GameObject_t1756533147 * ___tile0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<PlatformGenerator>()
#define GameObject_AddComponent_TisPlatformGenerator_t395172572_m1088084192(__this, method) ((  PlatformGenerator_t395172572 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3813873105_gshared)(__this, method)
// System.Void PlatformGenerator::Create()
extern "C"  void PlatformGenerator_Create_m875964463 (PlatformGenerator_t395172572 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds PlatformGenerator::GetBounds()
extern "C"  Bounds_t3033363703  PlatformGenerator_GetBounds_m3188841413 (PlatformGenerator_t395172572 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<OneWayPlatform>()
#define GameObject_AddComponent_TisOneWayPlatform_t3777955472_m2849326318(__this, method) ((  OneWayPlatform_t3777955472 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3813873105_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::.ctor(System.Int32)
#define List_1__ctor_m4211463620(__this, p0, method) ((  void (*) (List_1_t1125654279 *, int32_t, const RuntimeMethod*))List_1__ctor_m136460305_gshared)(__this, p0, method)
// System.Single PlatformPool::WidthOf(UnityEngine.GameObject)
extern "C"  float PlatformPool_WidthOf_m3300936485 (RuntimeObject * __this /* static, unused */, GameObject_t1756533147 * ___obj0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single PlatformPool::HeightOf(UnityEngine.GameObject)
extern "C"  float PlatformPool_HeightOf_m2336189100 (RuntimeObject * __this /* static, unused */, GameObject_t1756533147 * ___obj0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformPool::set_left_tile_size(UnityEngine.Vector2)
extern "C"  void PlatformPool_set_left_tile_size_m2354458551 (PlatformPool_t3559850805 * __this, Vector2_t2243707579  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformPool::set_mid_tile_size(UnityEngine.Vector2)
extern "C"  void PlatformPool_set_mid_tile_size_m2925512842 (PlatformPool_t3559850805 * __this, Vector2_t2243707579  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformPool::set_right_tile_size(UnityEngine.Vector2)
extern "C"  void PlatformPool_set_right_tile_size_m1029267146 (PlatformPool_t3559850805 * __this, Vector2_t2243707579  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.RuntimeFieldHandle)
extern "C"  void RuntimeHelpers_InitializeArray_m3920580167 (RuntimeObject * __this /* static, unused */, RuntimeArray * p0, RuntimeFieldHandle_t2331729674  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C"  void GameObject_SetActive_m2693135142 (GameObject_t1756533147 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mathf::Max(System.Int32[])
extern "C"  int32_t Mathf_Max_m1012832217 (RuntimeObject * __this /* static, unused */, Int32U5BU5D_t3030399641* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C"  Quaternion_t4030073918  Quaternion_get_identity_m443011477 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_localRotation_m3053695533 (Transform_t3275118058 * __this, Quaternion_t4030073918  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.GameObject> PlatformPool::GetPoolForType(PlatformType)
extern "C"  List_1_t1125654279 * PlatformPool_GetPoolForType_m1350965415 (PlatformPool_t3559850805 * __this, int32_t ___type0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::set_Item(System.Int32,!0)
#define List_1_set_Item_m1811192197(__this, p0, p1, method) ((  void (*) (List_1_t1125654279 *, int32_t, GameObject_t1756533147 *, const RuntimeMethod*))List_1_set_Item_m4246197648_gshared)(__this, p0, p1, method)
// System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
extern "C"  bool GameObject_get_activeInHierarchy_m2532098784 (GameObject_t1756533147 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformPool::ActivateTile(UnityEngine.GameObject,UnityEngine.GameObject)
extern "C"  void PlatformPool_ActivateTile_m4049973885 (PlatformPool_t3559850805 * __this, GameObject_t1756533147 * ___parent0, GameObject_t1756533147 * ___tile1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Controller2D::.ctor()
extern "C"  void Controller2D__ctor_m1972723229 (Controller2D_t802485922 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Player2D::set_current_score(System.Int32)
extern "C"  void Player2D_set_current_score_m4026685404 (Player2D_t165353355 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Player2D::SpawnEffectFade()
extern "C"  RuntimeObject* Player2D_SpawnEffectFade_m1418828654 (Player2D_t165353355 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Player2D/<SpawnEffectFade>c__Iterator0::.ctor()
extern "C"  void U3CSpawnEffectFadeU3Ec__Iterator0__ctor_m3178076772 (U3CSpawnEffectFadeU3Ec__Iterator0_t2652861317 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Controller2D::Direction()
extern "C"  bool Controller2D_Direction_m18427550 (Controller2D_t802485922 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern "C"  Quaternion_t4030073918  Quaternion_Euler_m868989838 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Controller2D::Move(Controller2D/MoveMSG)
extern "C"  void Controller2D_Move_m3918239025 (Controller2D_t802485922 * __this, MoveMSG_t2690495395  ___move0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Player2D::HandleScore()
extern "C"  void Player2D_HandleScore_m3074900822 (Player2D_t165353355 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Player2D/<Flicker>c__Iterator1::.ctor()
extern "C"  void U3CFlickerU3Ec__Iterator1__ctor_m615044603 (U3CFlickerU3Ec__Iterator1_t3991061274 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Player2D::EndGame()
extern "C"  void Player2D_EndGame_m4133746719 (Player2D_t165353355 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Player2D::Flicker(System.Single)
extern "C"  RuntimeObject* Player2D_Flicker_m4174058381 (Player2D_t165353355 * __this, float ___flickerRate0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::IgnoreLayerCollision(System.Int32,System.Int32)
extern "C"  void Physics2D_IgnoreLayerCollision_m1669703862 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(UnityEngine.Coroutine)
extern "C"  void MonoBehaviour_StopCoroutine_m606332095 (MonoBehaviour_t1158329972 * __this, Coroutine_t2299508840 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t4030073918  Transform_get_rotation_m2617026815 (Transform_t3275118058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Slerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t4030073918  Quaternion_Slerp_m3187037907 (RuntimeObject * __this /* static, unused */, Quaternion_t4030073918  p0, Quaternion_t4030073918  p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_rotation_m2824446320 (Transform_t3275118058 * __this, Quaternion_t4030073918  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Player2D::get_current_score()
extern "C"  int32_t Player2D_get_current_score_m3494890519 (Player2D_t165353355 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<LoadScene>()
#define GameObject_GetComponent_TisLoadScene_t1133380052_m1576248279(__this, method) ((  LoadScene_t1133380052 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// System.Void LoadScene::LoadSceneWithIndex(System.Int32)
extern "C"  void LoadScene_LoadSceneWithIndex_m216160262 (LoadScene_t1133380052 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::Lerp(UnityEngine.Color,UnityEngine.Color,System.Single)
extern "C"  Color_t2020392075  Color_Lerp_m527444458 (RuntimeObject * __this /* static, unused */, Color_t2020392075  p0, Color_t2020392075  p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.ParticleSystem>()
#define GameObject_GetComponent_TisParticleSystem_t3394631041_m1329366749(__this, method) ((  ParticleSystem_t3394631041 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// UnityEngine.ParticleSystem/MainModule UnityEngine.ParticleSystem::get_main()
extern "C"  MainModule_t6751348  ParticleSystem_get_main_m2275307502 (ParticleSystem_t3394631041 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/MinMaxGradient UnityEngine.ParticleSystem/MainModule::get_startColor()
extern "C"  MinMaxGradient_t3708298175  MainModule_get_startColor_m642758877 (MainModule_t6751348 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.ParticleSystem/MinMaxGradient::get_color()
extern "C"  Color_t2020392075  MinMaxGradient_get_color_m1448527895 (MinMaxGradient_t3708298175 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MinMaxGradient::.ctor(UnityEngine.Color)
extern "C"  void MinMaxGradient__ctor_m2960432810 (MinMaxGradient_t3708298175 * __this, Color_t2020392075  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::set_startColor(UnityEngine.ParticleSystem/MinMaxGradient)
extern "C"  void MainModule_set_startColor_m3844897932 (MainModule_t6751348 * __this, MinMaxGradient_t3708298175  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C"  void Object_DontDestroyOnLoad_m2658633409 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Text>()
#define Component_GetComponent_TisText_t356221433_m1342661039(__this, method) ((  Text_t356221433 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2724124387_gshared)(__this, method)
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m56707527 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_right()
extern "C"  Vector3_t2243707580  Vector3_get_right_m1958771095 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Ship/<Fade>c__Iterator0::.ctor()
extern "C"  void U3CFadeU3Ec__Iterator0__ctor_m49909007 (U3CFadeU3Ec__Iterator0_t3715132008 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Ship::get_active()
extern "C"  bool Ship_get_active_m1657109792 (Ship_t1116303770 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Behaviour::get_enabled()
extern "C"  bool Behaviour_get_enabled_m646668963 (Behaviour_t955675639 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Ship::Fade(System.Int32)
extern "C"  RuntimeObject* Ship_Fade_m2975741434 (Ship_t1116303770 * __this, int32_t ___dir0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.RectTransform>()
#define Component_GetComponent_TisRectTransform_t3349966182_m1310250299(__this, method) ((  RectTransform_t3349966182 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2724124387_gshared)(__this, method)
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C"  Transform_t3275118058 * Transform_get_parent_m2752514051 (Transform_t3275118058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::GetWorldCorners(UnityEngine.Vector3[])
extern "C"  void RectTransform_GetWorldCorners_m3853164225 (RectTransform_t3349966182 * __this, Vector3U5BU5D_t1172311765* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Transform_InverseTransformPoint_m2749799913 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
extern "C"  Rect_t3681755626  RectTransform_get_rect_m2724092203 (RectTransform_t3349966182 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3)
extern "C"  bool Rect_Contains_m846865666 (Rect_t3681755626 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern "C"  Vector3_t2243707580  Transform_get_localPosition_m2747128641 (Transform_t3275118058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Ship::FollowTarget()
extern "C"  void Ship_FollowTarget_m1654037237 (Ship_t1116303770 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C"  Vector3_t2243707580  Vector3_get_forward_m1316319684 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::AngleAxis(System.Single,UnityEngine.Vector3)
extern "C"  Quaternion_t4030073918  Quaternion_AngleAxis_m1602210320 (RuntimeObject * __this /* static, unused */, float p0, Vector3_t2243707580  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Ship::Fire()
extern "C"  void Ship_Fire_m2857528497 (Ship_t1116303770 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
#define Object_Instantiate_TisGameObject_t1756533147_m3064851704(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (RuntimeObject * /* static, unused */, GameObject_t1756533147 *, Vector3_t2243707580 , Quaternion_t4030073918 , const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m3829784634_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UnityEngine.GameObject[] UnityEngine.GameObject::FindGameObjectsWithTag(System.String)
extern "C"  GameObjectU5BU5D_t3057952154* GameObject_FindGameObjectsWithTag_m3720927271 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object)
extern "C"  void GameObject_SendMessage_m2475750736 (GameObject_t1756533147 * __this, String_t* p0, RuntimeObject * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Controller2D::.ctor()
extern "C"  void Controller2D__ctor_m1972723229 (Controller2D_t802485922 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Controller2D__ctor_m1972723229_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_move_speed_2((6.0f));
		__this->set_acceleration_air_time_3((0.1f));
		__this->set_acceleration_ground_time_4((0.1f));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(PolygonCollider2D_t3220183178_0_0_0_var), /*hidden argument*/NULL);
		__this->set_ColliderType_5(L_0);
		__this->set_facing_right_16((bool)1);
		__this->set_apply_gravity_18((bool)1);
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Controller2D::Start()
extern "C"  void Controller2D_Start_m774250573 (Controller2D_t802485922 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Controller2D_Start_m774250573_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Rigidbody2D_t502193897 * L_0 = Component_GetComponent_TisRigidbody2D_t502193897_m3702757851(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t502193897_m3702757851_RuntimeMethod_var);
		__this->set_rigidbody_17(L_0);
		Collider2D_t646061738 * L_1 = __this->get_collider_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m2516226135(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0034;
		}
	}
	{
		Type_t * L_3 = __this->get_ColliderType_5();
		Component_t3819376471 * L_4 = Component_GetComponent_m1409092944(__this, L_3, /*hidden argument*/NULL);
		__this->set_collider_13(((Collider2D_t646061738 *)IsInstClass((RuntimeObject*)L_4, Collider2D_t646061738_il2cpp_TypeInfo_var)));
	}

IL_0034:
	{
		StringU5BU5D_t1642385972* L_5 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, _stringLiteral1086556859);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1086556859);
		int32_t L_6 = LayerMask_GetMask_m4167990913(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		LayerMask_t3188175821  L_7 = LayerMask_op_Implicit_m1796475436(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->set_platform_layer_mask_14(L_7);
		return;
	}
}
// System.Void Controller2D::Move(Controller2D/MoveMSG)
extern "C"  void Controller2D_Move_m3918239025 (Controller2D_t802485922 * __this, MoveMSG_t2690495395  ___move0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Controller2D_Move_m3918239025_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	float* G_B2_0 = NULL;
	float G_B2_1 = 0.0f;
	float G_B2_2 = 0.0f;
	Vector2_t2243707579 * G_B2_3 = NULL;
	float* G_B1_0 = NULL;
	float G_B1_1 = 0.0f;
	float G_B1_2 = 0.0f;
	Vector2_t2243707579 * G_B1_3 = NULL;
	float G_B3_0 = 0.0f;
	float* G_B3_1 = NULL;
	float G_B3_2 = 0.0f;
	float G_B3_3 = 0.0f;
	Vector2_t2243707579 * G_B3_4 = NULL;
	{
		Rigidbody2D_t502193897 * L_0 = __this->get_rigidbody_17();
		NullCheck(L_0);
		Vector2_t2243707579  L_1 = Rigidbody2D_get_velocity_m1631921214(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_x_0();
		float L_3 = (&___move0)->get_horizontal_0();
		float L_4 = __this->get_move_speed_2();
		float* L_5 = __this->get_address_of_velocity_x_smoothing_15();
		bool L_6 = Controller2D_IsGrounded_m2912631519(__this, /*hidden argument*/NULL);
		G_B1_0 = L_5;
		G_B1_1 = ((float)((float)L_3*(float)L_4));
		G_B1_2 = L_2;
		G_B1_3 = (&V_0);
		if (!L_6)
		{
			G_B2_0 = L_5;
			G_B2_1 = ((float)((float)L_3*(float)L_4));
			G_B2_2 = L_2;
			G_B2_3 = (&V_0);
			goto IL_003f;
		}
	}
	{
		float L_7 = __this->get_acceleration_ground_time_4();
		G_B3_0 = L_7;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		goto IL_0045;
	}

IL_003f:
	{
		float L_8 = __this->get_acceleration_air_time_3();
		G_B3_0 = L_8;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
	}

IL_0045:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_9 = Mathf_SmoothDamp_m3504509584(NULL /*static, unused*/, G_B3_3, G_B3_2, G_B3_1, G_B3_0, /*hidden argument*/NULL);
		G_B3_4->set_x_0(L_9);
		float L_10 = (&___move0)->get_horizontal_0();
		if ((!(((float)L_10) > ((float)(0.0f)))))
		{
			goto IL_0076;
		}
	}
	{
		bool L_11 = __this->get_facing_right_16();
		if (L_11)
		{
			goto IL_0076;
		}
	}
	{
		Controller2D_Flip_m689078882(__this, /*hidden argument*/NULL);
		goto IL_0098;
	}

IL_0076:
	{
		float L_12 = (&___move0)->get_horizontal_0();
		if ((!(((float)L_12) < ((float)(0.0f)))))
		{
			goto IL_0098;
		}
	}
	{
		bool L_13 = __this->get_facing_right_16();
		if (!L_13)
		{
			goto IL_0098;
		}
	}
	{
		Controller2D_Flip_m689078882(__this, /*hidden argument*/NULL);
	}

IL_0098:
	{
		int32_t L_14 = (&___move0)->get_jump_1();
		V_1 = L_14;
		int32_t L_15 = V_1;
		switch (L_15)
		{
			case 0:
			{
				goto IL_00b7;
			}
			case 1:
			{
				goto IL_00c9;
			}
			case 2:
			{
				goto IL_00db;
			}
		}
	}
	{
		goto IL_00ed;
	}

IL_00b7:
	{
		float L_16 = Controller2D_MaxJumpVelocity_m2382483678(__this, /*hidden argument*/NULL);
		(&V_0)->set_y_1(L_16);
		goto IL_00f2;
	}

IL_00c9:
	{
		float L_17 = Controller2D_MinJumpVelocity_m4136881028(__this, /*hidden argument*/NULL);
		(&V_0)->set_y_1(L_17);
		goto IL_00f2;
	}

IL_00db:
	{
		float L_18 = Controller2D_DoubleJumpVelocity_m2506104035(__this, /*hidden argument*/NULL);
		(&V_0)->set_y_1(L_18);
		goto IL_00f2;
	}

IL_00ed:
	{
		goto IL_00f2;
	}

IL_00f2:
	{
		Rigidbody2D_t502193897 * L_19 = __this->get_rigidbody_17();
		Vector2_t2243707579  L_20 = V_0;
		NullCheck(L_19);
		Rigidbody2D_set_velocity_m2161463521(L_19, L_20, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Controller2D::FixedUpdate()
extern "C"  void Controller2D_FixedUpdate_m1824218694 (Controller2D_t802485922 * __this, const RuntimeMethod* method)
{
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Rigidbody2D_t502193897 * L_0 = __this->get_rigidbody_17();
		NullCheck(L_0);
		int32_t L_1 = Rigidbody2D_get_bodyType_m512807397(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) == ((int32_t)2)))
		{
			goto IL_0070;
		}
	}
	{
		Rigidbody2D_t502193897 * L_2 = __this->get_rigidbody_17();
		NullCheck(L_2);
		int32_t L_3 = Rigidbody2D_get_bodyType_m512807397(L_2, /*hidden argument*/NULL);
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0070;
		}
	}
	{
		bool L_4 = __this->get_apply_gravity_18();
		if (!L_4)
		{
			goto IL_0070;
		}
	}
	{
		Rigidbody2D_t502193897 * L_5 = __this->get_rigidbody_17();
		Rigidbody2D_t502193897 * L_6 = __this->get_rigidbody_17();
		NullCheck(L_6);
		Vector2_t2243707579  L_7 = Rigidbody2D_get_velocity_m1631921214(L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		float L_8 = (&V_0)->get_x_0();
		Rigidbody2D_t502193897 * L_9 = __this->get_rigidbody_17();
		NullCheck(L_9);
		Vector2_t2243707579  L_10 = Rigidbody2D_get_velocity_m1631921214(L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		float L_11 = (&V_1)->get_y_1();
		float L_12 = Controller2D_Gravity_m418077541(__this, /*hidden argument*/NULL);
		float L_13 = Time_get_fixedDeltaTime_m2740561775(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector2__ctor_m847450945((&L_14), L_8, ((float)((float)L_11+(float)((float)((float)L_12*(float)L_13)))), /*hidden argument*/NULL);
		NullCheck(L_5);
		Rigidbody2D_set_velocity_m2161463521(L_5, L_14, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return;
	}
}
// System.Boolean Controller2D::Direction()
extern "C"  bool Controller2D_Direction_m18427550 (Controller2D_t802485922 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_facing_right_16();
		return L_0;
	}
}
// System.Void Controller2D::Flip()
extern "C"  void Controller2D_Flip_m689078882 (Controller2D_t802485922 * __this, const RuntimeMethod* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = __this->get_facing_right_16();
		__this->set_facing_right_16((bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0));
		Transform_t3275118058 * L_1 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_localScale_m46214814(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t2243707580 * L_3 = (&V_0);
		float L_4 = L_3->get_x_1();
		L_3->set_x_1(((float)((float)L_4*(float)(-1.0f))));
		Transform_t3275118058 * L_5 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_6 = V_0;
		NullCheck(L_5);
		Transform_set_localScale_m1442831667(L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Controller2D::IsGrounded()
extern "C"  bool Controller2D_IsGrounded_m2912631519 (Controller2D_t802485922 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Controller2D_IsGrounded_m2912631519_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Bounds_t3033363703  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t V_4 = 0;
	Bounds_t3033363703  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t2243707580  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector2_t2243707579  V_7;
	memset(&V_7, 0, sizeof(V_7));
	int32_t V_8 = 0;
	Vector2_t2243707579  V_9;
	memset(&V_9, 0, sizeof(V_9));
	RaycastHit2D_t4063908774  V_10;
	memset(&V_10, 0, sizeof(V_10));
	{
		Collider2D_t646061738 * L_0 = __this->get_collider_13();
		NullCheck(L_0);
		Bounds_t3033363703  L_1 = Collider2D_get_bounds_m242690441(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Vector3_t2243707580  L_2 = Bounds_get_min_m3351470040((&V_0), /*hidden argument*/NULL);
		V_2 = L_2;
		float L_3 = (&V_2)->get_x_1();
		Vector3_t2243707580  L_4 = Bounds_get_min_m3351470040((&V_0), /*hidden argument*/NULL);
		V_3 = L_4;
		float L_5 = (&V_3)->get_y_2();
		Vector2__ctor_m847450945((&V_1), L_3, L_5, /*hidden argument*/NULL);
		Collider2D_t646061738 * L_6 = __this->get_collider_13();
		NullCheck(L_6);
		Bounds_t3033363703  L_7 = Collider2D_get_bounds_m242690441(L_6, /*hidden argument*/NULL);
		V_5 = L_7;
		Vector3_t2243707580  L_8 = Bounds_get_size_m3951110137((&V_5), /*hidden argument*/NULL);
		V_6 = L_8;
		float L_9 = (&V_6)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_10 = Mathf_RoundToInt_m1833913145(NULL /*static, unused*/, ((float)((float)L_9/(float)(0.25f))), /*hidden argument*/NULL);
		V_4 = L_10;
		Rigidbody2D_t502193897 * L_11 = __this->get_rigidbody_17();
		NullCheck(L_11);
		Vector2_t2243707579  L_12 = Rigidbody2D_get_velocity_m1631921214(L_11, /*hidden argument*/NULL);
		V_7 = L_12;
		float L_13 = (&V_7)->get_y_1();
		if ((!(((float)L_13) <= ((float)(0.0f)))))
		{
			goto IL_010a;
		}
	}
	{
		V_8 = 0;
		goto IL_0101;
	}

IL_0081:
	{
		Vector2_t2243707579  L_14 = V_1;
		V_9 = L_14;
		Vector2_t2243707579  L_15 = V_9;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2243707579_il2cpp_TypeInfo_var);
		Vector2_t2243707579  L_16 = Vector2_get_right_m4262310679(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_17 = V_8;
		Vector2_t2243707579  L_18 = Vector2_op_Multiply_m3676538483(NULL /*static, unused*/, L_16, ((float)((float)(0.25f)*(float)(((float)((float)L_17))))), /*hidden argument*/NULL);
		Vector2_t2243707579  L_19 = Vector2_op_Addition_m4229571528(NULL /*static, unused*/, L_15, L_18, /*hidden argument*/NULL);
		V_9 = L_19;
		Vector2_t2243707579  L_20 = V_9;
		Vector2_t2243707579  L_21 = Vector2_get_up_m1721661564(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579  L_22 = Vector2_op_UnaryNegation_m1410369452(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		LayerMask_t3188175821  L_23 = __this->get_platform_layer_mask_14();
		int32_t L_24 = LayerMask_op_Implicit_m1604355880(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		RaycastHit2D_t4063908774  L_25 = Physics2D_Raycast_m2434840196(NULL /*static, unused*/, L_20, L_22, (0.4f), L_24, /*hidden argument*/NULL);
		V_10 = L_25;
		Vector2_t2243707579  L_26 = V_9;
		Vector3_t2243707580  L_27 = Vector2_op_Implicit_m129629632(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		Vector2_t2243707579  L_28 = Vector2_get_up_m1721661564(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579  L_29 = Vector2_op_UnaryNegation_m1410369452(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		Vector2_t2243707579  L_30 = Vector2_op_Multiply_m3676538483(NULL /*static, unused*/, L_29, (0.4f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_31 = Vector2_op_Implicit_m129629632(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		Color_t2020392075  L_32 = Color_get_red_m3374418718(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_DrawRay_m999504727(NULL /*static, unused*/, L_27, L_31, L_32, /*hidden argument*/NULL);
		RaycastHit2D_t4063908774  L_33 = V_10;
		bool L_34 = RaycastHit2D_op_Implicit_m3937142044(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_00fb;
		}
	}
	{
		return (bool)1;
	}

IL_00fb:
	{
		int32_t L_35 = V_8;
		V_8 = ((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_0101:
	{
		int32_t L_36 = V_8;
		int32_t L_37 = V_4;
		if ((((int32_t)L_36) < ((int32_t)L_37)))
		{
			goto IL_0081;
		}
	}

IL_010a:
	{
		return (bool)0;
	}
}
// System.Single Controller2D::Gravity()
extern "C"  float Controller2D_Gravity_m418077541 (Controller2D_t802485922 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Controller2D_Gravity_m418077541_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_0 = powf((0.3f), (2.0f));
		return ((float)((float)(-5.0f)/(float)L_0));
	}
}
// System.Single Controller2D::MaxJumpVelocity()
extern "C"  float Controller2D_MaxJumpVelocity_m2382483678 (Controller2D_t802485922 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Controller2D_MaxJumpVelocity_m2382483678_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = Controller2D_Gravity_m418077541(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_1 = fabsf(L_0);
		return ((float)((float)L_1*(float)(0.3f)));
	}
}
// System.Single Controller2D::DoubleJumpVelocity()
extern "C"  float Controller2D_DoubleJumpVelocity_m2506104035 (Controller2D_t802485922 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Controller2D_DoubleJumpVelocity_m2506104035_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = Controller2D_Gravity_m418077541(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_1 = fabsf(L_0);
		float L_2 = sqrtf(((float)((float)((float)((float)(2.0f)*(float)L_1))*(float)(1.5f))));
		return L_2;
	}
}
// System.Single Controller2D::MinJumpVelocity()
extern "C"  float Controller2D_MinJumpVelocity_m4136881028 (Controller2D_t802485922 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Controller2D_MinJumpVelocity_m4136881028_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = Controller2D_Gravity_m418077541(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_1 = fabsf(L_0);
		float L_2 = sqrtf(((float)((float)((float)((float)(2.0f)*(float)L_1))*(float)(1.0f))));
		return L_2;
	}
}
// System.Single Controller2D::GetMaxJumpHeight()
extern "C"  float Controller2D_GetMaxJumpHeight_m1470428714 (Controller2D_t802485922 * __this, const RuntimeMethod* method)
{
	{
		return (2.5f);
	}
}
// System.Single Controller2D::GetMinJumpHeight()
extern "C"  float Controller2D_GetMinJumpHeight_m1114922044 (Controller2D_t802485922 * __this, const RuntimeMethod* method)
{
	{
		return (1.0f);
	}
}
// System.Single Controller2D::GetJumpApexTime()
extern "C"  float Controller2D_GetJumpApexTime_m3129997450 (Controller2D_t802485922 * __this, const RuntimeMethod* method)
{
	{
		return (0.3f);
	}
}
// System.Single Controller2D::GetMoveSpeed()
extern "C"  float Controller2D_GetMoveSpeed_m1351954791 (Controller2D_t802485922 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_move_speed_2();
		return L_0;
	}
}
// UnityEngine.Bounds Controller2D::GetBounds()
extern "C"  Bounds_t3033363703  Controller2D_GetBounds_m1748675743 (Controller2D_t802485922 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Controller2D_GetBounds_m1748675743_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Bounds_t3033363703  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Collider2D_t646061738 * V_1 = NULL;
	Collider2DU5BU5D_t3535523695* V_2 = NULL;
	int32_t V_3 = 0;
	{
		Collider2D_t646061738 * L_0 = __this->get_collider_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m2516226135(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		Type_t * L_2 = __this->get_ColliderType_5();
		Component_t3819376471 * L_3 = Component_GetComponent_m1409092944(__this, L_2, /*hidden argument*/NULL);
		__this->set_collider_13(((Collider2D_t646061738 *)IsInstClass((RuntimeObject*)L_3, Collider2D_t646061738_il2cpp_TypeInfo_var)));
	}

IL_0028:
	{
		Collider2D_t646061738 * L_4 = __this->get_collider_13();
		NullCheck(L_4);
		Bounds_t3033363703  L_5 = Collider2D_get_bounds_m242690441(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		Collider2DU5BU5D_t3535523695* L_6 = Component_GetComponents_TisCollider2D_t646061738_m753859713(__this, /*hidden argument*/Component_GetComponents_TisCollider2D_t646061738_m753859713_RuntimeMethod_var);
		V_2 = L_6;
		V_3 = 0;
		goto IL_0068;
	}

IL_0042:
	{
		Collider2DU5BU5D_t3535523695* L_7 = V_2;
		int32_t L_8 = V_3;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Collider2D_t646061738 * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		V_1 = L_10;
		Bounds_t3033363703  L_11 = V_0;
		Collider2D_t646061738 * L_12 = V_1;
		NullCheck(L_12);
		Bounds_t3033363703  L_13 = Collider2D_get_bounds_m242690441(L_12, /*hidden argument*/NULL);
		bool L_14 = Bounds_op_Inequality_m1440807554(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0064;
		}
	}
	{
		Collider2D_t646061738 * L_15 = V_1;
		NullCheck(L_15);
		Bounds_t3033363703  L_16 = Collider2D_get_bounds_m242690441(L_15, /*hidden argument*/NULL);
		Bounds_Encapsulate_m2657927334((&V_0), L_16, /*hidden argument*/NULL);
	}

IL_0064:
	{
		int32_t L_17 = V_3;
		V_3 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0068:
	{
		int32_t L_18 = V_3;
		Collider2DU5BU5D_t3535523695* L_19 = V_2;
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_19)->max_length)))))))
		{
			goto IL_0042;
		}
	}
	{
		Bounds_t3033363703  L_20 = V_0;
		return L_20;
	}
}
// System.Void DecideInput::.ctor()
extern "C"  void DecideInput__ctor_m4252368003 (DecideInput_t109196168 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DecideInput::Awake()
extern "C"  void DecideInput_Awake_m4169716228 (DecideInput_t109196168 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DecideInput_Awake_m4169716228_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Player2D_t165353355 * V_0 = NULL;
	GameObject_t1756533147 * V_1 = NULL;
	GameObject_t1756533147 * V_2 = NULL;
	Canvas_t209405766 * V_3 = NULL;
	JoyStick_t513152754 * V_4 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Preferences_t60026258_il2cpp_TypeInfo_var);
		String_t* L_0 = ((Preferences_t60026258_StaticFields*)il2cpp_codegen_static_fields_for(Preferences_t60026258_il2cpp_TypeInfo_var))->get_player_tag_2();
		GameObject_t1756533147 * L_1 = GameObject_FindGameObjectWithTag_m1433464258(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Player2D_t165353355 * L_2 = GameObject_GetComponent_TisPlayer2D_t165353355_m1754251190(L_1, /*hidden argument*/GameObject_GetComponent_TisPlayer2D_t165353355_m1754251190_RuntimeMethod_var);
		V_0 = L_2;
		String_t* L_3 = ((Preferences_t60026258_StaticFields*)il2cpp_codegen_static_fields_for(Preferences_t60026258_il2cpp_TypeInfo_var))->get_control_HUD_path_4();
		Object_t1021602117 * L_4 = Resources_Load_m3480536692(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_1 = ((GameObject_t1756533147 *)IsInstSealed((RuntimeObject*)L_4, GameObject_t1756533147_il2cpp_TypeInfo_var));
		GameObject_t1756533147 * L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_6 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_5, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_RuntimeMethod_var);
		V_2 = L_6;
		GameObject_t1756533147 * L_7 = V_2;
		NullCheck(L_7);
		Canvas_t209405766 * L_8 = GameObject_GetComponent_TisCanvas_t209405766_m195193039(L_7, /*hidden argument*/GameObject_GetComponent_TisCanvas_t209405766_m195193039_RuntimeMethod_var);
		V_3 = L_8;
		Canvas_t209405766 * L_9 = V_3;
		NullCheck(L_9);
		Canvas_set_renderMode_m1632154567(L_9, 1, /*hidden argument*/NULL);
		Canvas_t209405766 * L_10 = V_3;
		Camera_t189460977 * L_11 = Camera_get_main_m881971336(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		Canvas_set_worldCamera_m2637367989(L_10, L_11, /*hidden argument*/NULL);
		Canvas_t209405766 * L_12 = V_3;
		NullCheck(L_12);
		Canvas_set_sortingLayerName_m3179341450(L_12, _stringLiteral884247417, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_13 = V_2;
		NullCheck(L_13);
		JoyStick_t513152754 * L_14 = GameObject_GetComponentInChildren_TisJoyStick_t513152754_m3063844157(L_13, /*hidden argument*/GameObject_GetComponentInChildren_TisJoyStick_t513152754_m3063844157_RuntimeMethod_var);
		V_4 = L_14;
		Player2D_t165353355 * L_15 = V_0;
		JoyStick_t513152754 * L_16 = V_4;
		NullCheck(L_15);
		Player2D_SetInput_m1210245776(L_15, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FallingPlatform::.ctor()
extern "C"  void FallingPlatform__ctor_m35378679 (FallingPlatform_t2590793300 * __this, const RuntimeMethod* method)
{
	{
		__this->set_timer_2((1.0f));
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FallingPlatform::OnCollisionStay2D(UnityEngine.Collision2D)
extern "C"  void FallingPlatform_OnCollisionStay2D_m3937589890 (FallingPlatform_t2590793300 * __this, Collision2D_t1539500754 * ___col0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FallingPlatform_OnCollisionStay2D_m3937589890_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ParticleSystemU5BU5D_t1490986844* V_0 = NULL;
	ParticleSystem_t3394631041 * V_1 = NULL;
	ParticleSystemU5BU5D_t1490986844* V_2 = NULL;
	int32_t V_3 = 0;
	{
		Collision2D_t1539500754 * L_0 = ___col0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Collision2D_get_gameObject_m1296200797(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = GameObject_get_tag_m3359901967(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Preferences_t60026258_il2cpp_TypeInfo_var);
		String_t* L_3 = ((Preferences_t60026258_StaticFields*)il2cpp_codegen_static_fields_for(Preferences_t60026258_il2cpp_TypeInfo_var))->get_player_tag_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0093;
		}
	}
	{
		bool L_5 = __this->get_sparks_flying_3();
		if (L_5)
		{
			goto IL_0082;
		}
	}
	{
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		ParticleSystemU5BU5D_t1490986844* L_7 = GameObject_GetComponentsInChildren_TisParticleSystem_t3394631041_m2199618894(L_6, /*hidden argument*/GameObject_GetComponentsInChildren_TisParticleSystem_t3394631041_m2199618894_RuntimeMethod_var);
		V_0 = L_7;
		ParticleSystemU5BU5D_t1490986844* L_8 = V_0;
		V_2 = L_8;
		V_3 = 0;
		goto IL_0072;
	}

IL_003a:
	{
		ParticleSystemU5BU5D_t1490986844* L_9 = V_2;
		int32_t L_10 = V_3;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		ParticleSystem_t3394631041 * L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		V_1 = L_12;
		ParticleSystem_t3394631041 * L_13 = V_1;
		NullCheck(L_13);
		String_t* L_14 = Component_get_tag_m124558427(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		bool L_15 = String_Equals_m2633592423(L_14, _stringLiteral4243273451, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_0068;
		}
	}
	{
		ParticleSystem_t3394631041 * L_16 = V_1;
		NullCheck(L_16);
		String_t* L_17 = Component_get_tag_m124558427(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		bool L_18 = String_Equals_m2633592423(L_17, _stringLiteral2566379072, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_006e;
		}
	}

IL_0068:
	{
		ParticleSystem_t3394631041 * L_19 = V_1;
		NullCheck(L_19);
		ParticleSystem_Play_m2510524101(L_19, /*hidden argument*/NULL);
	}

IL_006e:
	{
		int32_t L_20 = V_3;
		V_3 = ((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0072:
	{
		int32_t L_21 = V_3;
		ParticleSystemU5BU5D_t1490986844* L_22 = V_2;
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_22)->max_length)))))))
		{
			goto IL_003a;
		}
	}
	{
		__this->set_sparks_flying_3((bool)1);
	}

IL_0082:
	{
		float L_23 = __this->get_timer_2();
		MonoBehaviour_Invoke_m2201735803(__this, _stringLiteral1973320476, L_23, /*hidden argument*/NULL);
	}

IL_0093:
	{
		return;
	}
}
// System.Void FallingPlatform::DropPlatform()
extern "C"  void FallingPlatform_DropPlatform_m757953137 (FallingPlatform_t2590793300 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FallingPlatform_DropPlatform_m757953137_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ParticleSystemU5BU5D_t1490986844* V_0 = NULL;
	ParticleSystem_t3394631041 * V_1 = NULL;
	ParticleSystemU5BU5D_t1490986844* V_2 = NULL;
	int32_t V_3 = 0;
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Rigidbody2D_t502193897 * L_1 = GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143(L_0, /*hidden argument*/GameObject_GetComponent_TisRigidbody2D_t502193897_m812242143_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m2516226135(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004e;
		}
	}
	{
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		ParticleSystemU5BU5D_t1490986844* L_4 = GameObject_GetComponentsInChildren_TisParticleSystem_t3394631041_m2199618894(L_3, /*hidden argument*/GameObject_GetComponentsInChildren_TisParticleSystem_t3394631041_m2199618894_RuntimeMethod_var);
		V_0 = L_4;
		ParticleSystemU5BU5D_t1490986844* L_5 = V_0;
		V_2 = L_5;
		V_3 = 0;
		goto IL_0039;
	}

IL_002b:
	{
		ParticleSystemU5BU5D_t1490986844* L_6 = V_2;
		int32_t L_7 = V_3;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		ParticleSystem_t3394631041 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = L_9;
		ParticleSystem_t3394631041 * L_10 = V_1;
		NullCheck(L_10);
		ParticleSystem_Stop_m3868680149(L_10, /*hidden argument*/NULL);
		int32_t L_11 = V_3;
		V_3 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0039:
	{
		int32_t L_12 = V_3;
		ParticleSystemU5BU5D_t1490986844* L_13 = V_2;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_13)->max_length)))))))
		{
			goto IL_002b;
		}
	}
	{
		GameObject_t1756533147 * L_14 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		GameObject_AddComponent_TisRigidbody2D_t502193897_m2081092396(L_14, /*hidden argument*/GameObject_AddComponent_TisRigidbody2D_t502193897_m2081092396_RuntimeMethod_var);
	}

IL_004e:
	{
		return;
	}
}
// System.Void FollowTarget::.ctor()
extern "C"  void FollowTarget__ctor_m3038774147 (FollowTarget_t2817477884 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FollowTarget::Start()
extern "C"  void FollowTarget_Start_m1233455719 (FollowTarget_t2817477884 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FollowTarget_Start_m1233455719_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_0 = Vector3_get_zero_m3202043185(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_velocity_4(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Preferences_t60026258_il2cpp_TypeInfo_var);
		String_t* L_1 = ((Preferences_t60026258_StaticFields*)il2cpp_codegen_static_fields_for(Preferences_t60026258_il2cpp_TypeInfo_var))->get_player_tag_2();
		GameObject_t1756533147 * L_2 = GameObject_FindGameObjectWithTag_m1433464258(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = GameObject_get_transform_m3490276752(L_2, /*hidden argument*/NULL);
		__this->set_target_3(L_3);
		return;
	}
}
// System.Void FollowTarget::FixedUpdate()
extern "C"  void FollowTarget_FixedUpdate_m2936679024 (FollowTarget_t2817477884 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FollowTarget_FixedUpdate_m2936679024_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float G_B3_0 = 0.0f;
	Vector3_t2243707580 * G_B3_1 = NULL;
	float G_B2_0 = 0.0f;
	Vector3_t2243707580 * G_B2_1 = NULL;
	float G_B4_0 = 0.0f;
	float G_B4_1 = 0.0f;
	Vector3_t2243707580 * G_B4_2 = NULL;
	{
		Transform_t3275118058 * L_0 = __this->get_target_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00e6;
		}
	}
	{
		Camera_t189460977 * L_2 = Camera_get_main_m881971336(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = __this->get_target_3();
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = Transform_get_position_m2304215762(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t2243707580  L_5 = Camera_WorldToViewportPoint_m311010509(L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		Transform_t3275118058 * L_6 = __this->get_target_3();
		NullCheck(L_6);
		Vector3_t2243707580  L_7 = Transform_get_position_m2304215762(L_6, /*hidden argument*/NULL);
		Camera_t189460977 * L_8 = Camera_get_main_m881971336(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = (&V_0)->get_z_3();
		Vector3_t2243707580  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m1555724485((&L_10), (0.5f), (0.5f), L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t2243707580  L_11 = Camera_ViewportToWorldPoint_m283095757(L_8, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_12 = Vector3_op_Subtraction_m4046047256(NULL /*static, unused*/, L_7, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		Vector3_t2243707580  L_13 = V_1;
		Transform_t3275118058 * L_14 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector3_t2243707580  L_15 = Transform_get_position_m2304215762(L_14, /*hidden argument*/NULL);
		Vector3_t2243707580  L_16 = Vector3_op_Addition_m394909128(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
		V_1 = L_16;
		Transform_t3275118058 * L_17 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t2243707580  L_18 = Transform_get_position_m2304215762(L_17, /*hidden argument*/NULL);
		Vector3_t2243707580  L_19 = V_1;
		Vector3_t2243707580 * L_20 = __this->get_address_of_velocity_4();
		Vector3_t2243707580  L_21 = Vector3_SmoothDamp_m3493216268(NULL /*static, unused*/, L_18, L_19, L_20, (0.15f), /*hidden argument*/NULL);
		V_2 = L_21;
		float L_22 = (&V_2)->get_x_1();
		float L_23 = (&V_2)->get_y_2();
		Transform_t3275118058 * L_24 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		NullCheck(L_24);
		Vector3_t2243707580  L_25 = Transform_get_position_m2304215762(L_24, /*hidden argument*/NULL);
		V_3 = L_25;
		float L_26 = (&V_3)->get_y_2();
		G_B2_0 = L_22;
		G_B2_1 = (&V_2);
		if ((!(((float)L_23) < ((float)L_26))))
		{
			G_B3_0 = L_22;
			G_B3_1 = (&V_2);
			goto IL_00c7;
		}
	}
	{
		Transform_t3275118058 * L_27 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		Vector3_t2243707580  L_28 = Transform_get_position_m2304215762(L_27, /*hidden argument*/NULL);
		V_4 = L_28;
		float L_29 = (&V_4)->get_y_2();
		G_B4_0 = L_29;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		goto IL_00ce;
	}

IL_00c7:
	{
		float L_30 = (&V_2)->get_y_2();
		G_B4_0 = L_30;
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
	}

IL_00ce:
	{
		float L_31 = (&V_2)->get_z_3();
		Vector3__ctor_m1555724485(G_B4_2, G_B4_1, G_B4_0, L_31, /*hidden argument*/NULL);
		Transform_t3275118058 * L_32 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_33 = V_2;
		NullCheck(L_32);
		Transform_set_position_m2942701431(L_32, L_33, /*hidden argument*/NULL);
	}

IL_00e6:
	{
		return;
	}
}
// System.Void GarbageCollector::.ctor()
extern "C"  void GarbageCollector__ctor_m2124820269 (GarbageCollector_t1348916102 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GarbageCollector::Update()
extern "C"  void GarbageCollector_Update_m4273671300 (GarbageCollector_t1348916102 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = Time_get_frameCount_m3403277510(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (((int32_t)((int32_t)L_0%(int32_t)((int32_t)30))))
		{
			goto IL_0012;
		}
	}
	{
		GC_Collect_m2249328497(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void HealthHUD::.ctor()
extern "C"  void HealthHUD__ctor_m204371146 (HealthHUD_t66113683 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HealthHUD::Awake()
extern "C"  void HealthHUD_Awake_m574236355 (HealthHUD_t66113683 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HealthHUD_Awake_m574236355_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Preferences_t60026258_il2cpp_TypeInfo_var);
		String_t* L_0 = ((Preferences_t60026258_StaticFields*)il2cpp_codegen_static_fields_for(Preferences_t60026258_il2cpp_TypeInfo_var))->get_player_tag_2();
		GameObject_t1756533147 * L_1 = GameObject_FindGameObjectWithTag_m1433464258(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Player2D_t165353355 * L_2 = GameObject_GetComponent_TisPlayer2D_t165353355_m1754251190(L_1, /*hidden argument*/GameObject_GetComponent_TisPlayer2D_t165353355_m1754251190_RuntimeMethod_var);
		__this->set_player_5(L_2);
		__this->set_errors_6((bool)0);
		GameObjectU5BU5D_t3057952154* L_3 = __this->get_hearts_2();
		NullCheck(L_3);
		Player2D_t165353355 * L_4 = __this->get_player_5();
		NullCheck(L_4);
		int32_t L_5 = Player2D_GetMaxHealth_m1138988990(L_4, /*hidden argument*/NULL);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_3)->max_length))))) == ((int32_t)L_5)))
		{
			goto IL_0045;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3299155069(NULL /*static, unused*/, _stringLiteral3924929881, /*hidden argument*/NULL);
		__this->set_errors_6((bool)1);
	}

IL_0045:
	{
		Player2D_t165353355 * L_6 = __this->get_player_5();
		NullCheck(L_6);
		int32_t L_7 = Player2D_GetMaxHealth_m1138988990(L_6, /*hidden argument*/NULL);
		__this->set_current_health_7(L_7);
		return;
	}
}
// System.Void HealthHUD::LateUpdate()
extern "C"  void HealthHUD_LateUpdate_m3904430699 (HealthHUD_t66113683 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HealthHUD_LateUpdate_m3904430699_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Image_t2042527209 * V_0 = NULL;
	int32_t V_1 = 0;
	Image_t2042527209 * V_2 = NULL;
	{
		bool L_0 = __this->get_errors_6();
		if (L_0)
		{
			goto IL_00db;
		}
	}
	{
		Player2D_t165353355 * L_1 = __this->get_player_5();
		NullCheck(L_1);
		int32_t L_2 = Player2D_GetHealth_m2808744642(L_1, /*hidden argument*/NULL);
		int32_t L_3 = __this->get_current_health_7();
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_00db;
		}
	}
	{
		int32_t L_4 = __this->get_current_health_7();
		Player2D_t165353355 * L_5 = __this->get_player_5();
		NullCheck(L_5);
		int32_t L_6 = Player2D_GetHealth_m2808744642(L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_4) <= ((int32_t)L_6)))
		{
			goto IL_0081;
		}
	}
	{
		GameObjectU5BU5D_t3057952154* L_7 = __this->get_hearts_2();
		int32_t L_8 = __this->get_current_health_7();
		int32_t L_9 = ((int32_t)((int32_t)L_8-(int32_t)1));
		V_1 = L_9;
		__this->set_current_health_7(L_9);
		int32_t L_10 = V_1;
		NullCheck(L_7);
		int32_t L_11 = L_10;
		GameObject_t1756533147 * L_12 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_12);
		Image_t2042527209 * L_13 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_12, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_RuntimeMethod_var);
		V_0 = L_13;
		Image_t2042527209 * L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_007c;
		}
	}
	{
		Sprite_t309593783 * L_16 = __this->get_emptyheart_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_17 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_007c;
		}
	}
	{
		Image_t2042527209 * L_18 = V_0;
		Sprite_t309593783 * L_19 = __this->get_emptyheart_4();
		NullCheck(L_18);
		Image_set_sprite_m1800056820(L_18, L_19, /*hidden argument*/NULL);
	}

IL_007c:
	{
		goto IL_00db;
	}

IL_0081:
	{
		int32_t L_20 = __this->get_current_health_7();
		GameObjectU5BU5D_t3057952154* L_21 = __this->get_hearts_2();
		NullCheck(L_21);
		if ((((int32_t)((int32_t)((int32_t)L_20+(int32_t)1))) >= ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_21)->max_length)))))))
		{
			goto IL_00db;
		}
	}
	{
		GameObjectU5BU5D_t3057952154* L_22 = __this->get_hearts_2();
		int32_t L_23 = __this->get_current_health_7();
		int32_t L_24 = ((int32_t)((int32_t)L_23+(int32_t)1));
		V_1 = L_24;
		__this->set_current_health_7(L_24);
		int32_t L_25 = V_1;
		NullCheck(L_22);
		int32_t L_26 = L_25;
		GameObject_t1756533147 * L_27 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		NullCheck(L_27);
		Image_t2042527209 * L_28 = GameObject_GetComponent_TisImage_t2042527209_m4162535761(L_27, /*hidden argument*/GameObject_GetComponent_TisImage_t2042527209_m4162535761_RuntimeMethod_var);
		V_2 = L_28;
		Image_t2042527209 * L_29 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_30 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00db;
		}
	}
	{
		Sprite_t309593783 * L_31 = __this->get_fullheart_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_32 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_00db;
		}
	}
	{
		Image_t2042527209 * L_33 = V_2;
		Sprite_t309593783 * L_34 = __this->get_fullheart_3();
		NullCheck(L_33);
		Image_set_sprite_m1800056820(L_33, L_34, /*hidden argument*/NULL);
	}

IL_00db:
	{
		return;
	}
}
// System.Void Input2D::.ctor()
extern "C"  void Input2D__ctor_m431867499 (Input2D_t2938435410 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JoyStick::.ctor()
extern "C"  void JoyStick__ctor_m3283126125 (JoyStick_t513152754 * __this, const RuntimeMethod* method)
{
	{
		Input2D__ctor_m431867499(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JoyStick::Start()
extern "C"  void JoyStick_Start_m3563140509 (JoyStick_t513152754 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JoyStick_Start_m3563140509_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Image_t2042527209 * L_0 = Component_GetComponent_TisImage_t2042527209_m2189462422(__this, /*hidden argument*/Component_GetComponent_TisImage_t2042527209_m2189462422_RuntimeMethod_var);
		__this->set_jsContainer_2(L_0);
		Transform_t3275118058 * L_1 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = Transform_GetChild_m3136145191(L_1, 0, /*hidden argument*/NULL);
		NullCheck(L_2);
		Image_t2042527209 * L_3 = Component_GetComponent_TisImage_t2042527209_m2189462422(L_2, /*hidden argument*/Component_GetComponent_TisImage_t2042527209_m2189462422_RuntimeMethod_var);
		__this->set_joystick_3(L_3);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_4 = Vector3_get_zero_m3202043185(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_InputDirection_4(L_4);
		return;
	}
}
// System.Void JoyStick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void JoyStick_OnDrag_m1637494730 (JoyStick_t513152754 * __this, PointerEventData_t1599784723 * ___eventData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JoyStick_OnDrag_m1637494730_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2243707579  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	Vector2_t2243707579  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float V_5 = 0.0f;
	Vector2_t2243707579  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector2_t2243707579  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector2_t2243707579  V_8;
	memset(&V_8, 0, sizeof(V_8));
	float G_B3_0 = 0.0f;
	float G_B6_0 = 0.0f;
	JoyStick_t513152754 * G_B8_0 = NULL;
	JoyStick_t513152754 * G_B7_0 = NULL;
	Vector3_t2243707580  G_B9_0;
	memset(&G_B9_0, 0, sizeof(G_B9_0));
	JoyStick_t513152754 * G_B9_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2243707579_il2cpp_TypeInfo_var);
		Vector2_t2243707579  L_0 = Vector2_get_zero_m1210615473(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Image_t2042527209 * L_1 = __this->get_jsContainer_2();
		NullCheck(L_1);
		RectTransform_t3349966182 * L_2 = Graphic_get_rectTransform_m2697395074(L_1, /*hidden argument*/NULL);
		PointerEventData_t1599784723 * L_3 = ___eventData0;
		NullCheck(L_3);
		Vector2_t2243707579  L_4 = PointerEventData_get_position_m2131765015(L_3, /*hidden argument*/NULL);
		PointerEventData_t1599784723 * L_5 = ___eventData0;
		NullCheck(L_5);
		Camera_t189460977 * L_6 = PointerEventData_get_pressEventCamera_m724559964(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		RectTransformUtility_ScreenPointToLocalPointInRectangle_m1119989730(NULL /*static, unused*/, L_2, L_4, L_6, (&V_0), /*hidden argument*/NULL);
		float L_7 = (&V_0)->get_x_0();
		Image_t2042527209 * L_8 = __this->get_jsContainer_2();
		NullCheck(L_8);
		RectTransform_t3349966182 * L_9 = Graphic_get_rectTransform_m2697395074(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector2_t2243707579  L_10 = RectTransform_get_sizeDelta_m2505406265(L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		float L_11 = (&V_1)->get_x_0();
		(&V_0)->set_x_0(((float)((float)L_7/(float)L_11)));
		float L_12 = (&V_0)->get_y_1();
		Image_t2042527209 * L_13 = __this->get_jsContainer_2();
		NullCheck(L_13);
		RectTransform_t3349966182 * L_14 = Graphic_get_rectTransform_m2697395074(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector2_t2243707579  L_15 = RectTransform_get_sizeDelta_m2505406265(L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		float L_16 = (&V_2)->get_y_1();
		(&V_0)->set_y_1(((float)((float)L_12/(float)L_16)));
		Image_t2042527209 * L_17 = __this->get_jsContainer_2();
		NullCheck(L_17);
		RectTransform_t3349966182 * L_18 = Graphic_get_rectTransform_m2697395074(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector2_t2243707579  L_19 = RectTransform_get_pivot_m3474500578(L_18, /*hidden argument*/NULL);
		V_4 = L_19;
		float L_20 = (&V_4)->get_x_0();
		if ((!(((float)L_20) == ((float)(1.0f)))))
		{
			goto IL_00ae;
		}
	}
	{
		float L_21 = (&V_0)->get_x_0();
		G_B3_0 = ((float)((float)((float)((float)L_21*(float)(2.0f)))+(float)(1.0f)));
		goto IL_00c1;
	}

IL_00ae:
	{
		float L_22 = (&V_0)->get_x_0();
		G_B3_0 = ((float)((float)((float)((float)L_22*(float)(2.0f)))-(float)(1.0f)));
	}

IL_00c1:
	{
		V_3 = G_B3_0;
		Image_t2042527209 * L_23 = __this->get_jsContainer_2();
		NullCheck(L_23);
		RectTransform_t3349966182 * L_24 = Graphic_get_rectTransform_m2697395074(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Vector2_t2243707579  L_25 = RectTransform_get_pivot_m3474500578(L_24, /*hidden argument*/NULL);
		V_6 = L_25;
		float L_26 = (&V_6)->get_y_1();
		if ((!(((float)L_26) == ((float)(1.0f)))))
		{
			goto IL_00fd;
		}
	}
	{
		float L_27 = (&V_0)->get_y_1();
		G_B6_0 = ((float)((float)((float)((float)L_27*(float)(2.0f)))+(float)(1.0f)));
		goto IL_0110;
	}

IL_00fd:
	{
		float L_28 = (&V_0)->get_y_1();
		G_B6_0 = ((float)((float)((float)((float)L_28*(float)(2.0f)))-(float)(1.0f)));
	}

IL_0110:
	{
		V_5 = G_B6_0;
		float L_29 = V_3;
		float L_30 = V_5;
		Vector3_t2243707580  L_31;
		memset(&L_31, 0, sizeof(L_31));
		Vector3__ctor_m1555724485((&L_31), L_29, L_30, (0.0f), /*hidden argument*/NULL);
		__this->set_InputDirection_4(L_31);
		Vector3_t2243707580 * L_32 = __this->get_address_of_InputDirection_4();
		float L_33 = Vector3_get_magnitude_m4021906415(L_32, /*hidden argument*/NULL);
		G_B7_0 = __this;
		if ((!(((float)L_33) > ((float)(1.0f)))))
		{
			G_B8_0 = __this;
			goto IL_014b;
		}
	}
	{
		Vector3_t2243707580 * L_34 = __this->get_address_of_InputDirection_4();
		Vector3_t2243707580  L_35 = Vector3_get_normalized_m1057036856(L_34, /*hidden argument*/NULL);
		G_B9_0 = L_35;
		G_B9_1 = G_B7_0;
		goto IL_0151;
	}

IL_014b:
	{
		Vector3_t2243707580  L_36 = __this->get_InputDirection_4();
		G_B9_0 = L_36;
		G_B9_1 = G_B8_0;
	}

IL_0151:
	{
		NullCheck(G_B9_1);
		G_B9_1->set_InputDirection_4(G_B9_0);
		Image_t2042527209 * L_37 = __this->get_joystick_3();
		NullCheck(L_37);
		RectTransform_t3349966182 * L_38 = Graphic_get_rectTransform_m2697395074(L_37, /*hidden argument*/NULL);
		Vector3_t2243707580 * L_39 = __this->get_address_of_InputDirection_4();
		float L_40 = L_39->get_x_1();
		Image_t2042527209 * L_41 = __this->get_jsContainer_2();
		NullCheck(L_41);
		RectTransform_t3349966182 * L_42 = Graphic_get_rectTransform_m2697395074(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		Vector2_t2243707579  L_43 = RectTransform_get_sizeDelta_m2505406265(L_42, /*hidden argument*/NULL);
		V_7 = L_43;
		float L_44 = (&V_7)->get_x_0();
		Vector3_t2243707580 * L_45 = __this->get_address_of_InputDirection_4();
		float L_46 = L_45->get_y_2();
		Image_t2042527209 * L_47 = __this->get_jsContainer_2();
		NullCheck(L_47);
		RectTransform_t3349966182 * L_48 = Graphic_get_rectTransform_m2697395074(L_47, /*hidden argument*/NULL);
		NullCheck(L_48);
		Vector2_t2243707579  L_49 = RectTransform_get_sizeDelta_m2505406265(L_48, /*hidden argument*/NULL);
		V_8 = L_49;
		float L_50 = (&V_8)->get_y_1();
		Vector3_t2243707580  L_51;
		memset(&L_51, 0, sizeof(L_51));
		Vector3__ctor_m2761639014((&L_51), ((float)((float)L_40*(float)((float)((float)L_44/(float)(3.0f))))), ((float)((float)((float)((float)L_46*(float)L_50))/(float)(3.0f))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2243707579_il2cpp_TypeInfo_var);
		Vector2_t2243707579  L_52 = Vector2_op_Implicit_m385881926(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		NullCheck(L_38);
		RectTransform_set_anchoredPosition_m4223859262(L_38, L_52, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JoyStick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void JoyStick_OnPointerDown_m1280761529 (JoyStick_t513152754 * __this, PointerEventData_t1599784723 * ___ped0, const RuntimeMethod* method)
{
	{
		PointerEventData_t1599784723 * L_0 = ___ped0;
		JoyStick_OnDrag_m1637494730(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JoyStick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C"  void JoyStick_OnPointerUp_m4080876746 (JoyStick_t513152754 * __this, PointerEventData_t1599784723 * ___ped0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JoyStick_OnPointerUp_m4080876746_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_0 = Vector3_get_zero_m3202043185(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_InputDirection_4(L_0);
		Image_t2042527209 * L_1 = __this->get_joystick_3();
		NullCheck(L_1);
		RectTransform_t3349966182 * L_2 = Graphic_get_rectTransform_m2697395074(L_1, /*hidden argument*/NULL);
		Vector3_t2243707580  L_3 = Vector3_get_zero_m3202043185(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2243707579_il2cpp_TypeInfo_var);
		Vector2_t2243707579  L_4 = Vector2_op_Implicit_m385881926(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		RectTransform_set_anchoredPosition_m4223859262(L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Single JoyStick::Horizontal()
extern "C"  float JoyStick_Horizontal_m201800301 (JoyStick_t513152754 * __this, const RuntimeMethod* method)
{
	{
		Vector3_t2243707580 * L_0 = __this->get_address_of_InputDirection_4();
		float L_1 = L_0->get_x_1();
		if ((!(((float)L_1) > ((float)(0.0f)))))
		{
			goto IL_001b;
		}
	}
	{
		return (1.0f);
	}

IL_001b:
	{
		Vector3_t2243707580 * L_2 = __this->get_address_of_InputDirection_4();
		float L_3 = L_2->get_x_1();
		if ((!(((float)L_3) < ((float)(0.0f)))))
		{
			goto IL_0036;
		}
	}
	{
		return (-1.0f);
	}

IL_0036:
	{
		return (0.0f);
	}
}
// System.Boolean JoyStick::JumpButtonDown()
extern "C"  bool JoyStick_JumpButtonDown_m1883720755 (JoyStick_t513152754 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_jump_down_5();
		return L_0;
	}
}
// System.Boolean JoyStick::JumpButtonUp()
extern "C"  bool JoyStick_JumpButtonUp_m1670842050 (JoyStick_t513152754 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_jump_up_6();
		return L_0;
	}
}
// System.Void JoyStick::JumpButtonClick()
extern "C"  void JoyStick_JumpButtonClick_m1478805243 (JoyStick_t513152754 * __this, const RuntimeMethod* method)
{
	{
		__this->set_jump_down_5((bool)1);
		return;
	}
}
// System.Void JoyStick::JumpButtonClickUp()
extern "C"  void JoyStick_JumpButtonClickUp_m1165425044 (JoyStick_t513152754 * __this, const RuntimeMethod* method)
{
	{
		__this->set_jump_up_6((bool)1);
		return;
	}
}
// System.Void JoyStick::Update()
extern "C"  void JoyStick_Update_m2311192148 (JoyStick_t513152754 * __this, const RuntimeMethod* method)
{
	{
		__this->set_jump_up_6((bool)0);
		__this->set_jump_down_5((bool)0);
		return;
	}
}
// System.Void KeyboardInput::.ctor()
extern "C"  void KeyboardInput__ctor_m1265847842 (KeyboardInput_t1795993661 * __this, const RuntimeMethod* method)
{
	{
		Input2D__ctor_m431867499(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single KeyboardInput::Horizontal()
extern "C"  float KeyboardInput_Horizontal_m1920898926 (KeyboardInput_t1795993661 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_horizontal_2();
		return L_0;
	}
}
// System.Boolean KeyboardInput::JumpButtonDown()
extern "C"  bool KeyboardInput_JumpButtonDown_m2129420462 (KeyboardInput_t1795993661 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_jump_button_down_3();
		return L_0;
	}
}
// System.Boolean KeyboardInput::JumpButtonUp()
extern "C"  bool KeyboardInput_JumpButtonUp_m2457343005 (KeyboardInput_t1795993661 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_jump_button_up_4();
		return L_0;
	}
}
// System.Void KeyboardInput::Update()
extern "C"  void KeyboardInput_Update_m2215788453 (KeyboardInput_t1795993661 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyboardInput_Update_m2215788453_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		float L_0 = Input_GetAxisRaw_m1913129537(NULL /*static, unused*/, _stringLiteral855845486, /*hidden argument*/NULL);
		__this->set_horizontal_2(L_0);
		bool L_1 = Input_GetButtonDown_m717298472(NULL /*static, unused*/, _stringLiteral842948034, /*hidden argument*/NULL);
		__this->set_jump_button_down_3(L_1);
		bool L_2 = Input_GetButtonUp_m3002013723(NULL /*static, unused*/, _stringLiteral842948034, /*hidden argument*/NULL);
		__this->set_jump_button_up_4(L_2);
		return;
	}
}
// System.Void LoadScene::.ctor()
extern "C"  void LoadScene__ctor_m515403255 (LoadScene_t1133380052 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LoadScene::LoadSceneWithIndex(System.Int32)
extern "C"  void LoadScene_LoadSceneWithIndex_m216160262 (LoadScene_t1133380052 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LoadScene_LoadSceneWithIndex_m216160262_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LoadTransition_t4049825803 * V_0 = NULL;
	{
		Camera_t189460977 * L_0 = Camera_get_main_m881971336(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m2159020946(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		LoadTransition_t4049825803 * L_2 = GameObject_GetComponent_TisLoadTransition_t4049825803_m3996224546(L_1, /*hidden argument*/GameObject_GetComponent_TisLoadTransition_t4049825803_m3996224546_RuntimeMethod_var);
		V_0 = L_2;
		LoadTransition_t4049825803 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002f;
		}
	}
	{
		int32_t L_5 = ___index0;
		LoadTransition_t4049825803 * L_6 = V_0;
		RuntimeObject* L_7 = LoadScene_LoadSceneWithTransition_m748142228(__this, L_5, L_6, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2678710497(__this, L_7, /*hidden argument*/NULL);
		goto IL_0035;
	}

IL_002f:
	{
		int32_t L_8 = ___index0;
		SceneManager_LoadScene_m1620702025(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_0035:
	{
		return;
	}
}
// System.Collections.IEnumerator LoadScene::LoadSceneWithTransition(System.Int32,LoadTransition)
extern "C"  RuntimeObject* LoadScene_LoadSceneWithTransition_m748142228 (LoadScene_t1133380052 * __this, int32_t ___index0, LoadTransition_t4049825803 * ___transition1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LoadScene_LoadSceneWithTransition_m748142228_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CLoadSceneWithTransitionU3Ec__Iterator0_t1207375911 * V_0 = NULL;
	{
		U3CLoadSceneWithTransitionU3Ec__Iterator0_t1207375911 * L_0 = (U3CLoadSceneWithTransitionU3Ec__Iterator0_t1207375911 *)il2cpp_codegen_object_new(U3CLoadSceneWithTransitionU3Ec__Iterator0_t1207375911_il2cpp_TypeInfo_var);
		U3CLoadSceneWithTransitionU3Ec__Iterator0__ctor_m2803670358(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoadSceneWithTransitionU3Ec__Iterator0_t1207375911 * L_1 = V_0;
		LoadTransition_t4049825803 * L_2 = ___transition1;
		NullCheck(L_1);
		L_1->set_transition_0(L_2);
		U3CLoadSceneWithTransitionU3Ec__Iterator0_t1207375911 * L_3 = V_0;
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		L_3->set_index_1(L_4);
		U3CLoadSceneWithTransitionU3Ec__Iterator0_t1207375911 * L_5 = V_0;
		return L_5;
	}
}
// System.Void LoadScene::QuitApplication()
extern "C"  void LoadScene_QuitApplication_m850102136 (LoadScene_t1133380052 * __this, const RuntimeMethod* method)
{
	{
		Application_Quit_m3096163801(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LoadScene/<LoadSceneWithTransition>c__Iterator0::.ctor()
extern "C"  void U3CLoadSceneWithTransitionU3Ec__Iterator0__ctor_m2803670358 (U3CLoadSceneWithTransitionU3Ec__Iterator0_t1207375911 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean LoadScene/<LoadSceneWithTransition>c__Iterator0::MoveNext()
extern "C"  bool U3CLoadSceneWithTransitionU3Ec__Iterator0_MoveNext_m463968662 (U3CLoadSceneWithTransitionU3Ec__Iterator0_t1207375911 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadSceneWithTransitionU3Ec__Iterator0_MoveNext_m463968662_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_004c;
			}
		}
	}
	{
		goto IL_005e;
	}

IL_0021:
	{
		LoadTransition_t4049825803 * L_2 = __this->get_transition_0();
		NullCheck(L_2);
		float L_3 = LoadTransition_BeginFade_m3778697406(L_2, 1, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_4 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m632080402(L_4, L_3, /*hidden argument*/NULL);
		__this->set_U24current_2(L_4);
		bool L_5 = __this->get_U24disposing_3();
		if (L_5)
		{
			goto IL_0047;
		}
	}
	{
		__this->set_U24PC_4(1);
	}

IL_0047:
	{
		goto IL_0060;
	}

IL_004c:
	{
		int32_t L_6 = __this->get_index_1();
		SceneManager_LoadScene_m1620702025(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->set_U24PC_4((-1));
	}

IL_005e:
	{
		return (bool)0;
	}

IL_0060:
	{
		return (bool)1;
	}
}
// System.Object LoadScene/<LoadSceneWithTransition>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CLoadSceneWithTransitionU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m305667704 (U3CLoadSceneWithTransitionU3Ec__Iterator0_t1207375911 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object LoadScene/<LoadSceneWithTransition>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CLoadSceneWithTransitionU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3054222544 (U3CLoadSceneWithTransitionU3Ec__Iterator0_t1207375911 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Void LoadScene/<LoadSceneWithTransition>c__Iterator0::Dispose()
extern "C"  void U3CLoadSceneWithTransitionU3Ec__Iterator0_Dispose_m3117672601 (U3CLoadSceneWithTransitionU3Ec__Iterator0_t1207375911 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_3((bool)1);
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void LoadScene/<LoadSceneWithTransition>c__Iterator0::Reset()
extern "C"  void U3CLoadSceneWithTransitionU3Ec__Iterator0_Reset_m773062411 (U3CLoadSceneWithTransitionU3Ec__Iterator0_t1207375911 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadSceneWithTransitionU3Ec__Iterator0_Reset_m773062411_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void LoadTransition::.ctor()
extern "C"  void LoadTransition__ctor_m2825857086 (LoadTransition_t4049825803 * __this, const RuntimeMethod* method)
{
	{
		__this->set_draw_depth_4(((int32_t)-1000));
		__this->set_alpha_5((1.0f));
		__this->set_fade_dir_6((-1));
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LoadTransition::OnEnable()
extern "C"  void LoadTransition_OnEnable_m2776499082 (LoadTransition_t4049825803 * __this, const RuntimeMethod* method)
{
	{
		LoadTransition_BeginFade_m3778697406(__this, (-1), /*hidden argument*/NULL);
		return;
	}
}
// System.Single LoadTransition::BeginFade(System.Int32)
extern "C"  float LoadTransition_BeginFade_m3778697406 (LoadTransition_t4049825803 * __this, int32_t ___direction0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___direction0;
		__this->set_fade_dir_6(L_0);
		return (0.6f);
	}
}
// System.Void LoadTransition::OnGUI()
extern "C"  void LoadTransition_OnGUI_m3203620930 (LoadTransition_t4049825803 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LoadTransition_OnGUI_m3203620930_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Color_t2020392075  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Color_t2020392075  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		float L_0 = __this->get_alpha_5();
		int32_t L_1 = __this->get_fade_dir_6();
		float L_2 = Time_get_deltaTime_m3925508629(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_alpha_5(((float)((float)L_0+(float)((float)((float)((float)((float)(((float)((float)L_1)))*(float)(0.6f)))*(float)L_2)))));
		float L_3 = __this->get_alpha_5();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_4 = Mathf_Clamp01_m1777088257(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		__this->set_alpha_5(L_4);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		Color_t2020392075  L_5 = GUI_get_color_m3388074834(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_5;
		float L_6 = (&V_0)->get_r_0();
		Color_t2020392075  L_7 = GUI_get_color_m3388074834(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_7;
		float L_8 = (&V_1)->get_g_1();
		Color_t2020392075  L_9 = GUI_get_color_m3388074834(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_9;
		float L_10 = (&V_2)->get_b_2();
		float L_11 = __this->get_alpha_5();
		Color_t2020392075  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Color__ctor_m2736804035((&L_12), L_6, L_8, L_10, L_11, /*hidden argument*/NULL);
		GUI_set_color_m1289920875(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		int32_t L_13 = __this->get_draw_depth_4();
		GUI_set_depth_m314110018(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		int32_t L_14 = Screen_get_width_m3526633787(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_15 = Screen_get_height_m3831644396(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t3681755626  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Rect__ctor_m2109726426((&L_16), (0.0f), (0.0f), (((float)((float)L_14))), (((float)((float)L_15))), /*hidden argument*/NULL);
		Texture2D_t3542995729 * L_17 = __this->get_fade_texture_3();
		GUI_DrawTexture_m178176081(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MacroGenerator::.ctor()
extern "C"  void MacroGenerator__ctor_m1259505300 (MacroGenerator_t1652377675 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MacroGenerator::Awake()
extern "C"  void MacroGenerator_Awake_m1893318087 (MacroGenerator_t1652377675 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MacroGenerator_Awake_m1893318087_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		Camera_t189460977 * L_0 = Camera_get_main_m881971336(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2243707579  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector2__ctor_m847450945((&L_1), (1.0f), (1.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2243707579_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_2 = Vector2_op_Implicit_m129629632(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t2243707580  L_3 = Camera_ViewportToWorldPoint_m283095757(L_0, L_2, /*hidden argument*/NULL);
		Vector2_t2243707579  L_4 = Vector2_op_Implicit_m385881926(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = (&V_0)->get_y_1();
		V_1 = ((float)((float)L_5*(float)(2.0f)));
		float L_6 = (&V_0)->get_x_0();
		V_2 = ((float)((float)L_6*(float)(2.0f)));
		float L_7 = V_2;
		float L_8 = V_1;
		Vector2_t2243707579  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector2__ctor_m847450945((&L_9), L_7, L_8, /*hidden argument*/NULL);
		__this->set_screen_size_7(L_9);
		List_1_t1125654279 * L_10 = (List_1_t1125654279 *)il2cpp_codegen_object_new(List_1_t1125654279_il2cpp_TypeInfo_var);
		List_1__ctor_m704351054(L_10, /*hidden argument*/List_1__ctor_m704351054_RuntimeMethod_var);
		__this->set_micro_levels_8(L_10);
		RuntimeObject * L_11 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_11, /*hidden argument*/NULL);
		__this->set__lock_10(L_11);
		Vector2_t2243707579  L_12 = Vector2_get_zero_m1210615473(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_last_platform_pos_12(L_12);
		__this->set_difficulty_11((0.1f));
		MonoBehaviour_InvokeRepeating_m505769937(__this, _stringLiteral520868631, (0.0f), (0.5f), /*hidden argument*/NULL);
		int32_t L_13 = LayerMask_NameToLayer_m3213714080(NULL /*static, unused*/, _stringLiteral1875862075, /*hidden argument*/NULL);
		int32_t L_14 = LayerMask_NameToLayer_m3213714080(NULL /*static, unused*/, _stringLiteral3342734734, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		Physics2D_IgnoreLayerCollision_m1690433947(NULL /*static, unused*/, L_13, L_14, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MacroGenerator::Start()
extern "C"  void MacroGenerator_Start_m67427416 (MacroGenerator_t1652377675 * __this, const RuntimeMethod* method)
{
	{
		MacroGenerator_CreateLevel_m2116947579(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MacroGenerator::Update()
extern "C"  void MacroGenerator_Update_m1313472171 (MacroGenerator_t1652377675 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MacroGenerator_Update_m1313472171_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	GameObject_t1756533147 * V_1 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		RuntimeObject * L_0 = __this->get__lock_10();
		V_0 = L_0;
		RuntimeObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			List_1_t1125654279 * L_2 = __this->get_micro_levels_8();
			NullCheck(L_2);
			int32_t L_3 = List_1_get_Count_m2764296230(L_2, /*hidden argument*/List_1_get_Count_m2764296230_RuntimeMethod_var);
			if ((((int32_t)L_3) <= ((int32_t)0)))
			{
				goto IL_0050;
			}
		}

IL_001e:
		{
			List_1_t1125654279 * L_4 = __this->get_micro_levels_8();
			NullCheck(L_4);
			int32_t L_5 = List_1_get_Count_m2764296230(L_4, /*hidden argument*/List_1_get_Count_m2764296230_RuntimeMethod_var);
			if (((int32_t)((int32_t)L_5%(int32_t)5)))
			{
				goto IL_0050;
			}
		}

IL_0030:
		{
			List_1_t1125654279 * L_6 = __this->get_micro_levels_8();
			NullCheck(L_6);
			GameObject_t1756533147 * L_7 = List_1_get_Item_m939767277(L_6, 0, /*hidden argument*/List_1_get_Item_m939767277_RuntimeMethod_var);
			V_1 = L_7;
			List_1_t1125654279 * L_8 = __this->get_micro_levels_8();
			GameObject_t1756533147 * L_9 = V_1;
			NullCheck(L_8);
			List_1_Remove_m2287078133(L_8, L_9, /*hidden argument*/List_1_Remove_m2287078133_RuntimeMethod_var);
			GameObject_t1756533147 * L_10 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			Object_Destroy_m3959286051(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		}

IL_0050:
		{
			IL2CPP_LEAVE(0x5C, FINALLY_0055);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0055;
	}

FINALLY_0055:
	{ // begin finally (depth: 1)
		RuntimeObject * L_11 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(85)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(85)
	{
		IL2CPP_JUMP_TBL(0x5C, IL_005c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_005c:
	{
		return;
	}
}
// System.Void MacroGenerator::CreateLevel(System.Boolean)
extern "C"  void MacroGenerator_CreateLevel_m2116947579 (MacroGenerator_t1652377675 * __this, bool ___base_level0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MacroGenerator_CreateLevel_m2116947579_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	int32_t V_1 = 0;
	MicroGenerator_t2658307363 * V_2 = NULL;
	float V_3 = 0.0f;
	RuntimeObject * V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = __this->get_total_created_level_9();
		int32_t L_1 = L_0;
		V_1 = L_1;
		__this->set_total_created_level_9(((int32_t)((int32_t)L_1+(int32_t)1)));
		int32_t L_2 = V_1;
		int32_t L_3 = L_2;
		RuntimeObject * L_4 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m2000667605(NULL /*static, unused*/, _stringLiteral2138312532, L_4, _stringLiteral372029317, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_6 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m4283735819(L_6, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		GameObject_t1756533147 * L_7 = V_0;
		NullCheck(L_7);
		MicroGenerator_t2658307363 * L_8 = GameObject_AddComponent_TisMicroGenerator_t2658307363_m1900695421(L_7, /*hidden argument*/GameObject_AddComponent_TisMicroGenerator_t2658307363_m1900695421_RuntimeMethod_var);
		V_2 = L_8;
		MicroGenerator_t2658307363 * L_9 = V_2;
		NullCheck(L_9);
		MicroGenerator_set_blocks_in_level_m2818797397(L_9, 4, /*hidden argument*/NULL);
		MicroGenerator_t2658307363 * L_10 = V_2;
		float L_11 = __this->get_difficulty_11();
		NullCheck(L_10);
		MicroGenerator_set_difficulty_m4066586773(L_10, L_11, /*hidden argument*/NULL);
		MicroGenerator_t2658307363 * L_12 = V_2;
		Vector2_t2243707579 * L_13 = __this->get_address_of_screen_size_7();
		float L_14 = L_13->get_y_1();
		Vector2_t2243707579 * L_15 = __this->get_address_of_screen_size_7();
		float L_16 = L_15->get_x_0();
		Vector2_t2243707579  L_17 = __this->get_last_platform_pos_12();
		bool L_18 = ___base_level0;
		NullCheck(L_12);
		Vector2_t2243707579  L_19 = MicroGenerator_CreateLevel_m1130512212(L_12, L_14, L_16, L_17, L_18, /*hidden argument*/NULL);
		__this->set_last_platform_pos_12(L_19);
		int32_t L_20 = __this->get_total_created_level_9();
		if (((int32_t)((int32_t)L_20%(int32_t)((int32_t)10))))
		{
			goto IL_009f;
		}
	}
	{
		float L_21 = __this->get_difficulty_11();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_22 = Mathf_Clamp_m1779415170(NULL /*static, unused*/, ((float)((float)(10.0f)*(float)L_21)), (0.0f), (10.0f), /*hidden argument*/NULL);
		V_3 = L_22;
		float L_23 = V_3;
		MacroGenerator_ActivateShip_m2260158446(__this, L_23, /*hidden argument*/NULL);
	}

IL_009f:
	{
		RuntimeObject * L_24 = __this->get__lock_10();
		V_4 = L_24;
		RuntimeObject * L_25 = V_4;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
	}

IL_00ae:
	try
	{ // begin try (depth: 1)
		List_1_t1125654279 * L_26 = __this->get_micro_levels_8();
		GameObject_t1756533147 * L_27 = V_0;
		NullCheck(L_26);
		List_1_Add_m3441471442(L_26, L_27, /*hidden argument*/List_1_Add_m3441471442_RuntimeMethod_var);
		IL2CPP_LEAVE(0xC7, FINALLY_00bf);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00bf;
	}

FINALLY_00bf:
	{ // begin finally (depth: 1)
		RuntimeObject * L_28 = V_4;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(191)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(191)
	{
		IL2CPP_JUMP_TBL(0xC7, IL_00c7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00c7:
	{
		return;
	}
}
// System.Void MacroGenerator::ActivateShip(System.Single)
extern "C"  void MacroGenerator_ActivateShip_m2260158446 (MacroGenerator_t1652377675 * __this, float ___duration0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MacroGenerator_ActivateShip_m2260158446_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_FindGameObjectWithTag_m1433464258(NULL /*static, unused*/, _stringLiteral1844382288, /*hidden argument*/NULL);
		NullCheck(L_0);
		Ship_t1116303770 * L_1 = GameObject_GetComponent_TisShip_t1116303770_m1555807537(L_0, /*hidden argument*/GameObject_GetComponent_TisShip_t1116303770_m1555807537_RuntimeMethod_var);
		NullCheck(L_1);
		Ship_set_active_m3844682543(L_1, (bool)1, /*hidden argument*/NULL);
		float L_2 = ___duration0;
		MonoBehaviour_Invoke_m2201735803(__this, _stringLiteral3290965114, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MacroGenerator::DeactivateShip()
extern "C"  void MacroGenerator_DeactivateShip_m3391759862 (MacroGenerator_t1652377675 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MacroGenerator_DeactivateShip_m3391759862_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_FindGameObjectWithTag_m1433464258(NULL /*static, unused*/, _stringLiteral1844382288, /*hidden argument*/NULL);
		NullCheck(L_0);
		Ship_t1116303770 * L_1 = GameObject_GetComponent_TisShip_t1116303770_m1555807537(L_0, /*hidden argument*/GameObject_GetComponent_TisShip_t1116303770_m1555807537_RuntimeMethod_var);
		NullCheck(L_1);
		Ship_set_active_m3844682543(L_1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MacroGenerator::MeasureDifficulty()
extern "C"  void MacroGenerator_MeasureDifficulty_m3480219085 (MacroGenerator_t1652377675 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_difficulty_11();
		__this->set_difficulty_11(((float)((float)L_0+(float)(0.06f))));
		return;
	}
}
// System.Void MicroGenerator::.ctor()
extern "C"  void MicroGenerator__ctor_m1387823548 (MicroGenerator_t2658307363 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 MicroGenerator::get_blocks_in_level()
extern "C"  int32_t MicroGenerator_get_blocks_in_level_m3243240612 (MicroGenerator_t2658307363 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3Cblocks_in_levelU3Ek__BackingField_13();
		return L_0;
	}
}
// System.Void MicroGenerator::set_blocks_in_level(System.Int32)
extern "C"  void MicroGenerator_set_blocks_in_level_m2818797397 (MicroGenerator_t2658307363 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3Cblocks_in_levelU3Ek__BackingField_13(L_0);
		return;
	}
}
// System.Single MicroGenerator::get_difficulty()
extern "C"  float MicroGenerator_get_difficulty_m1319765580 (MicroGenerator_t2658307363 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CdifficultyU3Ek__BackingField_14();
		return L_0;
	}
}
// System.Void MicroGenerator::set_difficulty(System.Single)
extern "C"  void MicroGenerator_set_difficulty_m4066586773 (MicroGenerator_t2658307363 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CdifficultyU3Ek__BackingField_14(L_0);
		return;
	}
}
// System.Void MicroGenerator::Awake()
extern "C"  void MicroGenerator_Awake_m2021636335 (MicroGenerator_t2658307363 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MicroGenerator_Awake_m2021636335_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Preferences_t60026258_il2cpp_TypeInfo_var);
		String_t* L_0 = ((Preferences_t60026258_StaticFields*)il2cpp_codegen_static_fields_for(Preferences_t60026258_il2cpp_TypeInfo_var))->get_player_tag_2();
		GameObject_t1756533147 * L_1 = GameObject_FindGameObjectWithTag_m1433464258(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Player2D_t165353355 * L_2 = GameObject_GetComponent_TisPlayer2D_t165353355_m1754251190(L_1, /*hidden argument*/GameObject_GetComponent_TisPlayer2D_t165353355_m1754251190_RuntimeMethod_var);
		__this->set_player_11(L_2);
		return;
	}
}
// UnityEngine.Vector2 MicroGenerator::CreateLevel(System.Single,System.Single,UnityEngine.Vector2,System.Boolean)
extern "C"  Vector2_t2243707579  MicroGenerator_CreateLevel_m1130512212 (MicroGenerator_t2658307363 * __this, float ___height0, float ___width1, Vector2_t2243707579  ___last_platform2, bool ___base_level3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MicroGenerator_CreateLevel_m1130512212_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	GameObject_t1756533147 * V_2 = NULL;
	float V_3 = 0.0f;
	GameObject_t1756533147 * V_4 = NULL;
	PlatformGenerator_t395172572 * V_5 = NULL;
	float V_6 = 0.0f;
	Bounds_t3033363703  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t2243707580  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector2_t2243707579  V_9;
	memset(&V_9, 0, sizeof(V_9));
	float V_10 = 0.0f;
	Vector2_t2243707579  V_11;
	memset(&V_11, 0, sizeof(V_11));
	float V_12 = 0.0f;
	float V_13 = 0.0f;
	float V_14 = 0.0f;
	float V_15 = 0.0f;
	float V_16 = 0.0f;
	float V_17 = 0.0f;
	float V_18 = 0.0f;
	float V_19 = 0.0f;
	Vector2_t2243707579  V_20;
	memset(&V_20, 0, sizeof(V_20));
	float V_21 = 0.0f;
	Vector2_t2243707579  V_22;
	memset(&V_22, 0, sizeof(V_22));
	bool V_23 = false;
	SpriteRenderer_t1209076198 * V_24 = NULL;
	SpriteRendererU5BU5D_t1098056643* V_25 = NULL;
	int32_t V_26 = 0;
	Color_t2020392075  V_27;
	memset(&V_27, 0, sizeof(V_27));
	Color_t2020392075  V_28;
	memset(&V_28, 0, sizeof(V_28));
	Color_t2020392075  V_29;
	memset(&V_29, 0, sizeof(V_29));
	BoxCollider2D_t948534547 * V_30 = NULL;
	Bounds_t3033363703  V_31;
	memset(&V_31, 0, sizeof(V_31));
	Vector3_t2243707580  V_32;
	memset(&V_32, 0, sizeof(V_32));
	Bounds_t3033363703  V_33;
	memset(&V_33, 0, sizeof(V_33));
	{
		PlatformPool_t3559850805 * L_0 = __this->get_pool_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m2516226135(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		GameObject_t1756533147 * L_2 = GameObject_FindGameObjectWithTag_m1433464258(NULL /*static, unused*/, _stringLiteral4287929323, /*hidden argument*/NULL);
		NullCheck(L_2);
		PlatformPool_t3559850805 * L_3 = GameObject_GetComponent_TisPlatformPool_t3559850805_m831201488(L_2, /*hidden argument*/GameObject_GetComponent_TisPlatformPool_t3559850805_m831201488_RuntimeMethod_var);
		__this->set_pool_7(L_3);
	}

IL_0026:
	{
		List_1_t1125654279 * L_4 = __this->get_generated_platform_8();
		if (L_4)
		{
			goto IL_0041;
		}
	}
	{
		List_1_t1125654279 * L_5 = (List_1_t1125654279 *)il2cpp_codegen_object_new(List_1_t1125654279_il2cpp_TypeInfo_var);
		List_1__ctor_m704351054(L_5, /*hidden argument*/List_1__ctor_m704351054_RuntimeMethod_var);
		__this->set_generated_platform_8(L_5);
		goto IL_004c;
	}

IL_0041:
	{
		List_1_t1125654279 * L_6 = __this->get_generated_platform_8();
		NullCheck(L_6);
		List_1_Clear_m4030601119(L_6, /*hidden argument*/List_1_Clear_m4030601119_RuntimeMethod_var);
	}

IL_004c:
	{
		bool L_7 = __this->get_has_created_level_9();
		if (L_7)
		{
			goto IL_0416;
		}
	}
	{
		int32_t L_8 = MicroGenerator_get_blocks_in_level_m3243240612(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_8) <= ((int32_t)0)))
		{
			goto IL_0416;
		}
	}
	{
		V_0 = 0;
		goto IL_0394;
	}

IL_006a:
	{
		bool L_9 = ___base_level3;
		if (!L_9)
		{
			goto IL_00ea;
		}
	}
	{
		int32_t L_10 = V_0;
		if (L_10)
		{
			goto IL_00ea;
		}
	}
	{
		float L_11 = ___height0;
		PlatformPool_t3559850805 * L_12 = __this->get_pool_7();
		NullCheck(L_12);
		Vector2_t2243707579  L_13 = PlatformPool_get_left_tile_size_m2896263910(L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		float L_14 = (&V_1)->get_y_1();
		Vector2__ctor_m847450945((&___last_platform2), (0.0f), ((float)((float)((float)((float)((-((float)((float)L_11/(float)(2.0f)))))+(float)((float)((float)L_14/(float)(2.0f)))))+(float)(0.1f))), /*hidden argument*/NULL);
		int32_t L_15 = V_0;
		int32_t L_16 = L_15;
		RuntimeObject * L_17 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_16);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m2000667605(NULL /*static, unused*/, _stringLiteral1333771709, L_17, _stringLiteral372029317, /*hidden argument*/NULL);
		Vector2_t2243707579  L_19 = ___last_platform2;
		float L_20 = ___width1;
		GameObject_t1756533147 * L_21 = PlatformGenerator_GeneratePlatform_m470059935(NULL /*static, unused*/, L_18, L_19, L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		GameObject_t1756533147 * L_22 = V_2;
		NullCheck(L_22);
		Transform_t3275118058 * L_23 = GameObject_get_transform_m3490276752(L_22, /*hidden argument*/NULL);
		Transform_t3275118058 * L_24 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_set_parent_m3178143156(L_23, L_24, /*hidden argument*/NULL);
		List_1_t1125654279 * L_25 = __this->get_generated_platform_8();
		GameObject_t1756533147 * L_26 = V_2;
		NullCheck(L_25);
		List_1_Add_m3441471442(L_25, L_26, /*hidden argument*/List_1_Add_m3441471442_RuntimeMethod_var);
		goto IL_0390;
	}

IL_00ea:
	{
		float L_27 = ___width1;
		float L_28 = Random_Range_m4291245934(NULL /*static, unused*/, (1.0f), ((float)((float)L_27/(float)(4.0f))), /*hidden argument*/NULL);
		V_3 = L_28;
		int32_t L_29 = V_0;
		int32_t L_30 = L_29;
		RuntimeObject * L_31 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_30);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m2000667605(NULL /*static, unused*/, _stringLiteral1333771709, L_31, _stringLiteral372029317, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2243707579_il2cpp_TypeInfo_var);
		Vector2_t2243707579  L_33 = Vector2_get_zero_m1210615473(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_34 = V_3;
		GameObject_t1756533147 * L_35 = PlatformGenerator_GeneratePlatform_m470059935(NULL /*static, unused*/, L_32, L_33, L_34, /*hidden argument*/NULL);
		V_4 = L_35;
		GameObject_t1756533147 * L_36 = V_4;
		NullCheck(L_36);
		PlatformGenerator_t395172572 * L_37 = GameObject_GetComponent_TisPlatformGenerator_t395172572_m3551609875(L_36, /*hidden argument*/GameObject_GetComponent_TisPlatformGenerator_t395172572_m3551609875_RuntimeMethod_var);
		V_5 = L_37;
		Player2D_t165353355 * L_38 = __this->get_player_11();
		NullCheck(L_38);
		Bounds_t3033363703  L_39 = Controller2D_GetBounds_m1748675743(L_38, /*hidden argument*/NULL);
		V_7 = L_39;
		Vector3_t2243707580  L_40 = Bounds_get_size_m3951110137((&V_7), /*hidden argument*/NULL);
		V_8 = L_40;
		float L_41 = (&V_8)->get_y_2();
		PlatformPool_t3559850805 * L_42 = __this->get_pool_7();
		NullCheck(L_42);
		Vector2_t2243707579  L_43 = PlatformPool_get_left_tile_size_m2896263910(L_42, /*hidden argument*/NULL);
		V_9 = L_43;
		float L_44 = (&V_9)->get_y_1();
		V_6 = ((-((float)((float)((float)((float)L_41/(float)(2.0f)))+(float)((float)((float)L_44/(float)(2.0f)))))));
		PlatformPool_t3559850805 * L_45 = __this->get_pool_7();
		NullCheck(L_45);
		Vector2_t2243707579  L_46 = PlatformPool_get_mid_tile_size_m1657243535(L_45, /*hidden argument*/NULL);
		V_11 = L_46;
		float L_47 = (&V_11)->get_x_0();
		PlatformGenerator_t395172572 * L_48 = V_5;
		NullCheck(L_48);
		int32_t L_49 = PlatformGenerator_get_UsedTiles_m3574650352(L_48, /*hidden argument*/NULL);
		V_10 = ((float)((float)((float)((float)L_47*(float)(((float)((float)L_49)))))/(float)(3.0f)));
		Player2D_t165353355 * L_50 = __this->get_player_11();
		NullCheck(L_50);
		float L_51 = Controller2D_GetMaxJumpHeight_m1470428714(L_50, /*hidden argument*/NULL);
		Player2D_t165353355 * L_52 = __this->get_player_11();
		NullCheck(L_52);
		float L_53 = Controller2D_GetMaxJumpHeight_m1470428714(L_52, /*hidden argument*/NULL);
		float L_54 = V_6;
		V_12 = ((float)((float)((float)((float)L_51-(float)((float)((float)L_53*(float)(0.05f)))))+(float)L_54));
		Player2D_t165353355 * L_55 = __this->get_player_11();
		NullCheck(L_55);
		float L_56 = Controller2D_GetMinJumpHeight_m1114922044(L_55, /*hidden argument*/NULL);
		Player2D_t165353355 * L_57 = __this->get_player_11();
		NullCheck(L_57);
		float L_58 = Controller2D_GetMinJumpHeight_m1114922044(L_57, /*hidden argument*/NULL);
		V_13 = ((float)((float)L_56-(float)((float)((float)L_58*(float)(0.05f)))));
		float L_59 = V_12;
		float L_60 = MicroGenerator_get_difficulty_m1319765580(__this, /*hidden argument*/NULL);
		float L_61 = V_13;
		float L_62 = V_12;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_63 = Mathf_Clamp_m1779415170(NULL /*static, unused*/, ((float)((float)L_59*(float)L_60)), L_61, L_62, /*hidden argument*/NULL);
		V_14 = L_63;
		float L_64 = V_13;
		float L_65 = V_14;
		float L_66 = Random_Range_m4291245934(NULL /*static, unused*/, L_64, L_65, /*hidden argument*/NULL);
		V_15 = L_66;
		float L_67 = V_15;
		Player2D_t165353355 * L_68 = __this->get_player_11();
		NullCheck(L_68);
		float L_69 = Controller2D_Gravity_m418077541(L_68, /*hidden argument*/NULL);
		float L_70 = sqrtf(((float)((float)((float)((float)(2.0f)*(float)L_67))/(float)((-L_69)))));
		V_16 = L_70;
		Player2D_t165353355 * L_71 = __this->get_player_11();
		NullCheck(L_71);
		float L_72 = Controller2D_GetMoveSpeed_m1351954791(L_71, /*hidden argument*/NULL);
		float L_73 = V_16;
		float L_74 = V_10;
		V_17 = ((float)((float)((float)((float)((float)((float)L_72*(float)L_73))*(float)(2.0f)))+(float)L_74));
		float L_75 = V_17;
		float L_76 = MicroGenerator_get_difficulty_m1319765580(__this, /*hidden argument*/NULL);
		float L_77 = V_17;
		float L_78 = Mathf_Clamp_m1779415170(NULL /*static, unused*/, ((float)((float)L_75*(float)L_76)), (0.0f), L_77, /*hidden argument*/NULL);
		V_17 = L_78;
		float L_79 = Random_Range_m4291245934(NULL /*static, unused*/, (-1.0f), (1.0f), /*hidden argument*/NULL);
		float L_80 = Mathf_Sign_m3240469006(NULL /*static, unused*/, L_79, /*hidden argument*/NULL);
		V_18 = L_80;
		float L_81 = V_17;
		float L_82 = V_18;
		V_17 = ((float)((float)L_81*(float)L_82));
		PlatformPool_t3559850805 * L_83 = __this->get_pool_7();
		NullCheck(L_83);
		Vector2_t2243707579  L_84 = PlatformPool_get_left_tile_size_m2896263910(L_83, /*hidden argument*/NULL);
		V_20 = L_84;
		float L_85 = (&V_20)->get_x_0();
		float L_86 = V_18;
		V_19 = ((float)((float)L_85*(float)L_86));
		PlatformPool_t3559850805 * L_87 = __this->get_pool_7();
		NullCheck(L_87);
		Vector2_t2243707579  L_88 = PlatformPool_get_left_tile_size_m2896263910(L_87, /*hidden argument*/NULL);
		V_22 = L_88;
		float L_89 = (&V_22)->get_y_1();
		V_21 = L_89;
		float L_90 = V_17;
		float L_91 = V_19;
		V_17 = ((float)((float)L_90+(float)L_91));
		float L_92 = V_15;
		float L_93 = V_21;
		V_15 = ((float)((float)L_92+(float)L_93));
		Vector2_t2243707579  L_94 = ___last_platform2;
		float L_95 = V_17;
		float L_96 = V_15;
		Vector2_t2243707579  L_97;
		memset(&L_97, 0, sizeof(L_97));
		Vector2__ctor_m847450945((&L_97), L_95, L_96, /*hidden argument*/NULL);
		Vector2_t2243707579  L_98 = Vector2_op_Addition_m4229571528(NULL /*static, unused*/, L_94, L_97, /*hidden argument*/NULL);
		___last_platform2 = L_98;
		GameObject_t1756533147 * L_99 = V_4;
		NullCheck(L_99);
		Transform_t3275118058 * L_100 = GameObject_get_transform_m3490276752(L_99, /*hidden argument*/NULL);
		Vector2_t2243707579  L_101 = ___last_platform2;
		Vector3_t2243707580  L_102 = Vector2_op_Implicit_m129629632(NULL /*static, unused*/, L_101, /*hidden argument*/NULL);
		NullCheck(L_100);
		Transform_set_position_m2942701431(L_100, L_102, /*hidden argument*/NULL);
		float L_103 = MicroGenerator_get_difficulty_m1319765580(__this, /*hidden argument*/NULL);
		if ((!(((float)L_103) > ((float)(1.0f)))))
		{
			goto IL_0371;
		}
	}
	{
		float L_104 = MicroGenerator_get_difficulty_m1319765580(__this, /*hidden argument*/NULL);
		float L_105 = MicroGenerator_get_difficulty_m1319765580(__this, /*hidden argument*/NULL);
		float L_106 = Random_Range_m4291245934(NULL /*static, unused*/, ((float)((float)(-1.0f)*(float)L_104)), ((float)((float)(1.0f)*(float)((float)((float)L_105/(float)(2.0f))))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_107 = Mathf_Sign_m3240469006(NULL /*static, unused*/, L_106, /*hidden argument*/NULL);
		V_23 = (bool)((((float)L_107) == ((float)(-1.0f)))? 1 : 0);
		bool L_108 = V_23;
		if (!L_108)
		{
			goto IL_0371;
		}
	}
	{
		GameObject_t1756533147 * L_109 = V_4;
		NullCheck(L_109);
		SpriteRendererU5BU5D_t1098056643* L_110 = GameObject_GetComponentsInChildren_TisSpriteRenderer_t1209076198_m2671756693(L_109, /*hidden argument*/GameObject_GetComponentsInChildren_TisSpriteRenderer_t1209076198_m2671756693_RuntimeMethod_var);
		V_25 = L_110;
		V_26 = 0;
		goto IL_035e;
	}

IL_030f:
	{
		SpriteRendererU5BU5D_t1098056643* L_111 = V_25;
		int32_t L_112 = V_26;
		NullCheck(L_111);
		int32_t L_113 = L_112;
		SpriteRenderer_t1209076198 * L_114 = (L_111)->GetAt(static_cast<il2cpp_array_size_t>(L_113));
		V_24 = L_114;
		SpriteRenderer_t1209076198 * L_115 = V_24;
		SpriteRenderer_t1209076198 * L_116 = V_24;
		NullCheck(L_116);
		Color_t2020392075  L_117 = SpriteRenderer_get_color_m4269640491(L_116, /*hidden argument*/NULL);
		V_27 = L_117;
		float L_118 = (&V_27)->get_r_0();
		SpriteRenderer_t1209076198 * L_119 = V_24;
		NullCheck(L_119);
		Color_t2020392075  L_120 = SpriteRenderer_get_color_m4269640491(L_119, /*hidden argument*/NULL);
		V_28 = L_120;
		float L_121 = (&V_28)->get_b_2();
		SpriteRenderer_t1209076198 * L_122 = V_24;
		NullCheck(L_122);
		Color_t2020392075  L_123 = SpriteRenderer_get_color_m4269640491(L_122, /*hidden argument*/NULL);
		V_29 = L_123;
		float L_124 = (&V_29)->get_g_1();
		Color_t2020392075  L_125;
		memset(&L_125, 0, sizeof(L_125));
		Color__ctor_m3831565866((&L_125), ((float)((float)L_118+(float)(30.0f))), L_121, L_124, /*hidden argument*/NULL);
		NullCheck(L_115);
		SpriteRenderer_set_color_m675324012(L_115, L_125, /*hidden argument*/NULL);
		int32_t L_126 = V_26;
		V_26 = ((int32_t)((int32_t)L_126+(int32_t)1));
	}

IL_035e:
	{
		int32_t L_127 = V_26;
		SpriteRendererU5BU5D_t1098056643* L_128 = V_25;
		NullCheck(L_128);
		if ((((int32_t)L_127) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_128)->max_length)))))))
		{
			goto IL_030f;
		}
	}
	{
		GameObject_t1756533147 * L_129 = V_4;
		NullCheck(L_129);
		GameObject_AddComponent_TisFallingPlatform_t2590793300_m4057477800(L_129, /*hidden argument*/GameObject_AddComponent_TisFallingPlatform_t2590793300_m4057477800_RuntimeMethod_var);
	}

IL_0371:
	{
		GameObject_t1756533147 * L_130 = V_4;
		NullCheck(L_130);
		Transform_t3275118058 * L_131 = GameObject_get_transform_m3490276752(L_130, /*hidden argument*/NULL);
		Transform_t3275118058 * L_132 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		NullCheck(L_131);
		Transform_set_parent_m3178143156(L_131, L_132, /*hidden argument*/NULL);
		List_1_t1125654279 * L_133 = __this->get_generated_platform_8();
		GameObject_t1756533147 * L_134 = V_4;
		NullCheck(L_133);
		List_1_Add_m3441471442(L_133, L_134, /*hidden argument*/List_1_Add_m3441471442_RuntimeMethod_var);
	}

IL_0390:
	{
		int32_t L_135 = V_0;
		V_0 = ((int32_t)((int32_t)L_135+(int32_t)1));
	}

IL_0394:
	{
		int32_t L_136 = V_0;
		int32_t L_137 = MicroGenerator_get_blocks_in_level_m3243240612(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_136) < ((int32_t)L_137)))
		{
			goto IL_006a;
		}
	}
	{
		GameObject_t1756533147 * L_138 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		NullCheck(L_138);
		BoxCollider2D_t948534547 * L_139 = GameObject_AddComponent_TisBoxCollider2D_t948534547_m2942557550(L_138, /*hidden argument*/GameObject_AddComponent_TisBoxCollider2D_t948534547_m2942557550_RuntimeMethod_var);
		V_30 = L_139;
		BoxCollider2D_t948534547 * L_140 = V_30;
		float L_141 = ___width1;
		Bounds_t3033363703  L_142 = MicroGenerator_GetBounds_m2624662798(__this, /*hidden argument*/NULL);
		V_31 = L_142;
		Vector3_t2243707580  L_143 = Bounds_get_size_m3951110137((&V_31), /*hidden argument*/NULL);
		V_32 = L_143;
		float L_144 = (&V_32)->get_y_2();
		Vector3_t2243707580  L_145;
		memset(&L_145, 0, sizeof(L_145));
		Vector3__ctor_m2761639014((&L_145), L_141, L_144, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2243707579_il2cpp_TypeInfo_var);
		Vector2_t2243707579  L_146 = Vector2_op_Implicit_m385881926(NULL /*static, unused*/, L_145, /*hidden argument*/NULL);
		NullCheck(L_140);
		BoxCollider2D_set_size_m30428549(L_140, L_146, /*hidden argument*/NULL);
		BoxCollider2D_t948534547 * L_147 = V_30;
		Bounds_t3033363703  L_148 = MicroGenerator_GetBounds_m2624662798(__this, /*hidden argument*/NULL);
		V_33 = L_148;
		Vector3_t2243707580  L_149 = Bounds_get_center_m2113902127((&V_33), /*hidden argument*/NULL);
		Vector2_t2243707579  L_150 = Vector2_op_Implicit_m385881926(NULL /*static, unused*/, L_149, /*hidden argument*/NULL);
		NullCheck(L_147);
		Collider2D_set_offset_m3644752562(L_147, L_150, /*hidden argument*/NULL);
		BoxCollider2D_t948534547 * L_151 = V_30;
		NullCheck(L_151);
		Collider2D_set_isTrigger_m3424244684(L_151, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_152 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		int32_t L_153 = LayerMask_NameToLayer_m3213714080(NULL /*static, unused*/, _stringLiteral1761669571, /*hidden argument*/NULL);
		NullCheck(L_152);
		GameObject_set_layer_m3642810622(L_152, L_153, /*hidden argument*/NULL);
		__this->set_has_created_level_9((bool)1);
	}

IL_0416:
	{
		Vector2_t2243707579  L_154 = ___last_platform2;
		return L_154;
	}
}
// UnityEngine.Bounds MicroGenerator::GetBounds()
extern "C"  Bounds_t3033363703  MicroGenerator_GetBounds_m2624662798 (MicroGenerator_t2658307363 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MicroGenerator_GetBounds_m2624662798_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SpriteRendererU5BU5D_t1098056643* V_0 = NULL;
	Bounds_t3033363703  V_1;
	memset(&V_1, 0, sizeof(V_1));
	SpriteRenderer_t1209076198 * V_2 = NULL;
	SpriteRendererU5BU5D_t1098056643* V_3 = NULL;
	int32_t V_4 = 0;
	{
		SpriteRendererU5BU5D_t1098056643* L_0 = Component_GetComponentsInChildren_TisSpriteRenderer_t1209076198_m1456864797(__this, /*hidden argument*/Component_GetComponentsInChildren_TisSpriteRenderer_t1209076198_m1456864797_RuntimeMethod_var);
		V_0 = L_0;
		SpriteRendererU5BU5D_t1098056643* L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = 0;
		SpriteRenderer_t1209076198 * L_3 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		Bounds_t3033363703  L_4 = Renderer_get_bounds_m2683206870(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		SpriteRendererU5BU5D_t1098056643* L_5 = Component_GetComponentsInChildren_TisSpriteRenderer_t1209076198_m1456864797(__this, /*hidden argument*/Component_GetComponentsInChildren_TisSpriteRenderer_t1209076198_m1456864797_RuntimeMethod_var);
		V_3 = L_5;
		V_4 = 0;
		goto IL_0054;
	}

IL_001f:
	{
		SpriteRendererU5BU5D_t1098056643* L_6 = V_3;
		int32_t L_7 = V_4;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		SpriteRenderer_t1209076198 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_2 = L_9;
		SpriteRenderer_t1209076198 * L_10 = V_2;
		NullCheck(L_10);
		Bounds_t3033363703  L_11 = Renderer_get_bounds_m2683206870(L_10, /*hidden argument*/NULL);
		Bounds_t3033363703  L_12 = L_11;
		RuntimeObject * L_13 = Box(Bounds_t3033363703_il2cpp_TypeInfo_var, &L_12);
		bool L_14 = Bounds_Equals_m497757122((&V_1), L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_004e;
		}
	}
	{
		SpriteRenderer_t1209076198 * L_15 = V_2;
		NullCheck(L_15);
		Bounds_t3033363703  L_16 = Renderer_get_bounds_m2683206870(L_15, /*hidden argument*/NULL);
		Bounds_Encapsulate_m2657927334((&V_1), L_16, /*hidden argument*/NULL);
	}

IL_004e:
	{
		int32_t L_17 = V_4;
		V_4 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0054:
	{
		int32_t L_18 = V_4;
		SpriteRendererU5BU5D_t1098056643* L_19 = V_3;
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_19)->max_length)))))))
		{
			goto IL_001f;
		}
	}
	{
		Bounds_t3033363703  L_20 = V_1;
		return L_20;
	}
}
// System.Void MicroGenerator::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void MicroGenerator_OnTriggerEnter2D_m1406339080 (MicroGenerator_t2658307363 * __this, Collider2D_t646061738 * ___collision0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MicroGenerator_OnTriggerEnter2D_m1406339080_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MacroGenerator_t1652377675 * V_0 = NULL;
	{
		bool L_0 = __this->get_has_created_another_level_10();
		if (L_0)
		{
			goto IL_0043;
		}
	}
	{
		Collider2D_t646061738 * L_1 = ___collision0;
		NullCheck(L_1);
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m2159020946(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = GameObject_get_tag_m3359901967(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Preferences_t60026258_il2cpp_TypeInfo_var);
		String_t* L_4 = ((Preferences_t60026258_StaticFields*)il2cpp_codegen_static_fields_for(Preferences_t60026258_il2cpp_TypeInfo_var))->get_player_tag_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0043;
		}
	}
	{
		GameObject_t1756533147 * L_6 = GameObject_FindGameObjectWithTag_m1433464258(NULL /*static, unused*/, _stringLiteral2380456193, /*hidden argument*/NULL);
		NullCheck(L_6);
		MacroGenerator_t1652377675 * L_7 = GameObject_GetComponent_TisMacroGenerator_t1652377675_m1524168656(L_6, /*hidden argument*/GameObject_GetComponent_TisMacroGenerator_t1652377675_m1524168656_RuntimeMethod_var);
		V_0 = L_7;
		MacroGenerator_t1652377675 * L_8 = V_0;
		NullCheck(L_8);
		MacroGenerator_CreateLevel_m2116947579(L_8, (bool)0, /*hidden argument*/NULL);
		__this->set_has_created_another_level_10((bool)1);
	}

IL_0043:
	{
		return;
	}
}
// System.Void Missile::.ctor()
extern "C"  void Missile__ctor_m2545921555 (Missile_t813944928 * __this, const RuntimeMethod* method)
{
	{
		Threat__ctor_m1337334961(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Missile::Start()
extern "C"  void Missile_Start_m208208487 (Missile_t813944928 * __this, const RuntimeMethod* method)
{
	{
		Controller2D_Start_m774250573(__this, /*hidden argument*/NULL);
		((Controller2D_t802485922 *)__this)->set_apply_gravity_18((bool)0);
		return;
	}
}
// System.Void Missile::Update()
extern "C"  void Missile_Update_m430249726 (Missile_t813944928 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Missile_Update_m430249726_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_exploded_21();
		if (L_0)
		{
			goto IL_0040;
		}
	}
	{
		Transform_t3275118058 * L_1 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_2 = L_1;
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_position_m2304215762(L_2, /*hidden argument*/NULL);
		Transform_t3275118058 * L_4 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Transform_get_up_m1990654328(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_6 = Vector3_op_Multiply_m2498445460(NULL /*static, unused*/, L_5, (7.0f), /*hidden argument*/NULL);
		float L_7 = Time_get_deltaTime_m3925508629(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t2243707580  L_8 = Vector3_op_Multiply_m2498445460(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3_t2243707580  L_9 = Vector3_op_Addition_m394909128(NULL /*static, unused*/, L_3, L_8, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_position_m2942701431(L_2, L_9, /*hidden argument*/NULL);
	}

IL_0040:
	{
		return;
	}
}
// System.Void Missile::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C"  void Missile_OnTriggerEnter2D_m2921101119 (Missile_t813944928 * __this, Collider2D_t646061738 * ___collision0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Missile_OnTriggerEnter2D_m2921101119_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ParticleSystem_t3394631041 * V_0 = NULL;
	{
		Collider2D_t646061738 * L_0 = Component_GetComponent_TisCollider2D_t646061738_m2950827934(__this, /*hidden argument*/Component_GetComponent_TisCollider2D_t646061738_m2950827934_RuntimeMethod_var);
		NullCheck(L_0);
		Behaviour_set_enabled_m602406666(L_0, (bool)0, /*hidden argument*/NULL);
		__this->set_exploded_21((bool)1);
		SpriteRenderer_t1209076198 * L_1 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_RuntimeMethod_var);
		NullCheck(L_1);
		SpriteRenderer_set_sprite_m3307887080(L_1, (Sprite_t309593783 *)NULL, /*hidden argument*/NULL);
		ParticleSystem_t3394631041 * L_2 = Component_GetComponentInChildren_TisParticleSystem_t3394631041_m347610273(__this, /*hidden argument*/Component_GetComponentInChildren_TisParticleSystem_t3394631041_m347610273_RuntimeMethod_var);
		V_0 = L_2;
		ParticleSystem_t3394631041 * L_3 = V_0;
		NullCheck(L_3);
		ParticleSystem_Play_m2510524101(L_3, /*hidden argument*/NULL);
		Collider2D_t646061738 * L_4 = ___collision0;
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m2159020946(L_4, /*hidden argument*/NULL);
		Threat_Hit_m4230409352(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Missile::Damage()
extern "C"  int32_t Missile_Damage_m3204155024 (Missile_t813944928 * __this, const RuntimeMethod* method)
{
	{
		return 1;
	}
}
// System.Void Missile::Destroy()
extern "C"  void Missile_Destroy_m1179550945 (Missile_t813944928 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Missile_Destroy_m1179550945_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m682534386(NULL /*static, unused*/, L_0, (2.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Missile::OnBecameInvisible()
extern "C"  void Missile_OnBecameInvisible_m3876137554 (Missile_t813944928 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Missile_OnBecameInvisible_m3876137554_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m682534386(NULL /*static, unused*/, L_0, (2.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void OneWayPlatform::.ctor()
extern "C"  void OneWayPlatform__ctor_m3451239287 (OneWayPlatform_t3777955472 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OneWayPlatform::Start()
extern "C"  void OneWayPlatform_Start_m1213573435 (OneWayPlatform_t3777955472 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OneWayPlatform_Start_m1213573435_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Bounds_t3033363703  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Bounds_t3033363703  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Preferences_t60026258_il2cpp_TypeInfo_var);
		String_t* L_0 = ((Preferences_t60026258_StaticFields*)il2cpp_codegen_static_fields_for(Preferences_t60026258_il2cpp_TypeInfo_var))->get_player_tag_2();
		GameObject_t1756533147 * L_1 = GameObject_FindGameObjectWithTag_m1433464258(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Player2D_t165353355 * L_2 = GameObject_GetComponent_TisPlayer2D_t165353355_m1754251190(L_1, /*hidden argument*/GameObject_GetComponent_TisPlayer2D_t165353355_m1754251190_RuntimeMethod_var);
		__this->set_player_7(L_2);
		Player2D_t165353355 * L_3 = __this->get_player_7();
		NullCheck(L_3);
		Bounds_t3033363703  L_4 = Controller2D_GetBounds_m1748675743(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		Vector3_t2243707580  L_5 = Bounds_get_min_m3351470040((&V_1), /*hidden argument*/NULL);
		V_0 = L_5;
		BoxCollider2D_t948534547 * L_6 = __this->get_collider_5();
		NullCheck(L_6);
		Bounds_t3033363703  L_7 = Collider2D_get_bounds_m242690441(L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		Vector3_t2243707580  L_8 = Bounds_get_max_m1939845278((&V_2), /*hidden argument*/NULL);
		V_3 = L_8;
		float L_9 = (&V_3)->get_y_2();
		__this->set_collider_top_6(L_9);
		return;
	}
}
// System.Void OneWayPlatform::Update()
extern "C"  void OneWayPlatform_Update_m3913346926 (OneWayPlatform_t3777955472 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (OneWayPlatform_Update_m3913346926_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	Vector2_t2243707579  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Bounds_t3033363703  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_t2243707579  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Bounds_t3033363703  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector2_t2243707579  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector2_t2243707579  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Bounds_t3033363703  V_10;
	memset(&V_10, 0, sizeof(V_10));
	Vector3_t2243707580  V_11;
	memset(&V_11, 0, sizeof(V_11));
	{
		Camera_t189460977 * L_0 = Camera_get_main_m881971336(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = GameObject_get_transform_m3490276752(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_position_m2304215762(L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t2243707580  L_4 = Camera_WorldToViewportPoint_m311010509(L_0, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = (&V_0)->get_y_2();
		V_1 = (bool)((((float)L_5) < ((float)(0.0f)))? 1 : 0);
		bool L_6 = V_1;
		if (!L_6)
		{
			goto IL_003b;
		}
	}
	{
		GameObject_t1756533147 * L_7 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m3959286051(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_003b:
	{
		Player2D_t165353355 * L_8 = __this->get_player_7();
		NullCheck(L_8);
		Bounds_t3033363703  L_9 = Controller2D_GetBounds_m1748675743(L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		Vector3_t2243707580  L_10 = Bounds_get_min_m3351470040((&V_3), /*hidden argument*/NULL);
		V_4 = L_10;
		float L_11 = (&V_4)->get_y_2();
		Vector2__ctor_m847450945((&V_2), (-100.0f), L_11, /*hidden argument*/NULL);
		Player2D_t165353355 * L_12 = __this->get_player_7();
		NullCheck(L_12);
		Bounds_t3033363703  L_13 = Controller2D_GetBounds_m1748675743(L_12, /*hidden argument*/NULL);
		V_6 = L_13;
		Vector3_t2243707580  L_14 = Bounds_get_min_m3351470040((&V_6), /*hidden argument*/NULL);
		V_7 = L_14;
		float L_15 = (&V_7)->get_y_2();
		Vector2__ctor_m847450945((&V_5), (100.0f), L_15, /*hidden argument*/NULL);
		Vector2_t2243707579  L_16 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2243707579_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_17 = Vector2_op_Implicit_m129629632(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		Vector2_t2243707579  L_18 = V_5;
		Vector3_t2243707580  L_19 = Vector2_op_Implicit_m129629632(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		Color_t2020392075  L_20 = Color_get_black_m455352838(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_DrawLine_m1261213347(NULL /*static, unused*/, L_17, L_19, L_20, /*hidden argument*/NULL);
		float L_21 = __this->get_collider_top_6();
		Vector2__ctor_m847450945((&V_8), (-100.0f), ((float)((float)L_21-(float)(0.35f))), /*hidden argument*/NULL);
		float L_22 = __this->get_collider_top_6();
		Vector2__ctor_m847450945((&V_9), (100.0f), ((float)((float)L_22-(float)(0.35f))), /*hidden argument*/NULL);
		Vector2_t2243707579  L_23 = V_8;
		Vector3_t2243707580  L_24 = Vector2_op_Implicit_m129629632(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		Vector2_t2243707579  L_25 = V_9;
		Vector3_t2243707580  L_26 = Vector2_op_Implicit_m129629632(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		Color_t2020392075  L_27 = Color_get_green_m3094897666(NULL /*static, unused*/, /*hidden argument*/NULL);
		Debug_DrawLine_m1261213347(NULL /*static, unused*/, L_24, L_26, L_27, /*hidden argument*/NULL);
		float L_28 = __this->get_collider_top_6();
		Player2D_t165353355 * L_29 = __this->get_player_7();
		NullCheck(L_29);
		Bounds_t3033363703  L_30 = Controller2D_GetBounds_m1748675743(L_29, /*hidden argument*/NULL);
		V_10 = L_30;
		Vector3_t2243707580  L_31 = Bounds_get_min_m3351470040((&V_10), /*hidden argument*/NULL);
		V_11 = L_31;
		float L_32 = (&V_11)->get_y_2();
		if ((!(((float)((float)((float)L_28-(float)(0.35f)))) > ((float)L_32))))
		{
			goto IL_0133;
		}
	}
	{
		GameObject_t1756533147 * L_33 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		int32_t L_34 = LayerMask_NameToLayer_m3213714080(NULL /*static, unused*/, _stringLiteral2096328193, /*hidden argument*/NULL);
		NullCheck(L_33);
		GameObject_set_layer_m3642810622(L_33, L_34, /*hidden argument*/NULL);
		goto IL_0148;
	}

IL_0133:
	{
		GameObject_t1756533147 * L_35 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		int32_t L_36 = LayerMask_NameToLayer_m3213714080(NULL /*static, unused*/, _stringLiteral1086556859, /*hidden argument*/NULL);
		NullCheck(L_35);
		GameObject_set_layer_m3642810622(L_35, L_36, /*hidden argument*/NULL);
	}

IL_0148:
	{
		return;
	}
}
// System.Void PlatformGenerator::.ctor()
extern "C"  void PlatformGenerator__ctor_m3101191299 (PlatformGenerator_t395172572 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 PlatformGenerator::get_UsedTiles()
extern "C"  int32_t PlatformGenerator_get_UsedTiles_m3574650352 (PlatformGenerator_t395172572 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CUsedTilesU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void PlatformGenerator::set_UsedTiles(System.Int32)
extern "C"  void PlatformGenerator_set_UsedTiles_m4242393899 (PlatformGenerator_t395172572 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CUsedTilesU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void PlatformGenerator::Create()
extern "C"  void PlatformGenerator_Create_m875964463 (PlatformGenerator_t395172572 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformGenerator_Create_m875964463_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	Vector2_t2243707579  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	int32_t V_7 = 0;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	GameObject_t1756533147 * V_10 = NULL;
	int32_t V_11 = 0;
	GameObject_t1756533147 * V_12 = NULL;
	GameObject_t1756533147 * V_13 = NULL;
	GameObject_t1756533147 * V_14 = NULL;
	GameObject_t1756533147 * V_15 = NULL;
	{
		PlatformPool_t3559850805 * L_0 = __this->get_pool_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m2516226135(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		GameObject_t1756533147 * L_2 = GameObject_FindGameObjectWithTag_m1433464258(NULL /*static, unused*/, _stringLiteral4287929323, /*hidden argument*/NULL);
		NullCheck(L_2);
		PlatformPool_t3559850805 * L_3 = GameObject_GetComponent_TisPlatformPool_t3559850805_m831201488(L_2, /*hidden argument*/GameObject_GetComponent_TisPlatformPool_t3559850805_m831201488_RuntimeMethod_var);
		__this->set_pool_5(L_3);
	}

IL_0026:
	{
		List_1_t1125654279 * L_4 = __this->get_platform_tiles_4();
		if (L_4)
		{
			goto IL_003c;
		}
	}
	{
		List_1_t1125654279 * L_5 = (List_1_t1125654279 *)il2cpp_codegen_object_new(List_1_t1125654279_il2cpp_TypeInfo_var);
		List_1__ctor_m704351054(L_5, /*hidden argument*/List_1__ctor_m704351054_RuntimeMethod_var);
		__this->set_platform_tiles_4(L_5);
	}

IL_003c:
	{
		float L_6 = __this->get_approximate_width_2();
		if ((!(((float)L_6) > ((float)(0.0f)))))
		{
			goto IL_02e1;
		}
	}
	{
		PlatformPool_t3559850805 * L_7 = __this->get_pool_5();
		NullCheck(L_7);
		Vector2_t2243707579  L_8 = PlatformPool_get_left_tile_size_m2896263910(L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		float L_9 = (&V_1)->get_x_0();
		V_0 = L_9;
		PlatformPool_t3559850805 * L_10 = __this->get_pool_5();
		NullCheck(L_10);
		Vector2_t2243707579  L_11 = PlatformPool_get_mid_tile_size_m1657243535(L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		float L_12 = (&V_3)->get_x_0();
		V_2 = L_12;
		PlatformPool_t3559850805 * L_13 = __this->get_pool_5();
		NullCheck(L_13);
		Vector2_t2243707579  L_14 = PlatformPool_get_right_tile_size_m2644186623(L_13, /*hidden argument*/NULL);
		V_5 = L_14;
		float L_15 = (&V_5)->get_x_0();
		V_4 = L_15;
		float L_16 = V_0;
		float L_17 = V_4;
		V_6 = ((float)((float)L_16+(float)L_17));
		goto IL_009b;
	}

IL_0095:
	{
		float L_18 = V_6;
		float L_19 = V_2;
		V_6 = ((float)((float)L_18+(float)L_19));
	}

IL_009b:
	{
		float L_20 = V_6;
		float L_21 = __this->get_approximate_width_2();
		if ((((float)L_20) < ((float)L_21)))
		{
			goto IL_0095;
		}
	}
	{
		float L_22 = V_6;
		float L_23 = V_0;
		float L_24 = V_4;
		float L_25 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_26 = Mathf_RoundToInt_m1833913145(NULL /*static, unused*/, ((float)((float)((float)((float)L_22-(float)((float)((float)L_23+(float)L_24))))/(float)L_25)), /*hidden argument*/NULL);
		V_7 = L_26;
		int32_t L_27 = V_7;
		PlatformGenerator_set_UsedTiles_m4242393899(__this, ((int32_t)((int32_t)L_27+(int32_t)2)), /*hidden argument*/NULL);
		int32_t L_28 = V_7;
		if ((((int32_t)L_28) <= ((int32_t)0)))
		{
			goto IL_021c;
		}
	}
	{
		int32_t L_29 = V_7;
		V_8 = ((float)((float)(((float)((float)L_29)))/(float)(2.0f)));
		float L_30 = V_0;
		float L_31 = V_8;
		float L_32 = V_2;
		float L_33 = V_2;
		V_9 = ((float)((float)((float)((float)((-L_30))-(float)((float)((float)L_31*(float)L_32))))+(float)((float)((float)L_33/(float)(2.0f)))));
		PlatformPool_t3559850805 * L_34 = __this->get_pool_5();
		GameObject_t1756533147 * L_35 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		NullCheck(L_34);
		GameObject_t1756533147 * L_36 = PlatformPool_GetTileWithType_m1945733132(L_34, L_35, 0, /*hidden argument*/NULL);
		V_10 = L_36;
		GameObject_t1756533147 * L_37 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_38 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_37, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_0136;
		}
	}
	{
		GameObject_t1756533147 * L_39 = V_10;
		NullCheck(L_39);
		Transform_t3275118058 * L_40 = GameObject_get_transform_m3490276752(L_39, /*hidden argument*/NULL);
		float L_41 = V_9;
		Vector2_t2243707579  L_42;
		memset(&L_42, 0, sizeof(L_42));
		Vector2__ctor_m847450945((&L_42), L_41, (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2243707579_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_43 = Vector2_op_Implicit_m129629632(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		NullCheck(L_40);
		Transform_set_localPosition_m1073050816(L_40, L_43, /*hidden argument*/NULL);
		List_1_t1125654279 * L_44 = __this->get_platform_tiles_4();
		GameObject_t1756533147 * L_45 = V_10;
		NullCheck(L_44);
		List_1_Add_m3441471442(L_44, L_45, /*hidden argument*/List_1_Add_m3441471442_RuntimeMethod_var);
		goto IL_0140;
	}

IL_0136:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3299155069(NULL /*static, unused*/, _stringLiteral2395017070, /*hidden argument*/NULL);
	}

IL_0140:
	{
		float L_46 = V_9;
		float L_47 = V_0;
		V_9 = ((float)((float)L_46+(float)L_47));
		V_11 = 1;
		goto IL_01b4;
	}

IL_014e:
	{
		PlatformPool_t3559850805 * L_48 = __this->get_pool_5();
		GameObject_t1756533147 * L_49 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		NullCheck(L_48);
		GameObject_t1756533147 * L_50 = PlatformPool_GetTileWithType_m1945733132(L_48, L_49, 1, /*hidden argument*/NULL);
		V_12 = L_50;
		GameObject_t1756533147 * L_51 = V_12;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_52 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_51, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_019e;
		}
	}
	{
		GameObject_t1756533147 * L_53 = V_12;
		NullCheck(L_53);
		Transform_t3275118058 * L_54 = GameObject_get_transform_m3490276752(L_53, /*hidden argument*/NULL);
		float L_55 = V_9;
		Vector2_t2243707579  L_56;
		memset(&L_56, 0, sizeof(L_56));
		Vector2__ctor_m847450945((&L_56), L_55, (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2243707579_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_57 = Vector2_op_Implicit_m129629632(NULL /*static, unused*/, L_56, /*hidden argument*/NULL);
		NullCheck(L_54);
		Transform_set_localPosition_m1073050816(L_54, L_57, /*hidden argument*/NULL);
		List_1_t1125654279 * L_58 = __this->get_platform_tiles_4();
		GameObject_t1756533147 * L_59 = V_12;
		NullCheck(L_58);
		List_1_Add_m3441471442(L_58, L_59, /*hidden argument*/List_1_Add_m3441471442_RuntimeMethod_var);
		goto IL_01a8;
	}

IL_019e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3299155069(NULL /*static, unused*/, _stringLiteral2328503377, /*hidden argument*/NULL);
	}

IL_01a8:
	{
		float L_60 = V_9;
		float L_61 = V_2;
		V_9 = ((float)((float)L_60+(float)L_61));
		int32_t L_62 = V_11;
		V_11 = ((int32_t)((int32_t)L_62+(int32_t)1));
	}

IL_01b4:
	{
		int32_t L_63 = V_11;
		int32_t L_64 = V_7;
		if ((((int32_t)L_63) <= ((int32_t)L_64)))
		{
			goto IL_014e;
		}
	}
	{
		PlatformPool_t3559850805 * L_65 = __this->get_pool_5();
		GameObject_t1756533147 * L_66 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		NullCheck(L_65);
		GameObject_t1756533147 * L_67 = PlatformPool_GetTileWithType_m1945733132(L_65, L_66, 2, /*hidden argument*/NULL);
		V_13 = L_67;
		GameObject_t1756533147 * L_68 = V_13;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_69 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_68, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_69)
		{
			goto IL_020d;
		}
	}
	{
		GameObject_t1756533147 * L_70 = V_13;
		NullCheck(L_70);
		Transform_t3275118058 * L_71 = GameObject_get_transform_m3490276752(L_70, /*hidden argument*/NULL);
		float L_72 = V_9;
		Vector2_t2243707579  L_73;
		memset(&L_73, 0, sizeof(L_73));
		Vector2__ctor_m847450945((&L_73), L_72, (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2243707579_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_74 = Vector2_op_Implicit_m129629632(NULL /*static, unused*/, L_73, /*hidden argument*/NULL);
		NullCheck(L_71);
		Transform_set_localPosition_m1073050816(L_71, L_74, /*hidden argument*/NULL);
		List_1_t1125654279 * L_75 = __this->get_platform_tiles_4();
		GameObject_t1756533147 * L_76 = V_13;
		NullCheck(L_75);
		List_1_Add_m3441471442(L_75, L_76, /*hidden argument*/List_1_Add_m3441471442_RuntimeMethod_var);
		goto IL_0217;
	}

IL_020d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3299155069(NULL /*static, unused*/, _stringLiteral2241924114, /*hidden argument*/NULL);
	}

IL_0217:
	{
		goto IL_02dc;
	}

IL_021c:
	{
		PlatformPool_t3559850805 * L_77 = __this->get_pool_5();
		GameObject_t1756533147 * L_78 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		NullCheck(L_77);
		GameObject_t1756533147 * L_79 = PlatformPool_GetTileWithType_m1945733132(L_77, L_78, 0, /*hidden argument*/NULL);
		V_14 = L_79;
		GameObject_t1756533147 * L_80 = V_14;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_81 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_80, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_81)
		{
			goto IL_0272;
		}
	}
	{
		GameObject_t1756533147 * L_82 = V_14;
		NullCheck(L_82);
		Transform_t3275118058 * L_83 = GameObject_get_transform_m3490276752(L_82, /*hidden argument*/NULL);
		float L_84 = V_0;
		Vector2_t2243707579  L_85;
		memset(&L_85, 0, sizeof(L_85));
		Vector2__ctor_m847450945((&L_85), ((float)((float)((-L_84))/(float)(2.0f))), (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2243707579_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_86 = Vector2_op_Implicit_m129629632(NULL /*static, unused*/, L_85, /*hidden argument*/NULL);
		NullCheck(L_83);
		Transform_set_localPosition_m1073050816(L_83, L_86, /*hidden argument*/NULL);
		List_1_t1125654279 * L_87 = __this->get_platform_tiles_4();
		GameObject_t1756533147 * L_88 = V_14;
		NullCheck(L_87);
		List_1_Add_m3441471442(L_87, L_88, /*hidden argument*/List_1_Add_m3441471442_RuntimeMethod_var);
		goto IL_027c;
	}

IL_0272:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3299155069(NULL /*static, unused*/, _stringLiteral2395017070, /*hidden argument*/NULL);
	}

IL_027c:
	{
		PlatformPool_t3559850805 * L_89 = __this->get_pool_5();
		GameObject_t1756533147 * L_90 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		NullCheck(L_89);
		GameObject_t1756533147 * L_91 = PlatformPool_GetTileWithType_m1945733132(L_89, L_90, 2, /*hidden argument*/NULL);
		V_15 = L_91;
		GameObject_t1756533147 * L_92 = V_15;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_93 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_92, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_93)
		{
			goto IL_02d2;
		}
	}
	{
		GameObject_t1756533147 * L_94 = V_15;
		NullCheck(L_94);
		Transform_t3275118058 * L_95 = GameObject_get_transform_m3490276752(L_94, /*hidden argument*/NULL);
		float L_96 = V_4;
		Vector2_t2243707579  L_97;
		memset(&L_97, 0, sizeof(L_97));
		Vector2__ctor_m847450945((&L_97), ((float)((float)L_96/(float)(2.0f))), (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2243707579_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_98 = Vector2_op_Implicit_m129629632(NULL /*static, unused*/, L_97, /*hidden argument*/NULL);
		NullCheck(L_95);
		Transform_set_localPosition_m1073050816(L_95, L_98, /*hidden argument*/NULL);
		List_1_t1125654279 * L_99 = __this->get_platform_tiles_4();
		GameObject_t1756533147 * L_100 = V_15;
		NullCheck(L_99);
		List_1_Add_m3441471442(L_99, L_100, /*hidden argument*/List_1_Add_m3441471442_RuntimeMethod_var);
		goto IL_02dc;
	}

IL_02d2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3299155069(NULL /*static, unused*/, _stringLiteral2241924114, /*hidden argument*/NULL);
	}

IL_02dc:
	{
		goto IL_02fb;
	}

IL_02e1:
	{
		float L_101 = __this->get_approximate_width_2();
		if ((!(((float)L_101) < ((float)(0.0f)))))
		{
			goto IL_02fb;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m1382493163(NULL /*static, unused*/, _stringLiteral1393335811, /*hidden argument*/NULL);
	}

IL_02fb:
	{
		return;
	}
}
// UnityEngine.Bounds PlatformGenerator::GetBounds()
extern "C"  Bounds_t3033363703  PlatformGenerator_GetBounds_m3188841413 (PlatformGenerator_t395172572 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformGenerator_GetBounds_m3188841413_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SpriteRenderer_t1209076198 * V_0 = NULL;
	Bounds_t3033363703  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	SpriteRenderer_t1209076198 * V_3 = NULL;
	Bounds_t3033363703  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		List_1_t1125654279 * L_0 = __this->get_platform_tiles_4();
		NullCheck(L_0);
		int32_t L_1 = List_1_get_Count_m2764296230(L_0, /*hidden argument*/List_1_get_Count_m2764296230_RuntimeMethod_var);
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0084;
		}
	}
	{
		List_1_t1125654279 * L_2 = __this->get_platform_tiles_4();
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = List_1_get_Item_m939767277(L_2, 0, /*hidden argument*/List_1_get_Item_m939767277_RuntimeMethod_var);
		NullCheck(L_3);
		SpriteRenderer_t1209076198 * L_4 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_3, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_RuntimeMethod_var);
		V_0 = L_4;
		SpriteRenderer_t1209076198 * L_5 = V_0;
		NullCheck(L_5);
		Bounds_t3033363703  L_6 = Renderer_get_bounds_m2683206870(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		V_2 = 0;
		goto IL_0071;
	}

IL_0031:
	{
		List_1_t1125654279 * L_7 = __this->get_platform_tiles_4();
		int32_t L_8 = V_2;
		NullCheck(L_7);
		GameObject_t1756533147 * L_9 = List_1_get_Item_m939767277(L_7, L_8, /*hidden argument*/List_1_get_Item_m939767277_RuntimeMethod_var);
		NullCheck(L_9);
		SpriteRenderer_t1209076198 * L_10 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_9, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_RuntimeMethod_var);
		V_3 = L_10;
		SpriteRenderer_t1209076198 * L_11 = V_3;
		NullCheck(L_11);
		Bounds_t3033363703  L_12 = Renderer_get_bounds_m2683206870(L_11, /*hidden argument*/NULL);
		Bounds_t3033363703  L_13 = L_12;
		RuntimeObject * L_14 = Box(Bounds_t3033363703_il2cpp_TypeInfo_var, &L_13);
		bool L_15 = Bounds_Equals_m497757122((&V_1), L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_006d;
		}
	}
	{
		SpriteRenderer_t1209076198 * L_16 = V_3;
		NullCheck(L_16);
		Bounds_t3033363703  L_17 = Renderer_get_bounds_m2683206870(L_16, /*hidden argument*/NULL);
		Bounds_Encapsulate_m2657927334((&V_1), L_17, /*hidden argument*/NULL);
	}

IL_006d:
	{
		int32_t L_18 = V_2;
		V_2 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0071:
	{
		int32_t L_19 = V_2;
		List_1_t1125654279 * L_20 = __this->get_platform_tiles_4();
		NullCheck(L_20);
		int32_t L_21 = List_1_get_Count_m2764296230(L_20, /*hidden argument*/List_1_get_Count_m2764296230_RuntimeMethod_var);
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_0031;
		}
	}
	{
		Bounds_t3033363703  L_22 = V_1;
		return L_22;
	}

IL_0084:
	{
		Initobj (Bounds_t3033363703_il2cpp_TypeInfo_var, (&V_4));
		Bounds_t3033363703  L_23 = V_4;
		return L_23;
	}
}
// System.Void PlatformGenerator::OnDestroy()
extern "C"  void PlatformGenerator_OnDestroy_m3031313454 (PlatformGenerator_t395172572 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformGenerator_OnDestroy_m3031313454_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		PlatformPool_t3559850805 * L_0 = __this->get_pool_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		PlatformGenerator_ReturnTiles_m2688380672(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void PlatformGenerator::ReturnTiles()
extern "C"  void PlatformGenerator_ReturnTiles_m2688380672 (PlatformGenerator_t395172572 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformGenerator_ReturnTiles_m2688380672_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0022;
	}

IL_0007:
	{
		PlatformPool_t3559850805 * L_0 = __this->get_pool_5();
		List_1_t1125654279 * L_1 = __this->get_platform_tiles_4();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		GameObject_t1756533147 * L_3 = List_1_get_Item_m939767277(L_1, L_2, /*hidden argument*/List_1_get_Item_m939767277_RuntimeMethod_var);
		NullCheck(L_0);
		PlatformPool_ReturnTile_m1498587706(L_0, L_3, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_0022:
	{
		int32_t L_5 = V_0;
		List_1_t1125654279 * L_6 = __this->get_platform_tiles_4();
		NullCheck(L_6);
		int32_t L_7 = List_1_get_Count_m2764296230(L_6, /*hidden argument*/List_1_get_Count_m2764296230_RuntimeMethod_var);
		if ((((int32_t)L_5) < ((int32_t)L_7)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// UnityEngine.GameObject PlatformGenerator::GeneratePlatform(System.String,UnityEngine.Vector2,System.Single)
extern "C"  GameObject_t1756533147 * PlatformGenerator_GeneratePlatform_m470059935 (RuntimeObject * __this /* static, unused */, String_t* ___name0, Vector2_t2243707579  ___pos1, float ___width2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformGenerator_GeneratePlatform_m470059935_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	PlatformGenerator_t395172572 * V_1 = NULL;
	BoxCollider2D_t948534547 * V_2 = NULL;
	Bounds_t3033363703  V_3;
	memset(&V_3, 0, sizeof(V_3));
	OneWayPlatform_t3777955472 * V_4 = NULL;
	ParticleSystemU5BU5D_t1490986844* V_5 = NULL;
	ParticleSystem_t3394631041 * V_6 = NULL;
	ParticleSystemU5BU5D_t1490986844* V_7 = NULL;
	int32_t V_8 = 0;
	{
		String_t* L_0 = ___name0;
		GameObject_t1756533147 * L_1 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m4283735819(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t1756533147 * L_2 = V_0;
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = GameObject_get_transform_m3490276752(L_2, /*hidden argument*/NULL);
		Vector2_t2243707579  L_4 = ___pos1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2243707579_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_5 = Vector2_op_Implicit_m129629632(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_position_m2942701431(L_3, L_5, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_6 = V_0;
		NullCheck(L_6);
		PlatformGenerator_t395172572 * L_7 = GameObject_AddComponent_TisPlatformGenerator_t395172572_m1088084192(L_6, /*hidden argument*/GameObject_AddComponent_TisPlatformGenerator_t395172572_m1088084192_RuntimeMethod_var);
		V_1 = L_7;
		PlatformGenerator_t395172572 * L_8 = V_1;
		float L_9 = ___width2;
		NullCheck(L_8);
		L_8->set_approximate_width_2(L_9);
		PlatformGenerator_t395172572 * L_10 = V_1;
		NullCheck(L_10);
		PlatformGenerator_Create_m875964463(L_10, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = V_0;
		NullCheck(L_11);
		BoxCollider2D_t948534547 * L_12 = GameObject_AddComponent_TisBoxCollider2D_t948534547_m2942557550(L_11, /*hidden argument*/GameObject_AddComponent_TisBoxCollider2D_t948534547_m2942557550_RuntimeMethod_var);
		V_2 = L_12;
		BoxCollider2D_t948534547 * L_13 = V_2;
		PlatformGenerator_t395172572 * L_14 = V_1;
		NullCheck(L_14);
		Bounds_t3033363703  L_15 = PlatformGenerator_GetBounds_m3188841413(L_14, /*hidden argument*/NULL);
		V_3 = L_15;
		Vector3_t2243707580  L_16 = Bounds_get_size_m3951110137((&V_3), /*hidden argument*/NULL);
		Vector3_t2243707580  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Vector3__ctor_m2761639014((&L_17), (0.2f), (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_18 = Vector3_op_Addition_m394909128(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		Vector2_t2243707579  L_19 = Vector2_op_Implicit_m385881926(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		NullCheck(L_13);
		BoxCollider2D_set_size_m30428549(L_13, L_19, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_20 = V_0;
		NullCheck(L_20);
		OneWayPlatform_t3777955472 * L_21 = GameObject_AddComponent_TisOneWayPlatform_t3777955472_m2849326318(L_20, /*hidden argument*/GameObject_AddComponent_TisOneWayPlatform_t3777955472_m2849326318_RuntimeMethod_var);
		V_4 = L_21;
		OneWayPlatform_t3777955472 * L_22 = V_4;
		BoxCollider2D_t948534547 * L_23 = V_2;
		NullCheck(L_22);
		L_22->set_collider_5(L_23);
		GameObject_t1756533147 * L_24 = V_0;
		NullCheck(L_24);
		ParticleSystemU5BU5D_t1490986844* L_25 = GameObject_GetComponentsInChildren_TisParticleSystem_t3394631041_m2199618894(L_24, /*hidden argument*/GameObject_GetComponentsInChildren_TisParticleSystem_t3394631041_m2199618894_RuntimeMethod_var);
		V_5 = L_25;
		ParticleSystemU5BU5D_t1490986844* L_26 = V_5;
		V_7 = L_26;
		V_8 = 0;
		goto IL_00c4;
	}

IL_0084:
	{
		ParticleSystemU5BU5D_t1490986844* L_27 = V_7;
		int32_t L_28 = V_8;
		NullCheck(L_27);
		int32_t L_29 = L_28;
		ParticleSystem_t3394631041 * L_30 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		V_6 = L_30;
		ParticleSystem_t3394631041 * L_31 = V_6;
		NullCheck(L_31);
		String_t* L_32 = Component_get_tag_m124558427(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		bool L_33 = String_Equals_m2633592423(L_32, _stringLiteral4243273451, /*hidden argument*/NULL);
		if (L_33)
		{
			goto IL_00b7;
		}
	}
	{
		ParticleSystem_t3394631041 * L_34 = V_6;
		NullCheck(L_34);
		String_t* L_35 = Component_get_tag_m124558427(L_34, /*hidden argument*/NULL);
		NullCheck(L_35);
		bool L_36 = String_Equals_m2633592423(L_35, _stringLiteral2566379072, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_00be;
		}
	}

IL_00b7:
	{
		ParticleSystem_t3394631041 * L_37 = V_6;
		NullCheck(L_37);
		ParticleSystem_Stop_m3868680149(L_37, /*hidden argument*/NULL);
	}

IL_00be:
	{
		int32_t L_38 = V_8;
		V_8 = ((int32_t)((int32_t)L_38+(int32_t)1));
	}

IL_00c4:
	{
		int32_t L_39 = V_8;
		ParticleSystemU5BU5D_t1490986844* L_40 = V_7;
		NullCheck(L_40);
		if ((((int32_t)L_39) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_40)->max_length)))))))
		{
			goto IL_0084;
		}
	}
	{
		GameObject_t1756533147 * L_41 = V_0;
		return L_41;
	}
}
// System.Void PlatformPool::.ctor()
extern "C"  void PlatformPool__ctor_m1831357396 (PlatformPool_t3559850805 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 PlatformPool::get_left_tile_size()
extern "C"  Vector2_t2243707579  PlatformPool_get_left_tile_size_m2896263910 (PlatformPool_t3559850805 * __this, const RuntimeMethod* method)
{
	{
		Vector2_t2243707579  L_0 = __this->get_U3Cleft_tile_sizeU3Ek__BackingField_12();
		return L_0;
	}
}
// System.Void PlatformPool::set_left_tile_size(UnityEngine.Vector2)
extern "C"  void PlatformPool_set_left_tile_size_m2354458551 (PlatformPool_t3559850805 * __this, Vector2_t2243707579  ___value0, const RuntimeMethod* method)
{
	{
		Vector2_t2243707579  L_0 = ___value0;
		__this->set_U3Cleft_tile_sizeU3Ek__BackingField_12(L_0);
		return;
	}
}
// UnityEngine.Vector2 PlatformPool::get_mid_tile_size()
extern "C"  Vector2_t2243707579  PlatformPool_get_mid_tile_size_m1657243535 (PlatformPool_t3559850805 * __this, const RuntimeMethod* method)
{
	{
		Vector2_t2243707579  L_0 = __this->get_U3Cmid_tile_sizeU3Ek__BackingField_13();
		return L_0;
	}
}
// System.Void PlatformPool::set_mid_tile_size(UnityEngine.Vector2)
extern "C"  void PlatformPool_set_mid_tile_size_m2925512842 (PlatformPool_t3559850805 * __this, Vector2_t2243707579  ___value0, const RuntimeMethod* method)
{
	{
		Vector2_t2243707579  L_0 = ___value0;
		__this->set_U3Cmid_tile_sizeU3Ek__BackingField_13(L_0);
		return;
	}
}
// UnityEngine.Vector2 PlatformPool::get_right_tile_size()
extern "C"  Vector2_t2243707579  PlatformPool_get_right_tile_size_m2644186623 (PlatformPool_t3559850805 * __this, const RuntimeMethod* method)
{
	{
		Vector2_t2243707579  L_0 = __this->get_U3Cright_tile_sizeU3Ek__BackingField_14();
		return L_0;
	}
}
// System.Void PlatformPool::set_right_tile_size(UnityEngine.Vector2)
extern "C"  void PlatformPool_set_right_tile_size_m1029267146 (PlatformPool_t3559850805 * __this, Vector2_t2243707579  ___value0, const RuntimeMethod* method)
{
	{
		Vector2_t2243707579  L_0 = ___value0;
		__this->set_U3Cright_tile_sizeU3Ek__BackingField_14(L_0);
		return;
	}
}
// System.Void PlatformPool::Awake()
extern "C"  void PlatformPool_Awake_m1199261169 (PlatformPool_t3559850805 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformPool_Awake_m1199261169_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	GameObject_t1756533147 * V_1 = NULL;
	GameObject_t1756533147 * V_2 = NULL;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	GameObject_t1756533147 * V_5 = NULL;
	GameObject_t1756533147 * V_6 = NULL;
	GameObject_t1756533147 * V_7 = NULL;
	{
		RuntimeObject * L_0 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_0, /*hidden argument*/NULL);
		__this->set__lock_11(L_0);
		List_1_t1125654279 * L_1 = (List_1_t1125654279 *)il2cpp_codegen_object_new(List_1_t1125654279_il2cpp_TypeInfo_var);
		List_1__ctor_m4211463620(L_1, ((int32_t)50), /*hidden argument*/List_1__ctor_m4211463620_RuntimeMethod_var);
		__this->set_left_tiles_8(L_1);
		List_1_t1125654279 * L_2 = (List_1_t1125654279 *)il2cpp_codegen_object_new(List_1_t1125654279_il2cpp_TypeInfo_var);
		List_1__ctor_m4211463620(L_2, ((int32_t)100), /*hidden argument*/List_1__ctor_m4211463620_RuntimeMethod_var);
		__this->set_mid_tiles_9(L_2);
		List_1_t1125654279 * L_3 = (List_1_t1125654279 *)il2cpp_codegen_object_new(List_1_t1125654279_il2cpp_TypeInfo_var);
		List_1__ctor_m4211463620(L_3, ((int32_t)50), /*hidden argument*/List_1__ctor_m4211463620_RuntimeMethod_var);
		__this->set_right_tiles_10(L_3);
		Object_t1021602117 * L_4 = Resources_Load_m3480536692(NULL /*static, unused*/, _stringLiteral1964570358, /*hidden argument*/NULL);
		V_0 = ((GameObject_t1756533147 *)IsInstSealed((RuntimeObject*)L_4, GameObject_t1756533147_il2cpp_TypeInfo_var));
		Object_t1021602117 * L_5 = Resources_Load_m3480536692(NULL /*static, unused*/, _stringLiteral511365757, /*hidden argument*/NULL);
		V_1 = ((GameObject_t1756533147 *)IsInstSealed((RuntimeObject*)L_5, GameObject_t1756533147_il2cpp_TypeInfo_var));
		Object_t1021602117 * L_6 = Resources_Load_m3480536692(NULL /*static, unused*/, _stringLiteral763868829, /*hidden argument*/NULL);
		V_2 = ((GameObject_t1756533147 *)IsInstSealed((RuntimeObject*)L_6, GameObject_t1756533147_il2cpp_TypeInfo_var));
		GameObject_t1756533147 * L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Equality_m2516226135(NULL /*static, unused*/, L_7, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0078;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3299155069(NULL /*static, unused*/, _stringLiteral3893686066, /*hidden argument*/NULL);
	}

IL_0078:
	{
		GameObject_t1756533147 * L_9 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Equality_m2516226135(NULL /*static, unused*/, L_9, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_008e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3299155069(NULL /*static, unused*/, _stringLiteral1695186766, /*hidden argument*/NULL);
	}

IL_008e:
	{
		GameObject_t1756533147 * L_11 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Equality_m2516226135(NULL /*static, unused*/, L_11, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00a4;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3299155069(NULL /*static, unused*/, _stringLiteral2859695323, /*hidden argument*/NULL);
	}

IL_00a4:
	{
		GameObject_t1756533147 * L_13 = V_0;
		NullCheck(L_13);
		SpriteRenderer_t1209076198 * L_14 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_13, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_RuntimeMethod_var);
		NullCheck(L_14);
		Color_t2020392075  L_15 = SpriteRenderer_get_color_m4269640491(L_14, /*hidden argument*/NULL);
		__this->set_original_color_15(L_15);
		GameObject_t1756533147 * L_16 = V_0;
		float L_17 = PlatformPool_WidthOf_m3300936485(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_18 = V_0;
		float L_19 = PlatformPool_HeightOf_m2336189100(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		Vector2_t2243707579  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Vector2__ctor_m847450945((&L_20), L_17, L_19, /*hidden argument*/NULL);
		PlatformPool_set_left_tile_size_m2354458551(__this, L_20, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_21 = V_1;
		float L_22 = PlatformPool_WidthOf_m3300936485(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_23 = V_1;
		float L_24 = PlatformPool_HeightOf_m2336189100(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		Vector2_t2243707579  L_25;
		memset(&L_25, 0, sizeof(L_25));
		Vector2__ctor_m847450945((&L_25), L_22, L_24, /*hidden argument*/NULL);
		PlatformPool_set_mid_tile_size_m2925512842(__this, L_25, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_26 = V_2;
		float L_27 = PlatformPool_WidthOf_m3300936485(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_28 = V_2;
		float L_29 = PlatformPool_HeightOf_m2336189100(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		Vector2_t2243707579  L_30;
		memset(&L_30, 0, sizeof(L_30));
		Vector2__ctor_m847450945((&L_30), L_27, L_29, /*hidden argument*/NULL);
		PlatformPool_set_right_tile_size_m1029267146(__this, L_30, /*hidden argument*/NULL);
		Int32U5BU5D_t3030399641* L_31 = ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_31, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1486305142____U24fieldU2D5CF7DBEB679CBFEC1FCB176A380E936AD360D7AA_0_FieldInfo_var), /*hidden argument*/NULL);
		V_3 = L_31;
		V_4 = 0;
		goto IL_01c2;
	}

IL_0114:
	{
		int32_t L_32 = V_4;
		if ((((int32_t)L_32) >= ((int32_t)((int32_t)50))))
		{
			goto IL_014c;
		}
	}
	{
		GameObject_t1756533147 * L_33 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_34 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_33, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_RuntimeMethod_var);
		V_5 = L_34;
		GameObject_t1756533147 * L_35 = V_5;
		NullCheck(L_35);
		GameObject_SetActive_m2693135142(L_35, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_36 = V_5;
		NullCheck(L_36);
		Transform_t3275118058 * L_37 = GameObject_get_transform_m3490276752(L_36, /*hidden argument*/NULL);
		Transform_t3275118058 * L_38 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		NullCheck(L_37);
		Transform_set_parent_m3178143156(L_37, L_38, /*hidden argument*/NULL);
		List_1_t1125654279 * L_39 = __this->get_left_tiles_8();
		GameObject_t1756533147 * L_40 = V_5;
		NullCheck(L_39);
		List_1_Add_m3441471442(L_39, L_40, /*hidden argument*/List_1_Add_m3441471442_RuntimeMethod_var);
	}

IL_014c:
	{
		int32_t L_41 = V_4;
		if ((((int32_t)L_41) >= ((int32_t)((int32_t)100))))
		{
			goto IL_0184;
		}
	}
	{
		GameObject_t1756533147 * L_42 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_43 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_42, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_RuntimeMethod_var);
		V_6 = L_43;
		GameObject_t1756533147 * L_44 = V_6;
		NullCheck(L_44);
		GameObject_SetActive_m2693135142(L_44, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_45 = V_6;
		NullCheck(L_45);
		Transform_t3275118058 * L_46 = GameObject_get_transform_m3490276752(L_45, /*hidden argument*/NULL);
		Transform_t3275118058 * L_47 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		NullCheck(L_46);
		Transform_set_parent_m3178143156(L_46, L_47, /*hidden argument*/NULL);
		List_1_t1125654279 * L_48 = __this->get_mid_tiles_9();
		GameObject_t1756533147 * L_49 = V_6;
		NullCheck(L_48);
		List_1_Add_m3441471442(L_48, L_49, /*hidden argument*/List_1_Add_m3441471442_RuntimeMethod_var);
	}

IL_0184:
	{
		int32_t L_50 = V_4;
		if ((((int32_t)L_50) >= ((int32_t)((int32_t)50))))
		{
			goto IL_01bc;
		}
	}
	{
		GameObject_t1756533147 * L_51 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_52 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_51, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_RuntimeMethod_var);
		V_7 = L_52;
		GameObject_t1756533147 * L_53 = V_7;
		NullCheck(L_53);
		GameObject_SetActive_m2693135142(L_53, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_54 = V_7;
		NullCheck(L_54);
		Transform_t3275118058 * L_55 = GameObject_get_transform_m3490276752(L_54, /*hidden argument*/NULL);
		Transform_t3275118058 * L_56 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		NullCheck(L_55);
		Transform_set_parent_m3178143156(L_55, L_56, /*hidden argument*/NULL);
		List_1_t1125654279 * L_57 = __this->get_right_tiles_10();
		GameObject_t1756533147 * L_58 = V_7;
		NullCheck(L_57);
		List_1_Add_m3441471442(L_57, L_58, /*hidden argument*/List_1_Add_m3441471442_RuntimeMethod_var);
	}

IL_01bc:
	{
		int32_t L_59 = V_4;
		V_4 = ((int32_t)((int32_t)L_59+(int32_t)1));
	}

IL_01c2:
	{
		int32_t L_60 = V_4;
		Int32U5BU5D_t3030399641* L_61 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_62 = Mathf_Max_m1012832217(NULL /*static, unused*/, L_61, /*hidden argument*/NULL);
		if ((((int32_t)L_60) < ((int32_t)L_62)))
		{
			goto IL_0114;
		}
	}
	{
		return;
	}
}
// System.Single PlatformPool::WidthOf(UnityEngine.GameObject)
extern "C"  float PlatformPool_WidthOf_m3300936485 (RuntimeObject * __this /* static, unused */, GameObject_t1756533147 * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformPool_WidthOf_m3300936485_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SpriteRenderer_t1209076198 * V_0 = NULL;
	Bounds_t3033363703  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		GameObject_t1756533147 * L_0 = ___obj0;
		NullCheck(L_0);
		SpriteRenderer_t1209076198 * L_1 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_0, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_RuntimeMethod_var);
		V_0 = L_1;
		SpriteRenderer_t1209076198 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		SpriteRenderer_t1209076198 * L_4 = V_0;
		NullCheck(L_4);
		Bounds_t3033363703  L_5 = Renderer_get_bounds_m2683206870(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Vector3_t2243707580  L_6 = Bounds_get_size_m3951110137((&V_1), /*hidden argument*/NULL);
		V_2 = L_6;
		float L_7 = (&V_2)->get_x_1();
		return L_7;
	}

IL_002a:
	{
		return (0.0f);
	}
}
// System.Single PlatformPool::HeightOf(UnityEngine.GameObject)
extern "C"  float PlatformPool_HeightOf_m2336189100 (RuntimeObject * __this /* static, unused */, GameObject_t1756533147 * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformPool_HeightOf_m2336189100_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SpriteRenderer_t1209076198 * V_0 = NULL;
	Bounds_t3033363703  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		GameObject_t1756533147 * L_0 = ___obj0;
		NullCheck(L_0);
		SpriteRenderer_t1209076198 * L_1 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_0, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_RuntimeMethod_var);
		V_0 = L_1;
		SpriteRenderer_t1209076198 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m3768854296(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		SpriteRenderer_t1209076198 * L_4 = V_0;
		NullCheck(L_4);
		Bounds_t3033363703  L_5 = Renderer_get_bounds_m2683206870(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Vector3_t2243707580  L_6 = Bounds_get_size_m3951110137((&V_1), /*hidden argument*/NULL);
		V_2 = L_6;
		float L_7 = (&V_2)->get_y_2();
		return L_7;
	}

IL_002a:
	{
		return (0.0f);
	}
}
// System.Collections.Generic.List`1<UnityEngine.GameObject> PlatformPool::GetPoolForType(PlatformType)
extern "C"  List_1_t1125654279 * PlatformPool_GetPoolForType_m1350965415 (PlatformPool_t3559850805 * __this, int32_t ___type0, const RuntimeMethod* method)
{
	List_1_t1125654279 * V_0 = NULL;
	{
		V_0 = (List_1_t1125654279 *)NULL;
		int32_t L_0 = ___type0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_0019;
			}
			case 1:
			{
				goto IL_0025;
			}
			case 2:
			{
				goto IL_0031;
			}
		}
	}
	{
		goto IL_003d;
	}

IL_0019:
	{
		List_1_t1125654279 * L_1 = __this->get_left_tiles_8();
		V_0 = L_1;
		goto IL_0042;
	}

IL_0025:
	{
		List_1_t1125654279 * L_2 = __this->get_mid_tiles_9();
		V_0 = L_2;
		goto IL_0042;
	}

IL_0031:
	{
		List_1_t1125654279 * L_3 = __this->get_right_tiles_10();
		V_0 = L_3;
		goto IL_0042;
	}

IL_003d:
	{
		goto IL_0042;
	}

IL_0042:
	{
		List_1_t1125654279 * L_4 = V_0;
		return L_4;
	}
}
// System.Void PlatformPool::ActivateTile(UnityEngine.GameObject,UnityEngine.GameObject)
extern "C"  void PlatformPool_ActivateTile_m4049973885 (PlatformPool_t3559850805 * __this, GameObject_t1756533147 * ___parent0, GameObject_t1756533147 * ___tile1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformPool_ActivateTile_m4049973885_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = ___tile1;
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = GameObject_get_transform_m3490276752(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t4030073918_il2cpp_TypeInfo_var);
		Quaternion_t4030073918  L_2 = Quaternion_get_identity_m443011477(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_localRotation_m3053695533(L_1, L_2, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = ___tile1;
		NullCheck(L_3);
		SpriteRenderer_t1209076198 * L_4 = GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631(L_3, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t1209076198_m1184556631_RuntimeMethod_var);
		Color_t2020392075  L_5 = __this->get_original_color_15();
		NullCheck(L_4);
		SpriteRenderer_set_color_m675324012(L_4, L_5, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_6 = ___tile1;
		NullCheck(L_6);
		Transform_t3275118058 * L_7 = GameObject_get_transform_m3490276752(L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = ___parent0;
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m3490276752(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_set_parent_m3178143156(L_7, L_9, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = ___tile1;
		NullCheck(L_10);
		GameObject_SetActive_m2693135142(L_10, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GameObject PlatformPool::GetTileWithType(UnityEngine.GameObject,PlatformType)
extern "C"  GameObject_t1756533147 * PlatformPool_GetTileWithType_m1945733132 (PlatformPool_t3559850805 * __this, GameObject_t1756533147 * ___parent0, int32_t ___type1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformPool_GetTileWithType_m1945733132_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	List_1_t1125654279 * V_1 = NULL;
	int32_t V_2 = 0;
	GameObject_t1756533147 * V_3 = NULL;
	GameObject_t1756533147 * V_4 = NULL;
	GameObject_t1756533147 * V_5 = NULL;
	GameObject_t1756533147 * V_6 = NULL;
	GameObject_t1756533147 * V_7 = NULL;
	GameObject_t1756533147 * V_8 = NULL;
	GameObject_t1756533147 * V_9 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		RuntimeObject * L_0 = __this->get__lock_11();
		V_0 = L_0;
		RuntimeObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			int32_t L_2 = ___type1;
			List_1_t1125654279 * L_3 = PlatformPool_GetPoolForType_m1350965415(__this, L_2, /*hidden argument*/NULL);
			V_1 = L_3;
			V_2 = 0;
			goto IL_013f;
		}

IL_001c:
		{
			List_1_t1125654279 * L_4 = V_1;
			int32_t L_5 = V_2;
			NullCheck(L_4);
			GameObject_t1756533147 * L_6 = List_1_get_Item_m939767277(L_4, L_5, /*hidden argument*/List_1_get_Item_m939767277_RuntimeMethod_var);
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			bool L_7 = Object_op_Equality_m2516226135(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
			if (!L_7)
			{
				goto IL_010e;
			}
		}

IL_002e:
		{
			int32_t L_8 = ___type1;
			switch (L_8)
			{
				case 0:
				{
					goto IL_0045;
				}
				case 1:
				{
					goto IL_0085;
				}
				case 2:
				{
					goto IL_00c7;
				}
			}
		}

IL_0040:
		{
			goto IL_0109;
		}

IL_0045:
		{
			Object_t1021602117 * L_9 = Resources_Load_m3480536692(NULL /*static, unused*/, _stringLiteral1964570358, /*hidden argument*/NULL);
			V_3 = ((GameObject_t1756533147 *)IsInstSealed((RuntimeObject*)L_9, GameObject_t1756533147_il2cpp_TypeInfo_var));
			GameObject_t1756533147 * L_10 = V_3;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			GameObject_t1756533147 * L_11 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_10, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_RuntimeMethod_var);
			V_4 = L_11;
			GameObject_t1756533147 * L_12 = V_4;
			NullCheck(L_12);
			GameObject_SetActive_m2693135142(L_12, (bool)0, /*hidden argument*/NULL);
			GameObject_t1756533147 * L_13 = V_4;
			NullCheck(L_13);
			Transform_t3275118058 * L_14 = GameObject_get_transform_m3490276752(L_13, /*hidden argument*/NULL);
			Transform_t3275118058 * L_15 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
			NullCheck(L_14);
			Transform_set_parent_m3178143156(L_14, L_15, /*hidden argument*/NULL);
			List_1_t1125654279 * L_16 = V_1;
			int32_t L_17 = V_2;
			GameObject_t1756533147 * L_18 = V_4;
			NullCheck(L_16);
			List_1_set_Item_m1811192197(L_16, L_17, L_18, /*hidden argument*/List_1_set_Item_m1811192197_RuntimeMethod_var);
			goto IL_010e;
		}

IL_0085:
		{
			Object_t1021602117 * L_19 = Resources_Load_m3480536692(NULL /*static, unused*/, _stringLiteral511365757, /*hidden argument*/NULL);
			V_5 = ((GameObject_t1756533147 *)IsInstSealed((RuntimeObject*)L_19, GameObject_t1756533147_il2cpp_TypeInfo_var));
			GameObject_t1756533147 * L_20 = V_5;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			GameObject_t1756533147 * L_21 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_20, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_RuntimeMethod_var);
			V_6 = L_21;
			GameObject_t1756533147 * L_22 = V_6;
			NullCheck(L_22);
			GameObject_SetActive_m2693135142(L_22, (bool)0, /*hidden argument*/NULL);
			GameObject_t1756533147 * L_23 = V_6;
			NullCheck(L_23);
			Transform_t3275118058 * L_24 = GameObject_get_transform_m3490276752(L_23, /*hidden argument*/NULL);
			Transform_t3275118058 * L_25 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
			NullCheck(L_24);
			Transform_set_parent_m3178143156(L_24, L_25, /*hidden argument*/NULL);
			List_1_t1125654279 * L_26 = V_1;
			int32_t L_27 = V_2;
			GameObject_t1756533147 * L_28 = V_6;
			NullCheck(L_26);
			List_1_set_Item_m1811192197(L_26, L_27, L_28, /*hidden argument*/List_1_set_Item_m1811192197_RuntimeMethod_var);
			goto IL_010e;
		}

IL_00c7:
		{
			Object_t1021602117 * L_29 = Resources_Load_m3480536692(NULL /*static, unused*/, _stringLiteral763868829, /*hidden argument*/NULL);
			V_7 = ((GameObject_t1756533147 *)IsInstSealed((RuntimeObject*)L_29, GameObject_t1756533147_il2cpp_TypeInfo_var));
			GameObject_t1756533147 * L_30 = V_7;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			GameObject_t1756533147 * L_31 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_30, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_RuntimeMethod_var);
			V_8 = L_31;
			GameObject_t1756533147 * L_32 = V_8;
			NullCheck(L_32);
			GameObject_SetActive_m2693135142(L_32, (bool)0, /*hidden argument*/NULL);
			GameObject_t1756533147 * L_33 = V_8;
			NullCheck(L_33);
			Transform_t3275118058 * L_34 = GameObject_get_transform_m3490276752(L_33, /*hidden argument*/NULL);
			Transform_t3275118058 * L_35 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
			NullCheck(L_34);
			Transform_set_parent_m3178143156(L_34, L_35, /*hidden argument*/NULL);
			List_1_t1125654279 * L_36 = V_1;
			int32_t L_37 = V_2;
			GameObject_t1756533147 * L_38 = V_8;
			NullCheck(L_36);
			List_1_set_Item_m1811192197(L_36, L_37, L_38, /*hidden argument*/List_1_set_Item_m1811192197_RuntimeMethod_var);
			goto IL_010e;
		}

IL_0109:
		{
			goto IL_010e;
		}

IL_010e:
		{
			List_1_t1125654279 * L_39 = V_1;
			int32_t L_40 = V_2;
			NullCheck(L_39);
			GameObject_t1756533147 * L_41 = List_1_get_Item_m939767277(L_39, L_40, /*hidden argument*/List_1_get_Item_m939767277_RuntimeMethod_var);
			NullCheck(L_41);
			bool L_42 = GameObject_get_activeInHierarchy_m2532098784(L_41, /*hidden argument*/NULL);
			if (L_42)
			{
				goto IL_013b;
			}
		}

IL_011f:
		{
			GameObject_t1756533147 * L_43 = ___parent0;
			List_1_t1125654279 * L_44 = V_1;
			int32_t L_45 = V_2;
			NullCheck(L_44);
			GameObject_t1756533147 * L_46 = List_1_get_Item_m939767277(L_44, L_45, /*hidden argument*/List_1_get_Item_m939767277_RuntimeMethod_var);
			PlatformPool_ActivateTile_m4049973885(__this, L_43, L_46, /*hidden argument*/NULL);
			List_1_t1125654279 * L_47 = V_1;
			int32_t L_48 = V_2;
			NullCheck(L_47);
			GameObject_t1756533147 * L_49 = List_1_get_Item_m939767277(L_47, L_48, /*hidden argument*/List_1_get_Item_m939767277_RuntimeMethod_var);
			V_9 = L_49;
			IL2CPP_LEAVE(0x15A, FINALLY_0153);
		}

IL_013b:
		{
			int32_t L_50 = V_2;
			V_2 = ((int32_t)((int32_t)L_50+(int32_t)1));
		}

IL_013f:
		{
			int32_t L_51 = V_2;
			List_1_t1125654279 * L_52 = V_1;
			NullCheck(L_52);
			int32_t L_53 = List_1_get_Count_m2764296230(L_52, /*hidden argument*/List_1_get_Count_m2764296230_RuntimeMethod_var);
			if ((((int32_t)L_51) < ((int32_t)L_53)))
			{
				goto IL_001c;
			}
		}

IL_014b:
		{
			V_9 = (GameObject_t1756533147 *)NULL;
			IL2CPP_LEAVE(0x15A, FINALLY_0153);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0153;
	}

FINALLY_0153:
	{ // begin finally (depth: 1)
		RuntimeObject * L_54 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(339)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(339)
	{
		IL2CPP_JUMP_TBL(0x15A, IL_015a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_015a:
	{
		GameObject_t1756533147 * L_55 = V_9;
		return L_55;
	}
}
// System.Void PlatformPool::ReturnTile(UnityEngine.GameObject)
extern "C"  void PlatformPool_ReturnTile_m1498587706 (PlatformPool_t3559850805 * __this, GameObject_t1756533147 * ___tile0, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		RuntimeObject * L_0 = __this->get__lock_11();
		V_0 = L_0;
		RuntimeObject * L_1 = V_0;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		GameObject_t1756533147 * L_2 = ___tile0;
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = GameObject_get_transform_m3490276752(L_2, /*hidden argument*/NULL);
		Transform_t3275118058 * L_4 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_parent_m3178143156(L_3, L_4, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_5 = ___tile0;
		NullCheck(L_5);
		GameObject_SetActive_m2693135142(L_5, (bool)0, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x31, FINALLY_002a);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_002a;
	}

FINALLY_002a:
	{ // begin finally (depth: 1)
		RuntimeObject * L_6 = V_0;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(42)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(42)
	{
		IL2CPP_JUMP_TBL(0x31, IL_0031)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0031:
	{
		return;
	}
}
// System.Void Player2D::.ctor()
extern "C"  void Player2D__ctor_m2359055762 (Player2D_t165353355 * __this, const RuntimeMethod* method)
{
	{
		Controller2D__ctor_m1972723229(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Player2D::get_current_score()
extern "C"  int32_t Player2D_get_current_score_m3494890519 (Player2D_t165353355 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3Ccurrent_scoreU3Ek__BackingField_30();
		return L_0;
	}
}
// System.Void Player2D::set_current_score(System.Int32)
extern "C"  void Player2D_set_current_score_m4026685404 (Player2D_t165353355 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3Ccurrent_scoreU3Ek__BackingField_30(L_0);
		return;
	}
}
// System.Void Player2D::Start()
extern "C"  void Player2D_Start_m995759222 (Player2D_t165353355 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Player2D_Start_m995759222_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Controller2D_Start_m774250573(__this, /*hidden argument*/NULL);
		__this->set_health_24(3);
		__this->set_is_immune_25((bool)0);
		SpriteRenderer_t1209076198 * L_0 = Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m2178781570_RuntimeMethod_var);
		__this->set_sprite_renderer_23(L_0);
		SpriteRenderer_t1209076198 * L_1 = __this->get_sprite_renderer_23();
		NullCheck(L_1);
		Color_t2020392075  L_2 = SpriteRenderer_get_color_m4269640491(L_1, /*hidden argument*/NULL);
		__this->set_original_color_27(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t4030073918_il2cpp_TypeInfo_var);
		Quaternion_t4030073918  L_3 = Quaternion_get_identity_m443011477(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_target_rotation_22(L_3);
		Transform_t3275118058 * L_4 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Transform_get_position_m2304215762(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		float L_6 = (&V_0)->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_7 = Mathf_RoundToInt_m1833913145(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->set_score_offset_29(L_7);
		Player2D_set_current_score_m4026685404(__this, 0, /*hidden argument*/NULL);
		MonoBehaviour_InvokeRepeating_m505769937(__this, _stringLiteral2810074457, (0.0f), (0.001f), /*hidden argument*/NULL);
		MonoBehaviour_Invoke_m2201735803(__this, _stringLiteral360295816, (1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Player2D::RemoveSpawnEffect()
extern "C"  void Player2D_RemoveSpawnEffect_m2043079224 (Player2D_t165353355 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = Player2D_SpawnEffectFade_m1418828654(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2678710497(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Player2D::SpawnEffectFade()
extern "C"  RuntimeObject* Player2D_SpawnEffectFade_m1418828654 (Player2D_t165353355 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Player2D_SpawnEffectFade_m1418828654_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CSpawnEffectFadeU3Ec__Iterator0_t2652861317 * V_0 = NULL;
	{
		U3CSpawnEffectFadeU3Ec__Iterator0_t2652861317 * L_0 = (U3CSpawnEffectFadeU3Ec__Iterator0_t2652861317 *)il2cpp_codegen_object_new(U3CSpawnEffectFadeU3Ec__Iterator0_t2652861317_il2cpp_TypeInfo_var);
		U3CSpawnEffectFadeU3Ec__Iterator0__ctor_m3178076772(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSpawnEffectFadeU3Ec__Iterator0_t2652861317 * L_1 = V_0;
		return L_1;
	}
}
// System.Void Player2D::Update()
extern "C"  void Player2D_Update_m805001203 (Player2D_t165353355 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Player2D_Update_m805001203_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MoveMSG_t2690495395  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t G_B4_0 = 0;
	{
		Initobj (MoveMSG_t2690495395_il2cpp_TypeInfo_var, (&V_0));
		(&V_0)->set_jump_1(3);
		Input2D_t2938435410 * L_0 = __this->get_input_28();
		NullCheck(L_0);
		float L_1 = VirtFuncInvoker0< float >::Invoke(4 /* System.Single Input2D::Horizontal() */, L_0);
		(&V_0)->set_horizontal_0(L_1);
		float L_2 = (&V_0)->get_horizontal_0();
		if ((((float)L_2) == ((float)(0.0f))))
		{
			goto IL_0068;
		}
	}
	{
		bool L_3 = Controller2D_Direction_m18427550(__this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0044;
		}
	}
	{
		G_B4_0 = (-1);
		goto IL_0045;
	}

IL_0044:
	{
		G_B4_0 = 1;
	}

IL_0045:
	{
		V_1 = G_B4_0;
		int32_t L_4 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t4030073918_il2cpp_TypeInfo_var);
		Quaternion_t4030073918  L_5 = Quaternion_Euler_m868989838(NULL /*static, unused*/, (0.0f), (0.0f), ((float)((float)(((float)((float)L_4)))*(float)(20.0f))), /*hidden argument*/NULL);
		__this->set_target_rotation_22(L_5);
		goto IL_0073;
	}

IL_0068:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t4030073918_il2cpp_TypeInfo_var);
		Quaternion_t4030073918  L_6 = Quaternion_get_identity_m443011477(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_target_rotation_22(L_6);
	}

IL_0073:
	{
		Input2D_t2938435410 * L_7 = __this->get_input_28();
		NullCheck(L_7);
		bool L_8 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean Input2D::JumpButtonDown() */, L_7);
		if (!L_8)
		{
			goto IL_0096;
		}
	}
	{
		bool L_9 = Controller2D_IsGrounded_m2912631519(__this, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0096;
		}
	}
	{
		(&V_0)->set_jump_1(0);
	}

IL_0096:
	{
		Input2D_t2938435410 * L_10 = __this->get_input_28();
		NullCheck(L_10);
		bool L_11 = VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean Input2D::JumpButtonUp() */, L_10);
		if (!L_11)
		{
			goto IL_00ce;
		}
	}
	{
		float L_12 = Controller2D_MinJumpVelocity_m4136881028(__this, /*hidden argument*/NULL);
		V_2 = L_12;
		Rigidbody2D_t502193897 * L_13 = ((Controller2D_t802485922 *)__this)->get_rigidbody_17();
		NullCheck(L_13);
		Vector2_t2243707579  L_14 = Rigidbody2D_get_velocity_m1631921214(L_13, /*hidden argument*/NULL);
		V_3 = L_14;
		float L_15 = (&V_3)->get_y_1();
		float L_16 = V_2;
		if ((!(((float)L_15) > ((float)L_16))))
		{
			goto IL_00ce;
		}
	}
	{
		(&V_0)->set_jump_1(1);
	}

IL_00ce:
	{
		MoveMSG_t2690495395  L_17 = V_0;
		Controller2D_Move_m3918239025(__this, L_17, /*hidden argument*/NULL);
		Player2D_HandleScore_m3074900822(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Player2D::Flicker(System.Single)
extern "C"  RuntimeObject* Player2D_Flicker_m4174058381 (Player2D_t165353355 * __this, float ___flickerRate0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Player2D_Flicker_m4174058381_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CFlickerU3Ec__Iterator1_t3991061274 * V_0 = NULL;
	{
		U3CFlickerU3Ec__Iterator1_t3991061274 * L_0 = (U3CFlickerU3Ec__Iterator1_t3991061274 *)il2cpp_codegen_object_new(U3CFlickerU3Ec__Iterator1_t3991061274_il2cpp_TypeInfo_var);
		U3CFlickerU3Ec__Iterator1__ctor_m615044603(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFlickerU3Ec__Iterator1_t3991061274 * L_1 = V_0;
		float L_2 = ___flickerRate0;
		NullCheck(L_1);
		L_1->set_flickerRate_5(L_2);
		U3CFlickerU3Ec__Iterator1_t3991061274 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_6(__this);
		U3CFlickerU3Ec__Iterator1_t3991061274 * L_4 = V_0;
		return L_4;
	}
}
// System.Void Player2D::SustainDamage(System.Int32)
extern "C"  void Player2D_SustainDamage_m3638841263 (Player2D_t165353355 * __this, int32_t ___Damage0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Player2D_SustainDamage_m3638841263_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_is_immune_25();
		if (L_0)
		{
			goto IL_0078;
		}
	}
	{
		int32_t L_1 = __this->get_health_24();
		__this->set_health_24(((int32_t)((int32_t)L_1-(int32_t)1)));
		int32_t L_2 = __this->get_health_24();
		if (L_2)
		{
			goto IL_0031;
		}
	}
	{
		__this->set_quitting_31((bool)1);
		Player2D_EndGame_m4133746719(__this, /*hidden argument*/NULL);
	}

IL_0031:
	{
		__this->set_is_immune_25((bool)1);
		RuntimeObject* L_3 = Player2D_Flicker_m4174058381(__this, (0.1f), /*hidden argument*/NULL);
		Coroutine_t2299508840 * L_4 = MonoBehaviour_StartCoroutine_m2678710497(__this, L_3, /*hidden argument*/NULL);
		__this->set_flicker_coroutine_26(L_4);
		MonoBehaviour_Invoke_m2201735803(__this, _stringLiteral3026533180, (2.0f), /*hidden argument*/NULL);
		int32_t L_5 = LayerMask_NameToLayer_m3213714080(NULL /*static, unused*/, _stringLiteral1875862075, /*hidden argument*/NULL);
		int32_t L_6 = LayerMask_NameToLayer_m3213714080(NULL /*static, unused*/, _stringLiteral3342734734, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		Physics2D_IgnoreLayerCollision_m1669703862(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
	}

IL_0078:
	{
		return;
	}
}
// System.Void Player2D::RemoveImmunity()
extern "C"  void Player2D_RemoveImmunity_m3717843014 (Player2D_t165353355 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Player2D_RemoveImmunity_m3717843014_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Coroutine_t2299508840 * L_0 = __this->get_flicker_coroutine_26();
		MonoBehaviour_StopCoroutine_m606332095(__this, L_0, /*hidden argument*/NULL);
		SpriteRenderer_t1209076198 * L_1 = __this->get_sprite_renderer_23();
		Color_t2020392075  L_2 = __this->get_original_color_27();
		NullCheck(L_1);
		SpriteRenderer_set_color_m675324012(L_1, L_2, /*hidden argument*/NULL);
		__this->set_is_immune_25((bool)0);
		int32_t L_3 = LayerMask_NameToLayer_m3213714080(NULL /*static, unused*/, _stringLiteral1875862075, /*hidden argument*/NULL);
		int32_t L_4 = LayerMask_NameToLayer_m3213714080(NULL /*static, unused*/, _stringLiteral3342734734, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t2540166467_il2cpp_TypeInfo_var);
		Physics2D_IgnoreLayerCollision_m1690433947(NULL /*static, unused*/, L_3, L_4, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Player2D::TiltTowards()
extern "C"  void Player2D_TiltTowards_m26670375 (Player2D_t165353355 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Player2D_TiltTowards_m26670375_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_1 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Quaternion_t4030073918  L_2 = Transform_get_rotation_m2617026815(L_1, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_3 = __this->get_target_rotation_22();
		float L_4 = Time_get_deltaTime_m3925508629(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t4030073918_il2cpp_TypeInfo_var);
		Quaternion_t4030073918  L_5 = Quaternion_Slerp_m3187037907(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_rotation_m2824446320(L_0, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Player2D::HandleScore()
extern "C"  void Player2D_HandleScore_m3074900822 (Player2D_t165353355 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Player2D_HandleScore_m3074900822_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = Transform_get_position_m2304215762(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		float L_2 = (&V_1)->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_3 = Mathf_RoundToInt_m1833913145(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_score_offset_29();
		V_0 = ((int32_t)((int32_t)L_3-(int32_t)L_4));
		int32_t L_5 = V_0;
		int32_t L_6 = Player2D_get_current_score_m3494890519(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_5) <= ((int32_t)L_6)))
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_7 = V_0;
		Player2D_set_current_score_m4026685404(__this, L_7, /*hidden argument*/NULL);
	}

IL_0033:
	{
		return;
	}
}
// System.Void Player2D::EndGame()
extern "C"  void Player2D_EndGame_m4133746719 (Player2D_t165353355 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Player2D_EndGame_m4133746719_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LoadScene_t1133380052 * V_0 = NULL;
	{
		Camera_t189460977 * L_0 = Camera_get_main_m881971336(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m2159020946(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		LoadScene_t1133380052 * L_2 = GameObject_GetComponent_TisLoadScene_t1133380052_m1576248279(L_1, /*hidden argument*/GameObject_GetComponent_TisLoadScene_t1133380052_m1576248279_RuntimeMethod_var);
		V_0 = L_2;
		LoadScene_t1133380052 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m1757773010(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0022;
		}
	}
	{
		LoadScene_t1133380052 * L_5 = V_0;
		NullCheck(L_5);
		LoadScene_LoadSceneWithIndex_m216160262(L_5, 2, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void Player2D::OnBecameInvisible()
extern "C"  void Player2D_OnBecameInvisible_m2890919415 (Player2D_t165353355 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_quitting_31();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		Player2D_EndGame_m4133746719(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void Player2D::OnApplicationQuit()
extern "C"  void Player2D_OnApplicationQuit_m2946361028 (Player2D_t165353355 * __this, const RuntimeMethod* method)
{
	{
		__this->set_quitting_31((bool)1);
		return;
	}
}
// System.Int32 Player2D::GetMaxHealth()
extern "C"  int32_t Player2D_GetMaxHealth_m1138988990 (Player2D_t165353355 * __this, const RuntimeMethod* method)
{
	{
		return 3;
	}
}
// System.Int32 Player2D::GetHealth()
extern "C"  int32_t Player2D_GetHealth_m2808744642 (Player2D_t165353355 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_health_24();
		return L_0;
	}
}
// System.Void Player2D::SetInput(Input2D)
extern "C"  void Player2D_SetInput_m1210245776 (Player2D_t165353355 * __this, Input2D_t2938435410 * ____input0, const RuntimeMethod* method)
{
	{
		Input2D_t2938435410 * L_0 = ____input0;
		__this->set_input_28(L_0);
		return;
	}
}
// System.Void Player2D/<Flicker>c__Iterator1::.ctor()
extern "C"  void U3CFlickerU3Ec__Iterator1__ctor_m615044603 (U3CFlickerU3Ec__Iterator1_t3991061274 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Player2D/<Flicker>c__Iterator1::MoveNext()
extern "C"  bool U3CFlickerU3Ec__Iterator1_MoveNext_m4204913829 (U3CFlickerU3Ec__Iterator1_t3991061274 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CFlickerU3Ec__Iterator1_MoveNext_m4204913829_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	U3CFlickerU3Ec__Iterator1_t3991061274 * G_B6_0 = NULL;
	U3CFlickerU3Ec__Iterator1_t3991061274 * G_B5_0 = NULL;
	Color_t2020392075  G_B7_0;
	memset(&G_B7_0, 0, sizeof(G_B7_0));
	U3CFlickerU3Ec__Iterator1_t3991061274 * G_B7_1 = NULL;
	{
		int32_t L_0 = __this->get_U24PC_9();
		V_0 = L_0;
		__this->set_U24PC_9((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0025;
			}
			case 1:
			{
				goto IL_010a;
			}
			case 2:
			{
				goto IL_0160;
			}
		}
	}
	{
		goto IL_016c;
	}

IL_0025:
	{
		__this->set_U3CaltU3E__0_0((bool)0);
		Player2D_t165353355 * L_2 = __this->get_U24this_6();
		NullCheck(L_2);
		Color_t2020392075  L_3 = L_2->get_original_color_27();
		__this->set_U3CstartU3E__0_1(L_3);
		Player2D_t165353355 * L_4 = __this->get_U24this_6();
		NullCheck(L_4);
		Color_t2020392075 * L_5 = L_4->get_address_of_original_color_27();
		float L_6 = L_5->get_g_1();
		Player2D_t165353355 * L_7 = __this->get_U24this_6();
		NullCheck(L_7);
		Color_t2020392075 * L_8 = L_7->get_address_of_original_color_27();
		float L_9 = L_8->get_b_2();
		Color_t2020392075  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Color__ctor_m3831565866((&L_10), (1.0f), ((float)((float)L_6*(float)(0.5f))), ((float)((float)L_9*(float)(0.5f))), /*hidden argument*/NULL);
		__this->set_U3CendU3E__0_2(L_10);
	}

IL_0079:
	{
		__this->set_U3CtU3E__1_3((0.0f));
		goto IL_011c;
	}

IL_0089:
	{
		bool L_11 = __this->get_U3CaltU3E__0_0();
		G_B5_0 = __this;
		if (!L_11)
		{
			G_B6_0 = __this;
			goto IL_00b7;
		}
	}
	{
		Color_t2020392075  L_12 = __this->get_U3CendU3E__0_2();
		Color_t2020392075  L_13 = __this->get_U3CstartU3E__0_1();
		float L_14 = __this->get_U3CtU3E__1_3();
		Color_t2020392075  L_15 = Color_Lerp_m527444458(NULL /*static, unused*/, L_12, L_13, ((float)((float)L_14/(float)(0.2f))), /*hidden argument*/NULL);
		G_B7_0 = L_15;
		G_B7_1 = G_B5_0;
		goto IL_00d4;
	}

IL_00b7:
	{
		Color_t2020392075  L_16 = __this->get_U3CstartU3E__0_1();
		Color_t2020392075  L_17 = __this->get_U3CendU3E__0_2();
		float L_18 = __this->get_U3CtU3E__1_3();
		Color_t2020392075  L_19 = Color_Lerp_m527444458(NULL /*static, unused*/, L_16, L_17, ((float)((float)L_18/(float)(0.2f))), /*hidden argument*/NULL);
		G_B7_0 = L_19;
		G_B7_1 = G_B6_0;
	}

IL_00d4:
	{
		NullCheck(G_B7_1);
		G_B7_1->set_U3CchangedColorU3E__2_4(G_B7_0);
		Player2D_t165353355 * L_20 = __this->get_U24this_6();
		NullCheck(L_20);
		SpriteRenderer_t1209076198 * L_21 = L_20->get_sprite_renderer_23();
		Color_t2020392075  L_22 = __this->get_U3CchangedColorU3E__2_4();
		NullCheck(L_21);
		SpriteRenderer_set_color_m675324012(L_21, L_22, /*hidden argument*/NULL);
		__this->set_U24current_7(NULL);
		bool L_23 = __this->get_U24disposing_8();
		if (L_23)
		{
			goto IL_0105;
		}
	}
	{
		__this->set_U24PC_9(1);
	}

IL_0105:
	{
		goto IL_016e;
	}

IL_010a:
	{
		float L_24 = __this->get_U3CtU3E__1_3();
		float L_25 = Time_get_deltaTime_m3925508629(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CtU3E__1_3(((float)((float)L_24+(float)L_25)));
	}

IL_011c:
	{
		float L_26 = __this->get_U3CtU3E__1_3();
		if ((((float)L_26) < ((float)(0.2f))))
		{
			goto IL_0089;
		}
	}
	{
		bool L_27 = __this->get_U3CaltU3E__0_0();
		__this->set_U3CaltU3E__0_0((bool)((((int32_t)L_27) == ((int32_t)0))? 1 : 0));
		float L_28 = __this->get_flickerRate_5();
		WaitForSeconds_t3839502067 * L_29 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m632080402(L_29, L_28, /*hidden argument*/NULL);
		__this->set_U24current_7(L_29);
		bool L_30 = __this->get_U24disposing_8();
		if (L_30)
		{
			goto IL_015b;
		}
	}
	{
		__this->set_U24PC_9(2);
	}

IL_015b:
	{
		goto IL_016e;
	}

IL_0160:
	{
		goto IL_0079;
	}
	// Dead block : IL_0165: ldarg.0

IL_016c:
	{
		return (bool)0;
	}

IL_016e:
	{
		return (bool)1;
	}
}
// System.Object Player2D/<Flicker>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CFlickerU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2832745769 (U3CFlickerU3Ec__Iterator1_t3991061274 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_7();
		return L_0;
	}
}
// System.Object Player2D/<Flicker>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CFlickerU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3276002481 (U3CFlickerU3Ec__Iterator1_t3991061274 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_7();
		return L_0;
	}
}
// System.Void Player2D/<Flicker>c__Iterator1::Dispose()
extern "C"  void U3CFlickerU3Ec__Iterator1_Dispose_m3911192280 (U3CFlickerU3Ec__Iterator1_t3991061274 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_8((bool)1);
		__this->set_U24PC_9((-1));
		return;
	}
}
// System.Void Player2D/<Flicker>c__Iterator1::Reset()
extern "C"  void U3CFlickerU3Ec__Iterator1_Reset_m1687388178 (U3CFlickerU3Ec__Iterator1_t3991061274 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CFlickerU3Ec__Iterator1_Reset_m1687388178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Player2D/<SpawnEffectFade>c__Iterator0::.ctor()
extern "C"  void U3CSpawnEffectFadeU3Ec__Iterator0__ctor_m3178076772 (U3CSpawnEffectFadeU3Ec__Iterator0_t2652861317 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Player2D/<SpawnEffectFade>c__Iterator0::MoveNext()
extern "C"  bool U3CSpawnEffectFadeU3Ec__Iterator0_MoveNext_m2533121988 (U3CSpawnEffectFadeU3Ec__Iterator0_t2652861317 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSpawnEffectFadeU3Ec__Iterator0_MoveNext_m2533121988_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	MainModule_t6751348  V_1;
	memset(&V_1, 0, sizeof(V_1));
	MinMaxGradient_t3708298175  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		int32_t L_0 = __this->get_U24PC_9();
		V_0 = L_0;
		__this->set_U24PC_9((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0109;
			}
		}
	}
	{
		goto IL_0138;
	}

IL_0021:
	{
		__this->set_U3CdurationU3E__0_0((2.0f));
		GameObject_t1756533147 * L_2 = GameObject_FindGameObjectWithTag_m1433464258(NULL /*static, unused*/, _stringLiteral802309048, /*hidden argument*/NULL);
		NullCheck(L_2);
		ParticleSystem_t3394631041 * L_3 = GameObject_GetComponent_TisParticleSystem_t3394631041_m1329366749(L_2, /*hidden argument*/GameObject_GetComponent_TisParticleSystem_t3394631041_m1329366749_RuntimeMethod_var);
		__this->set_U3Cp_sysU3E__0_1(L_3);
		ParticleSystem_t3394631041 * L_4 = __this->get_U3Cp_sysU3E__0_1();
		NullCheck(L_4);
		MainModule_t6751348  L_5 = ParticleSystem_get_main_m2275307502(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		MinMaxGradient_t3708298175  L_6 = MainModule_get_startColor_m642758877((&V_1), /*hidden argument*/NULL);
		V_2 = L_6;
		Color_t2020392075  L_7 = MinMaxGradient_get_color_m1448527895((&V_2), /*hidden argument*/NULL);
		__this->set_U3CstartU3E__0_2(L_7);
		Color_t2020392075 * L_8 = __this->get_address_of_U3CstartU3E__0_2();
		float L_9 = L_8->get_r_0();
		Color_t2020392075 * L_10 = __this->get_address_of_U3CstartU3E__0_2();
		float L_11 = L_10->get_g_1();
		Color_t2020392075 * L_12 = __this->get_address_of_U3CstartU3E__0_2();
		float L_13 = L_12->get_b_2();
		Color_t2020392075  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Color__ctor_m2736804035((&L_14), L_9, L_11, L_13, (0.0f), /*hidden argument*/NULL);
		__this->set_U3CendU3E__0_3(L_14);
		__this->set_U3CtU3E__1_4((0.0f));
		goto IL_011b;
	}

IL_00a3:
	{
		Color_t2020392075  L_15 = __this->get_U3CstartU3E__0_2();
		Color_t2020392075  L_16 = __this->get_U3CendU3E__0_3();
		float L_17 = __this->get_U3CtU3E__1_4();
		float L_18 = __this->get_U3CdurationU3E__0_0();
		Color_t2020392075  L_19 = Color_Lerp_m527444458(NULL /*static, unused*/, L_15, L_16, ((float)((float)L_17/(float)L_18)), /*hidden argument*/NULL);
		__this->set_U3CchangedColorU3E__2_5(L_19);
		ParticleSystem_t3394631041 * L_20 = __this->get_U3Cp_sysU3E__0_1();
		NullCheck(L_20);
		MainModule_t6751348  L_21 = ParticleSystem_get_main_m2275307502(L_20, /*hidden argument*/NULL);
		__this->set_U3CmainU3E__2_6(L_21);
		MainModule_t6751348 * L_22 = __this->get_address_of_U3CmainU3E__2_6();
		Color_t2020392075  L_23 = __this->get_U3CchangedColorU3E__2_5();
		MinMaxGradient_t3708298175  L_24;
		memset(&L_24, 0, sizeof(L_24));
		MinMaxGradient__ctor_m2960432810((&L_24), L_23, /*hidden argument*/NULL);
		MainModule_set_startColor_m3844897932(L_22, L_24, /*hidden argument*/NULL);
		__this->set_U24current_7(NULL);
		bool L_25 = __this->get_U24disposing_8();
		if (L_25)
		{
			goto IL_0104;
		}
	}
	{
		__this->set_U24PC_9(1);
	}

IL_0104:
	{
		goto IL_013a;
	}

IL_0109:
	{
		float L_26 = __this->get_U3CtU3E__1_4();
		float L_27 = Time_get_deltaTime_m3925508629(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CtU3E__1_4(((float)((float)L_26+(float)L_27)));
	}

IL_011b:
	{
		float L_28 = __this->get_U3CtU3E__1_4();
		float L_29 = __this->get_U3CdurationU3E__0_0();
		if ((((float)L_28) < ((float)L_29)))
		{
			goto IL_00a3;
		}
	}
	{
		goto IL_0138;
	}
	// Dead block : IL_0131: ldarg.0

IL_0138:
	{
		return (bool)0;
	}

IL_013a:
	{
		return (bool)1;
	}
}
// System.Object Player2D/<SpawnEffectFade>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CSpawnEffectFadeU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2063952104 (U3CSpawnEffectFadeU3Ec__Iterator0_t2652861317 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_7();
		return L_0;
	}
}
// System.Object Player2D/<SpawnEffectFade>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CSpawnEffectFadeU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m260869408 (U3CSpawnEffectFadeU3Ec__Iterator0_t2652861317 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_7();
		return L_0;
	}
}
// System.Void Player2D/<SpawnEffectFade>c__Iterator0::Dispose()
extern "C"  void U3CSpawnEffectFadeU3Ec__Iterator0_Dispose_m4178715439 (U3CSpawnEffectFadeU3Ec__Iterator0_t2652861317 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_8((bool)1);
		__this->set_U24PC_9((-1));
		return;
	}
}
// System.Void Player2D/<SpawnEffectFade>c__Iterator0::Reset()
extern "C"  void U3CSpawnEffectFadeU3Ec__Iterator0_Reset_m2103006869 (U3CSpawnEffectFadeU3Ec__Iterator0_t2652861317 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CSpawnEffectFadeU3Ec__Iterator0_Reset_m2103006869_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Preferences::.ctor()
extern "C"  void Preferences__ctor_m516258723 (Preferences_t60026258 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Preferences::Awake()
extern "C"  void Preferences_Awake_m2221711938 (Preferences_t60026258 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Preferences_Awake_m2221711938_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m2159020946(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2658633409(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Preferences::.cctor()
extern "C"  void Preferences__cctor_m1876238888 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Preferences__cctor_m1876238888_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Preferences_t60026258_StaticFields*)il2cpp_codegen_static_fields_for(Preferences_t60026258_il2cpp_TypeInfo_var))->set_player_tag_2(_stringLiteral1875862075);
		((Preferences_t60026258_StaticFields*)il2cpp_codegen_static_fields_for(Preferences_t60026258_il2cpp_TypeInfo_var))->set_control_HUD_path_4(_stringLiteral3746908446);
		((Preferences_t60026258_StaticFields*)il2cpp_codegen_static_fields_for(Preferences_t60026258_il2cpp_TypeInfo_var))->set_missile_prefab_path_5(_stringLiteral400676400);
		return;
	}
}
// System.Void Score::.ctor()
extern "C"  void Score__ctor_m797864275 (Score_t1518975274 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Score::Start()
extern "C"  void Score_Start_m1559619439 (Score_t1518975274 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Score_Start_m1559619439_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_t356221433 * L_0 = Component_GetComponent_TisText_t356221433_m1342661039(__this, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_RuntimeMethod_var);
		__this->set_score_text_2(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Preferences_t60026258_il2cpp_TypeInfo_var);
		String_t* L_1 = ((Preferences_t60026258_StaticFields*)il2cpp_codegen_static_fields_for(Preferences_t60026258_il2cpp_TypeInfo_var))->get_player_tag_2();
		GameObject_t1756533147 * L_2 = GameObject_FindGameObjectWithTag_m1433464258(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Player2D_t165353355 * L_3 = GameObject_GetComponent_TisPlayer2D_t165353355_m1754251190(L_2, /*hidden argument*/GameObject_GetComponent_TisPlayer2D_t165353355_m1754251190_RuntimeMethod_var);
		__this->set_player_4(L_3);
		return;
	}
}
// System.Void Score::LateUpdate()
extern "C"  void Score_LateUpdate_m2915824364 (Score_t1518975274 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Score_LateUpdate_m2915824364_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->get_score_3();
		Player2D_t165353355 * L_1 = __this->get_player_4();
		NullCheck(L_1);
		int32_t L_2 = Player2D_get_current_score_m3494890519(L_1, /*hidden argument*/NULL);
		if ((((float)L_0) == ((float)(((float)((float)L_2))))))
		{
			goto IL_0054;
		}
	}
	{
		Player2D_t165353355 * L_3 = __this->get_player_4();
		NullCheck(L_3);
		int32_t L_4 = Player2D_get_current_score_m3494890519(L_3, /*hidden argument*/NULL);
		__this->set_score_3((((float)((float)L_4))));
		Text_t356221433 * L_5 = __this->get_score_text_2();
		float L_6 = __this->get_score_3();
		float L_7 = L_6;
		RuntimeObject * L_8 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1756683522, L_8, /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, L_9);
		float L_10 = __this->get_score_3();
		IL2CPP_RUNTIME_CLASS_INIT(Preferences_t60026258_il2cpp_TypeInfo_var);
		((Preferences_t60026258_StaticFields*)il2cpp_codegen_static_fields_for(Preferences_t60026258_il2cpp_TypeInfo_var))->set_score_3(L_10);
	}

IL_0054:
	{
		return;
	}
}
// System.Void Ship::.ctor()
extern "C"  void Ship__ctor_m4202732153 (Ship_t1116303770 * __this, const RuntimeMethod* method)
{
	{
		__this->set_move_speed_7((4.0f));
		__this->set_time_to_fire_9((4.0f));
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Ship::get_active()
extern "C"  bool Ship_get_active_m1657109792 (Ship_t1116303770 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CactiveU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void Ship::set_active(System.Boolean)
extern "C"  void Ship_set_active_m3844682543 (Ship_t1116303770 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CactiveU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void Ship::Start()
extern "C"  void Ship_Start_m1273291801 (Ship_t1116303770 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ship_Start_m1273291801_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Ship_set_active_m3844682543(__this, (bool)0, /*hidden argument*/NULL);
		__this->set_fading_12((bool)0);
		Image_t2042527209 * L_0 = Component_GetComponent_TisImage_t2042527209_m2189462422(__this, /*hidden argument*/Component_GetComponent_TisImage_t2042527209_m2189462422_RuntimeMethod_var);
		__this->set_image_11(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Preferences_t60026258_il2cpp_TypeInfo_var);
		String_t* L_1 = ((Preferences_t60026258_StaticFields*)il2cpp_codegen_static_fields_for(Preferences_t60026258_il2cpp_TypeInfo_var))->get_player_tag_2();
		GameObject_t1756533147 * L_2 = GameObject_FindGameObjectWithTag_m1433464258(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = GameObject_get_transform_m3490276752(L_2, /*hidden argument*/NULL);
		__this->set_target_3(L_3);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_4 = Vector3_get_right_m1958771095(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_direction_5(L_4);
		__this->set_corners_6(((Vector3U5BU5D_t1172311765*)SZArrayNew(Vector3U5BU5D_t1172311765_il2cpp_TypeInfo_var, (uint32_t)4)));
		String_t* L_5 = ((Preferences_t60026258_StaticFields*)il2cpp_codegen_static_fields_for(Preferences_t60026258_il2cpp_TypeInfo_var))->get_missile_prefab_path_5();
		Object_t1021602117 * L_6 = Resources_Load_m3480536692(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		__this->set_missile_prefab_10(((GameObject_t1756533147 *)IsInstSealed((RuntimeObject*)L_6, GameObject_t1756533147_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Collections.IEnumerator Ship::Fade(System.Int32)
extern "C"  RuntimeObject* Ship_Fade_m2975741434 (Ship_t1116303770 * __this, int32_t ___dir0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ship_Fade_m2975741434_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CFadeU3Ec__Iterator0_t3715132008 * V_0 = NULL;
	{
		U3CFadeU3Ec__Iterator0_t3715132008 * L_0 = (U3CFadeU3Ec__Iterator0_t3715132008 *)il2cpp_codegen_object_new(U3CFadeU3Ec__Iterator0_t3715132008_il2cpp_TypeInfo_var);
		U3CFadeU3Ec__Iterator0__ctor_m49909007(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFadeU3Ec__Iterator0_t3715132008 * L_1 = V_0;
		int32_t L_2 = ___dir0;
		NullCheck(L_1);
		L_1->set_dir_1(L_2);
		U3CFadeU3Ec__Iterator0_t3715132008 * L_3 = V_0;
		NullCheck(L_3);
		L_3->set_U24this_4(__this);
		U3CFadeU3Ec__Iterator0_t3715132008 * L_4 = V_0;
		return L_4;
	}
}
// System.Void Ship::FixedUpdate()
extern "C"  void Ship_FixedUpdate_m2175155974 (Ship_t1116303770 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ship_FixedUpdate_m2175155974_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RectTransform_t3349966182 * V_0 = NULL;
	RectTransform_t3349966182 * V_1 = NULL;
	int32_t V_2 = 0;
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t3681755626  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		bool L_0 = Ship_get_active_m1657109792(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0105;
		}
	}
	{
		Image_t2042527209 * L_1 = __this->get_image_11();
		NullCheck(L_1);
		bool L_2 = Behaviour_get_enabled_m646668963(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0034;
		}
	}
	{
		bool L_3 = __this->get_fading_12();
		if (L_3)
		{
			goto IL_0034;
		}
	}
	{
		RuntimeObject* L_4 = Ship_Fade_m2975741434(__this, 1, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2678710497(__this, L_4, /*hidden argument*/NULL);
	}

IL_0034:
	{
		__this->set_inside_canvas_4((bool)1);
		RectTransform_t3349966182 * L_5 = Component_GetComponent_TisRectTransform_t3349966182_m1310250299(__this, /*hidden argument*/Component_GetComponent_TisRectTransform_t3349966182_m1310250299_RuntimeMethod_var);
		V_0 = L_5;
		RectTransform_t3349966182 * L_6 = V_0;
		NullCheck(L_6);
		Transform_t3275118058 * L_7 = Transform_get_parent_m2752514051(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		RectTransform_t3349966182 * L_8 = Component_GetComponent_TisRectTransform_t3349966182_m1310250299(L_7, /*hidden argument*/Component_GetComponent_TisRectTransform_t3349966182_m1310250299_RuntimeMethod_var);
		V_1 = L_8;
		RectTransform_t3349966182 * L_9 = V_0;
		Vector3U5BU5D_t1172311765* L_10 = __this->get_corners_6();
		NullCheck(L_9);
		RectTransform_GetWorldCorners_m3853164225(L_9, L_10, /*hidden argument*/NULL);
		Vector3U5BU5D_t1172311765* L_11 = __this->get_corners_6();
		if (!L_11)
		{
			goto IL_00fa;
		}
	}
	{
		V_2 = 0;
		goto IL_00a4;
	}

IL_006c:
	{
		RectTransform_t3349966182 * L_12 = V_1;
		Vector3U5BU5D_t1172311765* L_13 = __this->get_corners_6();
		int32_t L_14 = V_2;
		NullCheck(L_13);
		NullCheck(L_12);
		Vector3_t2243707580  L_15 = Transform_InverseTransformPoint_m2749799913(L_12, (*(Vector3_t2243707580 *)((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_14)))), /*hidden argument*/NULL);
		V_3 = L_15;
		RectTransform_t3349966182 * L_16 = V_1;
		NullCheck(L_16);
		Rect_t3681755626  L_17 = RectTransform_get_rect_m2724092203(L_16, /*hidden argument*/NULL);
		V_4 = L_17;
		Vector3_t2243707580  L_18 = V_3;
		bool L_19 = Rect_Contains_m846865666((&V_4), L_18, /*hidden argument*/NULL);
		if (L_19)
		{
			goto IL_00a0;
		}
	}
	{
		__this->set_inside_canvas_4((bool)0);
	}

IL_00a0:
	{
		int32_t L_20 = V_2;
		V_2 = ((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_00a4:
	{
		int32_t L_21 = V_2;
		Vector3U5BU5D_t1172311765* L_22 = __this->get_corners_6();
		NullCheck(L_22);
		if ((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_22)->max_length)))))))
		{
			goto IL_006c;
		}
	}
	{
		bool L_23 = __this->get_inside_canvas_4();
		if (L_23)
		{
			goto IL_00d3;
		}
	}
	{
		Vector3_t2243707580  L_24 = __this->get_direction_5();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_25 = Vector3_op_Multiply_m2498445460(NULL /*static, unused*/, L_24, (-1.0f), /*hidden argument*/NULL);
		__this->set_direction_5(L_25);
	}

IL_00d3:
	{
		Transform_t3275118058 * L_26 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_27 = L_26;
		NullCheck(L_27);
		Vector3_t2243707580  L_28 = Transform_get_localPosition_m2747128641(L_27, /*hidden argument*/NULL);
		Vector3_t2243707580  L_29 = __this->get_direction_5();
		float L_30 = __this->get_move_speed_7();
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_31 = Vector3_op_Multiply_m2498445460(NULL /*static, unused*/, L_29, L_30, /*hidden argument*/NULL);
		Vector3_t2243707580  L_32 = Vector3_op_Addition_m394909128(NULL /*static, unused*/, L_28, L_31, /*hidden argument*/NULL);
		NullCheck(L_27);
		Transform_set_localPosition_m1073050816(L_27, L_32, /*hidden argument*/NULL);
	}

IL_00fa:
	{
		Ship_FollowTarget_m1654037237(__this, /*hidden argument*/NULL);
		goto IL_012e;
	}

IL_0105:
	{
		Image_t2042527209 * L_33 = __this->get_image_11();
		NullCheck(L_33);
		bool L_34 = Behaviour_get_enabled_m646668963(L_33, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_012e;
		}
	}
	{
		bool L_35 = __this->get_fading_12();
		if (L_35)
		{
			goto IL_012e;
		}
	}
	{
		RuntimeObject* L_36 = Ship_Fade_m2975741434(__this, (-1), /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2678710497(__this, L_36, /*hidden argument*/NULL);
	}

IL_012e:
	{
		return;
	}
}
// System.Void Ship::FollowTarget()
extern "C"  void Ship_FollowTarget_m1654037237 (Ship_t1116303770 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ship_FollowTarget_m1654037237_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		Transform_t3275118058 * L_0 = __this->get_target_3();
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = Component_get_transform_m3374354972(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_position_m2304215762(L_1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t2243707580  L_4 = Transform_get_position_m2304215762(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_5 = Vector3_op_Subtraction_m4046047256(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		float L_6 = (&V_0)->get_y_2();
		float L_7 = (&V_0)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_8 = atan2f(L_6, L_7);
		V_1 = ((float)((float)L_8*(float)(57.29578f)));
		Transform_t3275118058 * L_9 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_10 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Quaternion_t4030073918  L_11 = Transform_get_rotation_m2617026815(L_10, /*hidden argument*/NULL);
		float L_12 = V_1;
		Vector3_t2243707580  L_13 = Vector3_get_forward_m1316319684(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t4030073918_il2cpp_TypeInfo_var);
		Quaternion_t4030073918  L_14 = Quaternion_AngleAxis_m1602210320(NULL /*static, unused*/, ((float)((float)L_12-(float)(90.0f))), L_13, /*hidden argument*/NULL);
		float L_15 = Time_get_fixedDeltaTime_m2740561775(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_16 = __this->get_move_speed_7();
		Quaternion_t4030073918  L_17 = Quaternion_Slerp_m3187037907(NULL /*static, unused*/, L_11, L_14, ((float)((float)L_15*(float)L_16)), /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_rotation_m2824446320(L_9, L_17, /*hidden argument*/NULL);
		float L_18 = __this->get_time_until_fire_8();
		float L_19 = __this->get_time_to_fire_9();
		if ((!(((float)L_18) > ((float)L_19))))
		{
			goto IL_0095;
		}
	}
	{
		Ship_Fire_m2857528497(__this, /*hidden argument*/NULL);
		__this->set_time_until_fire_8((0.0f));
	}

IL_0095:
	{
		float L_20 = __this->get_time_until_fire_8();
		float L_21 = Time_get_deltaTime_m3925508629(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_time_until_fire_8(((float)((float)L_20+(float)L_21)));
		return;
	}
}
// System.Void Ship::Fire()
extern "C"  void Ship_Fire_m2857528497 (Ship_t1116303770 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ship_Fire_m2857528497_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_missile_prefab_10();
		Transform_t3275118058 * L_1 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t2243707580  L_2 = Transform_get_position_m2304215762(L_1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = Component_get_transform_m3374354972(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Quaternion_t4030073918  L_4 = Transform_get_rotation_m2617026815(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_0, L_2, L_4, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_RuntimeMethod_var);
		return;
	}
}
// System.Void Ship/<Fade>c__Iterator0::.ctor()
extern "C"  void U3CFadeU3Ec__Iterator0__ctor_m49909007 (U3CFadeU3Ec__Iterator0_t3715132008 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Ship/<Fade>c__Iterator0::MoveNext()
extern "C"  bool U3CFadeU3Ec__Iterator0_MoveNext_m2778718649 (U3CFadeU3Ec__Iterator0_t3715132008 * __this, const RuntimeMethod* method)
{
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_7();
		V_0 = L_0;
		__this->set_U24PC_7((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0025;
			}
			case 1:
			{
				goto IL_00cf;
			}
			case 2:
			{
				goto IL_0179;
			}
		}
	}
	{
		goto IL_01c5;
	}

IL_0025:
	{
		Ship_t1116303770 * L_2 = __this->get_U24this_4();
		NullCheck(L_2);
		L_2->set_fading_12((bool)1);
		__this->set_U3CdurationU3E__0_0((1.0f));
		int32_t L_3 = __this->get_dir_1();
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_00f7;
		}
	}
	{
		Ship_t1116303770 * L_4 = __this->get_U24this_4();
		NullCheck(L_4);
		Image_t2042527209 * L_5 = L_4->get_image_11();
		NullCheck(L_5);
		Behaviour_set_enabled_m602406666(L_5, (bool)1, /*hidden argument*/NULL);
		__this->set_U3CtU3E__1_2((0.0f));
		goto IL_00e1;
	}

IL_0069:
	{
		Ship_t1116303770 * L_6 = __this->get_U24this_4();
		NullCheck(L_6);
		Image_t2042527209 * L_7 = L_6->get_image_11();
		Ship_t1116303770 * L_8 = __this->get_U24this_4();
		NullCheck(L_8);
		Image_t2042527209 * L_9 = L_8->get_image_11();
		NullCheck(L_9);
		Color_t2020392075  L_10 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_9);
		Color_t2020392075  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Color__ctor_m2736804035((&L_11), (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		float L_12 = __this->get_U3CtU3E__1_2();
		float L_13 = __this->get_U3CdurationU3E__0_0();
		Color_t2020392075  L_14 = Color_Lerp_m527444458(NULL /*static, unused*/, L_10, L_11, ((float)((float)L_12/(float)L_13)), /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_7, L_14);
		__this->set_U24current_5(NULL);
		bool L_15 = __this->get_U24disposing_6();
		if (L_15)
		{
			goto IL_00ca;
		}
	}
	{
		__this->set_U24PC_7(1);
	}

IL_00ca:
	{
		goto IL_01c7;
	}

IL_00cf:
	{
		float L_16 = __this->get_U3CtU3E__1_2();
		float L_17 = Time_get_deltaTime_m3925508629(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CtU3E__1_2(((float)((float)L_16+(float)L_17)));
	}

IL_00e1:
	{
		float L_18 = __this->get_U3CtU3E__1_2();
		float L_19 = __this->get_U3CdurationU3E__0_0();
		if ((((float)L_18) < ((float)L_19)))
		{
			goto IL_0069;
		}
	}
	{
		goto IL_01ad;
	}

IL_00f7:
	{
		int32_t L_20 = __this->get_dir_1();
		if ((!(((uint32_t)L_20) == ((uint32_t)(-1)))))
		{
			goto IL_01ad;
		}
	}
	{
		__this->set_U3CtU3E__2_3((0.0f));
		goto IL_018b;
	}

IL_0113:
	{
		Ship_t1116303770 * L_21 = __this->get_U24this_4();
		NullCheck(L_21);
		Image_t2042527209 * L_22 = L_21->get_image_11();
		Ship_t1116303770 * L_23 = __this->get_U24this_4();
		NullCheck(L_23);
		Image_t2042527209 * L_24 = L_23->get_image_11();
		NullCheck(L_24);
		Color_t2020392075  L_25 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_24);
		Color_t2020392075  L_26;
		memset(&L_26, 0, sizeof(L_26));
		Color__ctor_m2736804035((&L_26), (1.0f), (1.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		float L_27 = __this->get_U3CtU3E__2_3();
		float L_28 = __this->get_U3CdurationU3E__0_0();
		Color_t2020392075  L_29 = Color_Lerp_m527444458(NULL /*static, unused*/, L_25, L_26, ((float)((float)L_27/(float)L_28)), /*hidden argument*/NULL);
		NullCheck(L_22);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_22, L_29);
		__this->set_U24current_5(NULL);
		bool L_30 = __this->get_U24disposing_6();
		if (L_30)
		{
			goto IL_0174;
		}
	}
	{
		__this->set_U24PC_7(2);
	}

IL_0174:
	{
		goto IL_01c7;
	}

IL_0179:
	{
		float L_31 = __this->get_U3CtU3E__2_3();
		float L_32 = Time_get_deltaTime_m3925508629(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CtU3E__2_3(((float)((float)L_31+(float)L_32)));
	}

IL_018b:
	{
		float L_33 = __this->get_U3CtU3E__2_3();
		float L_34 = __this->get_U3CdurationU3E__0_0();
		if ((((float)L_33) < ((float)L_34)))
		{
			goto IL_0113;
		}
	}
	{
		Ship_t1116303770 * L_35 = __this->get_U24this_4();
		NullCheck(L_35);
		Image_t2042527209 * L_36 = L_35->get_image_11();
		NullCheck(L_36);
		Behaviour_set_enabled_m602406666(L_36, (bool)0, /*hidden argument*/NULL);
	}

IL_01ad:
	{
		Ship_t1116303770 * L_37 = __this->get_U24this_4();
		NullCheck(L_37);
		L_37->set_fading_12((bool)0);
		goto IL_01c5;
	}
	// Dead block : IL_01be: ldarg.0

IL_01c5:
	{
		return (bool)0;
	}

IL_01c7:
	{
		return (bool)1;
	}
}
// System.Object Ship/<Fade>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CFadeU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1162441433 (U3CFadeU3Ec__Iterator0_t3715132008 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Object Ship/<Fade>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CFadeU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3588117873 (U3CFadeU3Ec__Iterator0_t3715132008 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Void Ship/<Fade>c__Iterator0::Dispose()
extern "C"  void U3CFadeU3Ec__Iterator0_Dispose_m2767830802 (U3CFadeU3Ec__Iterator0_t3715132008 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_6((bool)1);
		__this->set_U24PC_7((-1));
		return;
	}
}
// System.Void Ship/<Fade>c__Iterator0::Reset()
extern "C"  void U3CFadeU3Ec__Iterator0_Reset_m4019819668 (U3CFadeU3Ec__Iterator0_t3715132008 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CFadeU3Ec__Iterator0_Reset_m4019819668_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ShowScore::.ctor()
extern "C"  void ShowScore__ctor_m4205901544 (ShowScore_t4012426113 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1825328214(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShowScore::Start()
extern "C"  void ShowScore_Start_m298709576 (ShowScore_t4012426113 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShowScore_Start_m298709576_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_t356221433 * L_0 = Component_GetComponent_TisText_t356221433_m1342661039(__this, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_RuntimeMethod_var);
		__this->set_score_text_2(L_0);
		Text_t356221433 * L_1 = __this->get_score_text_2();
		IL2CPP_RUNTIME_CLASS_INIT(Preferences_t60026258_il2cpp_TypeInfo_var);
		float L_2 = ((Preferences_t60026258_StaticFields*)il2cpp_codegen_static_fields_for(Preferences_t60026258_il2cpp_TypeInfo_var))->get_score_3();
		float L_3 = L_2;
		RuntimeObject * L_4 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral2018315346, L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_5);
		return;
	}
}
// System.Void Threat::.ctor()
extern "C"  void Threat__ctor_m1337334961 (Threat_t2614656216 * __this, const RuntimeMethod* method)
{
	{
		Controller2D__ctor_m1972723229(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GameObject[] Threat::PlayerObjects()
extern "C"  GameObjectU5BU5D_t3057952154* Threat_PlayerObjects_m1428971637 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Threat_PlayerObjects_m1428971637_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Preferences_t60026258_il2cpp_TypeInfo_var);
		String_t* L_0 = ((Preferences_t60026258_StaticFields*)il2cpp_codegen_static_fields_for(Preferences_t60026258_il2cpp_TypeInfo_var))->get_player_tag_2();
		GameObjectU5BU5D_t3057952154* L_1 = GameObject_FindGameObjectsWithTag_m3720927271(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Threat::Hit(UnityEngine.GameObject)
extern "C"  void Threat_Hit_m4230409352 (Threat_t2614656216 * __this, GameObject_t1756533147 * ___possible_player0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Threat_Hit_m4230409352_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = ___possible_player0;
		NullCheck(L_0);
		String_t* L_1 = GameObject_get_tag_m3359901967(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Preferences_t60026258_il2cpp_TypeInfo_var);
		String_t* L_2 = ((Preferences_t60026258_StaticFields*)il2cpp_codegen_static_fields_for(Preferences_t60026258_il2cpp_TypeInfo_var))->get_player_tag_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002b;
		}
	}
	{
		GameObject_t1756533147 * L_4 = ___possible_player0;
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 Threat::Damage() */, __this);
		int32_t L_6 = L_5;
		RuntimeObject * L_7 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		GameObject_SendMessage_m2475750736(L_4, _stringLiteral3935001822, L_7, /*hidden argument*/NULL);
	}

IL_002b:
	{
		VirtActionInvoker0::Invoke(7 /* System.Void Threat::Destroy() */, __this);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
