﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// UnityEngine.Gradient
struct Gradient_t3600583008;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.String
struct String_t;
// System.Void
struct Void_t1841601450;

extern RuntimeClass* MinMaxGradient_t3708298175_il2cpp_TypeInfo_var;
extern const uint32_t MainModule_get_startColor_m642758877_MetadataUsageId;
struct Gradient_t3600583008_marshaled_pinvoke;
struct Gradient_t3600583008;;
struct Gradient_t3600583008_marshaled_pinvoke;;
extern RuntimeClass* Gradient_t3600583008_il2cpp_TypeInfo_var;
extern const uint32_t MinMaxGradient_t3708298175_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
struct Gradient_t3600583008_marshaled_com;
struct Gradient_t3600583008_marshaled_com;;



#ifndef U3CMODULEU3E_T3783534225_H
#define U3CMODULEU3E_T3783534225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t3783534225 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T3783534225_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef BOOLEAN_T3825574718_H
#define BOOLEAN_T3825574718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3825574718 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3825574718, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3825574718_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3825574718_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef MAINMODULE_T6751348_H
#define MAINMODULE_T6751348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/MainModule
struct  MainModule_t6751348 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/MainModule::m_ParticleSystem
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(MainModule_t6751348, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t3394631041 * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t3394631041 ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t3394631041 * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParticleSystem_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_t6751348_marshaled_pinvoke
{
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_t6751348_marshaled_com
{
	ParticleSystem_t3394631041 * ___m_ParticleSystem_0;
};
#endif // MAINMODULE_T6751348_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef PARTICLESYSTEMSTOPBEHAVIOR_T3921148531_H
#define PARTICLESYSTEMSTOPBEHAVIOR_T3921148531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystemStopBehavior
struct  ParticleSystemStopBehavior_t3921148531 
{
public:
	// System.Int32 UnityEngine.ParticleSystemStopBehavior::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParticleSystemStopBehavior_t3921148531, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMSTOPBEHAVIOR_T3921148531_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef PARTICLESYSTEMGRADIENTMODE_T4205168402_H
#define PARTICLESYSTEMGRADIENTMODE_T4205168402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystemGradientMode
struct  ParticleSystemGradientMode_t4205168402 
{
public:
	// System.Int32 UnityEngine.ParticleSystemGradientMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ParticleSystemGradientMode_t4205168402, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMGRADIENTMODE_T4205168402_H
#ifndef GRADIENT_T3600583008_H
#define GRADIENT_T3600583008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Gradient
struct  Gradient_t3600583008  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Gradient::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Gradient_t3600583008, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Gradient
struct Gradient_t3600583008_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Gradient
struct Gradient_t3600583008_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // GRADIENT_T3600583008_H
#ifndef MINMAXGRADIENT_T3708298175_H
#define MINMAXGRADIENT_T3708298175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem/MinMaxGradient
struct  MinMaxGradient_t3708298175 
{
public:
	// UnityEngine.ParticleSystemGradientMode UnityEngine.ParticleSystem/MinMaxGradient::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.Gradient UnityEngine.ParticleSystem/MinMaxGradient::m_GradientMin
	Gradient_t3600583008 * ___m_GradientMin_1;
	// UnityEngine.Gradient UnityEngine.ParticleSystem/MinMaxGradient::m_GradientMax
	Gradient_t3600583008 * ___m_GradientMax_2;
	// UnityEngine.Color UnityEngine.ParticleSystem/MinMaxGradient::m_ColorMin
	Color_t2020392075  ___m_ColorMin_3;
	// UnityEngine.Color UnityEngine.ParticleSystem/MinMaxGradient::m_ColorMax
	Color_t2020392075  ___m_ColorMax_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(MinMaxGradient_t3708298175, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_GradientMin_1() { return static_cast<int32_t>(offsetof(MinMaxGradient_t3708298175, ___m_GradientMin_1)); }
	inline Gradient_t3600583008 * get_m_GradientMin_1() const { return ___m_GradientMin_1; }
	inline Gradient_t3600583008 ** get_address_of_m_GradientMin_1() { return &___m_GradientMin_1; }
	inline void set_m_GradientMin_1(Gradient_t3600583008 * value)
	{
		___m_GradientMin_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_GradientMin_1), value);
	}

	inline static int32_t get_offset_of_m_GradientMax_2() { return static_cast<int32_t>(offsetof(MinMaxGradient_t3708298175, ___m_GradientMax_2)); }
	inline Gradient_t3600583008 * get_m_GradientMax_2() const { return ___m_GradientMax_2; }
	inline Gradient_t3600583008 ** get_address_of_m_GradientMax_2() { return &___m_GradientMax_2; }
	inline void set_m_GradientMax_2(Gradient_t3600583008 * value)
	{
		___m_GradientMax_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_GradientMax_2), value);
	}

	inline static int32_t get_offset_of_m_ColorMin_3() { return static_cast<int32_t>(offsetof(MinMaxGradient_t3708298175, ___m_ColorMin_3)); }
	inline Color_t2020392075  get_m_ColorMin_3() const { return ___m_ColorMin_3; }
	inline Color_t2020392075 * get_address_of_m_ColorMin_3() { return &___m_ColorMin_3; }
	inline void set_m_ColorMin_3(Color_t2020392075  value)
	{
		___m_ColorMin_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMax_4() { return static_cast<int32_t>(offsetof(MinMaxGradient_t3708298175, ___m_ColorMax_4)); }
	inline Color_t2020392075  get_m_ColorMax_4() const { return ___m_ColorMax_4; }
	inline Color_t2020392075 * get_address_of_m_ColorMax_4() { return &___m_ColorMax_4; }
	inline void set_m_ColorMax_4(Color_t2020392075  value)
	{
		___m_ColorMax_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/MinMaxGradient
struct MinMaxGradient_t3708298175_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Gradient_t3600583008_marshaled_pinvoke ___m_GradientMin_1;
	Gradient_t3600583008_marshaled_pinvoke ___m_GradientMax_2;
	Color_t2020392075  ___m_ColorMin_3;
	Color_t2020392075  ___m_ColorMax_4;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/MinMaxGradient
struct MinMaxGradient_t3708298175_marshaled_com
{
	int32_t ___m_Mode_0;
	Gradient_t3600583008_marshaled_com* ___m_GradientMin_1;
	Gradient_t3600583008_marshaled_com* ___m_GradientMax_2;
	Color_t2020392075  ___m_ColorMin_3;
	Color_t2020392075  ___m_ColorMax_4;
};
#endif // MINMAXGRADIENT_T3708298175_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef PARTICLESYSTEM_T3394631041_H
#define PARTICLESYSTEM_T3394631041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ParticleSystem
struct  ParticleSystem_t3394631041  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEM_T3394631041_H

extern "C" void Gradient_t3600583008_marshal_pinvoke(const Gradient_t3600583008& unmarshaled, Gradient_t3600583008_marshaled_pinvoke& marshaled);
extern "C" void Gradient_t3600583008_marshal_pinvoke_back(const Gradient_t3600583008_marshaled_pinvoke& marshaled, Gradient_t3600583008& unmarshaled);
extern "C" void Gradient_t3600583008_marshal_pinvoke_cleanup(Gradient_t3600583008_marshaled_pinvoke& marshaled);
extern "C" void Gradient_t3600583008_marshal_com(const Gradient_t3600583008& unmarshaled, Gradient_t3600583008_marshaled_com& marshaled);
extern "C" void Gradient_t3600583008_marshal_com_back(const Gradient_t3600583008_marshaled_com& marshaled, Gradient_t3600583008& unmarshaled);
extern "C" void Gradient_t3600583008_marshal_com_cleanup(Gradient_t3600583008_marshaled_com& marshaled);


// System.Void UnityEngine.ParticleSystem/MainModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void MainModule__ctor_m724825857 (MainModule_t6751348 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Play(System.Boolean)
extern "C"  void ParticleSystem_Play_m2079214656 (ParticleSystem_t3394631041 * __this, bool ___withChildren0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Stop(System.Boolean,UnityEngine.ParticleSystemStopBehavior)
extern "C"  void ParticleSystem_Stop_m2791344602 (ParticleSystem_t3394631041 * __this, bool ___withChildren0, int32_t ___stopBehavior1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::SetStartColor(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxGradient&)
extern "C"  void MainModule_SetStartColor_m2492347281 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, MinMaxGradient_t3708298175 * ___gradient1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::set_startColor(UnityEngine.ParticleSystem/MinMaxGradient)
extern "C"  void MainModule_set_startColor_m3844897932 (MainModule_t6751348 * __this, MinMaxGradient_t3708298175  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::GetStartColor(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxGradient&)
extern "C"  void MainModule_GetStartColor_m2015758365 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, MinMaxGradient_t3708298175 * ___gradient1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/MinMaxGradient UnityEngine.ParticleSystem/MainModule::get_startColor()
extern "C"  MinMaxGradient_t3708298175  MainModule_get_startColor_m642758877 (MainModule_t6751348 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::.ctor()
extern "C"  void Gradient__ctor_m1008579686 (Gradient_t3600583008 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_black()
extern "C"  Color_t2020392075  Color_get_black_m455352838 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MinMaxGradient::.ctor(UnityEngine.Color)
extern "C"  void MinMaxGradient__ctor_m2960432810 (MinMaxGradient_t3708298175 * __this, Color_t2020392075  ___color0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.ParticleSystem/MinMaxGradient::get_color()
extern "C"  Color_t2020392075  MinMaxGradient_get_color_m1448527895 (MinMaxGradient_t3708298175 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.ParticleSystem/MainModule UnityEngine.ParticleSystem::get_main()
extern "C"  MainModule_t6751348  ParticleSystem_get_main_m2275307502 (ParticleSystem_t3394631041 * __this, const RuntimeMethod* method)
{
	MainModule_t6751348  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		MainModule_t6751348  L_0;
		memset(&L_0, 0, sizeof(L_0));
		MainModule__ctor_m724825857((&L_0), __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		MainModule_t6751348  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.ParticleSystem::Play(System.Boolean)
extern "C"  void ParticleSystem_Play_m2079214656 (ParticleSystem_t3394631041 * __this, bool ___withChildren0, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_Play_m2079214656_ftn) (ParticleSystem_t3394631041 *, bool);
	static ParticleSystem_Play_m2079214656_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Play_m2079214656_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Play(System.Boolean)");
	_il2cpp_icall_func(__this, ___withChildren0);
}
// System.Void UnityEngine.ParticleSystem::Play()
extern "C"  void ParticleSystem_Play_m2510524101 (ParticleSystem_t3394631041 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		bool L_0 = V_0;
		ParticleSystem_Play_m2079214656(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Stop(System.Boolean,UnityEngine.ParticleSystemStopBehavior)
extern "C"  void ParticleSystem_Stop_m2791344602 (ParticleSystem_t3394631041 * __this, bool ___withChildren0, int32_t ___stopBehavior1, const RuntimeMethod* method)
{
	typedef void (*ParticleSystem_Stop_m2791344602_ftn) (ParticleSystem_t3394631041 *, bool, int32_t);
	static ParticleSystem_Stop_m2791344602_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Stop_m2791344602_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Stop(System.Boolean,UnityEngine.ParticleSystemStopBehavior)");
	_il2cpp_icall_func(__this, ___withChildren0, ___stopBehavior1);
}
// System.Void UnityEngine.ParticleSystem::Stop()
extern "C"  void ParticleSystem_Stop_m3868680149 (ParticleSystem_t3394631041 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 1;
		V_1 = (bool)1;
		bool L_0 = V_1;
		int32_t L_1 = V_0;
		ParticleSystem_Stop_m2791344602(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t6751348_marshal_pinvoke(const MainModule_t6751348& unmarshaled, MainModule_t6751348_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void MainModule_t6751348_marshal_pinvoke_back(const MainModule_t6751348_marshaled_pinvoke& marshaled, MainModule_t6751348& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t6751348_marshal_pinvoke_cleanup(MainModule_t6751348_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t6751348_marshal_com(const MainModule_t6751348& unmarshaled, MainModule_t6751348_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void MainModule_t6751348_marshal_com_back(const MainModule_t6751348_marshaled_com& marshaled, MainModule_t6751348& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t6751348_marshal_com_cleanup(MainModule_t6751348_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/MainModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void MainModule__ctor_m724825857 (MainModule_t6751348 * __this, ParticleSystem_t3394631041 * ___particleSystem0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void MainModule__ctor_m724825857_AdjustorThunk (RuntimeObject * __this, ParticleSystem_t3394631041 * ___particleSystem0, const RuntimeMethod* method)
{
	MainModule_t6751348 * _thisAdjusted = reinterpret_cast<MainModule_t6751348 *>(__this + 1);
	MainModule__ctor_m724825857(_thisAdjusted, ___particleSystem0, method);
}
// System.Void UnityEngine.ParticleSystem/MainModule::set_startColor(UnityEngine.ParticleSystem/MinMaxGradient)
extern "C"  void MainModule_set_startColor_m3844897932 (MainModule_t6751348 * __this, MinMaxGradient_t3708298175  ___value0, const RuntimeMethod* method)
{
	{
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		MainModule_SetStartColor_m2492347281(NULL /*static, unused*/, L_0, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void MainModule_set_startColor_m3844897932_AdjustorThunk (RuntimeObject * __this, MinMaxGradient_t3708298175  ___value0, const RuntimeMethod* method)
{
	MainModule_t6751348 * _thisAdjusted = reinterpret_cast<MainModule_t6751348 *>(__this + 1);
	MainModule_set_startColor_m3844897932(_thisAdjusted, ___value0, method);
}
// UnityEngine.ParticleSystem/MinMaxGradient UnityEngine.ParticleSystem/MainModule::get_startColor()
extern "C"  MinMaxGradient_t3708298175  MainModule_get_startColor_m642758877 (MainModule_t6751348 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainModule_get_startColor_m642758877_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MinMaxGradient_t3708298175  V_0;
	memset(&V_0, 0, sizeof(V_0));
	MinMaxGradient_t3708298175  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Initobj (MinMaxGradient_t3708298175_il2cpp_TypeInfo_var, (&V_0));
		ParticleSystem_t3394631041 * L_0 = __this->get_m_ParticleSystem_0();
		MainModule_GetStartColor_m2015758365(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		MinMaxGradient_t3708298175  L_1 = V_0;
		V_1 = L_1;
		goto IL_001d;
	}

IL_001d:
	{
		MinMaxGradient_t3708298175  L_2 = V_1;
		return L_2;
	}
}
extern "C"  MinMaxGradient_t3708298175  MainModule_get_startColor_m642758877_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	MainModule_t6751348 * _thisAdjusted = reinterpret_cast<MainModule_t6751348 *>(__this + 1);
	return MainModule_get_startColor_m642758877(_thisAdjusted, method);
}
// System.Void UnityEngine.ParticleSystem/MainModule::SetStartColor(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxGradient&)
extern "C"  void MainModule_SetStartColor_m2492347281 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, MinMaxGradient_t3708298175 * ___gradient1, const RuntimeMethod* method)
{
	typedef void (*MainModule_SetStartColor_m2492347281_ftn) (ParticleSystem_t3394631041 *, MinMaxGradient_t3708298175 *);
	static MainModule_SetStartColor_m2492347281_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_SetStartColor_m2492347281_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::SetStartColor(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxGradient&)");
	_il2cpp_icall_func(___system0, ___gradient1);
}
// System.Void UnityEngine.ParticleSystem/MainModule::GetStartColor(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxGradient&)
extern "C"  void MainModule_GetStartColor_m2015758365 (RuntimeObject * __this /* static, unused */, ParticleSystem_t3394631041 * ___system0, MinMaxGradient_t3708298175 * ___gradient1, const RuntimeMethod* method)
{
	typedef void (*MainModule_GetStartColor_m2015758365_ftn) (ParticleSystem_t3394631041 *, MinMaxGradient_t3708298175 *);
	static MainModule_GetStartColor_m2015758365_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MainModule_GetStartColor_m2015758365_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/MainModule::GetStartColor(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxGradient&)");
	_il2cpp_icall_func(___system0, ___gradient1);
}




// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MinMaxGradient
extern "C" void MinMaxGradient_t3708298175_marshal_pinvoke(const MinMaxGradient_t3708298175& unmarshaled, MinMaxGradient_t3708298175_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Mode_0 = unmarshaled.get_m_Mode_0();
	if (unmarshaled.get_m_GradientMin_1() != NULL) Gradient_t3600583008_marshal_pinvoke(*unmarshaled.get_m_GradientMin_1(), marshaled.___m_GradientMin_1);
	if (unmarshaled.get_m_GradientMax_2() != NULL) Gradient_t3600583008_marshal_pinvoke(*unmarshaled.get_m_GradientMax_2(), marshaled.___m_GradientMax_2);
	marshaled.___m_ColorMin_3 = unmarshaled.get_m_ColorMin_3();
	marshaled.___m_ColorMax_4 = unmarshaled.get_m_ColorMax_4();
}
extern "C" void MinMaxGradient_t3708298175_marshal_pinvoke_back(const MinMaxGradient_t3708298175_marshaled_pinvoke& marshaled, MinMaxGradient_t3708298175& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MinMaxGradient_t3708298175_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t unmarshaled_m_Mode_temp_0 = 0;
	unmarshaled_m_Mode_temp_0 = marshaled.___m_Mode_0;
	unmarshaled.set_m_Mode_0(unmarshaled_m_Mode_temp_0);
	unmarshaled.set_m_GradientMin_1((Gradient_t3600583008 *)il2cpp_codegen_object_new(Gradient_t3600583008_il2cpp_TypeInfo_var));
	Gradient__ctor_m1008579686(unmarshaled.get_m_GradientMin_1(), NULL);
	Gradient_t3600583008_marshal_pinvoke_back(marshaled.___m_GradientMin_1, *unmarshaled.get_m_GradientMin_1());
	unmarshaled.set_m_GradientMax_2((Gradient_t3600583008 *)il2cpp_codegen_object_new(Gradient_t3600583008_il2cpp_TypeInfo_var));
	Gradient__ctor_m1008579686(unmarshaled.get_m_GradientMax_2(), NULL);
	Gradient_t3600583008_marshal_pinvoke_back(marshaled.___m_GradientMax_2, *unmarshaled.get_m_GradientMax_2());
	Color_t2020392075  unmarshaled_m_ColorMin_temp_3;
	memset(&unmarshaled_m_ColorMin_temp_3, 0, sizeof(unmarshaled_m_ColorMin_temp_3));
	unmarshaled_m_ColorMin_temp_3 = marshaled.___m_ColorMin_3;
	unmarshaled.set_m_ColorMin_3(unmarshaled_m_ColorMin_temp_3);
	Color_t2020392075  unmarshaled_m_ColorMax_temp_4;
	memset(&unmarshaled_m_ColorMax_temp_4, 0, sizeof(unmarshaled_m_ColorMax_temp_4));
	unmarshaled_m_ColorMax_temp_4 = marshaled.___m_ColorMax_4;
	unmarshaled.set_m_ColorMax_4(unmarshaled_m_ColorMax_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MinMaxGradient
extern "C" void MinMaxGradient_t3708298175_marshal_pinvoke_cleanup(MinMaxGradient_t3708298175_marshaled_pinvoke& marshaled)
{
	Gradient_t3600583008_marshal_pinvoke_cleanup(marshaled.___m_GradientMin_1);
	Gradient_t3600583008_marshal_pinvoke_cleanup(marshaled.___m_GradientMax_2);
}




// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MinMaxGradient
extern "C" void MinMaxGradient_t3708298175_marshal_com(const MinMaxGradient_t3708298175& unmarshaled, MinMaxGradient_t3708298175_marshaled_com& marshaled)
{
	marshaled.___m_Mode_0 = unmarshaled.get_m_Mode_0();
	if (unmarshaled.get_m_GradientMin_1() != NULL) Gradient_t3600583008_marshal_com(*unmarshaled.get_m_GradientMin_1(), *marshaled.___m_GradientMin_1);
	if (unmarshaled.get_m_GradientMax_2() != NULL) Gradient_t3600583008_marshal_com(*unmarshaled.get_m_GradientMax_2(), *marshaled.___m_GradientMax_2);
	marshaled.___m_ColorMin_3 = unmarshaled.get_m_ColorMin_3();
	marshaled.___m_ColorMax_4 = unmarshaled.get_m_ColorMax_4();
}
extern "C" void MinMaxGradient_t3708298175_marshal_com_back(const MinMaxGradient_t3708298175_marshaled_com& marshaled, MinMaxGradient_t3708298175& unmarshaled)
{
	int32_t unmarshaled_m_Mode_temp_0 = 0;
	unmarshaled_m_Mode_temp_0 = marshaled.___m_Mode_0;
	unmarshaled.set_m_Mode_0(unmarshaled_m_Mode_temp_0);
	if (unmarshaled.get_m_GradientMin_1() != NULL)
	{
		Gradient__ctor_m1008579686(unmarshaled.get_m_GradientMin_1(), NULL);
		Gradient_t3600583008_marshal_com_back(*marshaled.___m_GradientMin_1, *unmarshaled.get_m_GradientMin_1());
	}
	if (unmarshaled.get_m_GradientMax_2() != NULL)
	{
		Gradient__ctor_m1008579686(unmarshaled.get_m_GradientMax_2(), NULL);
		Gradient_t3600583008_marshal_com_back(*marshaled.___m_GradientMax_2, *unmarshaled.get_m_GradientMax_2());
	}
	Color_t2020392075  unmarshaled_m_ColorMin_temp_3;
	memset(&unmarshaled_m_ColorMin_temp_3, 0, sizeof(unmarshaled_m_ColorMin_temp_3));
	unmarshaled_m_ColorMin_temp_3 = marshaled.___m_ColorMin_3;
	unmarshaled.set_m_ColorMin_3(unmarshaled_m_ColorMin_temp_3);
	Color_t2020392075  unmarshaled_m_ColorMax_temp_4;
	memset(&unmarshaled_m_ColorMax_temp_4, 0, sizeof(unmarshaled_m_ColorMax_temp_4));
	unmarshaled_m_ColorMax_temp_4 = marshaled.___m_ColorMax_4;
	unmarshaled.set_m_ColorMax_4(unmarshaled_m_ColorMax_temp_4);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MinMaxGradient
extern "C" void MinMaxGradient_t3708298175_marshal_com_cleanup(MinMaxGradient_t3708298175_marshaled_com& marshaled)
{
	if (&(*marshaled.___m_GradientMin_1) != NULL) Gradient_t3600583008_marshal_com_cleanup(*marshaled.___m_GradientMin_1);
	if (&(*marshaled.___m_GradientMax_2) != NULL) Gradient_t3600583008_marshal_com_cleanup(*marshaled.___m_GradientMax_2);
}
// System.Void UnityEngine.ParticleSystem/MinMaxGradient::.ctor(UnityEngine.Color)
extern "C"  void MinMaxGradient__ctor_m2960432810 (MinMaxGradient_t3708298175 * __this, Color_t2020392075  ___color0, const RuntimeMethod* method)
{
	{
		__this->set_m_Mode_0(0);
		__this->set_m_GradientMin_1((Gradient_t3600583008 *)NULL);
		__this->set_m_GradientMax_2((Gradient_t3600583008 *)NULL);
		Color_t2020392075  L_0 = Color_get_black_m455352838(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_ColorMin_3(L_0);
		Color_t2020392075  L_1 = ___color0;
		__this->set_m_ColorMax_4(L_1);
		return;
	}
}
extern "C"  void MinMaxGradient__ctor_m2960432810_AdjustorThunk (RuntimeObject * __this, Color_t2020392075  ___color0, const RuntimeMethod* method)
{
	MinMaxGradient_t3708298175 * _thisAdjusted = reinterpret_cast<MinMaxGradient_t3708298175 *>(__this + 1);
	MinMaxGradient__ctor_m2960432810(_thisAdjusted, ___color0, method);
}
// UnityEngine.Color UnityEngine.ParticleSystem/MinMaxGradient::get_color()
extern "C"  Color_t2020392075  MinMaxGradient_get_color_m1448527895 (MinMaxGradient_t3708298175 * __this, const RuntimeMethod* method)
{
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t2020392075  L_0 = __this->get_m_ColorMax_4();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Color_t2020392075  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Color_t2020392075  MinMaxGradient_get_color_m1448527895_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	MinMaxGradient_t3708298175 * _thisAdjusted = reinterpret_cast<MinMaxGradient_t3708298175 *>(__this + 1);
	return MinMaxGradient_get_color_m1448527895(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
